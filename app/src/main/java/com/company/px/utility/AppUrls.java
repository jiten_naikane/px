package com.company.px.utility;


public class AppUrls
{



    public static String BASE_URL = "http://anuraggokanakonda.000webhostapp.com/api/v1/";
//    public static String BASE_URL = "http://picxture.com/api/v1/";
    public static String BASE_IMAGE_URL = "http://anuraggokanakonda.000webhostapp.com/";
    public static String BASE_PRODUCT_IMAGE_URL = "http://anuraggokanakonda.000webhostapp.com/api/assets/uploads/products/";
    public static String LOGIN ="verify_login";
    public static String REGISTRATION ="user";
    public static String PROFILE ="user/";
    public static String CREATE_ORDER ="order/create";
    public static String GET_CART ="cart/";

    public static String GET_THEME_TITLE ="festivals";
    public static String GET_STICKER_TITLE ="stickers";
    public static String GET_COUNTRY ="countries";
    public static String GET_MENUS ="menus";
    public static String GET_SLIDER_IMAGES ="slider";
    public static String GET_CATEGORY ="product_categories";
    public static String GET_CATEGORY_PRODUCT ="products/";

    public static String GET_PLANS ="plans";


    public static String ORDER_PAY ="order/pay";

}
