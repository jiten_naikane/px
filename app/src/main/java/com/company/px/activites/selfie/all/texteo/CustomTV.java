package com.company.px.activites.selfie.all.texteo;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.company.px.utility.AutoResizeTextView;


public class CustomTV extends StickerView{
    public AutoResizeTextView tv_main;

    public CustomTV(Context context) {
        super(context);
    }

    public CustomTV(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTV(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public View getMainView() {
        if(tv_main != null)
            return tv_main;

        tv_main = new AutoResizeTextView(getContext());
        tv_main.setMinTextSize(20);
        tv_main.setTextColor(Color.BLACK);
        tv_main.setGravity(Gravity.CENTER);
        tv_main.setTextSize(300);
//        tv_main.setMaxLines(400);
//        tv_main.setShadowLayer(4, 0, 0, Color.BLACK);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
//        params.gravity = Gravity.CENTER;
        tv_main.setLayoutParams(params);

        return tv_main;
    }




    public TextPaint getPaint() {
        if(tv_main!=null)
            return  tv_main.getPaint();

        return null;
    }

    public void setMaxLines(int lines){
        if (tv_main!=null)
            tv_main.setMaxLines(lines);
    }

    public void setShadowLayer(float radius, float dx, float dy, int color){
        if (tv_main!=null)
            tv_main.setShadowLayer(radius,dx,dy,color);
    }

    public void setTypeface(Typeface tf){
        if(tv_main!=null)
            tv_main.setTypeface(tf);
    }

    public void setTextSize(float size) {
        if(tv_main!=null)
            tv_main.setTextSize(size);
    }


    public void setTextColor(int textColor){
        if(tv_main!=null)
            tv_main.setTextColor(textColor);
    }

    public void setText(String text){
        if(tv_main!=null)
            tv_main.setText(text);
    }



    public String getText(){
        if(tv_main!=null)
            return tv_main.getText().toString();

        return null;
    }

    public static float pixelsToSp(Context context, float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }

    @Override
    protected void onScaling(boolean scaleUp) {
        super.onScaling(scaleUp);
    }
}
