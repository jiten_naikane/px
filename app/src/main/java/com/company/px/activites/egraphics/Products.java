package com.company.px.activites.egraphics;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.R;
import com.company.px.activites.logs.LoginActivity;
import com.company.px.adapter.PrivateProductAdapter;
import com.company.px.adapter.PublicProductAdapter;
import com.company.px.model.PrivateProductsModel;
import com.company.px.model.PublicProductsModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;
import com.company.px.utility.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Products extends AppCompatActivity implements View.OnClickListener {
    int in0 = 0, in1 = 0, in2 = 0, in3 = 0;

    private boolean checkInternet;
    TextView main_cat_name, new_order, pub_txt, pvt_txt, caption_1, caption_2;
    ImageView back_butt, pub_cr_img, pvt_cr_img, new_bg;
    LinearLayout pub_butt, pvt_butt;
    UserSessionManager session;
    RecyclerView private_pro_recycler, public_pro_recycler;
    ArrayList<PublicProductsModel> pubProModel = new ArrayList<PublicProductsModel>();
    ArrayList<PrivateProductsModel> pvtProModel = new ArrayList<PrivateProductsModel>();
    PublicProductAdapter PubProAdap;
    PrivateProductAdapter pvtProAdap;
    LinearLayoutManager pubLayoutManager, pvtLayoutManager;

    String cat_ID, royal_ID, edit_ID, catNAME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        session = new UserSessionManager(Products.this);
        cat_ID = getIntent().getExtras().getString("catID");
        royal_ID = getIntent().getExtras().getString("royalID");
        edit_ID = getIntent().getExtras().getString("editID");
        catNAME = getIntent().getExtras().getString("catName");
        main_cat_name = (TextView) findViewById(R.id.main_cat_name);
        main_cat_name.setText(catNAME);

        caption_1 = (TextView) findViewById(R.id.caption_1);
        caption_2 = (TextView) findViewById(R.id.caption_2);
        pub_txt = (TextView) findViewById(R.id.pub_txt);
        pvt_txt = (TextView) findViewById(R.id.pvt_txt);
        new_order = (TextView) findViewById(R.id.new_order);
        new_order.setOnClickListener(this);
        pub_butt = (LinearLayout) findViewById(R.id.pub_butt);
        pub_butt.setOnClickListener(this);
        pvt_butt = (LinearLayout) findViewById(R.id.pvt_butt);
        pvt_butt.setOnClickListener(this);

        new_bg = (ImageView) findViewById(R.id.new_bg);
        pub_cr_img = (ImageView) findViewById(R.id.pub_cr_img);
        pub_cr_img.setColorFilter(Color.BLACK);

        pvt_cr_img = (ImageView) findViewById(R.id.pvt_cr_img);
        back_butt = (ImageView) findViewById(R.id.back_butt);
        back_butt.setOnClickListener(this);
        back_butt.setColorFilter(getResources().getColor(R.color.white));

        public_pro_recycler = (RecyclerView) findViewById(R.id.public_pro_recycler);
        pubLayoutManager = new GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false);
        public_pro_recycler.setLayoutManager(pubLayoutManager);
        PubProAdap = new PublicProductAdapter(pubProModel, Products.this, R.layout.row_products_thumb, cat_ID);
        public_pro_recycler.setAdapter(PubProAdap);

        private_pro_recycler = (RecyclerView) findViewById(R.id.private_pro_recycler);
        pvtLayoutManager = new GridLayoutManager(this, 1, LinearLayoutManager.VERTICAL, false);
        private_pro_recycler.setLayoutManager(pvtLayoutManager);
        pvtProAdap = new PrivateProductAdapter(pvtProModel, Products.this, R.layout.row_products_thumb, cat_ID);
        private_pro_recycler.setAdapter(pvtProAdap);


        Log.d("rot", royal_ID + "//" + edit_ID);


        getProduct();

    }


    private void getProduct() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("Product_URL", AppUrls.BASE_URL + AppUrls.GET_CATEGORY_PRODUCT + "1/" + "1/" + "0");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_CATEGORY_PRODUCT + "1/" + "1/" + "0",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("PRODUCT_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONObject jObj = jsonObject.getJSONObject("data");

                                    JSONArray jarrayPublic = jObj.getJSONArray("pubic");
                                    for (int i = 0; i < jarrayPublic.length(); i++) {
                                        JSONObject jsonObject1 = jarrayPublic.getJSONObject(i);

                                        PublicProductsModel cList = new PublicProductsModel();
                                        cList.setId(jsonObject1.getString("id"));
                                        cList.setAbout_free_edits(jsonObject1.getString("about_free_edits"));
                                        cList.setAbout_output_files(jsonObject1.getString("about_output_files"));
                                        cList.setDescription(jsonObject1.getString("description"));
                                        cList.setDescription2(jsonObject1.getString("description2"));
                                        cList.setNormal_delivery_info(jsonObject1.getString("normal_delivery_info"));
                                        cList.setQuick_delivery_info(jsonObject1.getString("quick_delivery_info"));
                                        cList.setStyles_tags(jsonObject1.getString("styles_tags"));
                                        cList.setName(jsonObject1.getString("name"));
                                        cList.setIs_active(jsonObject1.getString("is_active"));
                                        cList.setImg1(jsonObject1.getString("img1"));
                                        cList.setImg2(jsonObject1.getString("img2"));
                                        cList.setImg3(jsonObject1.getString("img3"));
                                        cList.setImg4(jsonObject1.getString("img4"));
                                        cList.setImg5(jsonObject1.getString("img5"));
                                        cList.setImg6(jsonObject1.getString("img6"));
                                        cList.setImg7(jsonObject1.getString("img7"));
                                        cList.setImg8(jsonObject1.getString("img8"));
                                        cList.setImg9(jsonObject1.getString("img9"));
                                        cList.setImg10(jsonObject1.getString("img10"));
                                        cList.setPrice(jsonObject1.getString("price"));
                                        cList.setPrice_before_discount(jsonObject1.getString("price_before_discount"));

                                        pubProModel.add(cList);
                                    }
                                    public_pro_recycler.setAdapter(PubProAdap);


                                    JSONArray jarrayPrivate = jObj.getJSONArray("private");
                                    for (int i = 0; i < jarrayPrivate.length(); i++) {
                                        JSONObject jsonObject1 = jarrayPrivate.getJSONObject(i);

                                        PrivateProductsModel cList = new PrivateProductsModel();
                                        cList.setId(jsonObject1.getString("id"));
                                        cList.setAbout_free_edits(jsonObject1.getString("about_free_edits"));
                                        cList.setAbout_output_files(jsonObject1.getString("about_output_files"));
                                        cList.setDescription(jsonObject1.getString("description"));
                                        cList.setDescription2(jsonObject1.getString("description2"));
                                        cList.setNormal_delivery_info(jsonObject1.getString("normal_delivery_info"));
                                        cList.setQuick_delivery_info(jsonObject1.getString("quick_delivery_info"));
                                        cList.setStyles_tags(jsonObject1.getString("styles_tags"));
                                        cList.setName(jsonObject1.getString("name"));
                                        cList.setIs_active(jsonObject1.getString("is_active"));
                                        cList.setImg1(jsonObject1.getString("img1"));
                                        cList.setImg2(jsonObject1.getString("img2"));
                                        cList.setImg3(jsonObject1.getString("img3"));
                                        cList.setImg4(jsonObject1.getString("img4"));
                                        cList.setImg5(jsonObject1.getString("img5"));
                                        cList.setImg6(jsonObject1.getString("img6"));
                                        cList.setImg7(jsonObject1.getString("img7"));
                                        cList.setImg8(jsonObject1.getString("img8"));
                                        cList.setImg9(jsonObject1.getString("img9"));
                                        cList.setImg10(jsonObject1.getString("img10"));
                                        cList.setPrice(jsonObject1.getString("price"));
                                        cList.setPrice_before_discount(jsonObject1.getString("price_before_discount"));

                                        pvtProModel.add(cList);
                                    }
                                    private_pro_recycler.setAdapter(pvtProAdap);


                                    JSONObject jobjNew = jObj.getJSONObject("new_products");
                                    caption_1.setText(jobjNew.getString("new_order_caption"));
                                    caption_2.setText(jobjNew.getString("new_order_caption1"));

                                    Picasso.with(getApplicationContext())
                                            .load(AppUrls.BASE_IMAGE_URL + jobjNew.getString("new_order_image"))
                                            .placeholder(R.drawable.thumb_placeholder)
                                            .into(new_bg);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v) {
        if (v == new_order) {
            if (session.checkLogin()) {
                Intent login = new Intent(Products.this, LoginActivity.class);
                startActivity(login);
            } else {
                Intent toStep_one = new Intent(Products.this, StepOne_Description.class);
                startActivity(toStep_one);
            }
        }

        if (back_butt == v) {
            finish();
        }

        if (v == pub_butt) {
            public_pro_recycler.setVisibility(View.VISIBLE);
            private_pro_recycler.setVisibility(View.GONE);
            pub_butt.setBackgroundColor(Color.WHITE);
            pvt_butt.setBackgroundColor(Color.BLACK);
            pub_txt.setTextColor(Color.BLACK);
            pvt_txt.setTextColor(Color.WHITE);
            pub_cr_img.setColorFilter(Color.BLACK);
            pvt_cr_img.setColorFilter(Color.WHITE);

        }
        if (v == pvt_butt) {
            public_pro_recycler.setVisibility(View.GONE);
            private_pro_recycler.setVisibility(View.VISIBLE);
            pvt_butt.setBackgroundColor(Color.WHITE);
            pub_butt.setBackgroundColor(Color.BLACK);
            pvt_txt.setTextColor(Color.BLACK);
            pub_txt.setTextColor(Color.WHITE);
            pvt_cr_img.setColorFilter(Color.BLACK);
            pub_cr_img.setColorFilter(Color.WHITE);
        }
    }
}
