package com.company.px.activites.logs;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.R;
import com.company.px.activites.egraphics.StepOne_Description;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;
import com.company.px.utility.UserSessionManager;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener
{

    EditText first_name,last_name,mobile_edt,email_edt,edt_password,country_edttxt;
    RadioGroup radio_group;
    RadioButton radio_male, radio_female,radio_other,gender_status;
    ImageView btn_register,country_flag;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    ProgressDialog progressDialog;
    String send_gender,country_code;
    String EMAIL_REG = "\"^[\\\\w_\\\\.+]*[\\\\w_\\\\.]\\\\@([\\\\w]+\\\\.)+[\\\\w]+[\\\\w]$\";";
    String login_type, send_fb_user_data, send_fb_name, send_fb_lastname, send_fb_email, send_fb_gender, send_fb_provider, send_google_name, send_google_lastname,
            send_google_email, send_google_id, send_google_provider,sendDOB;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        userSessionManager=new UserSessionManager(RegisterActivity.this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Registering......");
        progressDialog.setProgressStyle(R.style.DialogTheme);
        Bundle bundle = getIntent().getExtras();
        login_type = bundle.getString("LOGIN");

        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        mobile_edt = (EditText) findViewById(R.id.mobile_edt);
        email_edt = (EditText) findViewById(R.id.email_edt);
        edt_password = (EditText) findViewById(R.id.edt_password);
        country_edttxt = (EditText) findViewById(R.id.country_edttxt);
        country_edttxt.setOnClickListener(this);

        radio_group = (RadioGroup) findViewById(R.id.radio_group);

        radio_male = (RadioButton) findViewById(R.id.radio_male);
        radio_female = (RadioButton) findViewById(R.id.radio_female);
        radio_other = (RadioButton) findViewById(R.id.radio_other);

        country_flag = (ImageView) findViewById(R.id.country_flag);

        btn_register = (ImageView) findViewById(R.id.btn_register);
        btn_register.setOnClickListener(this);

        country_edttxt.setText("+91");

        if (login_type.equals("Facebook")) {
            Bundle bundle_fb = getIntent().getExtras();
            send_fb_user_data = bundle_fb.getString("user_data_id");
            send_fb_name = bundle_fb.getString("name");
            send_fb_lastname = bundle_fb.getString("lastname");
            send_fb_email = bundle_fb.getString("email");
            send_fb_gender = bundle_fb.getString("gender");
            send_fb_provider = bundle_fb.getString("provider");
            if (!send_fb_email.equals(EMAIL_REG)) {
                email_edt.setFocusable(true);
            }
            first_name.setText(send_fb_name);
            first_name.setFocusable(false);
            last_name.setText(send_fb_lastname);
            last_name.setFocusable(false);
            email_edt.setText(send_fb_email);
            email_edt.setFocusable(false);

        }
        if (login_type.equals("Google")) {
            Bundle bundle_google = getIntent().getExtras();
            send_google_name = bundle_google.getString("name");
            send_google_lastname = bundle_google.getString("lastname");
            send_google_email = bundle_google.getString("email");
            send_google_id = bundle_google.getString("user_gmail_id");
            send_google_provider = bundle_google.getString("provider");
            first_name.setText(send_google_name);
            first_name.setFocusable(false);
            last_name.setText(send_google_lastname);
            last_name.setFocusable(false);
            email_edt.setText(send_google_email);
            email_edt.setFocusable(false);

        }

    }




    @Override
    public void onClick(View v)
    {
        if(v==btn_register)
        {
            if(validate())
            {
                checkInternet = NetworkChecking.isConnected(this);

                if (checkInternet)
                {
                    final String name = first_name.getText().toString().trim();
                    final String lastname = last_name.getText().toString().trim();
                    final String mobile = mobile_edt.getText().toString().trim();
                    final String email = email_edt.getText().toString().trim();
                    final String password = edt_password.getText().toString().trim();
                    final String code = country_edttxt.getText().toString().trim();

                    if (radio_male.isChecked() || radio_female.isChecked()|| radio_other.isChecked())
                    {
                        gender_status = (RadioButton) findViewById(radio_group.getCheckedRadioButtonId());

                        if (gender_status.getId() == radio_male.getId())
                        {
                            send_gender = "Male";

                        }
                        if (gender_status.getId() == radio_female.getId())
                        {
                            send_gender = "Female";

                        }
                        if(gender_status.getId()==radio_other.getId())
                        {
                            send_gender="Others";
                        }
                    }


                    Log.d("REGURL", AppUrls.BASE_URL + AppUrls.REGISTRATION);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTRATION,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    progressDialog.dismiss();
                                    Log.d("RESPONCELREG", response);
                                    try {

                                        JSONObject jsonObject = new JSONObject(response);
                                        String editSuccessResponceCode = jsonObject.getString("code");
                                        if (editSuccessResponceCode.equalsIgnoreCase("10100"))
                                        {
                                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                            progressDialog.dismiss();
                                            String user_id=jsonObject1.getString("user_id");
                                            /*"user_id": 1*/
                                            userSessionManager.createUserRegisterSession(user_id);
                                            Intent imain = new Intent(RegisterActivity.this, StepOne_Description.class);
                                            startActivity(imain);
                                            Toast.makeText(getApplicationContext(), "Registration Successfully...! ", Toast.LENGTH_SHORT).show();

                                        }
                                        if (editSuccessResponceCode.equals("10400"))
                                        {
                                            Toast.makeText(getApplicationContext(), "Email already exits", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10300"))
                                        {
                                            Toast.makeText(getApplicationContext(), "Mobile already exits", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("10200"))
                                        {
                                            Toast.makeText(getApplicationContext(), "Sorry, Try again....!", Toast.LENGTH_SHORT).show();
                                        }
                                        if (editSuccessResponceCode.equals("11786"))
                                        {
                                            Toast.makeText(getApplicationContext(), "All fields are required....!", Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();

                                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                    } else if (error instanceof AuthFailureError) {

                                    } else if (error instanceof ServerError) {

                                    } else if (error instanceof NetworkError) {

                                    } else if (error instanceof ParseError) {

                                    }
                                }
                            })
                    {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError
                        {
                            Map<String, String> params = new HashMap<String, String>();

                            if (login_type.equals("Manual")) {
                                params.put("fname", name);
                                params.put("lname", lastname);
                                params.put("gender", send_gender);
                                params.put("email", email);
                                params.put("mobile", mobile);
                                params.put("country_code", "91");
                                params.put("password", password);
                                params.put("provider", "Manual");
                                params.put("identifier", "xxxx");
                                Log.d("ManualREQUESTDATA:",params.toString());
                            }

                            if (login_type.equals("Google")) {
                                params.put("fname", name);
                                params.put("lname", lastname);
                                params.put("gender", send_gender);
                                params.put("email", email);
                                params.put("mobile", mobile);
                                params.put("country_code", code);
                                params.put("password", password);
                                params.put("provider", "Google");
                                params.put("identifier", send_google_id);
                                Log.d("GoogleREQUESTDATA:",params.toString());
                            }

                            if (login_type.equals("Facebook")) {
                                params.put("fname", name);
                                params.put("lname", lastname);
                                params.put("gender", send_gender);
                                params.put("email", email);
                                params.put("mobile", mobile);
                                params.put("country_code", code);
                                params.put("password", password);
                                params.put("provider", "Facebook");
                                params.put("identifier", send_fb_user_data);
                                Log.d("FBREQUESTDATA:",params.toString());
                            }

                            return params;
                        }
                      /* @Override
                       public byte[] getBody() throws AuthFailureError
                       {
                           Map<String, String> params = new HashMap<String, String>();
                           params.put("name", name);
                           params.put("email", email);
                           params.put("mobile", mobile);
                           params.put("conf_pwd", password);

                           Log.d("RegisterREQUESTDATA ", params.toString());
                           return new JSONObject(params).toString().getBytes();
                       }*/

                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    RequestQueue requestQueue = Volley.newRequestQueue(RegisterActivity.this);
                    requestQueue.add(stringRequest);


                }
                else
                {
                    Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }

        if(v==country_edttxt)
        {
            Intent countryIntent = new Intent(RegisterActivity.this, CountryListActivity.class);
            startActivityForResult(countryIntent, 1);
        }
    }

    private boolean validate()
    {


        boolean result = true;
        int flag = 0;

        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";


        String name = first_name.getText().toString().trim();
        if ((name == null || name.equals("") || name.length() < 3)) {
            first_name.setError("Minimum 3 characters required");
            result = false;
        }else
        {
            if(!name.matches("^[\\p{L} .'-]+$"))
            {
                first_name.setError("Special characters not allowed");
                result = false;

            }else {
               // first_name.setErrorEnabled(false);
            }
        }

        String lastname = last_name.getText().toString().trim();
        if ((lastname == null || lastname.equals("") || lastname.length() < 3)) {
            last_name.setError("Minimum 3 characters required");
            result = false;
        }else
        {
            if(!name.matches("^[\\p{L} .'-]+$"))
            {
                last_name.setError("Special characters not allowed");
                result = false;

            }else {
              //  last_name.setErrorEnabled(false);
            }
        }


        String email = email_edt.getText().toString().trim();
        if (!email.matches(EMAIL_REGEX)) {
            email_edt.setError("Invalid Email");
            result = false;
        }else
        {
           // email_edt.setErrorEnabled(false);
        }

        String mobile = mobile_edt.getText().toString().trim();
        if ((mobile == null || mobile.equals("")) || mobile.length() != 10 )
        {
            mobile_edt.setError("Invalid Mobile Number");
            result = false;
        }
        else
        {
       //     mobile_edt.setErrorEnabled(false);
        }

        String password = edt_password.getText().toString().trim();
        if (password.isEmpty() || password.length() < 6) {
            edt_password.setError("Minimum 6 characters required");
            result = false;
        }
        else
        {
            //edt_password.setError("Minimum 6 characters required");
        }


        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                country_code = data.getStringExtra("result");

                country_edttxt.setText(country_code);

                String flagpath = data.getStringExtra("flag");

                Log.d("PATHHH", flagpath + "/" + country_code);

                Picasso.with(RegisterActivity.this)
                        .load(flagpath)
                        .placeholder(R.drawable.flag)
                        .into(country_flag);

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
