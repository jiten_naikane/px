package com.company.px.activites.egraphics;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.company.px.R;

import java.io.FileNotFoundException;
import java.io.IOException;

public class BusinessCard extends AppCompatActivity implements View.OnClickListener {
    ImageView submit_butt, upload_logo;
    Switch toggle_switch;
    LinearLayout cNameLL, cLogoLL;
    TextView txt1, txt2;
    EditText companyName, tagline, name, designation, number, email, offNumber, offEmail;
    String companyNameS, taglineS, nameS, designationS, numberS, emailS, offNumberS, offEmailS, upload_logoS;

    private String file_name;
    Uri outputFileUri;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";
    String selectedImagePath = "";
    Bitmap bitmap ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_card);

        submit_butt = (ImageView) findViewById(R.id.submit_butt);
        submit_butt.setOnClickListener(this);
        toggle_switch = (Switch) findViewById(R.id.toggle_switch);
        cNameLL = (LinearLayout) findViewById(R.id.cNameLL);
        cLogoLL = (LinearLayout) findViewById(R.id.cLogoLL);
        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);

        upload_logo = (ImageView) findViewById(R.id.upload_logo);
        upload_logo.setOnClickListener(this);
        companyName = (EditText) findViewById(R.id.companyName);
        tagline = (EditText) findViewById(R.id.tagline);
        name = (EditText) findViewById(R.id.name);
        designation = (EditText) findViewById(R.id.designation);
        number = (EditText) findViewById(R.id.number);
        email = (EditText) findViewById(R.id.email);
        offNumber = (EditText) findViewById(R.id.offNumber);
        offEmail = (EditText) findViewById(R.id.offEmail);


        switchHide();

    }



//SWITCH TOGGLE
    private void switchHide()
    {
        toggle_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    cNameLL.setVisibility(View.VISIBLE);
                    cLogoLL.setVisibility(View.GONE);
                    txt2.setTextColor(getColor(R.color.light_blue_two));
                    txt1.setTextColor(getColor(R.color.gray_title));
                }else{
                    cNameLL.setVisibility(View.GONE);
                    cLogoLL.setVisibility(View.VISIBLE);
                    txt1.setTextColor(getColor(R.color.light_blue_two));
                    txt2.setTextColor(getColor(R.color.gray_title));

                }
            }
        });




    }



//DATA TRANSFER TO ANOTHER ACTIVITY
        private void getdata_one() {
        companyNameS = companyName.getText().toString();
        taglineS = tagline.getText().toString();
        nameS = name.getText().toString();
        designationS = designation.getText().toString();
        numberS = number.getText().toString();
        emailS = email.getText().toString();
        offNumberS = offNumber.getText().toString();
        offEmailS = offEmail.getText().toString();
        upload_logoS = upload_logo.getDrawable().toString();

        Intent toEditBusi = new Intent(BusinessCard.this, EditBusinesscard.class);
        toEditBusi.putExtra("companyNameS", companyNameS);
        toEditBusi.putExtra("taglineS", taglineS);
        toEditBusi.putExtra("nameS", nameS);
        toEditBusi.putExtra("designationS", designationS);
        toEditBusi.putExtra("numberS", numberS);
        toEditBusi.putExtra("emailS", emailS);
        toEditBusi.putExtra("offNumberS", offNumberS);
        toEditBusi.putExtra("offEmailS", offEmailS);
            Log.d("PATHhhhh", selectedImagePath);
        toEditBusi.putExtra("upload_logoS", selectedImagePath);


        startActivity(toEditBusi);
    }



//ONCLICK
    @Override
    public void onClick(View v)
    {
        if (v==submit_butt) {
            getdata_one();

        }

        if (v==upload_logo)
        {

            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, 1);

        }

       }

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

                if (resultCode == RESULT_OK) {

                    Uri targetUri = data.getData();
                    Log.d("TGGGGG", targetUri.toString());
                    Bitmap bitmap;
                    bitmap = decodeSampledBitmapFromUri(targetUri, upload_logo.getWidth(), upload_logo.getHeight());


                    if (bitmap == null) {
                        Toast.makeText(getApplicationContext(), "the image data could not be decoded" + targetUri.getPath(), Toast.LENGTH_LONG).show();

                    } else {

                        selectedImagePath = getPath(targetUri);// targetUri.getPath();
                        Log.d("GALLRYSSSSSSSSS", selectedImagePath);

                        upload_logo.setImageBitmap(bitmap);
                    }
                }


        }

    public String getPath(Uri uri) {

        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = managedQuery(uri, projection, null, null,
                null);

        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            return cursor.getString(columnIndex);
        }

        return uri.getPath();
    }

    public String getRealPathFromURI(Context context, Uri contentUri)
    {
        Cursor cursor = null;
        try {
            if("content".equals(contentUri.getScheme())) {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }

            else{
                return contentUri.getPath();
            }
        } finally {

            if (cursor != null) {
                cursor.close();
            }

        }

    }

    public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight)
    {
        Bitmap bm = null;
        try
        {

// First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
// Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,reqHeight);
// Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
        return bm;

    }

    public int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight)
    {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    private void showAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(BusinessCard.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ActivityCompat.requestPermissions(BusinessCard.this,
                                new String[]{android.Manifest.permission.CAMERA},
                                0);
                    }
                });
        alertDialog.show();
    }
    public static void saveToPreferences(Context context, String key, Boolean allowed) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putBoolean(key, allowed);
        prefsEditor.commit();
    }
    public static Boolean getFromPref(Context context, String key) {
        SharedPreferences myPrefs = context.getSharedPreferences(CAMERA_PREF,
                Context.MODE_PRIVATE);
        return (myPrefs.getBoolean(key, false));
    }

    private void showSettingsAlert() {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(BusinessCard.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("App needs to access the Camera.");

        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        //finish();
                    }
                });

        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "SETTINGS",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                        startInstalledAppDetailsActivity(BusinessCard.this);
                    }
                });

        alertDialog.show();
    }
    public static void startInstalledAppDetailsActivity(final Activity context)
    {
        if (context == null) {
            return;
        }

        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }




}
