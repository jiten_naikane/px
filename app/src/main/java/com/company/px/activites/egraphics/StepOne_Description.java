package com.company.px.activites.egraphics;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.R;
import com.company.px.adapter.PlanAdapter;
import com.company.px.adapter.SelectedImageAdapter2;
import com.company.px.model.PlanModel;
import com.company.px.model.SelectedImageModel2;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StepOne_Description extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    ImageView back_butt;
    LinearLayout nextButt;
    String user_id = "1", activity_id_S, main_catg_id_S, product_id_S, user_id_S, amount_S, title_S, description_S, design_style_S, email_id_S, whattsapp_no_S;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;
    RadioGroup design_choice_rad_grp;
    RadioButton design_my_choice_rad, design_designer_choice_rad;
    HorizontalScrollView design_choice_hL;
    EditText whatsapp_E, email_E, describe_E, title_E;
    TextInputLayout title_TIL, describe_TIL;
    RecyclerView plan_recycler;
    LinearLayoutManager linearLayoutManager;
    PlanAdapter planAdapter;
    ArrayList<PlanModel> planModel = new ArrayList<PlanModel>();
    TextView delv_err, upload_butt_fileManager, upload_butt_fileManager_2, fileManager_1_txt, fileManager_2_txt, upload_butt_gallery;
    View black_c, white_c, red_c, purple_c, pink_c, deepPurple_c, indigo_c, blue_c, cyan_c, lightBlue_c, teal_c, green_c, lightGreen_c, lime_c, yellow_c, amber_c, orange_c, deepOrange_c, brown_c, grey_c, blueGrey_c;
    CheckBox black_cb, white_cb, red_cb, purple_cb, pink_cb, deepPurple_cb, indigo_cb, blue_cb, cyan_cb, lightBlue_cb, teal_cb, green_cb, lightGreen_cb, lime_cb, yellow_cb, amber_cb, orange_cb, deepOrange_cb, brown_cb, grey_cb, blueGrey_cb;
    private int PICK_PDF_REQUEST = 1;
    private int PICK_PDF_REQUEST2 = 2;
    private int PICK_PDF_REQUEST_MULTIPLE = 3;
    private Uri filePath1, filePath2;
    RecyclerView multi_photo_recycler;
    SelectedImageAdapter2 selectedImageAdapter2;
    ArrayList<SelectedImageModel2> selectedImageModels2 = new ArrayList<SelectedImageModel2>();
    String fileNameMulti = "";
    ArrayList<String> encodedImageList;
    ArrayList<Uri> imagesUriList;
    String multi_imageURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_egraphic_step_one);
        super.onCreate(savedInstanceState);

        pprogressDialog = new ProgressDialog(StepOne_Description.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        encodedImageList = new ArrayList<>();
        Bundle bundle = getIntent().getExtras();
        main_catg_id_S = bundle.getString("MAINCATEGID");
        product_id_S = bundle.getString("PRODUCT_ID");
//        activity_id_S = bundle.getString("ACTIVITY_ID");
//
//        if (activity_id_S.equals("3"))
//        {
//            user_id_S = bundle.getString("user_id");
//            main_catg_id_S = bundle.getString("product_cat_id");
//            product_id_S = bundle.getString("product_id");
//            amount_S = bundle.getString("amount");
//            title_S = bundle.getString("title");
//            title_E.setText(title_S);
//            description_S = bundle.getString("description");
//            describe_E.setText(description_S);
//            design_style_S = bundle.getString("design_style");
//            email_id_S = bundle.getString("email_id");
//            email_E.setText(email_id_S);
//            whattsapp_no_S = bundle.getString("whattsapp");
//            whatsapp_E.setText(whattsapp_no_S);
//        }

        black_c = (View) findViewById(R.id.black_c);
        white_c = (View) findViewById(R.id.white_c);
        red_c = (View) findViewById(R.id.red_c);
        purple_c = (View) findViewById(R.id.purple_c);
        pink_c = (View) findViewById(R.id.pink_c);
        deepPurple_c = (View) findViewById(R.id.deepPurple_c);
        indigo_c = (View) findViewById(R.id.indigo_c);
        blue_c = (View) findViewById(R.id.blue_c);
        cyan_c = (View) findViewById(R.id.cyan_c);
        lightBlue_c = (View) findViewById(R.id.lightBlue_c);
        teal_c = (View) findViewById(R.id.teal_c);
        green_c = (View) findViewById(R.id.green_c);
        lightGreen_c = (View) findViewById(R.id.lightGreen_c);
        lime_c = (View) findViewById(R.id.lime_c);
        yellow_c = (View) findViewById(R.id.yellow_c);
        amber_c = (View) findViewById(R.id.amber_c);
        orange_c = (View) findViewById(R.id.orange_c);
        deepOrange_c = (View) findViewById(R.id.deepOrange_c);
        brown_c = (View) findViewById(R.id.brown_c);
        grey_c = (View) findViewById(R.id.grey_c);
        blueGrey_c = (View) findViewById(R.id.blueGrey_c);

        black_cb = (CheckBox) findViewById(R.id.black_cb);
        black_cb.setOnCheckedChangeListener(this);
        white_cb = (CheckBox) findViewById(R.id.white_cb);
        white_cb.setOnCheckedChangeListener(this);
        red_cb = (CheckBox) findViewById(R.id.red_cb);
        red_cb.setOnCheckedChangeListener(this);
        purple_cb = (CheckBox) findViewById(R.id.purple_cb);
        purple_cb.setOnCheckedChangeListener(this);
        pink_cb = (CheckBox) findViewById(R.id.pink_cb);
        pink_cb.setOnCheckedChangeListener(this);
        deepPurple_cb = (CheckBox) findViewById(R.id.deepPurple_cb);
        deepPurple_cb.setOnCheckedChangeListener(this);
        indigo_cb = (CheckBox) findViewById(R.id.indigo_cb);
        indigo_cb.setOnCheckedChangeListener(this);
        blue_cb = (CheckBox) findViewById(R.id.blue_cb);
        blue_cb.setOnCheckedChangeListener(this);
        cyan_cb = (CheckBox) findViewById(R.id.cyan_cb);
        cyan_cb.setOnCheckedChangeListener(this);
        lightBlue_cb = (CheckBox) findViewById(R.id.lightBlue_cb);
        lightBlue_cb.setOnCheckedChangeListener(this);
        teal_cb = (CheckBox) findViewById(R.id.teal_cb);
        teal_cb.setOnCheckedChangeListener(this);
        green_cb = (CheckBox) findViewById(R.id.green_cb);
        green_cb.setOnCheckedChangeListener(this);
        lightGreen_cb = (CheckBox) findViewById(R.id.lightGreen_cb);
        lightGreen_cb.setOnCheckedChangeListener(this);
        lime_cb = (CheckBox) findViewById(R.id.lime_cb);
        lime_cb.setOnCheckedChangeListener(this);
        yellow_cb = (CheckBox) findViewById(R.id.yellow_cb);
        yellow_cb.setOnCheckedChangeListener(this);
        amber_cb = (CheckBox) findViewById(R.id.amber_cb);
        amber_cb.setOnCheckedChangeListener(this);
        orange_cb = (CheckBox) findViewById(R.id.orange_cb);
        orange_cb.setOnCheckedChangeListener(this);
        deepOrange_cb = (CheckBox) findViewById(R.id.deepOrange_cb);
        deepOrange_cb.setOnCheckedChangeListener(this);
        brown_cb = (CheckBox) findViewById(R.id.brown_cb);
        brown_cb.setOnCheckedChangeListener(this);
        grey_cb = (CheckBox) findViewById(R.id.grey_cb);
        grey_cb.setOnCheckedChangeListener(this);
        blueGrey_cb = (CheckBox) findViewById(R.id.blueGrey_cb);
        blueGrey_cb.setOnCheckedChangeListener(this);


        title_TIL = (TextInputLayout) findViewById(R.id.title_TIL);
        describe_TIL = (TextInputLayout) findViewById(R.id.describe_TIL);
        delv_err = (TextView) findViewById(R.id.delv_err);

        upload_butt_gallery = (TextView) findViewById(R.id.upload_butt_gallery);
        upload_butt_gallery.setOnClickListener(this);

        upload_butt_fileManager = (TextView) findViewById(R.id.upload_butt_fileManager);
        upload_butt_fileManager.setOnClickListener(this);
        fileManager_1_txt = (TextView) findViewById(R.id.fileManager_1_txt);

        upload_butt_fileManager_2 = (TextView) findViewById(R.id.upload_butt_fileManager_2);
        upload_butt_fileManager_2.setOnClickListener(this);
        fileManager_2_txt = (TextView) findViewById(R.id.fileManager_2_txt);


        whatsapp_E = (EditText) findViewById(R.id.whatsapp_E);
        email_E = (EditText) findViewById(R.id.email_E);
        describe_E = (EditText) findViewById(R.id.describe_E);
        title_E = (EditText) findViewById(R.id.title_E);

        back_butt = (ImageView) findViewById(R.id.back_butt);
        back_butt.setOnClickListener(this);
        nextButt = (LinearLayout) findViewById(R.id.nextButt);
        nextButt.setOnClickListener(this);
        design_choice_hL = (HorizontalScrollView) findViewById(R.id.design_choice_hL);

        design_choice_rad_grp = (RadioGroup) findViewById(R.id.design_choice_rad_grp);
        design_my_choice_rad = (RadioButton) findViewById(R.id.design_my_choice_rad);
        design_designer_choice_rad = (RadioButton) findViewById(R.id.design_designer_choice_rad);

        design_choice_rad_grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (rb == design_my_choice_rad) {
                    design_choice_hL.setVisibility(View.VISIBLE);
                } else if (rb == design_designer_choice_rad) {
                    design_choice_hL.setVisibility(View.GONE);
                }
            }
        });


        plan_recycler = (RecyclerView) findViewById(R.id.plan_recycler);
        planAdapter = new PlanAdapter(planModel, StepOne_Description.this, R.layout.plan_row);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        plan_recycler.setLayoutManager(linearLayoutManager);


        multi_photo_recycler = (RecyclerView) findViewById(R.id.multi_photo_recycler);
        multi_photo_recycler.setHasFixedSize(true);

        multi_photo_recycler.setLayoutManager(new GridLayoutManager(StepOne_Description.this, 5, GridLayoutManager.VERTICAL, false));
        multi_photo_recycler.setNestedScrollingEnabled(false);
        multi_photo_recycler.setSaveFromParentEnabled(true);


        getPlans();


    }


    private void getPlans() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("PLAN_URL", AppUrls.BASE_URL + AppUrls.GET_PLANS + "/2");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_PLANS + "/2",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("PLAN_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        PlanModel pList = new PlanModel();
                                        pList.setName(jsonObject1.getString("name"));

                                        planModel.add(pList);
                                    }
                                    plan_recycler.setAdapter(planAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {
        if (v == nextButt) {
            sendDesignDescription();
        }
        if (v == back_butt) {
            finish();
        }
        if (v == upload_butt_fileManager) {
            ActivityCompat.requestPermissions(StepOne_Description.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_PDF_REQUEST);
            Intent intent = new Intent();
            // intent.setType("application/msword");
            intent.setType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "select File"), PICK_PDF_REQUEST);
        }
        if (v == upload_butt_fileManager_2) {
            ActivityCompat.requestPermissions(StepOne_Description.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_PDF_REQUEST2);
            Intent intent2 = new Intent();
            // intent.setType("application/msword");
            intent2.setType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            intent2.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent2, "select File"), PICK_PDF_REQUEST2);
        }
        if (v == upload_butt_gallery) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Choose application"), PICK_PDF_REQUEST_MULTIPLE);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
       // super.onActivityResult(requestCode, resultCode, data);

        Log.d("VALL", String.valueOf(PICK_PDF_REQUEST));

        if (requestCode == PICK_PDF_REQUEST )
        {
            Log.d("VALL", String.valueOf(PICK_PDF_REQUEST));
            filePath1 = data.getData();
            String uriString = filePath1.toString();
            File myFile = new File(uriString);
            String path = myFile.getAbsolutePath();
            Log.d("erwerew:", filePath1.toString());
            String displayName = null;

            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = getApplicationContext().getContentResolver().query(filePath1, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        Log.d("SHORTTTTTTT11:", displayName);
                        fileManager_1_txt.setText(displayName);


                    }
                } finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName();
                Log.d("SHORTTTTTTT22:", displayName);
                fileManager_1_txt.setText(displayName);
            }
        }
            else if (requestCode == PICK_PDF_REQUEST2 )
            {
                Log.d("SSSSS", String.valueOf(PICK_PDF_REQUEST2));
                filePath2 = data.getData();
                String uriString2 = filePath2.toString();
                File myFile2 = new File(uriString2);
                String path2 = myFile2.getAbsolutePath();
                Log.d("erwerew2:", filePath2.toString());
                String displayName2 = null;

                if (uriString2.startsWith("content://")) {
                    Cursor cursor2 = null;
                    try {
                        cursor2 = getApplicationContext().getContentResolver().query(filePath2, null, null, null, null);
                        if (cursor2 != null && cursor2.moveToFirst()) {
                            displayName2 = cursor2.getString(cursor2.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                            Log.d("SHORTTTTTTT11:", displayName2);
                            fileManager_2_txt.setText(displayName2);


                        }
                    } finally {
                        cursor2.close();
                    }
                } else if (uriString2.startsWith("file://")) {
                    displayName2 = myFile2.getName();
                    Log.d("SHORTTTTTTT22:", displayName2);
                    fileManager_2_txt.setText(displayName2);
                }


            }
           else if (requestCode == PICK_PDF_REQUEST_MULTIPLE )
            {
                try {

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    imagesUriList = new ArrayList<Uri>();


                    encodedImageList.clear();
                    if (data.getData() != null) {

                        Uri uri = data.getData();
                        imagesUriList.add(uri);
                        // Get the cursor
                        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        multi_imageURI = cursor.getString(columnIndex);

                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                        encodedImageList.add(encodedImage);
                        cursor.close();
                        SelectedImageModel2 sim = new SelectedImageModel2();

                        fileNameMulti = uri.toString();
                        sim.setImage_uri(fileNameMulti);
                        selectedImageModels2.add(sim);
                        Log.d("IMAGESINGLE:", selectedImageModels2.toString());
                        Log.d("FILENAMESINGLE:", fileNameMulti);


                        selectedImageAdapter2 = new SelectedImageAdapter2(selectedImageModels2, StepOne_Description.this, R.layout.row_selected_image2);
                        multi_photo_recycler.setAdapter(selectedImageAdapter2);


                    } else {
                        if (data.getClipData() != null) {

                            ClipData mClipData = data.getClipData();
                            ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                            for (int i = 0; i < mClipData.getItemCount(); i++) {

                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();
                                mArrayUri.add(uri);
                                // Get the cursor
                                Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                                // Move to first row
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                multi_imageURI = cursor.getString(columnIndex);

                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                                String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
                                encodedImageList.add(encodedImage);
                                cursor.close();
                                SelectedImageModel2 sim = new SelectedImageModel2();

                                fileNameMulti = uri.toString();
                                sim.setImage_uri(fileNameMulti);
                                selectedImageModels2.add(sim);
                                Log.d("IMAGEMULTIPLE:", selectedImageModels2.toString());
                                Log.d("FILENAMULTIPLE:", fileNameMulti);


                                selectedImageAdapter2 = new SelectedImageAdapter2(selectedImageModels2, StepOne_Description.this, R.layout.row_selected_image2);
                                multi_photo_recycler.setAdapter(selectedImageAdapter2);
                                // noImage.setText("Selected Images: " + mArrayUri.size());
                            }

                        }
                    }


                } catch (Exception e) {
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
                }
            }
            else {
                Toast.makeText(this, "",
                        Toast.LENGTH_LONG).show();
            }
        }



    private void sendDesignDescription() {
        Log.d("rrrrrr", "rrrrrr");
        checkInternet = NetworkChecking.isConnected(StepOne_Description.this);
        if (validate()) {
            if (checkInternet) {
                Log.d("DESIG_DESC_URL", AppUrls.BASE_URL + AppUrls.CREATE_ORDER);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.CREATE_ORDER,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("DESIG_DESC_RESP", response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("code");
                                    if (successResponceCode.equals("10100")) {
                                        pprogressDialog.dismiss();
                                        JSONObject jobj = jsonObject.getJSONObject("data");
                                        int order_ID = jobj.getInt("order_id");
                                        String cat_nam = jobj.getString("category_name");
//                                        Intent steptwo_intent = new Intent(StepOne_Description.this, StepThree_OrderSummary.class);
                                        Intent steptwo_intent = new Intent(StepOne_Description.this, StepTwo_Cart.class);
                                        String O_D_ID = String.valueOf(order_ID);

                                        steptwo_intent.putExtra("ORDER_ID", O_D_ID);
                                        Log.d("ORD_CAT_IIDD", String.valueOf(O_D_ID + "///" + main_catg_id_S));
                                        startActivity(steptwo_intent);
                                    }
                                    if (successResponceCode.equals("10200")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                                    }
                                    if (successResponceCode.equals("10300")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "User Not Exist..!", Toast.LENGTH_SHORT).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pprogressDialog.dismiss();

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", "121");
                        params.put("product_cat_id", main_catg_id_S);
                        params.put("product_id", product_id_S);
                        params.put("amount", amount_S);
                        params.put("title", title_S);
                        params.put("description", description_S);
                        params.put("design_style", design_style_S);
                        params.put("email_id", "jnaikane@gmail.com");
                        params.put("whattsapp_no", whattsapp_no_S);
                        // params.put("file1", "file");

                        Log.d("DESG_DESC_PARAM", params.toString());
                        return params;
                    }
                };
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                RequestQueue requestQueue = Volley.newRequestQueue(StepOne_Description.this);
                requestQueue.add(stringRequest);

            } else {
                pprogressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "No Internet Connection ..!!", Toast.LENGTH_SHORT).show();

            }
        }
    }


    private boolean validate() {
        whattsapp_no_S = whatsapp_E.getText().toString().trim();
        email_id_S = email_E.getText().toString().trim();
        description_S = describe_E.getText().toString().trim();
        title_S = title_E.getText().toString().trim();
        amount_S = "1999";
        design_style_S = "ABC";
//        String EMAIL_REGEX = "^[\\w_\\.+]*[\\w_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
//        String MOBILE_REGEX = "^[789]\\d{9}$";
        boolean result = true;


        if (whattsapp_no_S.isEmpty() && email_id_S.isEmpty()) {
            delv_err.setVisibility(View.VISIBLE);
            result = false;
        } else {
            delv_err.setVisibility(View.GONE);
            result = true;
        }

        if (description_S.isEmpty()) {
            describe_TIL.setError("Tell us your Design Requirement..");
            result = false;
        } else {
            describe_TIL.setErrorEnabled(false);
            result = true;
        }

        if (title_S.isEmpty()) {
            title_TIL.setError("What is your Design Title ?");
            result = false;
        } else {
            title_TIL.setErrorEnabled(false);
            result = true;
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (black_cb.isChecked()) {
            Toast.makeText(this, "Checked", Toast.LENGTH_SHORT).show();
        }
    }

    public void reloadeSelectedImage2() {

        selectedImageAdapter2.notifyDataSetChanged();
    }

}
