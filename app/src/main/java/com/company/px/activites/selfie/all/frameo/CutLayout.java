package com.company.px.activites.selfie.all.frameo;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Xfermode;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.RelativeLayout;


public class CutLayout extends RelativeLayout {
    public Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    public Xfermode pdMode = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
    public Path path = new Path();
    public int x_obliq = 0;
    public int y_obliq = 0;
    public int dist_x_obliq = 0;
    public int dist_y_obliq = 0;


    public int getDist_x_obliq() {
        return dist_x_obliq;
    }

    public void setDist_x_obliq(int dist_x_obliq) {
        this.dist_x_obliq = dist_x_obliq;
    }

    public int getDist_y_obliq() {
        return dist_y_obliq;
    }

    public void setDist_y_obliq(int dist_y_obliq) {
        this.dist_y_obliq = dist_y_obliq;
    }

    public int getY_obliq() {
        return y_obliq;
    }

    public void setY_obliq(int y_obliq) {
        this.y_obliq = y_obliq;
    }

    public int getX_obliq() {
        return x_obliq;
    }

    public void setX_obliq(int x_obliq) {
        this.x_obliq = x_obliq;
    }


     public CutLayout(Context context) {
         super(context);
     }

     public CutLayout(Context context, AttributeSet attrs) {
         super(context, attrs);
     }

     public CutLayout(Context context, AttributeSet attrs, int defStyleAttr) {
         super(context, attrs, defStyleAttr);
     }

     @TargetApi(Build.VERSION_CODES.LOLLIPOP)
     public CutLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
         super(context, attrs, defStyleAttr, defStyleRes);
     }

        @Override
        protected void dispatchDraw(Canvas canvas) {
            int saveCount = canvas.saveLayer(0, 0, getWidth(), getHeight(), null, Canvas.ALL_SAVE_FLAG);
            super.dispatchDraw(canvas);
            paint.setXfermode(pdMode);
            path.reset();
            path.moveTo(getDist_x_obliq(), getDist_y_obliq());
            path.lineTo(getWidth(), getHeight());
            path.lineTo(getHeight() - TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getX_obliq(), getResources().getDisplayMetrics()),
                    getHeight() - TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getY_obliq(), getResources().getDisplayMetrics()));
            path.close();
            canvas.drawPath(path, paint);
            canvas.restoreToCount(saveCount);
            paint.setXfermode(null);
        }


}
