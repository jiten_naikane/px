package com.company.px.activites.selfie.all.stickies;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.R;
import com.company.px.activites.selfie.all.texteo.Texteo;
import com.company.px.adapter.EmojiImagesAdapter_Stickme;
import com.company.px.adapter.EmojiImagesAdapter_Texteo;
import com.company.px.adapter.EmojiTitleAdapter_Stickme;
import com.company.px.adapter.EmojiTitleAdapter_Texteo;
import com.company.px.adapter.StickMeFestivalTitleAdapter;
import com.company.px.adapter.StickMeImagesHoriAdapter;
import com.company.px.model.EmojiImagesModel;
import com.company.px.model.EmojiTitlesModel;
import com.company.px.model.FestivalTitlesModel;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.company.px.R.*;

public class StickiesEdit extends Activity implements View.OnClickListener, View.OnTouchListener {

    public ImageView s1_tool_thumb, s2_tool_thumb, s3_tool_thumb, s4_tool_thumb, s5_tool_thumb, thumb_1_visi_ic, thumb_1_Invisi_ic, thumb_2_visi_ic, thumb_2_Invisi_ic, thumb_3_visi_ic, thumb_3_Invisi_ic, thumb_x_1, thumb_x_2, thumb_x_3, img_stick_thumb_1, img_stick_thumb_2, img_stick_thumb_3, img_stick_thumb_4, img_stick_thumb_5, sticky_visi_ic, sticky_Invisi_ic, sticky_lockIn_ic, sticky_lockOut_ic, flip_down_sticky, flip_right_sticky, sticky_s_1, sticky_s_2, sticky_s_3, emoji_Invisi_ic, emoji_visi_ic, emoji_lockOut_ic, emoji_lockIn_ic, emojiButt, flip_down, flip_right, pencil_ic_img, img_photo, sticky_1, sticky_2, sticky_3, sticky_4, sticky_5;
    private static int RESULT_LOAD_IMG = 1;
    String EDIT_CAT_ID, imgDecodableString, emoji_1_color, emoji_2_color, emoji_3_color;
    private boolean sticky_1x_bool = false, sticky_2x_bool = false, sticky_3x_bool = false, sticky_1_bool = false, sticky_2_bool = false, sticky_3_bool = false, sticky_bool = false, sticky_bool_s1 = false, sticky_bool_s2 = false, sticky_bool_s3 = false,
            emoji_1x_bool = false, emoji_2x_bool = false, emoji_3x_bool = false, emoji_1_bool = false, emoji_2_bool = false, emoji_3_bool = false, emoji_bool = false, emoji_bool_s1 = false, emoji_bool_s2 = false, emoji_bool_s3 = false, exit = false, photo_text_Butt_bool = true, slide_bw_bool, slide_contrast_bool, slide_opacity_bool;
    TextView upload_photo, emoji_txt_Butt, sticky_txt_Butt, outputButt, photo_text_Butt;
    public LinearLayout stick_rl_1, stick_rl_2, stick_rl_3, photo_edt_tool, ll1, ll2, photo_tab_ll, ll_img_tool, sticky_tool_ll, internet_txt_LL, sticky_tab_ll, emoji_tab_ll, emojiButt2, colors_ll;
    View black_c, white_c, red_c, purple_c, pink_c, deepPurple_c, indigo_c, blue_c, cyan_c, lightBlue_c, teal_c, green_c, lightGreen_c, lime_c, yellow_c, amber_c, orange_c, deepOrange_c, brown_c, grey_c, blueGrey_c, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9;
    float dX, dY;
    private Matrix matrix = new Matrix(), savedMatrix = new Matrix(), matrix_sticky_1 = new Matrix(), matrix_sticky_2 = new Matrix(), matrix_sticky_3 = new Matrix();
    private static final int NONE = 0, DRAG = 1, ZOOM = 2;
    private PointF start = new PointF(), mid = new PointF();
    private float newRot = 0f, d = 0f, oldDist = 1f, brightness = 9;
    private float[] lastEvent = null;

    RelativeLayout rL, theme_internet_LL;
    SeekBar contrast, bWSeek, size_sticky_seek;
    int sticky_1_size, sticky_2_size, sticky_3_size, mode, lastAction, getViewColo = Color.BLACK, bwValue, screen_height, screen_width, bw_p_photo_save = 50, contrast_p_photo_save = 100;
    BottomSheetBehavior mBottomSheetBehavior;
    StickerImageView emoji_s_1, emoji_s_2, emoji_s_3;

    private boolean checkInternet;
    RecyclerView fest_title_recycler, imagesHoriRecycler, emoji_recycler_titles, emoji_recycler_images;
    LinearLayoutManager linearLayoutManager, linearLayoutManager2, linearLayoutManager_ST;
    StickMeFestivalTitleAdapter stickMeFestivalTitleAdapter;
    ArrayList<FestivalTitlesModel> festTitleModel = new ArrayList<FestivalTitlesModel>();

    StickMeImagesHoriAdapter stickMeImagesHoriAdapter;
    ArrayList<ImagesHoriModel> imagesHoriModel = new ArrayList<ImagesHoriModel>();

    EmojiTitleAdapter_Stickme emojiTitleAdapterStickme;
    ArrayList<EmojiTitlesModel> emojiTitlesModels = new ArrayList<EmojiTitlesModel>();

    GridLayoutManager gridLayoutManager;
    EmojiImagesAdapter_Stickme emojiImagesAdapterStickme;
    ArrayList<EmojiImagesModel> emojiImagesModels = new ArrayList<EmojiImagesModel>();


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_stikc_me);


        Bundle bundle = getIntent().getExtras();
        EDIT_CAT_ID = bundle.getString("EDIT_CAT_ID");

        View bottomSheet = findViewById(id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        pencil_ic_img.setRotation(0);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        pencil_ic_img.setRotation(180);
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });


        c1 = (View) findViewById(id.c1);
        c1.setOnClickListener(this);
        c2 = (View) findViewById(id.c2);
        c2.setOnClickListener(this);
        c3 = (View) findViewById(id.c3);
        c3.setOnClickListener(this);
        c4 = (View) findViewById(id.c4);
        c4.setOnClickListener(this);
        c5 = (View) findViewById(id.c5);
        c5.setOnClickListener(this);
        c6 = (View) findViewById(id.c6);
        c6.setOnClickListener(this);
        c7 = (View) findViewById(id.c7);
        c7.setOnClickListener(this);
        c8 = (View) findViewById(id.c8);
        c8.setOnClickListener(this);
        c9 = (View) findViewById(id.c9);
        c9.setOnClickListener(this);
        black_c = (View) findViewById(id.black_c);
        black_c.setOnClickListener(this);
        white_c = (View) findViewById(id.white_c);
        white_c.setOnClickListener(this);
        red_c = (View) findViewById(id.red_c);
        red_c.setOnClickListener(this);
        purple_c = (View) findViewById(id.purple_c);
        purple_c.setOnClickListener(this);
        pink_c = (View) findViewById(id.pink_c);
        pink_c.setOnClickListener(this);
        deepPurple_c = (View) findViewById(id.deepPurple_c);
        deepPurple_c.setOnClickListener(this);
        indigo_c = (View) findViewById(id.indigo_c);
        indigo_c.setOnClickListener(this);
        blue_c = (View) findViewById(id.blue_c);
        blue_c.setOnClickListener(this);
        cyan_c = (View) findViewById(id.cyan_c);
        cyan_c.setOnClickListener(this);
        lightBlue_c = (View) findViewById(id.lightBlue_c);
        lightBlue_c.setOnClickListener(this);
        teal_c = (View) findViewById(id.teal_c);
        teal_c.setOnClickListener(this);
        green_c = (View) findViewById(id.green_c);
        green_c.setOnClickListener(this);
        lightGreen_c = (View) findViewById(id.lightGreen_c);
        lightGreen_c.setOnClickListener(this);
        lime_c = (View) findViewById(id.lime_c);
        lime_c.setOnClickListener(this);
        yellow_c = (View) findViewById(id.yellow_c);
        yellow_c.setOnClickListener(this);
        amber_c = (View) findViewById(id.amber_c);
        amber_c.setOnClickListener(this);
        orange_c = (View) findViewById(id.orange_c);
        orange_c.setOnClickListener(this);
        deepOrange_c = (View) findViewById(id.deepOrange_c);
        deepOrange_c.setOnClickListener(this);
        brown_c = (View) findViewById(id.brown_c);
        brown_c.setOnClickListener(this);
        grey_c = (View) findViewById(id.grey_c);
        grey_c.setOnClickListener(this);
        blueGrey_c = (View) findViewById(id.blueGrey_c);
        blueGrey_c.setOnClickListener(this);

        c1.setBackgroundResource(color.red_p1);
        c2.setBackgroundResource(color.red_p2);
        c3.setBackgroundResource(color.red_p3);
        c4.setBackgroundResource(color.red_p4);
        c5.setBackgroundResource(color.red_p5);
        c6.setBackgroundResource(color.red_p6);
        c7.setBackgroundResource(color.red_p7);
        c8.setBackgroundResource(color.red_p8);
        c9.setBackgroundResource(color.red_p9);

        img_stick_thumb_1 = (ImageView) findViewById(id.img_stick_thumb_1);
        img_stick_thumb_1.setOnClickListener(this);
        img_stick_thumb_2 = (ImageView) findViewById(id.img_stick_thumb_2);
        img_stick_thumb_2.setOnClickListener(this);
        img_stick_thumb_3 = (ImageView) findViewById(id.img_stick_thumb_3);
        img_stick_thumb_3.setOnClickListener(this);
        img_stick_thumb_4 = (ImageView) findViewById(id.img_stick_thumb_4);
        img_stick_thumb_4.setOnClickListener(this);
        img_stick_thumb_5 = (ImageView) findViewById(id.img_stick_thumb_5);
        img_stick_thumb_5.setOnClickListener(this);
        thumb_x_1 = (ImageView) findViewById(id.thumb_x_1);
        thumb_x_1.setOnClickListener(this);
        thumb_x_2 = (ImageView) findViewById(id.thumb_x_2);
        thumb_x_2.setOnClickListener(this);
        thumb_x_3 = (ImageView) findViewById(id.thumb_x_3);
        thumb_x_3.setOnClickListener(this);

        s1_tool_thumb = (ImageView) findViewById(id.s1_tool_thumb);
        s1_tool_thumb.setOnClickListener(this);
        s2_tool_thumb = (ImageView) findViewById(id.s2_tool_thumb);
        s2_tool_thumb.setOnClickListener(this);
        s3_tool_thumb = (ImageView) findViewById(id.s3_tool_thumb);
        s3_tool_thumb.setOnClickListener(this);
        s4_tool_thumb = (ImageView) findViewById(id.s4_tool_thumb);
        s4_tool_thumb.setOnClickListener(this);
        s5_tool_thumb = (ImageView) findViewById(id.s5_tool_thumb);
        s5_tool_thumb.setOnClickListener(this);

        thumb_1_visi_ic = (ImageView) findViewById(id.thumb_1_visi_ic);
        thumb_1_visi_ic.setOnClickListener(this);
        thumb_1_Invisi_ic = (ImageView) findViewById(id.thumb_1_Invisi_ic);
        thumb_1_Invisi_ic.setOnClickListener(this);
        thumb_2_visi_ic = (ImageView) findViewById(id.thumb_2_visi_ic);
        thumb_2_visi_ic.setOnClickListener(this);
        thumb_2_Invisi_ic = (ImageView) findViewById(id.thumb_2_Invisi_ic);
        thumb_2_Invisi_ic.setOnClickListener(this);
        thumb_3_visi_ic = (ImageView) findViewById(id.thumb_3_visi_ic);
        thumb_3_visi_ic.setOnClickListener(this);
        thumb_3_Invisi_ic = (ImageView) findViewById(id.thumb_3_Invisi_ic);
        thumb_3_Invisi_ic.setOnClickListener(this);

        sticky_visi_ic = (ImageView) findViewById(id.sticky_visi_ic);
        sticky_visi_ic.setOnClickListener(this);
        sticky_Invisi_ic = (ImageView) findViewById(id.sticky_Invisi_ic);
        sticky_Invisi_ic.setOnClickListener(this);
        sticky_lockIn_ic = (ImageView) findViewById(id.sticky_lockIn_ic);
        sticky_lockIn_ic.setOnClickListener(this);
        sticky_lockOut_ic = (ImageView) findViewById(id.sticky_lockOut_ic);
        sticky_lockOut_ic.setOnClickListener(this);

        emoji_visi_ic = (ImageView) findViewById(id.emoji_visi_ic);
        emoji_visi_ic.setOnClickListener(this);
        emoji_Invisi_ic = (ImageView) findViewById(id.emoji_Invisi_ic);
        emoji_Invisi_ic.setOnClickListener(this);
        emoji_lockIn_ic = (ImageView) findViewById(id.emoji_lockIn_ic);
        emoji_lockIn_ic.setOnClickListener(this);
        emoji_lockOut_ic = (ImageView) findViewById(id.emoji_lockOut_ic);
        emoji_lockOut_ic.setOnClickListener(this);
        emojiButt = (ImageView) findViewById(id.emojiButt);
        emojiButt.setOnClickListener(this);
        emoji_txt_Butt = (TextView) findViewById(id.emoji_txt_Butt);
        emoji_txt_Butt.setOnClickListener(this);
        sticky_txt_Butt = (TextView) findViewById(id.sticky_txt_Butt);
        sticky_txt_Butt.setOnClickListener(this);
        outputButt = (TextView) findViewById(id.outputButt);
        outputButt.setOnClickListener(this);
        photo_text_Butt = (TextView) findViewById(id.photo_text_Butt);
        photo_text_Butt.setOnClickListener(this);


        ll1 = (LinearLayout) findViewById(id.ll1);
        ll2 = (LinearLayout) findViewById(id.ll2);
        photo_edt_tool = (LinearLayout) findViewById(id.photo_edt_tool);
        contrast = (SeekBar) findViewById(id.contrast);
        bWSeek = (SeekBar) findViewById(id.bWSeek);
        size_sticky_seek = (SeekBar) findViewById(id.size_sticky_seek);

        flip_down = (ImageView) findViewById(id.flip_down);
        flip_down.setOnClickListener(this);
        flip_right = (ImageView) findViewById(id.flip_right);
        flip_right.setOnClickListener(this);
        flip_down_sticky = (ImageView) findViewById(id.flip_down_sticky);
        flip_down_sticky.setOnClickListener(this);
        flip_right_sticky = (ImageView) findViewById(id.flip_right_sticky);
        flip_right_sticky.setOnClickListener(this);

        pencil_ic_img = (ImageView) findViewById(id.pencil_ic_img);
        pencil_ic_img.setOnTouchListener(this);

        img_photo = (ImageView) findViewById(id.img_photo);
        img_photo.setOnTouchListener(this);

        stick_rl_1 = (LinearLayout) findViewById(id.stick_rl_1);
        stick_rl_2 = (LinearLayout) findViewById(id.stick_rl_2);
        stick_rl_3 = (LinearLayout) findViewById(id.stick_rl_3);
        theme_internet_LL = (RelativeLayout) findViewById(id.theme_internet_LL);
        rL = (RelativeLayout) findViewById(id.rL);
        upload_photo = (TextView) findViewById(id.upload_photo);
        upload_photo.setOnClickListener(this);
        photo_tab_ll = (LinearLayout) findViewById(id.photo_tab_ll);
        ll_img_tool = (LinearLayout) findViewById(id.ll_img_tool);
        sticky_tool_ll = (LinearLayout) findViewById(id.sticky_tool_ll);
        emoji_tab_ll = (LinearLayout) findViewById(id.emoji_tab_ll);
        sticky_tab_ll = (LinearLayout) findViewById(id.sticky_tab_ll);
        internet_txt_LL = (LinearLayout) findViewById(id.internet_txt_LL);
        internet_txt_LL.setOnClickListener(this);
        colors_ll = (LinearLayout) findViewById(id.colors_ll);
        emojiButt2 = (LinearLayout) findViewById(id.emojiButt2);
        emojiButt2.setOnClickListener(this);


        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        screen_width = displayMetrics.widthPixels;
        screen_height = displayMetrics.heightPixels;
        RelativeLayout.LayoutParams paramsRL = new RelativeLayout.LayoutParams(screen_width + 200, screen_width + 200);
        rL.setLayoutParams(paramsRL);

        contrast.setProgress(100);
        contrast.setMax(200);
        contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                contrast_p_photo_save = progress;
                img_photo.setColorFilter(setContrast(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        bWSeek.setMax(500);
        bWSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                bwValue = i;
                bw_p_photo_save = i;
                rgbChanger();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        size_sticky_seek.setMax(screen_width);
        size_sticky_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                RelativeLayout.LayoutParams imgParam = new RelativeLayout.LayoutParams(i, i);
                imgParam.addRule(RelativeLayout.CENTER_IN_PARENT);

             /*   if (sticky_1_bool) {
                    sticky_1_size = i;
                    sticky_s_1.setLayoutParams(imgParam);
                }
                if (sticky_2_bool) {
                    sticky_2_size = i;
                    sticky_s_2.setLayoutParams(imgParam);
                }
                if (sticky_3_bool) {
                    sticky_3_size = i;
                    sticky_s_3.setLayoutParams(imgParam);
                }*/

                Log.d("DFVGBH", String.valueOf(i + "||" + imgParam));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            /*    if (!sticky_1x_bool && !sticky_2x_bool && !sticky_3x_bool)
                {
                    Toast.makeText(StickiesEdit.this, "Please Choose a Sticky", Toast.LENGTH_SHORT).show();
                }*/
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        fest_title_recycler = (RecyclerView) findViewById(id.fest_title_recycler);
        stickMeFestivalTitleAdapter = new StickMeFestivalTitleAdapter(festTitleModel, StickiesEdit.this, layout.row_festival_titles);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        fest_title_recycler.setLayoutManager(linearLayoutManager);

        imagesHoriRecycler = (RecyclerView) findViewById(id.imagesHoriRecycler);
        stickMeImagesHoriAdapter = new StickMeImagesHoriAdapter(imagesHoriModel, StickiesEdit.this, layout.row_images_hori);
        linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        imagesHoriRecycler.setLayoutManager(linearLayoutManager2);


        getFestivalTitles();
        photoDialog();
    }


    public float getConvertedValue(int intVal) {
        float floatVal = (float) 0.0;
        floatVal = .1f * intVal;
        return floatVal;
    }

    private void getFestivalTitles() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            theme_internet_LL.setVisibility(View.VISIBLE);
            internet_txt_LL.setVisibility(View.GONE);

            Log.d("Stickies_Title_URL", AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/" + EDIT_CAT_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/" + EDIT_CAT_ID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Stickies_Title_RES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        FestivalTitlesModel fList = new FestivalTitlesModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setCreated_on(jsonObject1.getString("created_on"));
                                        fList.setColor_id(jsonObject1.getString("color_id"));

                                        festTitleModel.add(fList);
                                    }
                                    fest_title_recycler.setAdapter(stickMeFestivalTitleAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            internet_txt_LL.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void getImagesHori(String festTitleId) {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            imagesHoriModel.clear();
            Log.d("Stick_IMG_URL", AppUrls.BASE_URL + "images/festival/" + festTitleId + "/" + EDIT_CAT_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "images/festival/" + festTitleId + "/" + EDIT_CAT_ID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("STICKI_IMG_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        ImagesHoriModel ihList = new ImagesHoriModel();
                                        ihList.setFestival_id(jsonObject1.getString("festival_id"));
                                        ihList.setImg1(jsonObject1.getString("img1"));
                                        ihList.setImg2(jsonObject1.getString("img2"));
                                        ihList.setImg3(jsonObject1.getString("img3"));


                                        imagesHoriModel.add(ihList);
                                    }
                                    imagesHoriRecycler.setAdapter(stickMeImagesHoriAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public static PorterDuffColorFilter setContrast(int progress) {
        if (progress >= 100) {
            int value = (int) (progress - 100) * 255 / 100;

            return new PorterDuffColorFilter(Color.argb(value, 255, 255, 255), PorterDuff.Mode.OVERLAY);
        } else {
            int value = (int) (100 - progress) * 255 / 100;
            return new PorterDuffColorFilter(Color.argb(value, 0, 0, 0), PorterDuff.Mode.OVERLAY);
        }

    }

    public void rgbChanger() {
        float brightness = 9;

        if (photo_text_Butt_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, bwValue
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            img_photo.setColorFilter(colorFilter);
        }
    }

    private void photoDialog() {
        final ImageView upload_photo_d;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout.mixme_photo_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        upload_photo_d = (ImageView) dialog.findViewById(id.upload_photo_d);
        upload_photo_d.setColorFilter(Color.WHITE);


        upload_photo_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

                dialog.cancel();
            }
        });

        dialog.show();
    }

    //UPLOAD IMAGE LOGO
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                img_photo.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }


    public boolean onTouch(View view, MotionEvent event) {

        if (view == pencil_ic_img) {
            if (pencil_ic_img.getRotation() == 0) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else if (pencil_ic_img.getRotation() == 180) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }

        if (view == emoji_s_1 || view == emoji_s_2 || view == emoji_s_3) {
            if (view == emoji_s_1) {
                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(false);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(true);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(true);
                }

                emoji_bool_s1 = true;
                emoji_bool_s2 = false;
                emoji_bool_s3 = false;
                emoji_s_1.bringToFront();
            } else if (view == emoji_s_2) {

                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(true);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(false);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(true);
                }

                emoji_bool_s1 = false;
                emoji_bool_s2 = true;
                emoji_bool_s3 = false;
                emoji_s_2.bringToFront();

            } else if (view == emoji_s_3) {

                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(true);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(true);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(false);
                }

                emoji_bool_s1 = false;
                emoji_bool_s2 = false;
                emoji_bool_s3 = true;

                emoji_s_3.bringToFront();
            }

            emoji_bool = true;

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }


        if (view == sticky_s_1) {
            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            sticky_bool = true;
            sticky_bool_s1 = true;
            sticky_bool_s2 = false;
            sticky_bool_s3 = false;
            sticky_s_1.bringToFront();

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    savedMatrix.set(matrix_sticky_1);
                    start.set(event.getX(), event.getY());
                    mode = DRAG;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    oldDist = spacing(event);
                    if (oldDist > 10f) {
                        savedMatrix.set(matrix_sticky_1);
                        midPoint(mid, event);
                        mode = ZOOM;
                    }
                    lastEvent = new float[4];
                    lastEvent[0] = event.getX(0);
                    lastEvent[1] = event.getX(1);
                    lastEvent[2] = event.getY(0);
                    lastEvent[3] = event.getY(1);
                    d = rotation(event);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    mode = NONE;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        matrix_sticky_1.set(savedMatrix);
                        float dx = event.getX() - start.x;
                        float dy = event.getY() - start.y;
                        matrix_sticky_1.postTranslate(dx, dy);
                    } else if (mode == ZOOM) {
                        float newDist = spacing(event);
                        if (newDist > 10f) {
                            matrix_sticky_1.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix_sticky_1.postScale(scale, scale, mid.x, mid.y);
                        }
                        if (lastEvent != null && event.getPointerCount() == 2) {
                            newRot = rotation(event);
                            float r = newRot - d;
                            float[] values = new float[9];
                            matrix_sticky_1.getValues(values);
                            float tx = values[2];
                            float ty = values[5];
                            float sx = values[0];

                            float xc = (sticky_s_1.getWidth() / 5) * sx;
                            float yc = (sticky_s_1.getHeight() / 5) * sx;
                            matrix_sticky_1.postRotate(r, tx + xc, ty + yc);

                        }
                    }
                    break;
            }
            sticky_s_1.setImageMatrix(matrix_sticky_1);
        }


        if (view == sticky_s_2) {
            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            sticky_bool = true;
            sticky_bool_s1 = false;
            sticky_bool_s2 = true;
            sticky_bool_s3 = false;
            sticky_s_2.bringToFront();

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    savedMatrix.set(matrix_sticky_2);
                    start.set(event.getX(), event.getY());
                    mode = DRAG;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    oldDist = spacing(event);
                    if (oldDist > 10f) {
                        savedMatrix.set(matrix_sticky_2);
                        midPoint(mid, event);
                        mode = ZOOM;
                    }
                    lastEvent = new float[4];
                    lastEvent[0] = event.getX(0);
                    lastEvent[1] = event.getX(1);
                    lastEvent[2] = event.getY(0);
                    lastEvent[3] = event.getY(1);
                    d = rotation(event);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    mode = NONE;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        matrix_sticky_2.set(savedMatrix);
                        float dx = event.getX() - start.x;
                        float dy = event.getY() - start.y;
                        matrix_sticky_2.postTranslate(dx, dy);
                    } else if (mode == ZOOM) {
                        float newDist = spacing(event);
                        if (newDist > 10f) {
                            matrix_sticky_2.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix_sticky_2.postScale(scale, scale, mid.x, mid.y);
                        }
                        if (lastEvent != null && event.getPointerCount() == 2) {
                            newRot = rotation(event);
                            float r = newRot - d;
                            float[] values = new float[9];
                            matrix_sticky_2.getValues(values);
                            float tx = values[2];
                            float ty = values[5];
                            float sx = values[0];

                            float xc = (sticky_s_2.getWidth() / 5) * sx;
                            float yc = (sticky_s_2.getHeight() / 5) * sx;
                            matrix_sticky_2.postRotate(r, tx + xc, ty + yc);
                        }
                    }
                    break;
            }
            sticky_s_2.setImageMatrix(matrix_sticky_2);
        }


        if (view == sticky_s_3) {
            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            sticky_bool = true;
            sticky_bool_s1 = false;
            sticky_bool_s2 = false;
            sticky_bool_s3 = true;
            sticky_s_3.bringToFront();

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    savedMatrix.set(matrix_sticky_3);
                    start.set(event.getX(), event.getY());
                    mode = DRAG;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    oldDist = spacing(event);
                    if (oldDist > 10f) {
                        savedMatrix.set(matrix_sticky_3);
                        midPoint(mid, event);
                        mode = ZOOM;
                    }
                    lastEvent = new float[4];
                    lastEvent[0] = event.getX(0);
                    lastEvent[1] = event.getX(1);
                    lastEvent[2] = event.getY(0);
                    lastEvent[3] = event.getY(1);
                    d = rotation(event);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    mode = NONE;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        matrix_sticky_3.set(savedMatrix);
                        float dx = event.getX() - start.x;
                        float dy = event.getY() - start.y;
                        matrix_sticky_3.postTranslate(dx, dy);
                    } else if (mode == ZOOM) {
                        float newDist = spacing(event);
                        if (newDist > 10f) {
                            matrix_sticky_3.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix_sticky_3.postScale(scale, scale, mid.x, mid.y);
                        }
                        if (lastEvent != null && event.getPointerCount() == 2) {
                            newRot = rotation(event);
                            float r = newRot - d;
                            float[] values = new float[9];
                            matrix_sticky_3.getValues(values);
                            float tx = values[2];
                            float ty = values[5];
                            float sx = values[0];

                            float xc = (sticky_s_3.getWidth() / 5) * sx;
                            float yc = (sticky_s_3.getHeight() / 5) * sx;
                            matrix_sticky_3.postRotate(r, tx + xc, ty + yc);
                        }
                    }
                    break;
            }
            sticky_s_3.setImageMatrix(matrix_sticky_3);
        }


//        if (view == img_photo) {
//            if (emoji_bool_s1) {
//                emoji_s_1.setControlItemsHidden(true);
//            } else if (emoji_bool_s2) {
//                emoji_s_2.setControlItemsHidden(true);
//            } else if (emoji_bool_s3) {
//                emoji_s_3.setControlItemsHidden(true);
//            }
//
//            switch (event.getAction() & MotionEvent.ACTION_MASK) {
//                case MotionEvent.ACTION_DOWN:
//                    savedMatrix.set(matrix_img1);
//                    start.set(event.getX(), event.getY());
//                    mode = DRAG;
//                    lastEvent = null;
//                    break;
//                case MotionEvent.ACTION_POINTER_DOWN:
//                    oldDist = spacing(event);
//                    if (oldDist > 10f) {
//                        savedMatrix.set(matrix_img1);
//                        midPoint(mid, event);
//                        mode = ZOOM;
//                    }
//                    lastEvent = new float[4];
//                    lastEvent[0] = event.getX(0);
//                    lastEvent[1] = event.getX(1);
//                    lastEvent[2] = event.getY(0);
//                    lastEvent[3] = event.getY(1);
//                    d = rotation(event);
//                    break;
//                case MotionEvent.ACTION_UP:
//                case MotionEvent.ACTION_POINTER_UP:
//                    mode = NONE;
//                    lastEvent = null;
//                    break;
//                case MotionEvent.ACTION_MOVE:
//                    if (mode == DRAG) {
//                        matrix_img1.set(savedMatrix);
//                        float dx = event.getX() - start.x;
//                        float dy = event.getY() - start.y;
//                        matrix_img1.postTranslate(dx, dy);
//                    } else if (mode == ZOOM) {
//                        float newDist = spacing(event);
//                        if (newDist > 10f) {
//                            matrix_img1.set(savedMatrix);
//                            float scale = (newDist / oldDist);
//                            matrix_img1.postScale(scale, scale, mid.x, mid.y);
//                        }
//                        if (lastEvent != null && event.getPointerCount() == 2) {
//                            newRot = rotation(event);
//                            float r = newRot - d;
//                            float[] values = new float[9];
//                            matrix_img1.getValues(values);
//                            float tx = values[2];
//                            float ty = values[5];
//                            float sx = values[0];
//                            float xc = (img_photo.getWidth() / 2) * sx;
//                            float yc = (img_photo.getHeight() / 2) * sx;
//                            matrix_img1.postRotate(r, tx + xc, ty + yc);
//                        }
//                    }
//                    break;
//            }
//
//            img_photo.setImageMatrix(matrix_img1);
//        }

        return true;
    }


    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);

    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Unsaved work, Tap back again to exit", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }


    private void getEmojiTitles() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            emojiTitlesModels.clear();
            Log.d("Emoji_Title_URL", AppUrls.BASE_URL + "stickers/" + "4");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "stickers/" + "4",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Emoji_Title_RES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        EmojiTitlesModel fList = new EmojiTitlesModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setCreated_on(jsonObject1.getString("created_on"));
                                        fList.setColor_id(jsonObject1.getString("color_id"));

                                        emojiTitlesModels.add(fList);
                                    }
                                    emoji_recycler_titles.setAdapter(emojiTitleAdapterStickme);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void getEmojiImages(String festTitleId) {
        emojiImagesModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("Emoji_IMG_URL", AppUrls.BASE_URL + "images/stickers/" + festTitleId + "/" + "4");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "images/stickers/" + festTitleId + "/" + "4",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Emoji_IMG_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        EmojiImagesModel ihList = new EmojiImagesModel();
                                        ihList.setImg1(jsonObject1.getString("img1"));
                                        ihList.setImg2(jsonObject1.getString("img2"));
                                        ihList.setImg3(jsonObject1.getString("img3"));
                                        ihList.setColor_filter(jsonObject1.getString("field1"));

                                        emojiImagesModels.add(ihList);
                                    }
                                    emoji_recycler_images.setAdapter(emojiImagesAdapterStickme);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void emojis(Bitmap bit, String color_fil) {
        if (!emoji_1_bool || emoji_s_1.getVisibility() == View.GONE) {
            if (!emoji_1_bool) {
                emoji_s_1 = new StickerImageView(this);
                rL.addView(emoji_s_1);
                emoji_s_1.setOnTouchListener(this);
                emoji_1_bool = true;
                emoji_s_1.setImageBitmap(bit);
                emoji_1_color = color_fil;
                emoji_1x_bool = true;
            } else if (emoji_s_1.getVisibility() == View.GONE) {
                emoji_s_1.setVisibility(View.VISIBLE);
                emoji_s_1.setImageBitmap(bit);
                emoji_1_color = color_fil;
                emoji_1x_bool = false;
            }

        } else if (!emoji_2_bool || emoji_s_2.getVisibility() == View.GONE) {
            if (!emoji_2_bool) {
                emoji_s_2 = new StickerImageView(this);
                rL.addView(emoji_s_2);
                emoji_s_2.setOnTouchListener(this);
                emoji_2_bool = true;
                emoji_s_2.setImageBitmap(bit);
                emoji_2_color = color_fil;
                emoji_2x_bool = true;
            } else if (emoji_s_2.getVisibility() == View.GONE) {
                emoji_s_2.setVisibility(View.VISIBLE);
                emoji_s_2.setImageBitmap(bit);
                emoji_2_color = color_fil;
                emoji_2x_bool = false;
            }
        } else if (!emoji_3_bool || emoji_s_3.getVisibility() == View.GONE) {
            if (!emoji_3_bool) {
                emoji_s_3 = new StickerImageView(this);
                rL.addView(emoji_s_3);
                emoji_s_3.setOnTouchListener(this);
                emoji_3_bool = true;
                emoji_s_3.setImageBitmap(bit);
                emoji_3_color = color_fil;
                emoji_3x_bool = true;
            } else if (emoji_s_3.getVisibility() == View.GONE) {
                emoji_s_3.setVisibility(View.VISIBLE);
                emoji_s_3.setImageBitmap(bit);
                emoji_3_color = color_fil;
                emoji_3x_bool = false;
            }
        }
    }


    private void getLogoColor() {
        if (emoji_bool) {
            if (emoji_bool_s1) {
                if (emoji_1_bool && emoji_1_color.equals("1")) {
                    emoji_s_1.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }

            if (emoji_bool_s2) {
                if (emoji_2_bool && emoji_2_color.equals("1")) {
                    emoji_s_2.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }

            if (emoji_bool_s3) {
                if (emoji_3_bool && emoji_3_color.equals("1")) {
                    emoji_s_3.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    private void getEmojiDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout.emoji_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        emoji_recycler_titles = (RecyclerView) dialog.findViewById(id.emoji_recycler_titles);
        emojiTitleAdapterStickme = new EmojiTitleAdapter_Stickme(emojiTitlesModels, StickiesEdit.this, layout.row_festival_titles);
        linearLayoutManager_ST = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        emoji_recycler_titles.setLayoutManager(linearLayoutManager_ST);

        emoji_recycler_images = (RecyclerView) dialog.findViewById(id.emoji_recycler_images);
        emojiImagesAdapterStickme = new EmojiImagesAdapter_Stickme(emojiImagesModels, StickiesEdit.this, layout.row_images_emoji);
        gridLayoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        emoji_recycler_images.setLayoutManager(gridLayoutManager);

        getEmojiTitles();

        dialog.show();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void getSticky(Bitmap bit) {
        RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        sticky_visi_ic.setVisibility(View.VISIBLE);

        if (!sticky_1_bool || sticky_s_1.getVisibility() == View.GONE) {
            if (!sticky_1_bool) {
                sticky_s_1 = new ImageView(this);
                rL.addView(sticky_s_1);
                sticky_s_1.setOnTouchListener(this);
                sticky_1_bool = true;
                sticky_s_1.setImageBitmap(bit);
                img_stick_thumb_1.setImageBitmap(bit);
                s1_tool_thumb.setImageBitmap(bit);
                s1_tool_thumb.setVisibility(View.VISIBLE);
                stick_rl_1.setVisibility(View.VISIBLE);
                sticky_1x_bool = true;
                sticky_s_1.bringToFront();
                sticky_s_1.setEnabled(true);

                sticky_s_1.setLayoutParams(params2);
                RectF drawableRect2 = new RectF(0, 0, screen_width, screen_width);
                RectF viewRect2 = new RectF(0, 0, screen_width, screen_width);
                matrix.setRectToRect(drawableRect2, viewRect2, Matrix.ScaleToFit.CENTER);
                sticky_s_1.setScaleType(ImageView.ScaleType.MATRIX);
                sticky_s_1.setImageMatrix(matrix_sticky_1);
                s1_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
                s2_tool_thumb.setBackground(null);
                s3_tool_thumb.setBackground(null);
            } else if (sticky_s_1.getVisibility() == View.GONE) {
                sticky_s_1.setVisibility(View.VISIBLE);
                stick_rl_1.setVisibility(View.VISIBLE);
                sticky_s_1.setImageBitmap(bit);
                img_stick_thumb_1.setImageBitmap(bit);
                s1_tool_thumb.setImageBitmap(bit);
                sticky_1x_bool = false;
                sticky_s_1.bringToFront();
                sticky_s_1.setEnabled(true);
                s1_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
                s2_tool_thumb.setBackground(null);
                s3_tool_thumb.setBackground(null);
            }

        } else if (!sticky_2_bool || sticky_s_2.getVisibility() == View.GONE) {
            if (!sticky_2_bool) {
                sticky_s_2 = new ImageView(this);
                rL.addView(sticky_s_2);
                sticky_s_2.setOnTouchListener(this);
                sticky_2_bool = true;
                stick_rl_2.setVisibility(View.VISIBLE);
                sticky_s_2.setImageBitmap(bit);
                img_stick_thumb_2.setImageBitmap(bit);
                s2_tool_thumb.setImageBitmap(bit);
                s2_tool_thumb.setVisibility(View.VISIBLE);
                sticky_2x_bool = true;
                sticky_s_2.bringToFront();
                sticky_s_2.setEnabled(true);

                sticky_s_2.setLayoutParams(params2);
                RectF drawableRect2 = new RectF(0, 0, screen_width, screen_width);
                RectF viewRect2 = new RectF(0, 0, screen_width, screen_width);
                matrix.setRectToRect(drawableRect2, viewRect2, Matrix.ScaleToFit.CENTER);
                sticky_s_2.setScaleType(ImageView.ScaleType.MATRIX);
                sticky_s_2.setImageMatrix(matrix_sticky_2);
                s1_tool_thumb.setBackground(null);
                s2_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
                s3_tool_thumb.setBackground(null);

            } else if (sticky_s_2.getVisibility() == View.GONE) {
                sticky_s_2.setVisibility(View.VISIBLE);
                stick_rl_2.setVisibility(View.VISIBLE);
                sticky_s_2.setImageBitmap(bit);
                img_stick_thumb_2.setImageBitmap(bit);
                s2_tool_thumb.setImageBitmap(bit);
                sticky_2x_bool = false;
                sticky_s_2.bringToFront();
                sticky_s_2.setEnabled(true);
                s1_tool_thumb.setBackground(null);
                s2_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
                s3_tool_thumb.setBackground(null);
            }
        } else if (!sticky_3_bool || sticky_s_3.getVisibility() == View.GONE) {
            if (!sticky_3_bool) {
                sticky_s_3 = new ImageView(this);
                rL.addView(sticky_s_3);
                sticky_s_3.setOnTouchListener(this);
                sticky_3_bool = true;
                stick_rl_3.setVisibility(View.VISIBLE);
                sticky_s_3.setImageBitmap(bit);
                img_stick_thumb_3.setImageBitmap(bit);
                s3_tool_thumb.setImageBitmap(bit);
                s3_tool_thumb.setVisibility(View.VISIBLE);
                sticky_3x_bool = true;
                sticky_s_3.bringToFront();
                sticky_s_3.setEnabled(true);

                sticky_s_3.setLayoutParams(params2);
                RectF drawableRect2 = new RectF(0, 0, screen_width, screen_width);
                RectF viewRect2 = new RectF(0, 0, screen_width, screen_width);
                matrix.setRectToRect(drawableRect2, viewRect2, Matrix.ScaleToFit.CENTER);
                sticky_s_3.setScaleType(ImageView.ScaleType.MATRIX);
                sticky_s_3.setImageMatrix(matrix_sticky_3);
                s1_tool_thumb.setBackground(null);
                s2_tool_thumb.setBackground(null);
                s3_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));

            } else if (sticky_s_3.getVisibility() == View.GONE) {
                sticky_s_3.setVisibility(View.VISIBLE);
                stick_rl_3.setVisibility(View.VISIBLE);
                sticky_s_3.setImageBitmap(bit);
                s3_tool_thumb.setImageBitmap(bit);
                img_stick_thumb_3.setImageBitmap(bit);
                sticky_3x_bool = false;
                sticky_s_3.bringToFront();
                sticky_s_3.setEnabled(true);
                s1_tool_thumb.setBackground(null);
                s2_tool_thumb.setBackground(null);
                s3_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View view)
    {
        if (view == s1_tool_thumb)
        {
            sticky_s_1.bringToFront();
            sticky_s_1.setEnabled(true);
            s1_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
            s2_tool_thumb.setBackground(null);
            s3_tool_thumb.setBackground(null);
        }

        if (view == s2_tool_thumb) {
            sticky_s_2.bringToFront();
            sticky_s_2.setEnabled(true);
            s1_tool_thumb.setBackground(null);
            s2_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
            s3_tool_thumb.setBackground(null);
        }

        if (view == s3_tool_thumb) {
            sticky_s_3.bringToFront();
            sticky_s_3.setEnabled(true);
            s1_tool_thumb.setBackground(null);
            s2_tool_thumb.setBackground(null);
            s3_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
        }

        if (view == img_stick_thumb_1)
        {
            sticky_s_1.bringToFront();
            sticky_s_1.setEnabled(true);
            s1_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
            s2_tool_thumb.setBackground(null);
            s3_tool_thumb.setBackground(null);
        }

        if (view == img_stick_thumb_2) {
            sticky_s_2.bringToFront();
            sticky_s_2.setEnabled(true);
            s1_tool_thumb.setBackground(null);
            s2_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
            s3_tool_thumb.setBackground(null);
        }

        if (view == img_stick_thumb_3) {
            sticky_s_3.bringToFront();
            sticky_s_3.setEnabled(true);
            s1_tool_thumb.setBackground(null);
            s2_tool_thumb.setBackground(null);
            s3_tool_thumb.setBackground(getResources().getDrawable(R.drawable.butt_one));
        }

        if (thumb_1_visi_ic == view) {
            sticky_s_1.setVisibility(View.GONE);
            thumb_1_visi_ic.setVisibility(View.GONE);
            thumb_1_Invisi_ic.setVisibility(View.VISIBLE);
        }
        if (thumb_1_Invisi_ic == view) {
            sticky_s_1.setVisibility(View.VISIBLE);
            thumb_1_visi_ic.setVisibility(View.VISIBLE);
            thumb_1_Invisi_ic.setVisibility(View.GONE);
        }
        if (thumb_2_visi_ic == view) {
            sticky_s_2.setVisibility(View.GONE);
            thumb_2_visi_ic.setVisibility(View.GONE);
            thumb_2_Invisi_ic.setVisibility(View.VISIBLE);
        }
        if (thumb_2_Invisi_ic == view) {
            sticky_s_2.setVisibility(View.VISIBLE);
            thumb_2_visi_ic.setVisibility(View.VISIBLE);
            thumb_2_Invisi_ic.setVisibility(View.GONE);
        }

        if (thumb_3_visi_ic == view) {
            sticky_s_3.setVisibility(View.GONE);
            thumb_3_visi_ic.setVisibility(View.GONE);
            thumb_3_Invisi_ic.setVisibility(View.VISIBLE);
        }
        if (thumb_3_Invisi_ic == view) {
            sticky_s_3.setVisibility(View.VISIBLE);
            thumb_3_visi_ic.setVisibility(View.VISIBLE);
            thumb_3_Invisi_ic.setVisibility(View.GONE);
        }

        if (thumb_x_1 == view) {
            stick_rl_1.setVisibility(View.GONE);
            sticky_s_1.setVisibility(View.GONE);
            s1_tool_thumb.setVisibility(View.GONE);
        }
        if (thumb_x_2 == view) {
            stick_rl_2.setVisibility(View.GONE);
            sticky_s_2.setVisibility(View.GONE);
            s2_tool_thumb.setVisibility(View.GONE);
        }
        if (thumb_x_3 == view) {
            stick_rl_3.setVisibility(View.GONE);
            sticky_s_3.setVisibility(View.GONE);
            s3_tool_thumb.setVisibility(View.GONE);
        }


        if (view == emojiButt || view == emojiButt2) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                getEmojiDialog();
            } else {
                Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (view == outputButt) {
            outputDialog();
        }
        if (emoji_txt_Butt == view) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                setEmoji_LL();
            } else {
                Toast.makeText(this, "Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (view == photo_text_Butt) {
            photo_text_Butt_bool = true;

            photo_text_Butt.setTextColor(getColor(color.light_blue));
            sticky_txt_Butt.setTextColor(getColor(color.white));
            emoji_txt_Butt.setTextColor(getColor(color.white));
            photo_tab_ll.setBackgroundResource(color.dark_blue);
            sticky_tab_ll.setBackgroundResource(color.transparent);
            emoji_tab_ll.setBackgroundResource(color.transparent);

            emojiButt2.setVisibility(View.GONE);
            sticky_tool_ll.setVisibility(View.GONE);
            colors_ll.setVisibility(View.GONE);
            photo_edt_tool.setVisibility(View.VISIBLE);
            ll_img_tool.setVisibility(View.VISIBLE);

            bWSeek.setProgress(bw_p_photo_save);
            contrast.setProgress(contrast_p_photo_save);
        }

        if (view == sticky_txt_Butt) {
            photo_text_Butt.setTextColor(getColor(color.white));
            sticky_txt_Butt.setTextColor(getColor(color.light_blue));
            emoji_txt_Butt.setTextColor(getColor(color.white));
            photo_tab_ll.setBackgroundResource(color.transparent);
            sticky_tab_ll.setBackgroundResource(color.dark_blue);
            emoji_tab_ll.setBackgroundResource(color.transparent);

            emojiButt2.setVisibility(View.GONE);
            sticky_tool_ll.setVisibility(View.VISIBLE);
            colors_ll.setVisibility(View.GONE);
            photo_edt_tool.setVisibility(View.GONE);
            ll_img_tool.setVisibility(View.GONE);

        }


        if (view == upload_photo) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }


        if (view == flip_right) {
            if (img_photo.getScaleX() == 1) {
                img_photo.setScaleX(-1);
            } else {
                img_photo.setScaleX(1);
            }
        }

        if (view == flip_down) {
            if (img_photo.getScaleY() == 1) {
                img_photo.setScaleY(-1);
            } else {
                img_photo.setScaleY(1);
            }
        }

        if (view == flip_right_sticky) {
            if (sticky_bool_s1) {
                if (sticky_s_1.getScaleX() == 1) {
                    sticky_s_1.setScaleX(-1);
                } else {
                    sticky_s_1.setScaleX(1);
                }
            }
            if (sticky_bool_s2) {
                if (sticky_s_2.getScaleX() == 1) {
                    sticky_s_2.setScaleX(-1);
                } else {
                    sticky_s_2.setScaleX(1);
                }
            }
            if (sticky_bool_s3) {
                if (sticky_s_3.getScaleX() == 1) {
                    sticky_s_3.setScaleX(-1);
                } else {
                    sticky_s_3.setScaleX(1);
                }
            }
            if (!sticky_1x_bool && !sticky_2x_bool && !sticky_3x_bool) {
                Toast.makeText(this, "No sticky's to Flip", Toast.LENGTH_SHORT).show();
            }
        }

        if (view == flip_down_sticky) {
            if (sticky_bool_s1) {
                if (sticky_s_1.getScaleY() == 1) {
                    sticky_s_1.setScaleY(-1);
                } else {
                    sticky_s_1.setScaleY(1);
                }
            }
            if (sticky_bool_s2) {
                if (sticky_s_2.getScaleY() == 1) {
                    sticky_s_2.setScaleY(-1);
                } else {
                    sticky_s_2.setScaleY(1);
                }
            }
            if (sticky_bool_s3) {
                if (sticky_s_3.getScaleY() == 1) {
                    sticky_s_3.setScaleY(-1);
                } else {
                    sticky_s_3.setScaleY(1);
                }
            }
            if (!sticky_1x_bool && !sticky_2x_bool && !sticky_3x_bool) {
                Toast.makeText(this, "No sticky's to Flip", Toast.LENGTH_SHORT).show();
            }
        }


        if (view == sticky_lockIn_ic) {
            sticky_lockIn_ic.setVisibility(View.GONE);
            sticky_lockOut_ic.setVisibility(View.VISIBLE);

            if (sticky_1_bool) {
                sticky_s_1.setEnabled(true);
            }
            if (sticky_2_bool) {
                sticky_s_2.setEnabled(true);
            }
            if (sticky_3_bool) {
                sticky_s_3.setEnabled(true);
            }
            if (!sticky_1x_bool && !sticky_2x_bool && !sticky_3x_bool) {
                Toast.makeText(this, "No sticky's to Lock", Toast.LENGTH_SHORT).show();
            }
        }
        if (view == sticky_lockOut_ic) {
            sticky_lockOut_ic.setVisibility(View.GONE);
            sticky_lockIn_ic.setVisibility(View.VISIBLE);

            if (sticky_1_bool) {
                sticky_s_1.setEnabled(false);
            }
            if (sticky_2_bool) {
                sticky_s_2.setEnabled(false);
            }
            if (sticky_3_bool) {
                sticky_s_3.setEnabled(false);
            }

            if (!sticky_1x_bool && !sticky_2x_bool && !sticky_3x_bool) {
                Toast.makeText(this, "No sticky's to UnLock", Toast.LENGTH_SHORT).show();

            }
        }


        if (view == sticky_visi_ic) {
            sticky_visi_ic.setVisibility(View.GONE);
            sticky_Invisi_ic.setVisibility(View.VISIBLE);

            if (sticky_1_bool) {
                sticky_s_1.setVisibility(View.GONE);
            }
            if (sticky_2_bool) {
                sticky_s_2.setVisibility(View.GONE);
            }
            if (sticky_3_bool) {
                sticky_s_3.setVisibility(View.GONE);
            }
            if (!sticky_1x_bool && !sticky_2x_bool && !sticky_3x_bool) {
                Toast.makeText(this, "No sticky's to Hide", Toast.LENGTH_SHORT).show();
            }

        }
        if (view == sticky_Invisi_ic) {
            sticky_visi_ic.setVisibility(View.VISIBLE);
            sticky_Invisi_ic.setVisibility(View.GONE);
            if (sticky_1_bool) {
                sticky_s_1.setVisibility(View.VISIBLE);
            }
            if (sticky_2_bool) {
                sticky_s_2.setVisibility(View.VISIBLE);
            }
            if (sticky_3_bool) {
                sticky_s_3.setVisibility(View.VISIBLE);
            }
            if (!sticky_1x_bool && !sticky_2x_bool && !sticky_3x_bool) {
                Toast.makeText(this, "No sticky's to Show", Toast.LENGTH_SHORT).show();
            }
        }

        if (view == emoji_lockIn_ic) {
            emoji_lockIn_ic.setVisibility(View.GONE);
            emoji_lockOut_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool) {
                emoji_s_1.setEnabled(true);
            }
            if (emoji_2_bool) {
                emoji_s_2.setEnabled(true);
            }
            if (emoji_3_bool) {
                emoji_s_3.setEnabled(true);
            }
            if (!emoji_1x_bool && !emoji_2x_bool && !emoji_3x_bool) {
                Toast.makeText(this, "No emoji's to Lock", Toast.LENGTH_SHORT).show();
            }
        }
        if (view == emoji_lockOut_ic) {
            emoji_lockOut_ic.setVisibility(View.GONE);
            emoji_lockIn_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool) {
                emoji_s_1.setEnabled(false);
            }
            if (emoji_2_bool) {
                emoji_s_2.setEnabled(false);
            }
            if (emoji_3_bool) {
                emoji_s_3.setEnabled(false);
            }

            if (!emoji_1x_bool && !emoji_2x_bool && !emoji_3x_bool) {
                Toast.makeText(this, "No emoji's to UnLock", Toast.LENGTH_SHORT).show();

            }
        }

        if (view == emoji_visi_ic) {
            emoji_visi_ic.setVisibility(View.GONE);
            emoji_Invisi_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool) {
                emoji_s_1.setVisibility(View.GONE);
            }
            if (emoji_2_bool) {
                emoji_s_2.setVisibility(View.GONE);
            }
            if (emoji_3_bool) {
                emoji_s_3.setVisibility(View.GONE);
            }
            if (!emoji_1x_bool && !emoji_2x_bool && !emoji_3x_bool) {
                Toast.makeText(this, "No emoji's to Hide", Toast.LENGTH_SHORT).show();
            }

        }
        if (view == emoji_Invisi_ic) {
            emoji_visi_ic.setVisibility(View.VISIBLE);
            emoji_Invisi_ic.setVisibility(View.GONE);
            if (emoji_1_bool) {
                emoji_s_1.setVisibility(View.VISIBLE);
            }
            if (emoji_2_bool) {
                emoji_s_2.setVisibility(View.VISIBLE);
            }
            if (emoji_3_bool) {
                emoji_s_3.setVisibility(View.VISIBLE);
            }
            if (!emoji_1x_bool && !emoji_2x_bool && !emoji_3x_bool) {
                Toast.makeText(this, "No emoji's to Show", Toast.LENGTH_SHORT).show();
            }
        }


        if (view == red_c) {
            c1.setBackgroundResource(color.red_p1);
            c2.setBackgroundResource(color.red_p2);
            c3.setBackgroundResource(color.red_p3);
            c4.setBackgroundResource(color.red_p4);
            c5.setBackgroundResource(color.red_p5);
            c6.setBackgroundResource(color.red_p6);
            c7.setBackgroundResource(color.red_p7);
            c8.setBackgroundResource(color.red_p8);
            c9.setBackgroundResource(color.red_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == purple_c) {
            c1.setBackgroundResource(color.purple_p1);
            c2.setBackgroundResource(color.purple_p2);
            c3.setBackgroundResource(color.purple_p3);
            c4.setBackgroundResource(color.purple_p4);
            c5.setBackgroundResource(color.purple_p5);
            c6.setBackgroundResource(color.purple_p6);
            c7.setBackgroundResource(color.purple_p7);
            c8.setBackgroundResource(color.purple_p8);
            c9.setBackgroundResource(color.purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == pink_c) {
            c1.setBackgroundResource(color.pink_p1);
            c2.setBackgroundResource(color.pink_p2);
            c3.setBackgroundResource(color.pink_p3);
            c4.setBackgroundResource(color.pink_p4);
            c5.setBackgroundResource(color.pink_p5);
            c6.setBackgroundResource(color.pink_p6);
            c7.setBackgroundResource(color.pink_p7);
            c8.setBackgroundResource(color.pink_p8);
            c9.setBackgroundResource(color.pink_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == deepPurple_c) {
            c1.setBackgroundResource(color.deep_purple_p1);
            c2.setBackgroundResource(color.deep_purple_p2);
            c3.setBackgroundResource(color.deep_purple_p3);
            c4.setBackgroundResource(color.deep_purple_p4);
            c5.setBackgroundResource(color.deep_purple_p5);
            c6.setBackgroundResource(color.deep_purple_p6);
            c7.setBackgroundResource(color.deep_purple_p7);
            c8.setBackgroundResource(color.deep_purple_p8);
            c9.setBackgroundResource(color.deep_purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == indigo_c) {
            c1.setBackgroundResource(color.indigo_p1);
            c2.setBackgroundResource(color.indigo_p2);
            c3.setBackgroundResource(color.indigo_p3);
            c4.setBackgroundResource(color.indigo_p4);
            c5.setBackgroundResource(color.indigo_p5);
            c6.setBackgroundResource(color.indigo_p6);
            c7.setBackgroundResource(color.indigo_p7);
            c8.setBackgroundResource(color.indigo_p8);
            c9.setBackgroundResource(color.indigo_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == blue_c) {
            c1.setBackgroundResource(color.blue_p1);
            c2.setBackgroundResource(color.blue_p2);
            c3.setBackgroundResource(color.blue_p3);
            c4.setBackgroundResource(color.blue_p4);
            c5.setBackgroundResource(color.blue_p5);
            c6.setBackgroundResource(color.blue_p6);
            c7.setBackgroundResource(color.blue_p7);
            c8.setBackgroundResource(color.blue_p8);
            c9.setBackgroundResource(color.blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == cyan_c) {
            c1.setBackgroundResource(color.cyan_p1);
            c2.setBackgroundResource(color.cyan_p2);
            c3.setBackgroundResource(color.cyan_p3);
            c4.setBackgroundResource(color.cyan_p4);
            c5.setBackgroundResource(color.cyan_p5);
            c6.setBackgroundResource(color.cyan_p6);
            c7.setBackgroundResource(color.cyan_p7);
            c8.setBackgroundResource(color.cyan_p8);
            c9.setBackgroundResource(color.cyan_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lightBlue_c) {
            c1.setBackgroundResource(color.light_blue_p1);
            c2.setBackgroundResource(color.light_blue_p2);
            c3.setBackgroundResource(color.light_blue_p3);
            c4.setBackgroundResource(color.light_blue_p4);
            c5.setBackgroundResource(color.light_blue_p5);
            c6.setBackgroundResource(color.light_blue_p6);
            c7.setBackgroundResource(color.light_blue_p7);
            c8.setBackgroundResource(color.light_blue_p8);
            c9.setBackgroundResource(color.light_blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == teal_c) {
            c1.setBackgroundResource(color.teal_p1);
            c2.setBackgroundResource(color.teal_p2);
            c3.setBackgroundResource(color.teal_p3);
            c4.setBackgroundResource(color.teal_p4);
            c5.setBackgroundResource(color.teal_p5);
            c6.setBackgroundResource(color.teal_p6);
            c7.setBackgroundResource(color.teal_p7);
            c8.setBackgroundResource(color.teal_p8);
            c9.setBackgroundResource(color.teal_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == green_c) {
            c1.setBackgroundResource(color.green_p1);
            c2.setBackgroundResource(color.green_p2);
            c3.setBackgroundResource(color.green_p3);
            c4.setBackgroundResource(color.green_p4);
            c5.setBackgroundResource(color.green_p5);
            c6.setBackgroundResource(color.green_p6);
            c7.setBackgroundResource(color.green_p7);
            c8.setBackgroundResource(color.green_p8);
            c9.setBackgroundResource(color.green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lightGreen_c) {
            c1.setBackgroundResource(color.light_green_p1);
            c2.setBackgroundResource(color.light_green_p2);
            c3.setBackgroundResource(color.light_green_p3);
            c4.setBackgroundResource(color.light_green_p4);
            c5.setBackgroundResource(color.light_green_p5);
            c6.setBackgroundResource(color.light_green_p6);
            c7.setBackgroundResource(color.light_green_p7);
            c8.setBackgroundResource(color.light_green_p8);
            c9.setBackgroundResource(color.light_green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lime_c) {
            c1.setBackgroundResource(color.lime_p1);
            c2.setBackgroundResource(color.lime_p2);
            c3.setBackgroundResource(color.lime_p3);
            c4.setBackgroundResource(color.lime_p4);
            c5.setBackgroundResource(color.lime_p5);
            c6.setBackgroundResource(color.lime_p6);
            c7.setBackgroundResource(color.lime_p7);
            c8.setBackgroundResource(color.lime_p8);
            c9.setBackgroundResource(color.lime_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == yellow_c) {
            c1.setBackgroundResource(color.yellow_p1);
            c2.setBackgroundResource(color.yellow_p2);
            c3.setBackgroundResource(color.yellow_p3);
            c4.setBackgroundResource(color.yellow_p4);
            c5.setBackgroundResource(color.yellow_p5);
            c6.setBackgroundResource(color.yellow_p6);
            c7.setBackgroundResource(color.yellow_p7);
            c8.setBackgroundResource(color.yellow_p8);
            c9.setBackgroundResource(color.yellow_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == amber_c) {
            c1.setBackgroundResource(color.amber_p1);
            c2.setBackgroundResource(color.amber_p2);
            c3.setBackgroundResource(color.amber_p3);
            c4.setBackgroundResource(color.amber_p4);
            c5.setBackgroundResource(color.amber_p5);
            c6.setBackgroundResource(color.amber_p6);
            c7.setBackgroundResource(color.amber_p7);
            c8.setBackgroundResource(color.amber_p8);
            c9.setBackgroundResource(color.amber_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == orange_c) {
            c1.setBackgroundResource(color.orange_p1);
            c2.setBackgroundResource(color.orange_p2);
            c3.setBackgroundResource(color.orange_p3);
            c4.setBackgroundResource(color.orange_p4);
            c5.setBackgroundResource(color.orange_p5);
            c6.setBackgroundResource(color.orange_p6);
            c7.setBackgroundResource(color.orange_p7);
            c8.setBackgroundResource(color.orange_p8);
            c9.setBackgroundResource(color.orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == deepOrange_c) {
            c1.setBackgroundResource(color.dep_orange_p1);
            c2.setBackgroundResource(color.dep_orange_p2);
            c3.setBackgroundResource(color.dep_orange_p3);
            c4.setBackgroundResource(color.dep_orange_p4);
            c5.setBackgroundResource(color.dep_orange_p5);
            c6.setBackgroundResource(color.dep_orange_p6);
            c7.setBackgroundResource(color.dep_orange_p7);
            c8.setBackgroundResource(color.dep_orange_p8);
            c9.setBackgroundResource(color.dep_orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == brown_c) {
            c1.setBackgroundResource(color.brown_p1);
            c2.setBackgroundResource(color.brown_p2);
            c3.setBackgroundResource(color.brown_p3);
            c4.setBackgroundResource(color.brown_p4);
            c5.setBackgroundResource(color.brown_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }
        if (view == grey_c) {
            c1.setBackgroundResource(color.grey_p1);
            c2.setBackgroundResource(color.grey_p2);
            c3.setBackgroundResource(color.grey_p3);
            c4.setBackgroundResource(color.grey_p4);
            c5.setBackgroundResource(color.grey_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }
        if (view == blueGrey_c) {
            c1.setBackgroundResource(color.blue_grey_p1);
            c2.setBackgroundResource(color.blue_grey_p2);
            c3.setBackgroundResource(color.blue_grey_p3);
            c4.setBackgroundResource(color.blue_grey_p4);
            c5.setBackgroundResource(color.blue_grey_p4);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }

        if (view == black_c) {
            ll1.setVisibility(View.GONE);
            ColorDrawable cd = (ColorDrawable) black_c.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }
        if (view == white_c) {
            ll1.setVisibility(View.GONE);
            ColorDrawable cd = (ColorDrawable) white_c.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }


        if (view == c1) {
            ColorDrawable cd = (ColorDrawable) c1.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }


        if (view == c2) {
            ColorDrawable cd = (ColorDrawable) c2.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }


        if (view == c3) {
            ColorDrawable cd = (ColorDrawable) c3.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }


        if (view == c4) {
            ColorDrawable cd = (ColorDrawable) c4.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }


        if (view == c5) {
            ColorDrawable cd = (ColorDrawable) c5.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }


        if (view == c6) {
            ColorDrawable cd = (ColorDrawable) c6.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }


        if (view == c7) {
            ColorDrawable cd = (ColorDrawable) c7.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }


        if (view == c8) {
            ColorDrawable cd = (ColorDrawable) c8.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }


        if (view == c9) {
            ColorDrawable cd = (ColorDrawable) c9.getBackground();
            getViewColo = cd.getColor();

            getLogoColor();
        }

    }


    private void setEmoji_LL() {

        emoji_tab_ll.setBackgroundResource(color.dark_blue);
        sticky_tab_ll.setBackgroundResource(color.transparent);
        photo_tab_ll.setBackgroundResource(color.transparent);


        photo_text_Butt.setTextColor(getResources().getColor(color.white));
        emoji_txt_Butt.setTextColor(getResources().getColor(color.light_blue));
        sticky_txt_Butt.setTextColor(getResources().getColor(color.white));

        ll_img_tool.setVisibility(View.GONE);
        photo_edt_tool.setVisibility(View.GONE);
        sticky_tool_ll.setVisibility(View.GONE);
        emojiButt2.setVisibility(View.VISIBLE);
        colors_ll.setVisibility(View.VISIBLE);
        emoji_bool = true;
    }


    private void outputDialog() {
        final ImageView outPut_img;
        final TextView save_to_gallery_butt, share_to_fb, share_to_messenger, share_to_whatsapp, share_to_Instagram;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout.activity_edit_output);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        outPut_img = (ImageView) dialog.findViewById(id.outPut_img);
        save_to_gallery_butt = (TextView) dialog.findViewById(id.save_to_gallery_butt);
        share_to_whatsapp = (TextView) dialog.findViewById(id.share_to_whatsapp);

        final Bitmap bitmapOP = getBitmap(rL);
        outPut_img.setImageBitmap(bitmapOP);

        save_to_gallery_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(StickiesEdit.this, "Saved To Gallery", Toast.LENGTH_LONG).show();
                saveChart(bitmapOP, rL.getMeasuredHeight(), rL.getMeasuredWidth());

                dialog.cancel();
            }

        });

        share_to_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Picxture");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "PX Testing..");
//                sharingIntent.putExtra(Intent.EXTRA_STREAM, bitmapOP);
//                sharingIntent.setType("image/jpeg");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                dialog.cancel();
            }

        });


        dialog.show();
    }

    public Bitmap getBitmap(RelativeLayout layout) {
        layout.setDrawingCacheEnabled(true);
        layout.buildDrawingCache();
        Bitmap bmp = Bitmap.createBitmap(layout.getDrawingCache());
        layout.setDrawingCacheEnabled(false);
        return bmp;
    }

    public void saveChart(Bitmap getbitmap, float height, float width) {
        File folder = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString(), "Picxture");
        //   .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"myfolder");
        boolean success = false;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        Log.d("gggggg", folder.getPath());
        File file = new File(folder.getPath() + File.separator + "/" + timeStamp + ".png");
        if (!file.exists()) {
            try {
                success = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileOutputStream ostream = null;
        try {
            ostream = new FileOutputStream(file);
            System.out.println(ostream);
            Bitmap well = getbitmap;
            Bitmap save = Bitmap.createBitmap((int) width, (int) height, Bitmap.Config.ARGB_8888);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            Canvas now = new Canvas(save);
            now.drawRect(new Rect(0, 0, (int) width, (int) height), paint);
            now.drawBitmap(well,
                    new Rect(0, 0, well.getWidth(), well.getHeight()),
                    new Rect(0, 0, (int) width, (int) height), null);
            if (save == null) {
                System.out.println("NULL");
            }
            save.compress(Bitmap.CompressFormat.PNG, 100, ostream);
            ContentValues values = new ContentValues();
            //   values.put(Images.Media.TITLE, this.getString(R.string.picture_title));
            //   values.put(Images.Media.DESCRIPTION, this.getString(R.string.picture_description));
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.ImageColumns.BUCKET_ID, file.toString().toLowerCase(Locale.US).hashCode());
            values.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, file.getName().toLowerCase(Locale.US));
            values.put("_data", file.getAbsolutePath());

            ContentResolver cr = getContentResolver();
            cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        } catch (NullPointerException e) {
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(), &quot;Null error&quot;, Toast.LENGTH_SHORT).show();<br />
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // Toast.makeText(getApplicationContext(), &quot;File error&quot;, Toast.LENGTH_SHORT).show();<br />
        }
//        catch (IOException e){
//            e.printStackTrace();
//            // Toast.makeText(getApplicationContext(), &quot;IO error&quot;, Toast.LENGTH_SHORT).show();<br />
//        }
    }


}
