package com.company.px.activites;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.R;
import com.company.px.model.CategoriesModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MyProfile extends AppCompatActivity implements View.OnClickListener
{
    private boolean checkInternet;
    private String id, cu_Id_S, f_name_S, l_name_S, gender_S, email_S, countryCode_S, mobile_S, refCod_S, coins_S;
    TextView editButt, updateButt, referalCode, coinsCounter, userNam_tab;
    TextInputEditText email, fname, lname, countryCode, mobileNumber, gender;
    ImageView back_butt;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        id = "1";

        back_butt = (ImageView) findViewById(R.id.back_butt);
        back_butt.setOnClickListener(this);
        fname = (TextInputEditText) findViewById(R.id.fname);
        lname = (TextInputEditText) findViewById(R.id.lname);
        countryCode = (TextInputEditText) findViewById(R.id.countryCode);
        mobileNumber = (TextInputEditText) findViewById(R.id.mobileNumber);
        email = (TextInputEditText) findViewById(R.id.email);
        gender = (TextInputEditText) findViewById(R.id.gender);
        editButt = (TextView) findViewById(R.id.editButt);
        editButt.setOnClickListener(this);
        referalCode = (TextView) findViewById(R.id.referalCode);
        referalCode.setOnClickListener(this);
        userNam_tab = (TextView) findViewById(R.id.userNam_tab);
        coinsCounter = (TextView) findViewById(R.id.coinsCounter);
        updateButt = (TextView) findViewById(R.id.updateButt);
        updateButt.setOnClickListener(this);
        fname.setEnabled(false);
        lname.setEnabled(false);
        mobileNumber.setEnabled(false);
        email.setEnabled(false);
        countryCode.setEnabled(false);
        gender.setEnabled(false);


        getProfile();
    }



    private void getProfile()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet)
        {
            Log.d("Profile_URL", AppUrls.BASE_URL + AppUrls.PROFILE + id);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.PROFILE + id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Profile_RESP",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if(responceCode.equals("10100"))
                                {
                                    JSONObject jObjData = jsonObject.getJSONObject("data");

                                    cu_Id_S = jObjData.getString("id");
                                    f_name_S = jObjData.getString("fname");
                                    Log.d("Profile_RESP",f_name_S);
                                    fname.setText(f_name_S);
                                    userNam_tab.setText("Hello"+" "+f_name_S);
                                    l_name_S = jObjData.getString("lname");
                                    lname.setText(l_name_S);
                                    gender_S = jObjData.getString("gender");
                                    gender.setText(gender_S);
                                    email_S = jObjData.getString("email");
                                    email.setText(email_S);
                                    countryCode_S = jObjData.getString("country_code");
                                    countryCode.setText(countryCode_S);
                                    mobile_S = jObjData.getString("mobile");
                                    mobileNumber.setText(mobile_S);
                                    refCod_S = jObjData.getString("refercode");
                                    referalCode.setText(refCod_S);
                                    coins_S = jObjData.getString("coins");
                                    coinsCounter.setText(coins_S+"  "+"coins");
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }else {
            Toast.makeText(MyProfile.this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }



    @Override
    public void onClick(View view) {
        if(view==editButt)
        {
            editButt.setVisibility(View.GONE);
            updateButt.setVisibility(View.VISIBLE);
            fname.setEnabled(true);
            lname.setEnabled(true);
            mobileNumber.setEnabled(true);
            email.setEnabled(true);
            countryCode.setEnabled(true);
            gender.setEnabled(true);
        }
        if(view==updateButt)
        {
            editButt.setVisibility(View.VISIBLE);
            updateButt.setVisibility(View.GONE);
            fname.setEnabled(false);
            lname.setEnabled(false);
            mobileNumber.setEnabled(false);
            email.setEnabled(false);
            countryCode.setEnabled(false);
            gender.setEnabled(false);
        }
        if(view==referalCode)
        {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Picxture");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Referral Code : " + refCod_S + "\n" + "picxture.com");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
        if(view==back_butt)
        {
            finish();
        }

    }
}
