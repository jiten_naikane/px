package com.company.px.activites.egraphics;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.company.px.MainActivity;
import com.company.px.R;
import com.company.px.activites.PayConfirmActivity;
import com.company.px.utility.NetworkChecking;
import com.company.px.utility.PayPalConfig;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

public class StepThree_OrderSummary extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    String paymentAmount = "100", O_D_ID;
    LinearLayout pay_btn;
    public static final int PAYPAL_REQUEST_CODE = 123;
    ImageView back_butt, nxt;
    TextView orderId_T, catId_T;


    //Paypal Configuration Object
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PayPalConfig.PAYPAL_CLIENT_ID);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_summary_step_three);

        O_D_ID = getIntent().getExtras().getString("ORDER_ID");

        orderId_T = (TextView) findViewById(R.id.orderId_T);
        catId_T = (TextView) findViewById(R.id.catId_T);

        pay_btn = (LinearLayout) findViewById(R.id.pay_btn);
        pay_btn.setOnClickListener(this);
        nxt = (ImageView) findViewById(R.id.nxt);
        nxt.setColorFilter(Color.WHITE);
        back_butt = (ImageView) findViewById(R.id.back_butt);
        back_butt.setOnClickListener(this);




        orderId_T.setText(O_D_ID);


        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == PAYPAL_REQUEST_CODE)
        {
            if (resultCode == Activity.RESULT_OK)
            {

                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if (confirm != null) {
                    try {
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        Intent intent = new Intent(this, PayConfirmActivity.class);
                        intent.putExtra("PaymentDetails", paymentDetails);
//                        intent.putExtra("Order_id", String.valueOf(order_id));
                        intent.putExtra("Order_id", O_D_ID);
                        Log.i("OD_QQ",O_D_ID);

                        intent.putExtra("PaymentAmount", paymentAmount);
                        startActivity(intent);

                        Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }



    @Override
    public void onClick(View view) {
        if (view == pay_btn){
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet)
            {
                getPayment();
            }
            else
                {
                Toast.makeText(StepThree_OrderSummary.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }

        if (view==back_butt)
        {
            finish();
        }
    }

    private void getPayment() {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(paymentAmount)), "USD", "Amount",
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    @Override
    public void onBackPressed() {
        onPressingBack();
    }

    private void onPressingBack() {

        final Intent intent;

        intent = new Intent(StepThree_OrderSummary.this, MainActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(StepThree_OrderSummary.this);

        alertDialog.setTitle("Warning");

        alertDialog.setMessage("Do you cancel this transaction?");

        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

}
