package com.company.px.activites.selfie.all.dubf;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.MaskFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.R;
import com.company.px.activites.selfie.all.stickies.StickerImageView;
import com.company.px.adapter.DubfaceFestivalTitleAdapter;
import com.company.px.adapter.DubfaceImagesHoriAdapter;
import com.company.px.model.FestivalTitlesModel;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DubFaceEdit extends Activity implements View.OnClickListener, View.OnTouchListener {

    ImageView img_theme, upload_logo, outputButt;
    ImageButton flip_down, flip_right;
    RelativeLayout dialog_rL;
    LinearLayout ll1, ll2;
    View black_c, white_c, red_c, purple_c,pink_c, deepPurple_c, indigo_c, blue_c, cyan_c, lightBlue_c, teal_c, green_c, lightGreen_c, lime_c, yellow_c, amber_c, orange_c, deepOrange_c, brown_c, grey_c, blueGrey_c, c1, c2, c3, c4, c5, c6, c7, c8, c9;
    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_LOAD_IMG3 = 3;
    String imgDecodableString;
    private boolean imgBg, exit=false;
    Uri selectedURI1 ;
    String selected_cc_ImagePath1="" ;
    Intent CropIntent;
    Dialog dialog;
    int ersaerSize=30, screen_width, screen_height;
    int blur_value=1;


    Bitmap bp;
    Canvas bitmapCanvas;
    DrawView drawImg;
    Bitmap bitmapp;



    // these matrices will be used to move and zoom image
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;
    RelativeLayout rL;

    private boolean checkInternet;
    RecyclerView fest_title_recycler;
    LinearLayoutManager linearLayoutManager;
    DubfaceFestivalTitleAdapter dubfaceFestivalTitleAdapter;
    ArrayList<FestivalTitlesModel> festTitleModel = new ArrayList<FestivalTitlesModel>();

    RecyclerView imagesHoriRecycler;
    LinearLayoutManager linearLayoutManager2;
    DubfaceImagesHoriAdapter dubfaceImagesHoriAdapter;
    ArrayList<ImagesHoriModel> imagesHoriModel = new ArrayList<ImagesHoriModel>();

    StickerImageView user_img1;
    String EDIT_CAT_ID;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_dubface);

        rL = (RelativeLayout)findViewById(R.id.rL);

        outputButt = (ImageView) findViewById(R.id.outputButt);
        outputButt.setOnClickListener(this);

        Bundle bundle=getIntent().getExtras();
        EDIT_CAT_ID = bundle.getString("EDIT_CAT_ID");


        upload_logo =(ImageView) findViewById(R.id.upload_logo);
        upload_logo.setOnClickListener(this);

        img_theme = (ImageView) findViewById(R.id.img_theme);
        img_theme.setOnTouchListener(this);

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        screen_width = displayMetrics.widthPixels;
        screen_height = displayMetrics.heightPixels;
        RelativeLayout.LayoutParams paramsRL = new RelativeLayout.LayoutParams(screen_width, screen_width);
        rL.setLayoutParams(paramsRL);
        img_theme.setLayoutParams(paramsRL);
        RectF drawableRect = new RectF(0, 0, img_theme.getDrawable().getIntrinsicWidth(), img_theme.getDrawable().getIntrinsicHeight());
        RectF viewRect = new RectF(0, 0,  screen_width, screen_width);
        matrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.START);
        img_theme.setScaleType(ImageView.ScaleType.MATRIX);
        img_theme.setImageMatrix(matrix);


        fest_title_recycler=(RecyclerView)findViewById(R.id.fest_title_recycler) ;
        dubfaceFestivalTitleAdapter = new DubfaceFestivalTitleAdapter(festTitleModel, DubFaceEdit.this, R.layout.row_festival_titles);
        linearLayoutManager =  new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        fest_title_recycler.setLayoutManager(linearLayoutManager);

        imagesHoriRecycler=(RecyclerView)findViewById(R.id.imagesHoriRecycler) ;
        dubfaceImagesHoriAdapter = new DubfaceImagesHoriAdapter(imagesHoriModel, DubFaceEdit.this, R.layout.row_images_hori);
        linearLayoutManager2 =  new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        imagesHoriRecycler.setLayoutManager(linearLayoutManager2);


        getFestivalTitles();

    }



    private void getFestivalTitles()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet)
        {
            Log.d("Dub_URL", AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/" + EDIT_CAT_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/" + EDIT_CAT_ID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Dub_RESP",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if(responceCode.equals("10100"))
                                {
                                    JSONArray jarray=jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++)
                                    {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        FestivalTitlesModel fList = new FestivalTitlesModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setCreated_on(jsonObject1.getString("created_on"));
                                        fList.setColor_id(jsonObject1.getString("color_id"));

                                        festTitleModel.add(fList);
                                    }
                                    fest_title_recycler.setAdapter(dubfaceFestivalTitleAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void getImagesHori(String festTitleId)
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet)
        {
            imagesHoriModel.clear();
            Log.d("TIH_URL", AppUrls.BASE_URL + "images/festival/" + festTitleId + "/"+ EDIT_CAT_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "images/festival/" + festTitleId+ "/"+ EDIT_CAT_ID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("TIH_RESP",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if(responceCode.equals("10100"))
                                {
                                    JSONArray jarray=jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++)
                                    {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        ImagesHoriModel ihList = new ImagesHoriModel();
                                        ihList.setFestival_id(jsonObject1.getString("festival_id"));
                                        ihList.setImg1(jsonObject1.getString("img1"));
                                        ihList.setImg2(jsonObject1.getString("img2"));
                                        ihList.setImg3(jsonObject1.getString("img3"));

                                        imagesHoriModel.add(ihList);
                                    }
                                    imagesHoriRecycler.setAdapter(dubfaceImagesHoriAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }



    //UPLOAD IMAGE
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        // UPLOAD 1
        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
            // Get the Image from data
            selectedURI1 = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            // Get the cursor
            Cursor cursor = getContentResolver().query(selectedURI1, filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgDecodableString = cursor.getString(columnIndex);
            cursor.close();
            // Set the Image in ImageView after decoding the String
//            img_user_1.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

            Log.d("img1_SELEC_URI", selectedURI1.toString());
            Log.d("img1_Decode", imgDecodableString.toString());

            try {
                CropIntent = new Intent("com.android.camera.action.CROP");
                CropIntent.setDataAndType(selectedURI1, "image/*");
                CropIntent.putExtra("crop", "true");
                CropIntent.putExtra("outputX", 300);
                CropIntent.putExtra("outputY", 300);
                CropIntent.putExtra("aspectX", 10);
                CropIntent.putExtra("aspectY", 10);
                CropIntent.putExtra("scaleUpIfNeeded", true);
                CropIntent.putExtra("return-data", true);
                startActivityForResult(CropIntent, RESULT_LOAD_IMG3);
            } catch (ActivityNotFoundException e) {

            }

        }

       /*AFTER CROP*/
        else if (requestCode == RESULT_LOAD_IMG3) {
            Bundle bundle = data.getExtras();

            selected_cc_ImagePath1 = selectedURI1.getPath();

            Bitmap cropbitmap = bundle.getParcelable("data");
            selected_cc_ImagePath1 = String.valueOf(System.currentTimeMillis()) + ".jpg";
            File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath1);

            try {
                file.createNewFile();
                FileOutputStream fos = new FileOutputStream(file);
                cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
            } catch (IOException e) {
                Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
            }
            selected_cc_ImagePath1 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath1;

            getDialo();
        }


    }






    public boolean onTouch(View view, MotionEvent event)
    {

    if (view==img_theme)
//    if (view.equals(bitmapCanvas))
    {
        imgBg = true;

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                mode = DRAG;
                lastEvent = null;
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
                if (oldDist > 10f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                }
                lastEvent = new float[4];
                lastEvent[0] = event.getX(0);
                lastEvent[1] = event.getX(1);
                lastEvent[2] = event.getY(0);
                lastEvent[3] = event.getY(1);
                d = rotation(event);
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                lastEvent = null;
                break;
            case MotionEvent.ACTION_MOVE:
                if (mode == DRAG) {
                    matrix.set(savedMatrix);
                    float dx = event.getX() - start.x;
                    float dy = event.getY() - start.y;
                    matrix.postTranslate(dx, dy);
                } else if (mode == ZOOM) {
                    float newDist = spacing(event);
                    if (newDist > 10f) {
                        matrix.set(savedMatrix);
                        float scale = (newDist / oldDist);
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                    if (lastEvent != null && event.getPointerCount() == 2) {
                        newRot = rotation(event);
                        float r = newRot - d;
                        float[] values = new float[9];
                        matrix.getValues(values);
                        float tx = values[2];
                        float ty = values[5];
                        float sx = values[0];
                        float xc = (img_theme.getWidth() / 2) * sx;
                        float yc = (img_theme.getHeight() / 2) * sx;
                        matrix.postRotate(r, tx + xc, ty + yc);
                    }
                }
                break;
        }

        img_theme.setImageMatrix(matrix);
    }



    return true;
}



    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);

    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }

    @Override
    public void onBackPressed()
    {
        if (exit)
        {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Unsaved work, Tap back again to exit", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
    private  void getDialo()
    {
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dub_dialog_erase);
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        dialog_rL = (RelativeLayout) dialog.findViewById(R.id.dialog_rL);
        Button nextButt_dialog = (Button) dialog.findViewById(R.id.nextButt_dialog);
        SeekBar eraser_seek = (SeekBar) dialog.findViewById(R.id.eraser_seek);
        SeekBar blur_seek = (SeekBar) dialog.findViewById(R.id.blur_seek);
        Button redo_butt = (Button) dialog.findViewById(R.id.redo_butt);
        Button undo_butt = (Button) dialog.findViewById(R.id.undo_butt);
        RelativeLayout.LayoutParams paramsRL = new RelativeLayout.LayoutParams(screen_width, screen_width);
        dialog_rL.setLayoutParams(paramsRL);


        drawImg = new DrawView(this, selected_cc_ImagePath1);
        dialog_rL.addView(drawImg);

        eraser_seek.setProgress(30);
        eraser_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                ersaerSize = progress;
                drawImg.invalidate();
            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        blur_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                blur_value= progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        nextButt_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                user_img1 = new StickerImageView(DubFaceEdit.this);
                user_img1.setImageBitmap(bitmapp);

                rL.addView(user_img1);

                dialog.cancel();
            }
        });

        redo_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                drawImg.onClickRedo ();
            }
        });

        undo_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Log.d("kkk","kkkkk");
                drawImg.onClickUndo ();
            }
        });


        dialog.show();
    }


    private void outputDialog()
    {
        final ImageView outPut_img;
        final TextView save_to_gallery_butt, share_to_fb, share_to_messenger, share_to_whatsapp, share_to_Instagram;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_edit_output);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        outPut_img = (ImageView)  dialog.findViewById(R.id.outPut_img);
        save_to_gallery_butt = (TextView)  dialog.findViewById(R.id.save_to_gallery_butt);
        share_to_whatsapp = (TextView)  dialog.findViewById(R.id.share_to_whatsapp);

        final Bitmap bitmapOP = getBitmap(rL);
        outPut_img.setImageBitmap(bitmapOP);

        save_to_gallery_butt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Toast.makeText(DubFaceEdit.this,"Saved To Gallery",Toast.LENGTH_LONG).show();
                saveChart(bitmapOP, rL.getMeasuredHeight(), rL.getMeasuredWidth());

                dialog.cancel();
            }

        });

        share_to_whatsapp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Picxture");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "PX Testing..");
//                sharingIntent.putExtra(Intent.EXTRA_STREAM, bitmapOP);
//                sharingIntent.setType("image/jpeg");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                dialog.cancel();
            }

        });





        dialog.show();
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View view)
    {

        if (view==outputButt){
            outputDialog();
        }

        if (view==upload_logo) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }

        if (view==flip_right)
        {
            if(imgBg){
                if (img_theme.getScaleX()==1) {
                    img_theme.setScaleX(-1);
                }else {
                    img_theme.setScaleX(1);
                }
            }
        }

        if (view==flip_down)
        {
            if(imgBg){
                if (img_theme.getScaleY()==1) {
                    img_theme.setScaleY(-1);
                }else {
                    img_theme.setScaleY(1);
                }
            }
        }


            if (view == black_c){
                ll1.setVisibility(View.GONE);
                ColorDrawable cd = (ColorDrawable) black_c.getBackground();
                int colorId = cd.getColor();
            }
            if (view == white_c){
                ll1.setVisibility(View.GONE);
                ColorDrawable cd = (ColorDrawable) white_c.getBackground();
                int colorId = cd.getColor();
            }
            if (view == red_c){
                c1.setBackgroundResource(R.color.red_p1);
                c2.setBackgroundResource(R.color.red_p2);
                c3.setBackgroundResource(R.color.red_p3);
                c4.setBackgroundResource(R.color.red_p4);
                c5.setBackgroundResource(R.color.red_p5);
                c6.setBackgroundResource(R.color.red_p6);
                c7.setBackgroundResource(R.color.red_p7);
                c8.setBackgroundResource(R.color.red_p8);
                c9.setBackgroundResource(R.color.red_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == purple_c){
                c1.setBackgroundResource(R.color.purple_p1);
                c2.setBackgroundResource(R.color.purple_p2);
                c3.setBackgroundResource(R.color.purple_p3);
                c4.setBackgroundResource(R.color.purple_p4);
                c5.setBackgroundResource(R.color.purple_p5);
                c6.setBackgroundResource(R.color.purple_p6);
                c7.setBackgroundResource(R.color.purple_p7);
                c8.setBackgroundResource(R.color.purple_p8);
                c9.setBackgroundResource(R.color.purple_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == pink_c){
                c1.setBackgroundResource(R.color.pink_p1);
                c2.setBackgroundResource(R.color.pink_p2);
                c3.setBackgroundResource(R.color.pink_p3);
                c4.setBackgroundResource(R.color.pink_p4);
                c5.setBackgroundResource(R.color.pink_p5);
                c6.setBackgroundResource(R.color.pink_p6);
                c7.setBackgroundResource(R.color.pink_p7);
                c8.setBackgroundResource(R.color.pink_p8);
                c9.setBackgroundResource(R.color.pink_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == deepPurple_c){
                c1.setBackgroundResource(R.color.deep_purple_p1);
                c2.setBackgroundResource(R.color.deep_purple_p2);
                c3.setBackgroundResource(R.color.deep_purple_p3);
                c4.setBackgroundResource(R.color.deep_purple_p4);
                c5.setBackgroundResource(R.color.deep_purple_p5);
                c6.setBackgroundResource(R.color.deep_purple_p6);
                c7.setBackgroundResource(R.color.deep_purple_p7);
                c8.setBackgroundResource(R.color.deep_purple_p8);
                c9.setBackgroundResource(R.color.deep_purple_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == indigo_c){
                c1.setBackgroundResource(R.color.indigo_p1);
                c2.setBackgroundResource(R.color.indigo_p2);
                c3.setBackgroundResource(R.color.indigo_p3);
                c4.setBackgroundResource(R.color.indigo_p4);
                c5.setBackgroundResource(R.color.indigo_p5);
                c6.setBackgroundResource(R.color.indigo_p6);
                c7.setBackgroundResource(R.color.indigo_p7);
                c8.setBackgroundResource(R.color.indigo_p8);
                c9.setBackgroundResource(R.color.indigo_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == blue_c){
                c1.setBackgroundResource(R.color.blue_p1);
                c2.setBackgroundResource(R.color.blue_p2);
                c3.setBackgroundResource(R.color.blue_p3);
                c4.setBackgroundResource(R.color.blue_p4);
                c5.setBackgroundResource(R.color.blue_p5);
                c6.setBackgroundResource(R.color.blue_p6);
                c7.setBackgroundResource(R.color.blue_p7);
                c8.setBackgroundResource(R.color.blue_p8);
                c9.setBackgroundResource(R.color.blue_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == cyan_c){
                c1.setBackgroundResource(R.color.cyan_p1);
                c2.setBackgroundResource(R.color.cyan_p2);
                c3.setBackgroundResource(R.color.cyan_p3);
                c4.setBackgroundResource(R.color.cyan_p4);
                c5.setBackgroundResource(R.color.cyan_p5);
                c6.setBackgroundResource(R.color.cyan_p6);
                c7.setBackgroundResource(R.color.cyan_p7);
                c8.setBackgroundResource(R.color.cyan_p8);
                c9.setBackgroundResource(R.color.cyan_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == lightBlue_c){
                c1.setBackgroundResource(R.color.light_blue_p1);
                c2.setBackgroundResource(R.color.light_blue_p2);
                c3.setBackgroundResource(R.color.light_blue_p3);
                c4.setBackgroundResource(R.color.light_blue_p4);
                c5.setBackgroundResource(R.color.light_blue_p5);
                c6.setBackgroundResource(R.color.light_blue_p6);
                c7.setBackgroundResource(R.color.light_blue_p7);
                c8.setBackgroundResource(R.color.light_blue_p8);
                c9.setBackgroundResource(R.color.light_blue_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == teal_c){
                c1.setBackgroundResource(R.color.teal_p1);
                c2.setBackgroundResource(R.color.teal_p2);
                c3.setBackgroundResource(R.color.teal_p3);
                c4.setBackgroundResource(R.color.teal_p4);
                c5.setBackgroundResource(R.color.teal_p5);
                c6.setBackgroundResource(R.color.teal_p6);
                c7.setBackgroundResource(R.color.teal_p7);
                c8.setBackgroundResource(R.color.teal_p8);
                c9.setBackgroundResource(R.color.teal_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == green_c){
                c1.setBackgroundResource(R.color.green_p1);
                c2.setBackgroundResource(R.color.green_p2);
                c3.setBackgroundResource(R.color.green_p3);
                c4.setBackgroundResource(R.color.green_p4);
                c5.setBackgroundResource(R.color.green_p5);
                c6.setBackgroundResource(R.color.green_p6);
                c7.setBackgroundResource(R.color.green_p7);
                c8.setBackgroundResource(R.color.green_p8);
                c9.setBackgroundResource(R.color.green_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == lightGreen_c){
                c1.setBackgroundResource(R.color.light_green_p1);
                c2.setBackgroundResource(R.color.light_green_p2);
                c3.setBackgroundResource(R.color.light_green_p3);
                c4.setBackgroundResource(R.color.light_green_p4);
                c5.setBackgroundResource(R.color.light_green_p5);
                c6.setBackgroundResource(R.color.light_green_p6);
                c7.setBackgroundResource(R.color.light_green_p7);
                c8.setBackgroundResource(R.color.light_green_p8);
                c9.setBackgroundResource(R.color.light_green_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == lime_c){
                c1.setBackgroundResource(R.color.lime_p1);
                c2.setBackgroundResource(R.color.lime_p2);
                c3.setBackgroundResource(R.color.lime_p3);
                c4.setBackgroundResource(R.color.lime_p4);
                c5.setBackgroundResource(R.color.lime_p5);
                c6.setBackgroundResource(R.color.lime_p6);
                c7.setBackgroundResource(R.color.lime_p7);
                c8.setBackgroundResource(R.color.lime_p8);
                c9.setBackgroundResource(R.color.lime_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == yellow_c){
                c1.setBackgroundResource(R.color.yellow_p1);
                c2.setBackgroundResource(R.color.yellow_p2);
                c3.setBackgroundResource(R.color.yellow_p3);
                c4.setBackgroundResource(R.color.yellow_p4);
                c5.setBackgroundResource(R.color.yellow_p5);
                c6.setBackgroundResource(R.color.yellow_p6);
                c7.setBackgroundResource(R.color.yellow_p7);
                c8.setBackgroundResource(R.color.yellow_p8);
                c9.setBackgroundResource(R.color.yellow_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == amber_c){
                c1.setBackgroundResource(R.color.amber_p1);
                c2.setBackgroundResource(R.color.amber_p2);
                c3.setBackgroundResource(R.color.amber_p3);
                c4.setBackgroundResource(R.color.amber_p4);
                c5.setBackgroundResource(R.color.amber_p5);
                c6.setBackgroundResource(R.color.amber_p6);
                c7.setBackgroundResource(R.color.amber_p7);
                c8.setBackgroundResource(R.color.amber_p8);
                c9.setBackgroundResource(R.color.amber_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == orange_c){
                c1.setBackgroundResource(R.color.orange_p1);
                c2.setBackgroundResource(R.color.orange_p2);
                c3.setBackgroundResource(R.color.orange_p3);
                c4.setBackgroundResource(R.color.orange_p4);
                c5.setBackgroundResource(R.color.orange_p5);
                c6.setBackgroundResource(R.color.orange_p6);
                c7.setBackgroundResource(R.color.orange_p7);
                c8.setBackgroundResource(R.color.orange_p8);
                c9.setBackgroundResource(R.color.orange_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == deepOrange_c){
                c1.setBackgroundResource(R.color.dep_orange_p1);
                c2.setBackgroundResource(R.color.dep_orange_p2);
                c3.setBackgroundResource(R.color.dep_orange_p3);
                c4.setBackgroundResource(R.color.dep_orange_p4);
                c5.setBackgroundResource(R.color.dep_orange_p5);
                c6.setBackgroundResource(R.color.dep_orange_p6);
                c7.setBackgroundResource(R.color.dep_orange_p7);
                c8.setBackgroundResource(R.color.dep_orange_p8);
                c9.setBackgroundResource(R.color.dep_orange_p9);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.VISIBLE);
            }
            if (view == brown_c){
                c1.setBackgroundResource(R.color.brown_p1);
                c2.setBackgroundResource(R.color.brown_p2);
                c3.setBackgroundResource(R.color.brown_p3);
                c4.setBackgroundResource(R.color.brown_p4);
                c5.setBackgroundResource(R.color.brown_p5);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.GONE);
            }
            if (view == grey_c){
                c1.setBackgroundResource(R.color.grey_p1);
                c2.setBackgroundResource(R.color.grey_p2);
                c3.setBackgroundResource(R.color.grey_p3);
                c4.setBackgroundResource(R.color.grey_p4);
                c5.setBackgroundResource(R.color.grey_p5);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.GONE);
            }
            if (view == blueGrey_c){
                c1.setBackgroundResource(R.color.blue_grey_p1);
                c2.setBackgroundResource(R.color.blue_grey_p2);
                c3.setBackgroundResource(R.color.blue_grey_p3);
                c4.setBackgroundResource(R.color.blue_grey_p4);
                c5.setBackgroundResource(R.color.blue_grey_p5);
                ll1.setVisibility(View.VISIBLE);
                ll2.setVisibility(View.GONE);
            }


            if(view==c1)
            {
                ColorDrawable cd = (ColorDrawable) c1.getBackground();
                int colorId = cd.getColor();

            }
    }


//    public class DrawView extends View implements View.OnTouchListener {
//        private float x = 0, y = 0;
//
//        private Path dPath;
//        private Paint circle_paint, eraser_paint = new Paint();
//        private MaskFilter mBlur;
//
//        private ArrayList<Paint> paintPath = new ArrayList<Paint>();
//        private ArrayList<Paint> undoPaintPath = new ArrayList<Paint>();
//        private ArrayList<Path> paths = new ArrayList<Path>();
//        private ArrayList<Path> undonePaths = new ArrayList<Path>();
//        private float mX, mY;
//        private static final float TOUCH_TOLERANCE = 4;
//
//        public DrawView(Context context, String path) {
//            super(context);
//            setFocusable(true);
//            setFocusableInTouchMode(true);
//            this.setOnTouchListener(this);
//
//            bp = BitmapFactory.decodeFile(path);
//
//            // Set Bitmap
//            bitmapp = Bitmap.createBitmap(screen_width, screen_width, Bitmap.Config.ARGB_8888);
//            bitmapCanvas = new Canvas();
//            bitmapCanvas.setBitmap(bitmapp);
//            bitmapCanvas.drawBitmap(bp, null, new RectF(10, 10, screen_width - 10, screen_width - 10), null);
//
//            // Set eraser_properties
//            dPath = new Path();
//            circle_paint = new Paint();
//            circle_paint.setAntiAlias(true);
//            circle_paint.setColor(Color.BLUE);
//            circle_paint.setStyle(Paint.Style.FILL);
//            circle_paint.setStrokeJoin(Paint.Join.MITER);
//            circle_paint.setStrokeWidth(5f);
//
//            eraser_paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
//            eraser_paint.setAntiAlias(true);
//            eraser_paint.setAlpha(0xff);
//            eraser_paint.setDither(true);
//
//        }
//
//        @Override
//        public void onDraw(Canvas canvas) {
//            canvas.drawBitmap(bitmapp, 0, 0, circle_paint);
//            bitmapCanvas.drawCircle(x, y, ersaerSize, eraser_paint);
//            canvas.drawPath(dPath, eraser_paint);
//            dPath.reset();
//            dPath.addCircle(x, y, ersaerSize, Path.Direction.CW);
//
//            mBlur = new BlurMaskFilter(blur_value, BlurMaskFilter.Blur.SOLID);
//            eraser_paint.setMaskFilter(mBlur);
//
//
//            for (Path p : paths){
//                canvas.drawPath(p, eraser_paint);
//            }
//
//        }
//
//
//
//
////        public boolean onTouch(View view, MotionEvent event) {
////            x = (int) event.getX();
////            y = (int) event.getY() - 200;
////            dPath.addCircle(x, y, ersaerSize, Path.Direction.CW);
////
//////            dPath.reset();
////
////            int ac = event.getAction();
////            switch (ac) {
////                case MotionEvent.ACTION_DOWN:
////                    invalidate();
////                    break;
////                case MotionEvent.ACTION_MOVE:
////                    invalidate();
////                    break;
////                case MotionEvent.ACTION_UP:
////                    invalidate();
////                    paths.add(dPath);
////
////                    break;
////
////            }
////
////            invalidate();
////            return true;
////        }
//
//
//
//        private void touch_start(float x, float y) {
//            undonePaths.clear();
//            dPath.reset();
//            dPath.moveTo(x, y);
//            mX = x;
//            mY = y;
//        }
//        private void touch_move(float x, float y) {
//            float dx = Math.abs(x - mX);
//            float dy = Math.abs(y - mY);
//            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
//                dPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
//                mX = x;
//                mY = y;
//            }
//        }
//        private void touch_up() {
//            dPath.lineTo(mX, mY);
//            // commit the path to our offscreen
//            bitmapCanvas.drawPath(dPath, eraser_paint);
//            // kill this so we don't double draw
//            paths.add(dPath);
//            dPath = new Path();
//
//        }
//
//        public void onClickUndo () {
//            if (paths.size()>0)
//            {
//                undonePaths.add(paths.remove(paths.size()-1));
//                invalidate();
//            }
//            else
//            {
//
//            }
//            //toast the user
//        }
//
//        public void onClickRedo (){
//            if (undonePaths.size()>0)
//            {
//                paths.add(undonePaths.remove(undonePaths.size()-1));
//                invalidate();
//            }
//            else
//            {
//
//            }
//            //toast the user
//        }
//
//        @Override
//        public boolean onTouch(View arg0, MotionEvent event) {
//             x = event.getX();
//             y = event.getY()-200;
//
//            switch (event.getAction()) {
//                case MotionEvent.ACTION_DOWN:
//                    touch_start(x, y);
//                    invalidate();
//                    break;
//                case MotionEvent.ACTION_MOVE:
//                    touch_move(x, y);
//                    invalidate();
//                    break;
//                case MotionEvent.ACTION_UP:
//                    touch_up();
//                    invalidate();
//                    break;
//            }
//            return true;
//        }
//
//
//
//    }



    public class DrawView extends View implements View.OnTouchListener {
        private int x = 0, y = 0;

        private Path dPath;
        private Paint circle_paint, eraser_paint = new Paint();
        private MaskFilter mBlur;

        private ArrayList<Paint> paintPath = new ArrayList<Paint>();
        private ArrayList<Paint> undoPaintPath = new ArrayList<Paint>();
        private ArrayList<Path> paths = new ArrayList<Path>();
        private ArrayList<Path> undonePaths = new ArrayList<Path>();
        private float mX, mY;
        private static final float TOUCH_TOLERANCE = 4;

        public DrawView(Context context, String path) {
            super(context);
            setFocusable(true);
            setFocusableInTouchMode(true);
            this.setOnTouchListener(this);

            bp = BitmapFactory.decodeFile(path);

            // Set Bitmap
            bitmapp = Bitmap.createBitmap(screen_width, screen_width, Bitmap.Config.ARGB_8888);
            bitmapCanvas = new Canvas();
            bitmapCanvas.setBitmap(bitmapp);
            bitmapCanvas.drawBitmap(bp, null, new RectF(10, 10, screen_width - 10, screen_width - 10), null);

            // Set eraser_properties
            dPath = new Path();
            circle_paint = new Paint();
            circle_paint.setAntiAlias(true);
            circle_paint.setColor(Color.BLUE);
            circle_paint.setStyle(Paint.Style.FILL);
            circle_paint.setStrokeJoin(Paint.Join.MITER);
            circle_paint.setStrokeWidth(5f);

            eraser_paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            eraser_paint.setAntiAlias(true);
            eraser_paint.setAlpha(0xff);
            eraser_paint.setDither(true);

        }

        @Override
        public void onDraw(Canvas canvas) {
            canvas.drawBitmap(bitmapp, 0, 0, circle_paint);
            bitmapCanvas.drawCircle(x, y, ersaerSize, eraser_paint);
            canvas.drawPath(dPath, eraser_paint);
            dPath.reset();
            dPath.addCircle(x, y, ersaerSize, Path.Direction.CW);

            mBlur = new BlurMaskFilter(blur_value, BlurMaskFilter.Blur.SOLID);
            eraser_paint.setMaskFilter(mBlur);

            Log.d("wwwww",eraser_paint.toString());
            paintPath.add(eraser_paint);
            Log.d("rerere",paintPath.toString());

//           for (Paint p : paintPath) {
//              canvas.drawPath(p, eraser_paint);
//            }

        }


        public void  onClickUndo()
        {
            Log.d("OPOOIII",paintPath.toString());
            if (paintPath.size() > 0)
            {
//                undonePaths.add(paths.remove(paths.size()-1));

                undoPaintPath.add(paintPath.remove(paintPath.size() - 1));

                invalidate();
            } else {

            }
            //toast the user
        }

        public void onClickRedo() {
            if (undonePaths.size() > 0) {
                paths.add(undonePaths.remove(undonePaths.size() - 1));
                invalidate();
            } else {

            }
            //toast the user
        }


        public boolean onTouch(View view, MotionEvent event) {
            x = (int) event.getX();
            y = (int) event.getY() - 200;
            dPath.addCircle(x, y, ersaerSize, Path.Direction.CW);

//            dPath.reset();

            int ac = event.getAction();
            switch (ac) {
                case MotionEvent.ACTION_DOWN:
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    invalidate();
                    paths.add(dPath);

                    break;

            }

            invalidate();
            return true;
        }
    }


        protected Bitmap addBorderToBitmap(Bitmap srcBitmap, int borderWidth, int borderColor) {
            // Initialize a new Bitmap to make it bordered bitmap
            Bitmap dstBitmap = Bitmap.createBitmap(
                    srcBitmap.getWidth() + borderWidth * 2, // Width
                    srcBitmap.getHeight() + borderWidth * 2, // Height
                    Bitmap.Config.ARGB_8888 // Config
            );

            return dstBitmap;
        }


        public Bitmap getBitmap(RelativeLayout layout) {
            layout.setDrawingCacheEnabled(true);
            layout.buildDrawingCache();
            Bitmap bmp = Bitmap.createBitmap(layout.getDrawingCache());
            layout.setDrawingCacheEnabled(false);
            return bmp;
        }

        public void saveChart(Bitmap getbitmap, float screen_height, float screen_width)
        {
            //Toast.makeText(this, "SAVE", Toast.LENGTH_LONG).show();

            File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString(),"Picxture");
            //   .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"myfolder");
            boolean success = false;
            if (!folder.exists()) {
                success = folder.mkdirs();
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            Log.d("gggggg", folder.getPath());
            File file = new File(folder.getPath() + File.separator + "/" + timeStamp + ".png");
            if (!file.exists()) {
                try {
                    success = file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            FileOutputStream ostream = null;
            try {
                ostream = new FileOutputStream(file);
                System.out.println(ostream);
                Bitmap well = getbitmap;
                Bitmap save = Bitmap.createBitmap((int) screen_width, (int) screen_height, Bitmap.Config.ARGB_8888);
                Paint paint = new Paint();
                paint.setColor(Color.WHITE);
                Canvas now = new Canvas(save);
                now.drawRect(new Rect(0, 0, (int) screen_width, (int) screen_height), paint);
                now.drawBitmap(well,
                        new Rect(0, 0, well.getWidth(), well.getHeight()),
                        new Rect(0, 0, (int) screen_width, (int) screen_height), null);
                if (save == null) {
                    System.out.println("NULL");
                }
                save.compress(Bitmap.CompressFormat.PNG, 100, ostream);
                ContentValues values = new ContentValues();
                //   values.put(Images.Media.TITLE, this.getString(R.string.picture_title));
                //   values.put(Images.Media.DESCRIPTION, this.getString(R.string.picture_description));
                values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis ());
                values.put(MediaStore.Images.ImageColumns.BUCKET_ID, file.toString().toLowerCase(Locale.US).hashCode());
                values.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, file.getName().toLowerCase(Locale.US));
                values.put("_data", file.getAbsolutePath());

                ContentResolver cr = getContentResolver();
                cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } catch (NullPointerException e) {
                e.printStackTrace();
                //Toast.makeText(getApplicationContext(), &quot;Null error&quot;, Toast.LENGTH_SHORT).show();<br />
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                // Toast.makeText(getApplicationContext(), &quot;File error&quot;, Toast.LENGTH_SHORT).show();<br />
            }
//            catch (IOException e) {
//                e.printStackTrace();
//                // Toast.makeText(getApplicationContext(), &quot;IO error&quot;, Toast.LENGTH_SHORT).show();<br />
//            }
        }

}



