package com.company.px.activites.selfie.all.mixme;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.R;
import com.company.px.activites.selfie.all.frameo.CutLayout;
import com.company.px.activites.selfie.all.stickies.StickerImageView;
import com.company.px.activites.selfie.all.texteo.Texteo;
import com.company.px.adapter.EmojiImagesAdapter_Mixme;
import com.company.px.adapter.EmojiTitleAdapter_Mixme;
import com.company.px.adapter.MixmeFestivalTitleAdapter;
import com.company.px.adapter.MixmeImagesHoriAdapter;
import com.company.px.model.EmojiImagesModel;
import com.company.px.model.EmojiTitlesModel;
import com.company.px.model.FestivalTitlesModel;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class MIxme extends Activity implements View.OnClickListener, View.OnTouchListener {

    StickerImageView emoji_s_1, emoji_s_2, emoji_s_3;
    TextView emoji_edt_Butt, bg_multi_colo, style_2_txt, style_1_txt, imgcolor_mod_txt, img_shape_txt, outputButt, photo_1_Butt, photo_2_Butt, theme_edt_Butt, bg_colorsButt, double_colo, single_colo;
    public ImageView emojiButt, photo1_lockIn_ic, photo1_lockOut_ic, photo2_lockIn_ic, photo2_lockOut_ic, theme_lockIn_ic, theme_lockOut_ic, emoji_visi_ic, emoji_Invisi_ic, emoji_lockIn_ic, emoji_lockOut_ic, photo1_up_ic, photo2_up_ic, img_user_1, img_user_2, flip_down, flip_right, uplo_img_ic, pencil_ic_img, bg_visi_ic, bg_Invisi_ic, theme_visi_ic, theme_Invisi_ic, photo1_visi_ic, photo1_Invisi_ic, photo2_visi_ic, photo2_Invisi_ic, linear_gd_po_L, linear_gd_po_TL, linear_gd_po_T, linear_gd_po_TR, linear_gd_po_R, linear_gd_po_BR, linear_gd_po_B, linear_gd_po_BL, img_theme;
    SeekBar img_size_seek, x_axis_seek_obliq, y_axis_seek_obliq, distance_X_seek_obliq, distance_Y_seek_obliq, c_bg_grad_X_axis_seek, c_bg_grad_spacing_seek, c_bg_grad_Y_axis_seek, bg_grad_seek, bg_grad_seek2, bg_corner_seek, bWSeek, contrast, opacity;
    public LinearLayout bg_grad_selectors_LL, emojiButt2, emoji_tab_ll, internet_txt_LL, grad_bg_style2_ll, grad_bg_style1_ll, buttsLL, photoUpload_ll, upload_photo, img_color_adjust_ll, img_shape_adjust_ll, imgcolor_mod_Butt, img_shape_Butt, ll_angle, ll_St_1, bg_grad_Tools_ll, style1_LL, style2_LL, style_1_Butt, style_2_Butt, bgGrad_LL, ll1, ll2, colors_ll, t_LL_3, t_LL_4, photo1_tab_ll, photo2_tab_ll, theme_tab_ll, bg_tab_ll;
    RelativeLayout rL, theme_internet_LL;
    View imgcolor_mod_line, img_shape_line, c_bg_v1, c_bg_v2, bg_view, style_1_Butt_line, style_2_Butt_line, font_line, color_line, grad_c1, grad_c2, grad_c3, grad_c4, grad_c5, grad_c6, black_c, white_c, red_c, purple_c, pink_c, deepPurple_c, indigo_c, blue_c, cyan_c, lightBlue_c, teal_c, green_c, lightGreen_c, lime_c, yellow_c, amber_c, orange_c, deepOrange_c, brown_c, grey_c, blueGrey_c, c1, c2, c3, c4, c5, c6, c7, c8, c9;
    private boolean emoji_bool=false, emoji_bool_s1 = false, emoji_bool_s2 = false, emoji_bool_s3 = false, emoji_1x_bool = false, emoji_2x_bool = false, emoji_3x_bool = false, emoji_1_bool = false, emoji_2_bool = false, emoji_3_bool = false, exit = false, img_size_onstart_bool = false, contrast_onstart_bool = false, bw_onstart_bool = false, opacity_onstart_bool = false, xCut_onstart_bool = false, yCut_onstart_bool = false, dxCut_bool = false, dyCut_bool = false, photo_1_Butt_bool = true, photo_2_Butt_bool, c_bg_radio_rad, c_bg_radio_Lin = true, c_bg_gradV1 = true, c_bg_gradV2, photo_bool, llBg_bool, txt_colorsButt_bool, bg_colorsButt_bool=false, img_edt_Butt_bool, style_2_txt_bool, theme_edt_Butt_bool, grad_c1_bool, grad_c2_bool, grad_c3_bool, grad_c4_bool, grad_c5_bool, grad_c6_bool, imgBg, gradV1 = true, gradV2, radio_Lin, radio_rad;
    private static int RESULT_LOAD_IMG = 1;
    private static int RESULT_LOAD_IMG3 = 3;
    Uri selectedURI1;
    String selected_cc_ImagePath1 = "", imgDecodableString = "",  emoji_1_color, emoji_2_color, emoji_3_color;
    RadioButton c_bg_linearRadio, c_bg_radialRadio, gradBG_Type_liner, gradBG_Type_sweep, grad_linearRadio, grad_radialRadio;
    RadioGroup c_bg_group, grad_rad_group, gradBG_Type_grp;
    int c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, bg_GRAD_0_1 = 0, lin_grad_bg_x0 = 0, lin_grad_bg_x1 = 0, lin_grad_bg_y1 = 100;
    ColorDrawable g_bg_drw_1, g_bg_drw_2, g_bg_drw_3, g_bg_drw_4, g_bg_drw_5, g_bg_drw_6;
    GradientDrawable gd;
    float brightness = 9;
    CutLayout cutLayout_1, cutLayout_2;
    float dX, dY;
    int lastAction, img1_size_save, img2_size_save, img1_contrast_save = 100, img1_bw_save = 50, img1_opacity_save = 200, img1_Xcut_save, img1_Ycut, img1_DXcut, img1_DYcut, img2_contrast_save = 100, img2_bw_save = 50, img2_opacity_save = 200, img2_Xcut_save, img2_Ycut, img2_DXcut, img2_DYcut, theme_contrast_save = 100, theme_bw_save = 50, theme_opacity_save;
    Intent CropIntent;
    String path;
    BottomSheetBehavior mBottomSheetBehavior;
    RelativeLayout.LayoutParams paramsRL;

    // these matrices will be used to move and zoom image
    private Matrix matrix = new Matrix(),  savedMatrix = new Matrix(), matrix_theme = new Matrix();
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;
    int bwValue, gradType = 0, getViewColo, colorId2, grad_colorId_1, grad_colorId_2, grad_colorId_3, grad_colorId_4, grad_colorId_5, grad_colorId_6;
    float gradXF, gradYF;

    private boolean checkInternet;
    RecyclerView fest_title_recycler, imagesHoriRecycler, emoji_recycler_titles, emoji_recycler_images;
    LinearLayoutManager linearLayoutManager, linearLayoutManager2, linearLayoutManager_ST;
    MixmeFestivalTitleAdapter mixmeFestivalTitleAdapter;
    ArrayList<FestivalTitlesModel> festTitleModel = new ArrayList<FestivalTitlesModel>();

    MixmeImagesHoriAdapter mixmeImagesHoriAdapter;
    ArrayList<ImagesHoriModel> imagesHoriModel = new ArrayList<ImagesHoriModel>();

    EmojiTitleAdapter_Mixme emojiTitleAdapter;
    ArrayList<EmojiTitlesModel> emojiTitlesModels = new ArrayList<EmojiTitlesModel>();

    GridLayoutManager gridLayoutManager;
    EmojiImagesAdapter_Mixme emojiImagesAdapter;
    ArrayList<EmojiImagesModel> emojiImagesModels = new ArrayList<EmojiImagesModel>();

    String EDIT_CAT_ID;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mixme_edit);

        contrast = (SeekBar) findViewById(R.id.contrast);
        bWSeek = (SeekBar) findViewById(R.id.bWSeek);
        rL = (RelativeLayout) findViewById(R.id.rL);

        Bundle bundle = getIntent().getExtras();
        EDIT_CAT_ID = bundle.getString("EDIT_CAT_ID");

        opacity = (SeekBar) findViewById(R.id.opacity);

        uplo_img_ic = (ImageView) findViewById(R.id.uplo_img_ic);
        uplo_img_ic.setColorFilter(Color.WHITE);
        pencil_ic_img = (ImageView) findViewById(R.id.pencil_ic_img);
        pencil_ic_img.setOnTouchListener(this);
        pencil_ic_img.setColorFilter(getResources().getColor(R.color.light_blue));

        View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        pencil_ic_img.setRotation(0);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        pencil_ic_img.setRotation(180);
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });
        bg_visi_ic = (ImageView) findViewById(R.id.bg_visi_ic);
        bg_visi_ic.setOnClickListener(this);
        bg_Invisi_ic = (ImageView) findViewById(R.id.bg_Invisi_ic);
        bg_Invisi_ic.setOnClickListener(this);
        theme_visi_ic = (ImageView) findViewById(R.id.theme_visi_ic);
        theme_visi_ic.setOnClickListener(this);
        theme_Invisi_ic = (ImageView) findViewById(R.id.theme_Invisi_ic);
        theme_Invisi_ic.setOnClickListener(this);
        photo1_visi_ic = (ImageView) findViewById(R.id.photo1_visi_ic);
        photo1_visi_ic.setOnClickListener(this);
        photo1_Invisi_ic = (ImageView) findViewById(R.id.photo1_Invisi_ic);
        photo1_Invisi_ic.setOnClickListener(this);
        photo2_visi_ic = (ImageView) findViewById(R.id.photo2_visi_ic);
        photo2_visi_ic.setOnClickListener(this);
        photo2_Invisi_ic = (ImageView) findViewById(R.id.photo2_Invisi_ic);
        photo2_Invisi_ic.setOnClickListener(this);
        linear_gd_po_L = (ImageView) findViewById(R.id.linear_gd_po_L);
        linear_gd_po_L.setOnClickListener(this);
        linear_gd_po_TL = (ImageView) findViewById(R.id.linear_gd_po_TL);
        linear_gd_po_TL.setOnClickListener(this);
        linear_gd_po_T = (ImageView) findViewById(R.id.linear_gd_po_T);
        linear_gd_po_T.setOnClickListener(this);
        linear_gd_po_TR = (ImageView) findViewById(R.id.linear_gd_po_TR);
        linear_gd_po_TR.setOnClickListener(this);
        linear_gd_po_R = (ImageView) findViewById(R.id.linear_gd_po_R);
        linear_gd_po_R.setOnClickListener(this);
        linear_gd_po_BR = (ImageView) findViewById(R.id.linear_gd_po_BR);
        linear_gd_po_BR.setOnClickListener(this);
        linear_gd_po_B = (ImageView) findViewById(R.id.linear_gd_po_B);
        linear_gd_po_B.setOnClickListener(this);
        linear_gd_po_BL = (ImageView) findViewById(R.id.linear_gd_po_BL);
        linear_gd_po_BL.setOnClickListener(this);

        emoji_edt_Butt = (TextView) findViewById(R.id.emoji_edt_Butt);
        emoji_edt_Butt.setOnClickListener(this);
        bg_multi_colo = (TextView) findViewById(R.id.bg_multi_colo);
        bg_multi_colo.setOnClickListener(this);
        style_2_txt = (TextView) findViewById(R.id.style_2_txt);
        style_1_txt = (TextView) findViewById(R.id.style_1_txt);
        imgcolor_mod_txt = (TextView) findViewById(R.id.imgcolor_mod_txt);
        img_shape_txt = (TextView) findViewById(R.id.img_shape_txt);
        photo_1_Butt = (TextView) findViewById(R.id.photo_1_Butt);
        photo_1_Butt.setOnClickListener(this);
        photo_2_Butt = (TextView) findViewById(R.id.photo_2_Butt);
        photo_2_Butt.setOnClickListener(this);
        theme_edt_Butt = (TextView) findViewById(R.id.theme_edt_Butt);
        theme_edt_Butt.setOnClickListener(this);
        bg_colorsButt = (TextView) findViewById(R.id.bg_colorsButt);
        bg_colorsButt.setOnClickListener(this);
        outputButt = (TextView) findViewById(R.id.outputButt);
        outputButt.setOnClickListener(this);


        emojiButt = (ImageView) findViewById(R.id.emojiButt);
        emojiButt.setOnClickListener(this);
        photo1_lockIn_ic = (ImageView) findViewById(R.id.photo1_lockIn_ic);
        photo1_lockIn_ic.setOnClickListener(this);
        photo1_lockOut_ic = (ImageView) findViewById(R.id.photo1_lockOut_ic);
        photo1_lockOut_ic.setOnClickListener(this);
        photo2_lockIn_ic = (ImageView) findViewById(R.id.photo2_lockIn_ic);
        photo2_lockIn_ic.setOnClickListener(this);
        photo2_lockOut_ic = (ImageView) findViewById(R.id.photo2_lockOut_ic);
        photo2_lockOut_ic.setOnClickListener(this);

        theme_lockIn_ic = (ImageView) findViewById(R.id.theme_lockIn_ic);
        theme_lockIn_ic.setOnClickListener(this);
        theme_lockOut_ic = (ImageView) findViewById(R.id.theme_lockOut_ic);
        theme_lockOut_ic.setOnClickListener(this);
        emoji_lockIn_ic = (ImageView) findViewById(R.id.emoji_lockIn_ic);
        emoji_lockIn_ic.setOnClickListener(this);
        emoji_lockOut_ic = (ImageView) findViewById(R.id.emoji_lockOut_ic);
        emoji_lockOut_ic.setOnClickListener(this);
        emoji_visi_ic = (ImageView) findViewById(R.id.emoji_visi_ic);
        emoji_visi_ic.setOnClickListener(this);
        emoji_Invisi_ic = (ImageView) findViewById(R.id.emoji_Invisi_ic);
        emoji_Invisi_ic.setOnClickListener(this);
        photo1_up_ic = (ImageView) findViewById(R.id.photo1_up_ic);
        photo1_up_ic.setOnClickListener(this);
        photo2_up_ic = (ImageView) findViewById(R.id.photo2_up_ic);
        photo2_up_ic.setOnClickListener(this);

        flip_down = (ImageView) findViewById(R.id.flip_down);
        flip_down.setOnClickListener(this);
        flip_right = (ImageView) findViewById(R.id.flip_right);
        flip_right.setOnClickListener(this);


        gradBG_Type_grp = (RadioGroup) findViewById(R.id.gradBG_Type_grp);
        gradBG_Type_liner = (RadioButton) findViewById(R.id.gradBG_Type_liner);
        gradBG_Type_sweep = (RadioButton) findViewById(R.id.gradBG_Type_sweep);


        c_bg_group = (RadioGroup) findViewById(R.id.c_bg_group);
        c_bg_radialRadio = (RadioButton) findViewById(R.id.c_bg_radialRadio);
        c_bg_linearRadio = (RadioButton) findViewById(R.id.c_bg_linearRadio);


        theme_internet_LL = (RelativeLayout) findViewById(R.id.theme_internet_LL);
        bg_grad_selectors_LL = (LinearLayout) findViewById(R.id.bg_grad_selectors_LL);
        emojiButt2 = (LinearLayout) findViewById(R.id.emojiButt2);
        emojiButt2.setOnClickListener(this);
        internet_txt_LL = (LinearLayout) findViewById(R.id.internet_txt_LL);
        internet_txt_LL.setOnClickListener(this);
        emoji_tab_ll = (LinearLayout) findViewById(R.id.emoji_tab_ll);
        img_color_adjust_ll = (LinearLayout) findViewById(R.id.img_color_adjust_ll);
        img_shape_adjust_ll = (LinearLayout) findViewById(R.id.img_shape_adjust_ll);
        imgcolor_mod_Butt = (LinearLayout) findViewById(R.id.imgcolor_mod_Butt);
        imgcolor_mod_Butt.setOnClickListener(this);
        img_shape_Butt = (LinearLayout) findViewById(R.id.img_shape_Butt);
        img_shape_Butt.setOnClickListener(this);
        photo1_tab_ll = (LinearLayout) findViewById(R.id.photo1_tab_ll);
        photo2_tab_ll = (LinearLayout) findViewById(R.id.photo2_tab_ll);
        theme_tab_ll = (LinearLayout) findViewById(R.id.theme_tab_ll);
        bg_tab_ll = (LinearLayout) findViewById(R.id.bg_tab_ll);
        ll_St_1 = (LinearLayout) findViewById(R.id.ll_St_1);
        ll_angle = (LinearLayout) findViewById(R.id.ll_angle);
        style1_LL = (LinearLayout) findViewById(R.id.style1_LL);
        style2_LL = (LinearLayout) findViewById(R.id.style2_LL);
        bg_grad_Tools_ll = (LinearLayout) findViewById(R.id.bg_grad_Tools_ll);
        colors_ll = (LinearLayout) findViewById(R.id.colors_ll);
        t_LL_3 = (LinearLayout) findViewById(R.id.t_LL_3);
        t_LL_4 = (LinearLayout) findViewById(R.id.t_LL_4);
        grad_bg_style1_ll = (LinearLayout) findViewById(R.id.grad_bg_style1_ll);
        grad_bg_style2_ll = (LinearLayout) findViewById(R.id.grad_bg_style2_ll);
        buttsLL = (LinearLayout) findViewById(R.id.buttsLL);
        photoUpload_ll = (LinearLayout) findViewById(R.id.photoUpload_ll);
        upload_photo = (LinearLayout) findViewById(R.id.upload_photo);
        upload_photo.setOnClickListener(this);

        img_theme = (ImageView) findViewById(R.id.img_theme);
        img_theme.setOnTouchListener(this);
        img_theme.setEnabled(false);
        bg_view = (View) findViewById(R.id.bg_view);
        bg_view.setOnTouchListener(this);

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        int screen_width = displayMetrics.widthPixels;
        int screen_height = displayMetrics.heightPixels;


        paramsRL = new RelativeLayout.LayoutParams(screen_width, screen_width);
        RectF drawableRect = new RectF(0, 0, screen_width, screen_width);
        RectF viewRect = new RectF(0, 0, screen_width, screen_width);
        matrix_theme.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.START);
        img_theme.setScaleType(ImageView.ScaleType.MATRIX);
        img_theme.setImageMatrix(matrix_theme);

        rL.setLayoutParams(paramsRL);
        img_theme.setLayoutParams(paramsRL);
//        bg_view.setLayoutParams(paramsRL);

        style_1_Butt = (LinearLayout) findViewById(R.id.style_1_Butt);
        style_1_Butt.setOnClickListener(this);
        style_2_Butt = (LinearLayout) findViewById(R.id.style_2_Butt);
        style_2_Butt.setOnClickListener(this);
        bgGrad_LL = (LinearLayout) findViewById(R.id.bgGrad_LL);
        ll1 = (LinearLayout) findViewById(R.id.ll1);
        ll2 = (LinearLayout) findViewById(R.id.ll2);

        imgcolor_mod_line = (View) findViewById(R.id.imgcolor_mod_line);
        img_shape_line = (View) findViewById(R.id.img_shape_line);
        c_bg_v2 = (View) findViewById(R.id.c_bg_v2);
        c_bg_v2.setOnTouchListener(this);
        c_bg_v1 = (View) findViewById(R.id.c_bg_v1);
        c_bg_v1.setOnTouchListener(this);
        style_1_Butt_line = (View) findViewById(R.id.style_1_Butt_line);
        style_2_Butt_line = (View) findViewById(R.id.style_2_Butt_line);
        font_line = (View) findViewById(R.id.font_line);
        color_line = (View) findViewById(R.id.color_line);
        grad_c1 = (View) findViewById(R.id.grad_c1);
        grad_c1.setOnTouchListener(this);
        grad_c2 = (View) findViewById(R.id.grad_c2);
        grad_c2.setOnTouchListener(this);
        grad_c3 = (View) findViewById(R.id.grad_c3);
        grad_c3.setOnTouchListener(this);
        grad_c4 = (View) findViewById(R.id.grad_c4);
        grad_c4.setOnTouchListener(this);
        grad_c5 = (View) findViewById(R.id.grad_c5);
        grad_c5.setOnTouchListener(this);
        grad_c6 = (View) findViewById(R.id.grad_c6);
        grad_c6.setOnTouchListener(this);

        c1 = (View) findViewById(R.id.c1);
        c1.setOnClickListener(this);
        c2 = (View) findViewById(R.id.c2);
        c2.setOnClickListener(this);
        c3 = (View) findViewById(R.id.c3);
        c3.setOnClickListener(this);
        c4 = (View) findViewById(R.id.c4);
        c4.setOnClickListener(this);
        c5 = (View) findViewById(R.id.c5);
        c5.setOnClickListener(this);
        c6 = (View) findViewById(R.id.c6);
        c6.setOnClickListener(this);
        c7 = (View) findViewById(R.id.c7);
        c7.setOnClickListener(this);
        c8 = (View) findViewById(R.id.c8);
        c8.setOnClickListener(this);
        c9 = (View) findViewById(R.id.c9);
        c9.setOnClickListener(this);
        black_c = (View) findViewById(R.id.black_c);
        black_c.setOnClickListener(this);
        white_c = (View) findViewById(R.id.white_c);
        white_c.setOnClickListener(this);
        red_c = (View) findViewById(R.id.red_c);
        red_c.setOnClickListener(this);
        purple_c = (View) findViewById(R.id.purple_c);
        purple_c.setOnClickListener(this);
        pink_c = (View) findViewById(R.id.pink_c);
        pink_c.setOnClickListener(this);
        deepPurple_c = (View) findViewById(R.id.deepPurple_c);
        deepPurple_c.setOnClickListener(this);
        indigo_c = (View) findViewById(R.id.indigo_c);
        indigo_c.setOnClickListener(this);
        blue_c = (View) findViewById(R.id.blue_c);
        blue_c.setOnClickListener(this);
        cyan_c = (View) findViewById(R.id.cyan_c);
        cyan_c.setOnClickListener(this);
        lightBlue_c = (View) findViewById(R.id.lightBlue_c);
        lightBlue_c.setOnClickListener(this);
        teal_c = (View) findViewById(R.id.teal_c);
        teal_c.setOnClickListener(this);
        green_c = (View) findViewById(R.id.green_c);
        green_c.setOnClickListener(this);
        lightGreen_c = (View) findViewById(R.id.lightGreen_c);
        lightGreen_c.setOnClickListener(this);
        lime_c = (View) findViewById(R.id.lime_c);
        lime_c.setOnClickListener(this);
        yellow_c = (View) findViewById(R.id.yellow_c);
        yellow_c.setOnClickListener(this);
        amber_c = (View) findViewById(R.id.amber_c);
        amber_c.setOnClickListener(this);
        orange_c = (View) findViewById(R.id.orange_c);
        orange_c.setOnClickListener(this);
        deepOrange_c = (View) findViewById(R.id.deepOrange_c);
        deepOrange_c.setOnClickListener(this);
        brown_c = (View) findViewById(R.id.brown_c);
        brown_c.setOnClickListener(this);
        grey_c = (View) findViewById(R.id.grey_c);
        grey_c.setOnClickListener(this);
        blueGrey_c = (View) findViewById(R.id.blueGrey_c);
        blueGrey_c.setOnClickListener(this);
        c1.setBackgroundResource(R.color.red_p1);
        c2.setBackgroundResource(R.color.red_p2);
        c3.setBackgroundResource(R.color.red_p3);
        c4.setBackgroundResource(R.color.red_p4);
        c5.setBackgroundResource(R.color.red_p5);
        c6.setBackgroundResource(R.color.red_p6);
        c7.setBackgroundResource(R.color.red_p7);
        c8.setBackgroundResource(R.color.red_p8);
        c9.setBackgroundResource(R.color.red_p9);

        grad_rad_group = (RadioGroup) findViewById(R.id.grad_rad_group);
        grad_linearRadio = (RadioButton) findViewById(R.id.grad_linearRadio);
        grad_radialRadio = (RadioButton) findViewById(R.id.grad_radialRadio);

        cutLayout_1 = new CutLayout(this);
        rL.addView(cutLayout_1);
        img_user_1 = new ImageView(this);
        cutLayout_1.addView(img_user_1);
        cutLayout_1.setOnTouchListener(this);
        cutLayout_1.setVisibility(View.GONE);
        cutLayout_1.setLayoutParams(paramsRL);
        img_user_1.setLayoutParams(paramsRL);


        cutLayout_2 = new CutLayout(this);
        rL.addView(cutLayout_2);
        img_user_2 = new ImageView(this);
        cutLayout_2.addView(img_user_2);
        cutLayout_2.setOnTouchListener(this);
        cutLayout_2.setVisibility(View.GONE);
        cutLayout_2.setLayoutParams(paramsRL);
        img_user_2.setLayoutParams(paramsRL);

        contrast.setProgress(100);
        contrast.setMax(200);
        contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (photo_1_Butt_bool) {
                    img1_contrast_save = progress;
                } else if (photo_2_Butt_bool) {
                    img2_contrast_save = progress;
                } else if (theme_edt_Butt_bool) {
                    theme_contrast_save = progress;
                }


                if (contrast_onstart_bool) {
                    if (photo_1_Butt_bool) {
                        img_user_1.setColorFilter(setContrast(progress));
                    } else if (photo_2_Butt_bool) {
                        img_user_2.setColorFilter(setContrast(progress));
                    } else if (theme_edt_Butt_bool) {
                        img_theme.setColorFilter(setContrast(progress));
                    }
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                contrast_onstart_bool = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                contrast_onstart_bool = false;
            }
        });

        opacity.setMax(255);
        opacity.setProgress(200);
        opacity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (photo_1_Butt_bool) {
                    img1_opacity_save = progress;
                } else if (photo_2_Butt_bool) {
                    img2_opacity_save = progress;
                } else if (theme_edt_Butt_bool) {
                    theme_opacity_save = progress;
                }

                if (opacity_onstart_bool) {
                    if (photo_1_Butt_bool) {
                        img_user_1.setImageAlpha(progress);
                    } else if (photo_2_Butt_bool) {
                        img_user_2.setImageAlpha(progress);
                    } else if (theme_edt_Butt_bool) {
                        img_theme.setImageAlpha(progress);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                opacity_onstart_bool = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                opacity_onstart_bool = false;
            }
        });


        bWSeek.setMax(1000);
        bWSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                bwValue = i;

                if (photo_1_Butt_bool) {
                    img1_bw_save = bwValue;
                } else if (photo_2_Butt_bool) {
                    img2_bw_save = bwValue;
                } else if (theme_edt_Butt_bool) {
                    theme_bw_save = bwValue;
                }

                if (bw_onstart_bool) {
                    rgbChanger();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                bw_onstart_bool = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                bw_onstart_bool = false;
            }
        });


        img_size_seek = (SeekBar) findViewById(R.id.img_size_seek);
        x_axis_seek_obliq = (SeekBar) findViewById(R.id.x_axis_seek_obliq);
        y_axis_seek_obliq = (SeekBar) findViewById(R.id.y_axis_seek_obliq);
        distance_X_seek_obliq = (SeekBar) findViewById(R.id.distance_X_seek_obliq);
        distance_Y_seek_obliq = (SeekBar) findViewById(R.id.distance_Y_seek_obliq);

        img_size_seek.setProgress(screen_width);
        img_size_seek.setMax(screen_width);
        img_size_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (photo_1_Butt_bool) {
                    img1_size_save = i;
                } else if (photo_2_Butt_bool) {
                    img2_size_save = i;
                }

                if (img_size_onstart_bool) {
                    if (photo_1_Butt_bool) {
                        RelativeLayout.LayoutParams paramsRL_SEEK = new RelativeLayout.LayoutParams(i, i);
                        cutLayout_1.setLayoutParams(paramsRL_SEEK);
                        img_user_1.setLayoutParams(paramsRL_SEEK);
                    } else if (photo_2_Butt_bool) {
                        RelativeLayout.LayoutParams paramsRL_SEEK = new RelativeLayout.LayoutParams(i, i);
                        cutLayout_2.setLayoutParams(paramsRL_SEEK);
                        img_user_2.setLayoutParams(paramsRL_SEEK);
                    }
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                img_size_onstart_bool = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                img_size_onstart_bool = false;
            }
        });


        x_axis_seek_obliq.setMax(screen_width);
        x_axis_seek_obliq.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (photo_1_Butt_bool) {
                    img1_Xcut_save = i;
                } else if (photo_2_Butt_bool) {
                    img2_Xcut_save = i;
                }

                if (xCut_onstart_bool) {
                    if (photo_1_Butt_bool) {
                        cutLayout_1.invalidate();
                        cutLayout_1.setX_obliq(i);
                    } else if (photo_2_Butt_bool) {
                        cutLayout_2.invalidate();
                        cutLayout_2.setX_obliq(i);
                    }
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                xCut_onstart_bool = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                xCut_onstart_bool = false;
            }
        });

        y_axis_seek_obliq.setMax(screen_width);
        y_axis_seek_obliq.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (photo_1_Butt_bool) {
                    img1_Ycut = i;
                } else if (photo_2_Butt_bool) {
                    img2_Ycut = i;
                }

                if (yCut_onstart_bool) {
                    if (photo_1_Butt_bool) {
                        cutLayout_1.invalidate();
                        cutLayout_1.setY_obliq(i);
                    } else if (photo_2_Butt_bool) {
                        cutLayout_2.invalidate();
                        cutLayout_2.setY_obliq(i);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                yCut_onstart_bool = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                yCut_onstart_bool = false;
            }
        });


        distance_X_seek_obliq.setMax(screen_width);
        distance_X_seek_obliq.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (photo_1_Butt_bool) {
                    img1_DXcut = i;
                } else if (photo_2_Butt_bool) {
                    img2_DXcut = i;
                }

                if (dxCut_bool) {
                    if (photo_1_Butt_bool) {
                        cutLayout_1.invalidate();
                        cutLayout_1.setDist_x_obliq(i);
                    } else if (photo_2_Butt_bool) {
                        cutLayout_2.invalidate();
                        cutLayout_2.setDist_x_obliq(i);
                    }
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                dxCut_bool = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                dxCut_bool = false;
            }
        });

        distance_Y_seek_obliq.setMax(screen_width);
        distance_Y_seek_obliq.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (photo_1_Butt_bool) {
                    cutLayout_1.invalidate();
                    cutLayout_1.setDist_y_obliq(i);
                    img1_DYcut = i;

                } else if (photo_2_Butt_bool) {
                    cutLayout_2.invalidate();
                    cutLayout_2.setDist_y_obliq(i);
                    img2_DYcut = i;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                dyCut_bool = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                dyCut_bool = false;
            }
        });


        g_bg_drw_1 = (ColorDrawable) grad_c1.getBackground();
        g_bg_drw_2 = (ColorDrawable) grad_c2.getBackground();
        g_bg_drw_3 = (ColorDrawable) grad_c3.getBackground();
        g_bg_drw_4 = (ColorDrawable) grad_c4.getBackground();
        g_bg_drw_5 = (ColorDrawable) grad_c5.getBackground();
        g_bg_drw_6 = (ColorDrawable) grad_c6.getBackground();
        gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{grad_colorId_1, grad_colorId_2});
        gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
//        bg_view.setBackgroundDrawable(gd);


        gradBG_Type_grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("NewApi")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb2 = (RadioButton) group.findViewById(checkedId);

                grad_colorId_1 = g_bg_drw_1.getColor();
                grad_colorId_2 = g_bg_drw_2.getColor();
                grad_colorId_3 = g_bg_drw_3.getColor();
                grad_colorId_4 = g_bg_drw_4.getColor();
                grad_colorId_5 = g_bg_drw_5.getColor();
                grad_colorId_6 = g_bg_drw_6.getColor();
                gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{grad_colorId_1, grad_colorId_2, grad_colorId_3, grad_colorId_4, grad_colorId_5, grad_colorId_6});

                if (rb2 == gradBG_Type_liner) {
                    gradType = 0;
                    gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                    bg_view.setBackgroundDrawable(gd);
                    ll_St_1.setVisibility(View.GONE);
                    ll_angle.setVisibility(View.VISIBLE);
                } else if (rb2 == gradBG_Type_sweep) {
                    gradType = 1;
                    gd.setGradientType(GradientDrawable.SWEEP_GRADIENT);
                    bg_view.setBackgroundDrawable(gd);
                    ll_St_1.setVisibility(View.VISIBLE);
                    ll_angle.setVisibility(View.GONE);
                }

            }
        });


        bg_grad_seek = (SeekBar) findViewById(R.id.bg_grad_seek);
        bg_grad_seek.setMax(10);
        bg_grad_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gradXF = getConvertedValue(progress);
                gd.setGradientCenter(gradXF, gradYF);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bg_grad_seek2 = (SeekBar) findViewById(R.id.bg_grad_seek2);
        bg_grad_seek2.setMax(10);
        bg_grad_seek2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gradYF = getConvertedValue(progress);
                gd.setGradientCenter(gradXF, gradYF);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bg_corner_seek = (SeekBar) findViewById(R.id.bg_corner_seek);
        bg_corner_seek.setMax(360);
        bg_corner_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gd.setCornerRadius(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        c_bg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (rb == c_bg_linearRadio) {
                    c_bg_radio_Lin = true;
                    c_bg_radio_rad = false;
                    c_bg_grad_spacing_seek.setProgress(30);
                    c_bg_grad_X_axis_seek.setProgress(1);
                    c_bg_grad_Y_axis_seek.setProgress(1);

                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    int colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                } else if (rb == c_bg_radialRadio) {
                    c_bg_radio_Lin = false;
                    c_bg_radio_rad = true;
                    c_bg_grad_spacing_seek.setProgress(30);
                    c_bg_grad_X_axis_seek.setProgress(1);
                    c_bg_grad_Y_axis_seek.setProgress(1);

                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    int colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient1 = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient1);
                    bg_view.setBackground(drawable2);
                }
            }
        });

        c_bg_grad_spacing_seek = (SeekBar) findViewById(R.id.c_bg_grad_spacing_seek);
        c_bg_grad_spacing_seek.setMax(screen_width);
        c_bg_grad_spacing_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (c_bg_radio_rad && progress>2) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    int colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    c_bg_rad_s = progress;
                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient1 = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient1);
                    bg_view.setBackground(drawable2);
                } else if (c_bg_radio_Lin) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    int colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    lin_grad_bg_y1 = progress;
                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        c_bg_grad_X_axis_seek = (SeekBar) findViewById(R.id.c_bg_grad_X_axis_seek);
        c_bg_grad_X_axis_seek.setMax(screen_width);
        c_bg_grad_X_axis_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (c_bg_radio_rad && progress>20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    int colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    c_bg_rad_x = progress;
                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);

                } else if (c_bg_radio_Lin) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    int colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    lin_grad_bg_x1 = progress;
                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        c_bg_grad_Y_axis_seek = (SeekBar) findViewById(R.id.c_bg_grad_Y_axis_seek);
        c_bg_grad_Y_axis_seek.setMax(screen_width);
        c_bg_grad_Y_axis_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (c_bg_radio_rad && progress>2) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    int colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    c_bg_rad_y = progress;
                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                } else if (c_bg_radio_Lin) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    int colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    lin_grad_bg_x0 = progress;
                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        fest_title_recycler = (RecyclerView) findViewById(R.id.fest_title_recycler);
        mixmeFestivalTitleAdapter = new MixmeFestivalTitleAdapter(festTitleModel, MIxme.this, R.layout.row_festival_titles);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        fest_title_recycler.setLayoutManager(linearLayoutManager);

        imagesHoriRecycler = (RecyclerView) findViewById(R.id.imagesHoriRecycler);
        mixmeImagesHoriAdapter = new MixmeImagesHoriAdapter(imagesHoriModel, MIxme.this, R.layout.row_images_hori);
        linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        imagesHoriRecycler.setLayoutManager(linearLayoutManager2);


        getFestivalTitles();
        photoDialog();

    }


    public void rgbChanger() {
        if (photo_1_Butt_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, bwValue
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            img_user_1.setColorFilter(colorFilter);
        } else if (photo_2_Butt_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, bwValue
            };
            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            img_user_2.setColorFilter(colorFilter);
        } else if (theme_edt_Butt_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, bwValue
            };
            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            img_theme.setColorFilter(colorFilter);
        }

    }


    public float getConvertedValue(int intVal) {
        float floatVal = (float) 0.0;
        floatVal = .1f * intVal;
        return floatVal;
    }

    private void getFestivalTitles() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            theme_internet_LL.setVisibility(View.VISIBLE);
            internet_txt_LL.setVisibility(View.GONE);

            Log.d("TFT_URL", AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/" + EDIT_CAT_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/" + EDIT_CAT_ID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("TFT_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        FestivalTitlesModel fList = new FestivalTitlesModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setCreated_on(jsonObject1.getString("created_on"));
                                        fList.setColor_id(jsonObject1.getString("color_id"));

                                        festTitleModel.add(fList);
                                    }
                                    fest_title_recycler.setAdapter(mixmeFestivalTitleAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            internet_txt_LL.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void getImagesHori(String festTitleId) {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            imagesHoriModel.clear();
            Log.d("TIH_URL", AppUrls.BASE_URL + "images/festival/" + festTitleId);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "images/festival/" + festTitleId + "/" + EDIT_CAT_ID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("TIH_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        ImagesHoriModel ihList = new ImagesHoriModel();
                                        ihList.setFestival_id(jsonObject1.getString("festival_id"));
                                        ihList.setImg1(jsonObject1.getString("img1"));
                                        ihList.setImg2(jsonObject1.getString("img2"));
                                        ihList.setImg3(jsonObject1.getString("img3"));

                                        imagesHoriModel.add(ihList);
                                    }
                                    imagesHoriRecycler.setAdapter(mixmeImagesHoriAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // UPLOAD 1
        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
            // Get the Image from data
            selectedURI1 = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            // Get the cursor
            Cursor cursor = getContentResolver().query(selectedURI1, filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgDecodableString = cursor.getString(columnIndex);
            cursor.close();
            // Set the Image in ImageView after decoding the String
//            img_user_1.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

            Log.d("img1_SELEC_URI", selectedURI1.toString());
            Log.d("img1_Decode", imgDecodableString.toString());

            try {
                CropIntent = new Intent("com.android.camera.action.CROP");
                CropIntent.setDataAndType(selectedURI1, "image/*");
                CropIntent.putExtra("crop", "true");
                CropIntent.putExtra("outputX", 300);
                CropIntent.putExtra("outputY", 300);
                CropIntent.putExtra("aspectX", 10);
                CropIntent.putExtra("aspectY", 10);
                CropIntent.putExtra("scaleUpIfNeeded", true);
                CropIntent.putExtra("return-data", true);
                startActivityForResult(CropIntent, RESULT_LOAD_IMG3);
            } catch (ActivityNotFoundException e) {

            }

        }

        /*AFTER CROP*/
        else if (requestCode == RESULT_LOAD_IMG3) {
            if (data != null) {
                Bundle bundle = data.getExtras();

                selected_cc_ImagePath1 = selectedURI1.getPath();

                Bitmap cropbitmap = bundle.getParcelable("data");
                selected_cc_ImagePath1 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath1);

                try {
                    file.createNewFile();
                    FileOutputStream fos = new FileOutputStream(file);
                    cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                } catch (IOException e) {
                    Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                }
                selected_cc_ImagePath1 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath1;


                if (photo_1_Butt_bool) {
                    img_user_1.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath1));
//                img_user_1.setImageAlpha(200);
                    cutLayout_1.setVisibility(View.VISIBLE);

                    photo1_visi_ic.setVisibility(View.GONE);
                    photo1_Invisi_ic.setVisibility(View.VISIBLE);
                } else if (photo_2_Butt_bool) {
                    img_user_2.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath1));
                    img_user_2.setImageAlpha(200);
                    cutLayout_2.setVisibility(View.VISIBLE);

                    photo2_visi_ic.setVisibility(View.GONE);
                    photo2_Invisi_ic.setVisibility(View.VISIBLE);
                }

            }
        }


    }


    public boolean onTouch(View view, MotionEvent event) {
        if (view == pencil_ic_img) {
            if (pencil_ic_img.getRotation() == 0) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else if (pencil_ic_img.getRotation() == 180) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
        if (c_bg_v1 == view) {
            c_bg_gradV1 = true;
            c_bg_gradV2 = false;
            gradV2 = false;
            gradV1 = false;
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
        }
        if (c_bg_v2 == view) {
            c_bg_gradV2 = true;
            c_bg_gradV1 = false;
            gradV2 = false;
            gradV1 = false;
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
        }
        if (view == grad_c1) {
            grad_c1_bool = true;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c2) {
            grad_c1_bool = false;
            grad_c2_bool = true;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c3) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = true;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c4) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = true;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c5) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = true;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c6) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = true;
            gradV1 = false;
            gradV2 = false;
        }


        if (view == emoji_s_1 || view == emoji_s_2 || view == emoji_s_3) {
            if (view == emoji_s_1) {
                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(false);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(true);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(true);
                }

                emoji_bool_s1 = true;
                emoji_bool_s2 = false;
                emoji_bool_s3 = false;
                emoji_s_1.bringToFront();

            } else if (view == emoji_s_2) {

                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(true);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(false);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(true);
                }

                emoji_bool_s1 = false;
                emoji_bool_s2 = true;
                emoji_bool_s3 = false;
                emoji_s_2.bringToFront();

            } else if (view == emoji_s_3) {

                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(true);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(true);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(false);
                }

                emoji_bool_s1 = false;
                emoji_bool_s2 = false;
                emoji_bool_s3 = true;
                emoji_s_3.bringToFront();
            }

            emoji_bool = true;

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }


        if (view == img_theme) {
            imgBg = true;
            img_theme.bringToFront();

            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    savedMatrix.set(matrix);
                    start.set(event.getX(), event.getY());
                    mode = DRAG;
                    lastEvent = null;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setTheme_LL();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    oldDist = spacing(event);
                    if (oldDist > 10f) {
                        savedMatrix.set(matrix);
                        midPoint(mid, event);
                        mode = ZOOM;
                    }
                    lastEvent = new float[4];
                    lastEvent[0] = event.getX(0);
                    lastEvent[1] = event.getX(1);
                    lastEvent[2] = event.getY(0);
                    lastEvent[3] = event.getY(1);
                    d = rotation(event);
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    mode = NONE;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        matrix.set(savedMatrix);
                        float dx = event.getX() - start.x;
                        float dy = event.getY() - start.y;
                        matrix.postTranslate(dx, dy);
                    } else if (mode == ZOOM) {
                        float newDist = spacing(event);
                        if (newDist > 10f) {
                            matrix.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix.postScale(scale, scale, mid.x, mid.y);
                        }
                        if (lastEvent != null && event.getPointerCount() == 2) {
                            newRot = rotation(event);
                            float r = newRot - d;
                            float[] values = new float[9];
                            matrix.getValues(values);
                            float tx = values[2];
                            float ty = values[5];
                            float sx = values[0];
                            float xc = (img_theme.getWidth() / 2) * sx;
                            float yc = (img_theme.getHeight() / 2) * sx;
                            matrix.postRotate(r, tx + xc, ty + yc);
                        }
                    }
                    break;
            }

            img_theme.setImageMatrix(matrix);
        }


        if (view.equals(bg_view)) {
            float newX, newY;

            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setBg_LL();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
        }


        if (view.equals(cutLayout_1)) {
            float newX, newY;
            cutLayout_1.bringToFront();

            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhoto1_LL();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;
                    view.setX(newX);
                    view.setY(newY);
                    lastAction = MotionEvent.ACTION_MOVE;
                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
        }

        if (view.equals(cutLayout_2)) {
            float newX, newY;
            cutLayout_2.bringToFront();

            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhoto2_LL();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
        }

        return true;
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);

    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }


    private void photoDialog() {
        final ImageView upload_photo_d;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.mixme_photo_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        upload_photo_d = (ImageView) dialog.findViewById(R.id.upload_photo_d);
        upload_photo_d.setColorFilter(Color.WHITE);


        upload_photo_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

                photo1_Invisi_ic.setVisibility(View.GONE);
                photo1_visi_ic.setVisibility(View.VISIBLE);

                dialog.cancel();
            }
        });

        dialog.show();
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Unsaved work, Tap back again to exit", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

    private void outputDialog() {
        final ImageView outPut_img;
        final TextView save_to_gallery_butt, share_to_fb, share_to_messenger, share_to_whatsapp, share_to_Instagram;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_edit_output);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        outPut_img = (ImageView) dialog.findViewById(R.id.outPut_img);
        save_to_gallery_butt = (TextView) dialog.findViewById(R.id.save_to_gallery_butt);
        share_to_whatsapp = (TextView) dialog.findViewById(R.id.share_to_whatsapp);

        final Bitmap bitmapOP = getBitmap(rL);
        outPut_img.setImageBitmap(bitmapOP);

        save_to_gallery_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MIxme.this, "Saved To Gallery", Toast.LENGTH_LONG).show();
                saveChart(bitmapOP, rL.getMeasuredHeight(), rL.getMeasuredWidth());

                dialog.cancel();
            }

        });

        share_to_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Picxture");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "PX Testing..");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                dialog.cancel();
            }

        });


        dialog.show();
    }


    public static PorterDuffColorFilter setContrast(int progress) {
        if (progress >= 100) {
            int value = (int) (progress - 100) * 255 / 100;

            return new PorterDuffColorFilter(Color.argb(value, 255, 255, 255), PorterDuff.Mode.OVERLAY);
        } else {
            int value = (int) (100 - progress) * 255 / 100;
            return new PorterDuffColorFilter(Color.argb(value, 0, 0, 0), PorterDuff.Mode.OVERLAY);
        }

    }


    @SuppressLint("NewApi")
    private void getGradBg_Style_1() {
        g_bg_drw_1 = (ColorDrawable) grad_c1.getBackground();
        g_bg_drw_2 = (ColorDrawable) grad_c2.getBackground();
        g_bg_drw_3 = (ColorDrawable) grad_c3.getBackground();
        g_bg_drw_4 = (ColorDrawable) grad_c4.getBackground();
        g_bg_drw_5 = (ColorDrawable) grad_c5.getBackground();
        g_bg_drw_6 = (ColorDrawable) grad_c6.getBackground();

        if (bg_colorsButt_bool && bg_GRAD_0_1 == 0) {
            bg_view.setBackgroundColor(getViewColo);
            bg_visi_ic.setVisibility(View.VISIBLE);
            bg_Invisi_ic.setVisibility(View.GONE);
        } else if (bg_colorsButt_bool && bg_GRAD_0_1 == 1) {
            bg_visi_ic.setVisibility(View.VISIBLE);
            bg_Invisi_ic.setVisibility(View.GONE);
            if (grad_c1_bool) {
                grad_c1.setBackgroundColor(getViewColo);
            } else if (grad_c2_bool) {
                grad_c2.setBackgroundColor(getViewColo);
            } else if (grad_c3_bool) {
                grad_c3.setBackgroundColor(getViewColo);
            } else if (grad_c4_bool) {
                grad_c4.setBackgroundColor(getViewColo);
            } else if (grad_c5_bool) {
                grad_c5.setBackgroundColor(getViewColo);
            } else if (grad_c6_bool) {
                grad_c6.setBackgroundColor(getViewColo);
            }

            grad_colorId_1 = g_bg_drw_1.getColor();
            grad_colorId_2 = g_bg_drw_2.getColor();
            grad_colorId_3 = g_bg_drw_3.getColor();
            grad_colorId_4 = g_bg_drw_4.getColor();
            grad_colorId_5 = g_bg_drw_5.getColor();
            grad_colorId_6 = g_bg_drw_6.getColor();
            gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{grad_colorId_1, grad_colorId_2, grad_colorId_3, grad_colorId_4, grad_colorId_5, grad_colorId_6});

            if (gradType == 0) {
                gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                bg_view.setBackgroundDrawable(gd);
            } else if (gradType == 1) {
                gd.setGradientType(GradientDrawable.SWEEP_GRADIENT);
                bg_view.setBackgroundDrawable(gd);
            }
        }
    }


    private void getGradBg_Style_2() {
        bg_visi_ic.setVisibility(View.VISIBLE);
        bg_Invisi_ic.setVisibility(View.GONE);

        if (style_2_txt_bool) {
            if (c_bg_gradV1) {
                c_bg_v1.setBackgroundColor(getViewColo);
            } else if (c_bg_gradV2) {
                c_bg_v2.setBackgroundColor(getViewColo);
            }
            ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
            ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
            int colorId1 = cd1.getColor();
            colorId2 = cd2.getColor();

            if (c_bg_radio_Lin) {
                int[] color = {colorId1, colorId2};
                float[] position = {0, 1};
                Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                Shader shader_gradient = lin_grad;

                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                drawable2.getPaint().setShader(shader_gradient);
                bg_view.setBackground(drawable2);
            } else if (c_bg_radio_rad) {
                Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                Shader shader_gradient = rad_grad;

                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                drawable2.getPaint().setShader(shader_gradient);
                bg_view.setBackground(drawable2);
            }
        }
    }


    private void getEmojiTitles() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            emojiTitlesModels.clear();
            Log.d("Emoji_Title_URL", AppUrls.BASE_URL + "stickers/" + "4");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "stickers/" + "4",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Emoji_Title_RES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        EmojiTitlesModel fList = new EmojiTitlesModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setCreated_on(jsonObject1.getString("created_on"));
                                        fList.setColor_id(jsonObject1.getString("color_id"));

                                        emojiTitlesModels.add(fList);
                                    }
                                    emoji_recycler_titles.setAdapter(emojiTitleAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void getEmojiImages(String festTitleId) {
        emojiImagesModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("Emoji_IMG_URL", AppUrls.BASE_URL + "images/stickers/" + festTitleId + "/" + "4");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "images/stickers/" + festTitleId + "/" + "4",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Emoji_IMG_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        EmojiImagesModel ihList = new EmojiImagesModel();
                                        ihList.setImg1(jsonObject1.getString("img1"));
                                        ihList.setImg2(jsonObject1.getString("img2"));
                                        ihList.setImg3(jsonObject1.getString("img3"));
                                        ihList.setColor_filter(jsonObject1.getString("field1"));

                                        emojiImagesModels.add(ihList);
                                    }
                                    emoji_recycler_images.setAdapter(emojiImagesAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void emojis(Bitmap bit, String color_fil) {
        if (!emoji_1_bool || emoji_s_1.getVisibility() == View.GONE) {
            if (!emoji_1_bool) {
                emoji_s_1 = new StickerImageView(this);
                rL.addView(emoji_s_1);
                emoji_s_1.setOnTouchListener(this);
                emoji_1_bool = true;
                emoji_s_1.setImageBitmap(bit);
                emoji_1_color = color_fil;
                emoji_1x_bool = true;

            } else if (emoji_s_1.getVisibility() == View.GONE) {
                emoji_s_1.setVisibility(View.VISIBLE);
                emoji_s_1.setImageBitmap(bit);
                emoji_1_color = color_fil;
                emoji_1x_bool = false;
            }

        } else if (!emoji_2_bool || emoji_s_2.getVisibility() == View.GONE) {
            if (!emoji_2_bool) {
                emoji_s_2 = new StickerImageView(this);
                rL.addView(emoji_s_2);
                emoji_s_2.setOnTouchListener(this);
                emoji_2_bool = true;
                emoji_s_2.setImageBitmap(bit);
                emoji_2_color = color_fil;
                emoji_2x_bool = true;

            } else if (emoji_s_2.getVisibility() == View.GONE) {
                emoji_s_2.setVisibility(View.VISIBLE);
                emoji_s_2.setImageBitmap(bit);
                emoji_2_color = color_fil;
                emoji_2x_bool = false;
            }
        } else if (!emoji_3_bool || emoji_s_3.getVisibility() == View.GONE) {
            if (!emoji_3_bool) {
                emoji_s_3 = new StickerImageView(this);
                rL.addView(emoji_s_3);
                emoji_s_3.setOnTouchListener(this);
                emoji_3_bool = true;
                emoji_s_3.setImageBitmap(bit);
                emoji_3_color = color_fil;
                emoji_3x_bool = true;

            } else if (emoji_s_3.getVisibility() == View.GONE) {
                emoji_s_3.setVisibility(View.VISIBLE);
                emoji_s_3.setImageBitmap(bit);
                emoji_3_color = color_fil;
                emoji_3x_bool = false;
            }
        }
    }

    private void getLogoColor() {
        if (emoji_bool) {
            if (emoji_bool_s1) {
                if (emoji_1_bool && emoji_1_color.equals("1")) {
                    emoji_s_1.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }

            if (emoji_bool_s2) {
                if (emoji_2_bool && emoji_2_color.equals("1")) {
                    emoji_s_2.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }

            if (emoji_bool_s3) {
                if (emoji_3_bool && emoji_3_color.equals("1")) {
                    emoji_s_3.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    private void getEmojiDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.emoji_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        emoji_recycler_titles = (RecyclerView) dialog.findViewById(R.id.emoji_recycler_titles);
        emojiTitleAdapter = new EmojiTitleAdapter_Mixme(emojiTitlesModels, MIxme.this, R.layout.row_festival_titles);
        linearLayoutManager_ST = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        emoji_recycler_titles.setLayoutManager(linearLayoutManager_ST);

        emoji_recycler_images = (RecyclerView) dialog.findViewById(R.id.emoji_recycler_images);
        emojiImagesAdapter = new EmojiImagesAdapter_Mixme(emojiImagesModels, MIxme.this, R.layout.row_images_emoji);
        gridLayoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        emoji_recycler_images.setLayoutManager(gridLayoutManager);

        getEmojiTitles();

        dialog.show();
    }


    @SuppressLint({"ResourceAsColor", "NewApi"})
    @Override
    public void onClick(View view) {
        if (internet_txt_LL == view) {
            Intent intent_refresh = getIntent();
            finish();
            startActivity(intent_refresh);
        }

        if (view == emojiButt||view==emojiButt2) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                getEmojiDialog();
            } else {
                Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (view == bg_multi_colo) {
            if (bg_multi_colo.getText().toString().trim().equals("Multi Colors ?"))
            {
                grad_bg_style1_ll.setVisibility(View.VISIBLE);
                bgGrad_LL.setVisibility(View.VISIBLE);
                bg_grad_Tools_ll.setVisibility(View.VISIBLE);
                bg_GRAD_0_1 = 1;
                style_2_txt_bool = false;
                bg_multi_colo.setText("Single Color ?");

            }
            else if (bg_multi_colo.getText().toString().trim().equals("Single Color ?"))
            {
                bgGrad_LL.setVisibility(View.GONE);
                bg_grad_Tools_ll.setVisibility(View.GONE);
                bg_GRAD_0_1 = 0;
                style_2_txt_bool = false;
                grad_bg_style1_ll.setVisibility(View.GONE);
                grad_bg_style2_ll.setVisibility(View.GONE);
                bg_multi_colo.setText("Multi Colors ?");

            }
        }

        if (view == photo1_up_ic) {
            cutLayout_1.bringToFront();
        }
        if (view == photo2_up_ic) {
            cutLayout_2.bringToFront();
        }


        if (view == imgcolor_mod_Butt) {
            imgcolor_mod_txt.setTextColor(getColor(R.color.light_blue));
            img_shape_txt.setTextColor(getColor(R.color.white));
            imgcolor_mod_line.setBackgroundResource(R.color.light_blue);
            img_shape_line.setBackgroundResource(R.color.blue_grey_p5);
            img_color_adjust_ll.setVisibility(View.VISIBLE);
            img_shape_adjust_ll.setVisibility(View.GONE);
        }
        if (view == img_shape_Butt) {
            imgcolor_mod_txt.setTextColor(getColor(R.color.white));
            img_shape_txt.setTextColor(getColor(R.color.light_blue));
            imgcolor_mod_line.setBackgroundResource(R.color.blue_grey_p5);
            img_shape_line.setBackgroundResource(R.color.light_blue);
            img_color_adjust_ll.setVisibility(View.GONE);
            img_shape_adjust_ll.setVisibility(View.VISIBLE);
        }

        if (view==emoji_lockIn_ic)
        {
            emoji_lockIn_ic.setVisibility(View.GONE);
            emoji_lockOut_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool)
            {
                emoji_s_1.setEnabled(true);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setEnabled(true);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setEnabled(true);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to Lock", Toast.LENGTH_SHORT).show();

            }


        }
        if (view==emoji_lockOut_ic)
        {
            emoji_lockOut_ic.setVisibility(View.GONE);
            emoji_lockIn_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool)
            {
                emoji_s_1.setEnabled(false);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setEnabled(false);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setEnabled(false);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to UnLock", Toast.LENGTH_SHORT).show();

            }
        }

        if (view == emoji_visi_ic) {
            emoji_visi_ic.setVisibility(View.GONE);
            emoji_Invisi_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool)
            {
                emoji_s_1.setVisibility(View.GONE);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setVisibility(View.GONE);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setVisibility(View.GONE);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to Hide", Toast.LENGTH_SHORT).show();

            }

        }
        if (view == emoji_Invisi_ic) {
            emoji_visi_ic.setVisibility(View.VISIBLE);
            emoji_Invisi_ic.setVisibility(View.GONE);
            if (emoji_1_bool)
            {
                emoji_s_1.setVisibility(View.VISIBLE);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setVisibility(View.VISIBLE);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setVisibility(View.VISIBLE);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to Show", Toast.LENGTH_SHORT).show();

            }
        }

        if (view==photo1_lockOut_ic)
        {
            photo1_lockOut_ic.setVisibility(View.GONE);
            photo1_lockIn_ic.setVisibility(View.VISIBLE);
            cutLayout_1.setEnabled(false);
        }
        if (view==photo1_lockIn_ic)
        {
            photo1_lockIn_ic.setVisibility(View.GONE);
            photo1_lockOut_ic.setVisibility(View.VISIBLE);
            cutLayout_1.setEnabled(true);
        }
        if (view==photo2_lockOut_ic)
        {
            photo2_lockOut_ic.setVisibility(View.GONE);
            photo2_lockIn_ic.setVisibility(View.VISIBLE);
            cutLayout_2.setEnabled(false);
        }
        if (view==photo2_lockIn_ic)
        {
            photo2_lockIn_ic.setVisibility(View.GONE);
            photo2_lockOut_ic.setVisibility(View.VISIBLE);
            cutLayout_2.setEnabled(true);
        }

        if (view==theme_lockOut_ic)
        {
            theme_lockOut_ic.setVisibility(View.GONE);
            theme_lockIn_ic.setVisibility(View.VISIBLE);
            img_theme.setEnabled(false);
        }
        if (view==theme_lockIn_ic)
        {
            theme_lockIn_ic.setVisibility(View.GONE);
            theme_lockOut_ic.setVisibility(View.VISIBLE);
            img_theme.setEnabled(true);
        }


        if (view == bg_visi_ic) {
            bg_visi_ic.setVisibility(View.GONE);
            bg_Invisi_ic.setVisibility(View.VISIBLE);
            bg_view.setVisibility(View.GONE);
        }
        if (view == bg_Invisi_ic) {
            bg_visi_ic.setVisibility(View.VISIBLE);
            bg_Invisi_ic.setVisibility(View.GONE);
            bg_view.setVisibility(View.VISIBLE);
        }
        if (view == theme_visi_ic) {
            theme_visi_ic.setVisibility(View.GONE);
            theme_Invisi_ic.setVisibility(View.VISIBLE);
            img_theme.setVisibility(View.GONE);
        }
        if (view == theme_Invisi_ic) {
            theme_visi_ic.setVisibility(View.VISIBLE);
            theme_Invisi_ic.setVisibility(View.GONE);
            img_theme.setVisibility(View.VISIBLE);
        }
        if (view == photo1_visi_ic) {
            photo1_visi_ic.setVisibility(View.GONE);
            photo1_Invisi_ic.setVisibility(View.VISIBLE);
            img_user_1.setVisibility(View.VISIBLE);
        }
        if (view == photo1_Invisi_ic) {
            photo1_visi_ic.setVisibility(View.VISIBLE);
            photo1_Invisi_ic.setVisibility(View.GONE);
            img_user_1.setVisibility(View.GONE);
        }
        if (view == photo2_visi_ic) {
            photo2_visi_ic.setVisibility(View.GONE);
            photo2_Invisi_ic.setVisibility(View.VISIBLE);
            img_user_2.setVisibility(View.VISIBLE);
        }
        if (view == photo2_Invisi_ic) {
            photo2_visi_ic.setVisibility(View.VISIBLE);
            photo2_Invisi_ic.setVisibility(View.GONE);
            img_user_2.setVisibility(View.GONE);
        }


        if (bg_colorsButt == view) {
            setBg_LL();
        }

        if (theme_edt_Butt == view) {
            setTheme_LL();
        }
        if (photo_1_Butt == view) {
            setPhoto1_LL();
        }
        if (photo_2_Butt == view) {
            setPhoto2_LL();
        }

        if (view == style_1_Butt) {
            style1_LL.setVisibility(View.VISIBLE);
            style2_LL.setVisibility(View.GONE);
            style_1_Butt_line.setBackgroundResource(R.color.white);
            style_2_Butt_line.setBackgroundResource(R.color.blue_grey_p5);
            style_2_txt_bool = false;
            style_1_txt.setTextColor(getColor(R.color.light_blue));
            style_2_txt.setTextColor(getColor(R.color.white));
            style_1_Butt_line.setBackgroundResource(R.color.light_blue);
            style_2_Butt_line.setBackgroundResource(R.color.blue_grey_p5);
            grad_bg_style1_ll.setVisibility(View.VISIBLE);
            grad_bg_style2_ll.setVisibility(View.GONE);
        }
        if (view == style_2_Butt) {
            style2_LL.setVisibility(View.VISIBLE);
            style1_LL.setVisibility(View.GONE);
            style_2_Butt_line.setBackgroundResource(R.color.white);
            style_1_Butt_line.setBackgroundResource(R.color.blue_grey_p5);
            style_2_txt_bool = true;
            style_1_txt.setTextColor(getColor(R.color.white));
            style_2_txt.setTextColor(getColor(R.color.light_blue));
            style_1_Butt_line.setBackgroundResource(R.color.blue_grey_p5);
            style_2_Butt_line.setBackgroundResource(R.color.light_blue);
            grad_bg_style1_ll.setVisibility(View.GONE);
            grad_bg_style2_ll.setVisibility(View.VISIBLE);
        }

        if (view == upload_photo) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }

        if (emoji_edt_Butt == view) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                setEmoji_LL();
            } else {
                Toast.makeText(this, "Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (view == flip_right) {
            if (theme_edt_Butt_bool) {
                if (img_theme.getScaleX() == 1) {
                    img_theme.setScaleX(-1);
                } else {
                    img_theme.setScaleX(1);
                }
            } else if (photo_1_Butt_bool) {
                if (cutLayout_1.getScaleX() == 1) {
                    cutLayout_1.setScaleX(-1);
                } else {
                    cutLayout_1.setScaleX(1);
                }
            } else if (photo_2_Butt_bool) {
                if (cutLayout_2.getScaleX() == 1) {
                    cutLayout_2.setScaleX(-1);
                } else {
                    cutLayout_2.setScaleX(1);
                }
            }
        }

        if (view == flip_down) {
            if (theme_edt_Butt_bool) {
                if (img_theme.getScaleY() == 1) {
                    img_theme.setScaleY(-1);
                } else {
                    img_theme.setScaleY(1);
                }
            } else if (photo_1_Butt_bool) {
                if (cutLayout_1.getScaleY() == 1) {
                    cutLayout_1.setScaleY(-1);
                } else {
                    cutLayout_1.setScaleY(1);
                }
            } else if (photo_2_Butt_bool) {
                if (cutLayout_2.getScaleY() == 1) {
                    cutLayout_2.setScaleY(-1);
                } else {
                    cutLayout_2.setScaleY(1);
                }
            }
        }


        if (view == red_c) {
            c1.setBackgroundResource(R.color.red_p1);
            c2.setBackgroundResource(R.color.red_p2);
            c3.setBackgroundResource(R.color.red_p3);
            c4.setBackgroundResource(R.color.red_p4);
            c5.setBackgroundResource(R.color.red_p5);
            c6.setBackgroundResource(R.color.red_p6);
            c7.setBackgroundResource(R.color.red_p7);
            c8.setBackgroundResource(R.color.red_p8);
            c9.setBackgroundResource(R.color.red_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) red_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == purple_c) {
            c1.setBackgroundResource(R.color.purple_p1);
            c2.setBackgroundResource(R.color.purple_p2);
            c3.setBackgroundResource(R.color.purple_p3);
            c4.setBackgroundResource(R.color.purple_p4);
            c5.setBackgroundResource(R.color.purple_p5);
            c6.setBackgroundResource(R.color.purple_p6);
            c7.setBackgroundResource(R.color.purple_p7);
            c8.setBackgroundResource(R.color.purple_p8);
            c9.setBackgroundResource(R.color.purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) purple_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == pink_c) {
            c1.setBackgroundResource(R.color.pink_p1);
            c2.setBackgroundResource(R.color.pink_p2);
            c3.setBackgroundResource(R.color.pink_p3);
            c4.setBackgroundResource(R.color.pink_p4);
            c5.setBackgroundResource(R.color.pink_p5);
            c6.setBackgroundResource(R.color.pink_p6);
            c7.setBackgroundResource(R.color.pink_p7);
            c8.setBackgroundResource(R.color.pink_p8);
            c9.setBackgroundResource(R.color.pink_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) pink_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == deepPurple_c) {
            c1.setBackgroundResource(R.color.deep_purple_p1);
            c2.setBackgroundResource(R.color.deep_purple_p2);
            c3.setBackgroundResource(R.color.deep_purple_p3);
            c4.setBackgroundResource(R.color.deep_purple_p4);
            c5.setBackgroundResource(R.color.deep_purple_p5);
            c6.setBackgroundResource(R.color.deep_purple_p6);
            c7.setBackgroundResource(R.color.deep_purple_p7);
            c8.setBackgroundResource(R.color.deep_purple_p8);
            c9.setBackgroundResource(R.color.deep_purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) deepPurple_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == indigo_c) {
            c1.setBackgroundResource(R.color.indigo_p1);
            c2.setBackgroundResource(R.color.indigo_p2);
            c3.setBackgroundResource(R.color.indigo_p3);
            c4.setBackgroundResource(R.color.indigo_p4);
            c5.setBackgroundResource(R.color.indigo_p5);
            c6.setBackgroundResource(R.color.indigo_p6);
            c7.setBackgroundResource(R.color.indigo_p7);
            c8.setBackgroundResource(R.color.indigo_p8);
            c9.setBackgroundResource(R.color.indigo_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) indigo_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == blue_c) {
            c1.setBackgroundResource(R.color.blue_p1);
            c2.setBackgroundResource(R.color.blue_p2);
            c3.setBackgroundResource(R.color.blue_p3);
            c4.setBackgroundResource(R.color.blue_p4);
            c5.setBackgroundResource(R.color.blue_p5);
            c6.setBackgroundResource(R.color.blue_p6);
            c7.setBackgroundResource(R.color.blue_p7);
            c8.setBackgroundResource(R.color.blue_p8);
            c9.setBackgroundResource(R.color.blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) blue_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == cyan_c) {
            c1.setBackgroundResource(R.color.cyan_p1);
            c2.setBackgroundResource(R.color.cyan_p2);
            c3.setBackgroundResource(R.color.cyan_p3);
            c4.setBackgroundResource(R.color.cyan_p4);
            c5.setBackgroundResource(R.color.cyan_p5);
            c6.setBackgroundResource(R.color.cyan_p6);
            c7.setBackgroundResource(R.color.cyan_p7);
            c8.setBackgroundResource(R.color.cyan_p8);
            c9.setBackgroundResource(R.color.cyan_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) cyan_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == lightBlue_c) {
            c1.setBackgroundResource(R.color.light_blue_p1);
            c2.setBackgroundResource(R.color.light_blue_p2);
            c3.setBackgroundResource(R.color.light_blue_p3);
            c4.setBackgroundResource(R.color.light_blue_p4);
            c5.setBackgroundResource(R.color.light_blue_p5);
            c6.setBackgroundResource(R.color.light_blue_p6);
            c7.setBackgroundResource(R.color.light_blue_p7);
            c8.setBackgroundResource(R.color.light_blue_p8);
            c9.setBackgroundResource(R.color.light_blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) lightBlue_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == teal_c) {
            c1.setBackgroundResource(R.color.teal_p1);
            c2.setBackgroundResource(R.color.teal_p2);
            c3.setBackgroundResource(R.color.teal_p3);
            c4.setBackgroundResource(R.color.teal_p4);
            c5.setBackgroundResource(R.color.teal_p5);
            c6.setBackgroundResource(R.color.teal_p6);
            c7.setBackgroundResource(R.color.teal_p7);
            c8.setBackgroundResource(R.color.teal_p8);
            c9.setBackgroundResource(R.color.teal_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) teal_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == green_c) {
            c1.setBackgroundResource(R.color.green_p1);
            c2.setBackgroundResource(R.color.green_p2);
            c3.setBackgroundResource(R.color.green_p3);
            c4.setBackgroundResource(R.color.green_p4);
            c5.setBackgroundResource(R.color.green_p5);
            c6.setBackgroundResource(R.color.green_p6);
            c7.setBackgroundResource(R.color.green_p7);
            c8.setBackgroundResource(R.color.green_p8);
            c9.setBackgroundResource(R.color.green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) green_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == lightGreen_c) {
            c1.setBackgroundResource(R.color.light_green_p1);
            c2.setBackgroundResource(R.color.light_green_p2);
            c3.setBackgroundResource(R.color.light_green_p3);
            c4.setBackgroundResource(R.color.light_green_p4);
            c5.setBackgroundResource(R.color.light_green_p5);
            c6.setBackgroundResource(R.color.light_green_p6);
            c7.setBackgroundResource(R.color.light_green_p7);
            c8.setBackgroundResource(R.color.light_green_p8);
            c9.setBackgroundResource(R.color.light_green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) lightGreen_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == lime_c) {
            c1.setBackgroundResource(R.color.lime_p1);
            c2.setBackgroundResource(R.color.lime_p2);
            c3.setBackgroundResource(R.color.lime_p3);
            c4.setBackgroundResource(R.color.lime_p4);
            c5.setBackgroundResource(R.color.lime_p5);
            c6.setBackgroundResource(R.color.lime_p6);
            c7.setBackgroundResource(R.color.lime_p7);
            c8.setBackgroundResource(R.color.lime_p8);
            c9.setBackgroundResource(R.color.lime_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) lime_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == yellow_c) {
            c1.setBackgroundResource(R.color.yellow_p1);
            c2.setBackgroundResource(R.color.yellow_p2);
            c3.setBackgroundResource(R.color.yellow_p3);
            c4.setBackgroundResource(R.color.yellow_p4);
            c5.setBackgroundResource(R.color.yellow_p5);
            c6.setBackgroundResource(R.color.yellow_p6);
            c7.setBackgroundResource(R.color.yellow_p7);
            c8.setBackgroundResource(R.color.yellow_p8);
            c9.setBackgroundResource(R.color.yellow_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) yellow_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == amber_c) {
            c1.setBackgroundResource(R.color.amber_p1);
            c2.setBackgroundResource(R.color.amber_p2);
            c3.setBackgroundResource(R.color.amber_p3);
            c4.setBackgroundResource(R.color.amber_p4);
            c5.setBackgroundResource(R.color.amber_p5);
            c6.setBackgroundResource(R.color.amber_p6);
            c7.setBackgroundResource(R.color.amber_p7);
            c8.setBackgroundResource(R.color.amber_p8);
            c9.setBackgroundResource(R.color.amber_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) amber_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == orange_c) {
            c1.setBackgroundResource(R.color.orange_p1);
            c2.setBackgroundResource(R.color.orange_p2);
            c3.setBackgroundResource(R.color.orange_p3);
            c4.setBackgroundResource(R.color.orange_p4);
            c5.setBackgroundResource(R.color.orange_p5);
            c6.setBackgroundResource(R.color.orange_p6);
            c7.setBackgroundResource(R.color.orange_p7);
            c8.setBackgroundResource(R.color.orange_p8);
            c9.setBackgroundResource(R.color.orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) orange_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == deepOrange_c) {
            c1.setBackgroundResource(R.color.dep_orange_p1);
            c2.setBackgroundResource(R.color.dep_orange_p2);
            c3.setBackgroundResource(R.color.dep_orange_p3);
            c4.setBackgroundResource(R.color.dep_orange_p4);
            c5.setBackgroundResource(R.color.dep_orange_p5);
            c6.setBackgroundResource(R.color.dep_orange_p6);
            c7.setBackgroundResource(R.color.dep_orange_p7);
            c8.setBackgroundResource(R.color.dep_orange_p8);
            c9.setBackgroundResource(R.color.dep_orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);

            ColorDrawable cd = (ColorDrawable) deepOrange_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == brown_c) {
            c1.setBackgroundResource(R.color.brown_p1);
            c2.setBackgroundResource(R.color.brown_p2);
            c3.setBackgroundResource(R.color.brown_p3);
            c4.setBackgroundResource(R.color.brown_p4);
            c5.setBackgroundResource(R.color.brown_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);

            ColorDrawable cd = (ColorDrawable) brown_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == grey_c) {
            c1.setBackgroundResource(R.color.grey_p1);
            c2.setBackgroundResource(R.color.grey_p2);
            c3.setBackgroundResource(R.color.grey_p3);
            c4.setBackgroundResource(R.color.grey_p4);
            c5.setBackgroundResource(R.color.grey_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);

            ColorDrawable cd = (ColorDrawable) grey_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == blueGrey_c) {
            c1.setBackgroundResource(R.color.blue_grey_p1);
            c2.setBackgroundResource(R.color.blue_grey_p2);
            c3.setBackgroundResource(R.color.blue_grey_p3);
            c4.setBackgroundResource(R.color.blue_grey_p4);
            c5.setBackgroundResource(R.color.blue_grey_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);

            ColorDrawable cd = (ColorDrawable) blueGrey_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }

        if (view == black_c) {
            ll1.setVisibility(View.GONE);
            ColorDrawable cd = (ColorDrawable) black_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == white_c) {
            ll1.setVisibility(View.GONE);
            ColorDrawable cd = (ColorDrawable) white_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c1) {
            ColorDrawable cd = (ColorDrawable) c1.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c2) {
            ColorDrawable cd = (ColorDrawable) c2.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c3) {
            ColorDrawable cd = (ColorDrawable) c3.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c4) {
            ColorDrawable cd = (ColorDrawable) c4.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c5) {
            ColorDrawable cd = (ColorDrawable) c5.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c6) {
            ColorDrawable cd = (ColorDrawable) c6.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c7) {
            ColorDrawable cd = (ColorDrawable) c7.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c8) {
            ColorDrawable cd = (ColorDrawable) c8.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c9) {
            ColorDrawable cd = (ColorDrawable) c9.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == linear_gd_po_T) {
            gd.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
        } else if (view == linear_gd_po_TR) {
            gd.setOrientation(GradientDrawable.Orientation.TR_BL);
        } else if (view == linear_gd_po_R) {
            gd.setOrientation(GradientDrawable.Orientation.RIGHT_LEFT);
        } else if (view == linear_gd_po_BR) {
            gd.setOrientation(GradientDrawable.Orientation.BR_TL);
        } else if (view == linear_gd_po_B) {
            gd.setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
        } else if (view == linear_gd_po_BL) {
            gd.setOrientation(GradientDrawable.Orientation.BL_TR);
        } else if (view == linear_gd_po_L) {
            gd.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
        } else if (view == linear_gd_po_TL) {
            gd.setOrientation(GradientDrawable.Orientation.TL_BR);
        }

        if (view == outputButt) {
            outputDialog();
        }

    }

    private void setBg_LL() {
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.light_blue));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_1_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_2_Butt.setTextColor(getResources().getColor(R.color.white));
        t_LL_3.setVisibility(View.VISIBLE);
        emojiButt2.setVisibility(View.GONE);
        bg_grad_selectors_LL.setVisibility(View.VISIBLE);
        colors_ll.setVisibility(View.VISIBLE);
        t_LL_4.setVisibility(View.GONE);
        buttsLL.setVisibility(View.GONE);
        photoUpload_ll.setVisibility(View.GONE);
        txt_colorsButt_bool = false;
        photo_1_Butt_bool = false;
        photo_2_Butt_bool = false;
        bg_colorsButt_bool = true;
        img_edt_Butt_bool = false;
        style_2_txt_bool = false;
        theme_edt_Butt_bool = false;
        llBg_bool = true;
        photo_bool = false;
        emoji_bool = false;

        emoji_tab_ll.setBackgroundResource(R.color.transparent);
        photo1_tab_ll.setBackgroundResource(R.color.transparent);
        photo2_tab_ll.setBackgroundResource(R.color.transparent);
        theme_tab_ll.setBackgroundResource(R.color.transparent);
        bg_tab_ll.setBackgroundResource(R.color.dark_blue);
    }

    private void setTheme_LL() {
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.white));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.light_blue));
        photo_1_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_2_Butt.setTextColor(getResources().getColor(R.color.white));
        bg_grad_selectors_LL.setVisibility(View.GONE);
        emojiButt2.setVisibility(View.GONE);
        t_LL_3.setVisibility(View.GONE);
        colors_ll.setVisibility(View.GONE);
        img_shape_adjust_ll.setVisibility(View.GONE);
        t_LL_4.setVisibility(View.VISIBLE);
        img_color_adjust_ll.setVisibility(View.VISIBLE);
        buttsLL.setVisibility(View.GONE);
        photoUpload_ll.setVisibility(View.GONE);
        txt_colorsButt_bool = false;
        bg_colorsButt_bool = false;
        photo_1_Butt_bool = false;
        photo_2_Butt_bool = false;
        img_edt_Butt_bool = true;
        style_2_txt_bool = false;
        theme_edt_Butt_bool = true;
        llBg_bool = false;
        photo_bool = false;
        emoji_bool = false;

        photo1_tab_ll.setBackgroundResource(R.color.transparent);
        photo2_tab_ll.setBackgroundResource(R.color.transparent);
        theme_tab_ll.setBackgroundResource(R.color.dark_blue);
        bg_tab_ll.setBackgroundResource(R.color.transparent);
        emoji_tab_ll.setBackgroundResource(R.color.transparent);

        contrast.setProgress(theme_contrast_save);
        bWSeek.setProgress(theme_bw_save);
        opacity.setProgress(theme_opacity_save);
    }

    private void setPhoto1_LL() {
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.white));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_1_Butt.setTextColor(getResources().getColor(R.color.light_blue));
        photo_2_Butt.setTextColor(getResources().getColor(R.color.white));
        bg_grad_selectors_LL.setVisibility(View.GONE);
        t_LL_3.setVisibility(View.GONE);
        emojiButt2.setVisibility(View.GONE);
        colors_ll.setVisibility(View.GONE);
        t_LL_4.setVisibility(View.VISIBLE);
        buttsLL.setVisibility(View.VISIBLE);
        photoUpload_ll.setVisibility(View.VISIBLE);
        bg_colorsButt_bool = false;
        img_edt_Butt_bool = true;
        theme_edt_Butt_bool = false;
        llBg_bool = false;
        photo_bool = true;
        style_2_txt_bool = false;
        photo_1_Butt_bool = true;
        photo_2_Butt_bool = false;
        emoji_bool = false;

        photo1_tab_ll.setBackgroundResource(R.color.dark_blue);
        photo2_tab_ll.setBackgroundResource(R.color.transparent);
        theme_tab_ll.setBackgroundResource(R.color.transparent);
        bg_tab_ll.setBackgroundResource(R.color.transparent);
        emoji_tab_ll.setBackgroundResource(R.color.transparent);

        x_axis_seek_obliq.setProgress(img1_Xcut_save);
        y_axis_seek_obliq.setProgress(img1_Ycut);
        distance_X_seek_obliq.setProgress(img1_DXcut);
        distance_Y_seek_obliq.setProgress(img1_DYcut);
        contrast.setProgress(img1_contrast_save);
        bWSeek.setProgress(img1_bw_save);
        opacity.setProgress(img1_opacity_save);
    }

    private void setPhoto2_LL() {
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.white));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_1_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_2_Butt.setTextColor(getResources().getColor(R.color.light_blue));
        bg_grad_selectors_LL.setVisibility(View.GONE);
        emojiButt2.setVisibility(View.GONE);
        t_LL_3.setVisibility(View.GONE);
        colors_ll.setVisibility(View.GONE);
        t_LL_4.setVisibility(View.VISIBLE);
        buttsLL.setVisibility(View.VISIBLE);
        photoUpload_ll.setVisibility(View.VISIBLE);
        bg_colorsButt_bool = false;
        img_edt_Butt_bool = true;
        theme_edt_Butt_bool = false;
        llBg_bool = false;
        photo_bool = true;
        style_2_txt_bool = false;
        photo_1_Butt_bool = false;
        photo_2_Butt_bool = true;
        emoji_bool = false;

        emoji_tab_ll.setBackgroundResource(R.color.transparent);
        photo1_tab_ll.setBackgroundResource(R.color.transparent);
        photo1_tab_ll.setBackgroundResource(R.color.transparent);
        photo2_tab_ll.setBackgroundResource(R.color.dark_blue);
        theme_tab_ll.setBackgroundResource(R.color.transparent);
        bg_tab_ll.setBackgroundResource(R.color.transparent);

        x_axis_seek_obliq.setProgress(img2_Xcut_save);
        y_axis_seek_obliq.setProgress(img2_Ycut);
        distance_X_seek_obliq.setProgress(img2_DXcut);
        distance_Y_seek_obliq.setProgress(img2_DYcut);
        contrast.setProgress(img2_contrast_save);
        bWSeek.setProgress(img2_bw_save);
        opacity.setProgress(img2_opacity_save);
    }


    private void setEmoji_LL() {
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.light_blue));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.white));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_1_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_2_Butt.setTextColor(getResources().getColor(R.color.white));
        bg_grad_selectors_LL.setVisibility(View.GONE);
        t_LL_3.setVisibility(View.GONE);
        emojiButt2.setVisibility(View.VISIBLE);
        colors_ll.setVisibility(View.VISIBLE);
        t_LL_4.setVisibility(View.GONE);
        buttsLL.setVisibility(View.GONE);
        photoUpload_ll.setVisibility(View.GONE);
        txt_colorsButt_bool = false;
        photo_1_Butt_bool = false;
        photo_2_Butt_bool = false;
        bg_colorsButt_bool = false;
        img_edt_Butt_bool = false;
        style_2_txt_bool = false;
        theme_edt_Butt_bool = false;
        llBg_bool = false;
        photo_bool = false;
        emoji_bool = true;

        photo1_tab_ll.setBackgroundResource(R.color.transparent);
        photo2_tab_ll.setBackgroundResource(R.color.transparent);
        theme_tab_ll.setBackgroundResource(R.color.transparent);
        bg_tab_ll.setBackgroundResource(R.color.transparent);
        emoji_tab_ll.setBackgroundResource(R.color.dark_blue);
    }


    public Bitmap getBitmap(RelativeLayout layout) {
        layout.setDrawingCacheEnabled(true);
        layout.buildDrawingCache();
        Bitmap bmp = Bitmap.createBitmap(layout.getDrawingCache());
        layout.setDrawingCacheEnabled(false);
        return bmp;
    }

    public void saveChart(Bitmap getbitmap, float height, float width) {
        Toast.makeText(this, "SAVE", Toast.LENGTH_LONG).show();


     /*   path = Environment.getDataDirectory().getAbsolutePath().toString() + "/storage/emulated/0/Px";
        File mFolder = new File(path);
        if (!mFolder.exists()) {
            mFolder.mkdir();
        }
        File folder = new File("/sdcard/Px/");
        folder.mkdirs();*/

        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString(), "Picxture");
        //   .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"myfolder");
        boolean success = false;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        Log.d("gggggg", folder.getPath());
        File file = new File(folder + File.separator + "/" + timeStamp + ".png");
        if (!file.exists()) {
            try {
                success = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileOutputStream ostream = null;
        try {
            ostream = new FileOutputStream(file);
            System.out.println(ostream);
            Bitmap well = getbitmap;
            Bitmap save = Bitmap.createBitmap((int) width, (int) height, Bitmap.Config.ARGB_8888);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            Canvas now = new Canvas(save);
            now.drawRect(new Rect(0, 0, (int) width, (int) height), paint);
            now.drawBitmap(well,
                    new Rect(0, 0, well.getWidth(), well.getHeight()),
                    new Rect(0, 0, (int) width, (int) height), null);
            if (save == null) {
                System.out.println("NULL");
            }
            save.compress(Bitmap.CompressFormat.PNG, 100, ostream);

            ContentValues values = new ContentValues();
            //   values.put(Images.Media.TITLE, this.getString(R.string.picture_title));
            //   values.put(Images.Media.DESCRIPTION, this.getString(R.string.picture_description));
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.ImageColumns.BUCKET_ID, file.toString().toLowerCase(Locale.US).hashCode());
            values.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, file.getName().toLowerCase(Locale.US));
            values.put("_data", file.getAbsolutePath());

            ContentResolver cr = getContentResolver();
            cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        } catch (NullPointerException e) {
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(), &quot;Null error&quot;, Toast.LENGTH_SHORT).show();<br />
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // Toast.makeText(getApplicationContext(), &quot;File error&quot;, Toast.LENGTH_SHORT).show();<br />
        }
//        catch (IOException e){
//            e.printStackTrace();
//            // Toast.makeText(getApplicationContext(), &quot;IO error&quot;, Toast.LENGTH_SHORT).show();<br />
//        }
    }


}
