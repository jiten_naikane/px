package com.company.px.activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.MainActivity;
import com.company.px.R;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;
import com.company.px.utility.UserSessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PayConfirmActivity extends AppCompatActivity {

    TextView amount, pay_ID, status;
    String paymentId, paymentStatus,user_id;
    private boolean checkInternet;
    String O_D_ID;
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_confirm);

        session = new UserSessionManager(PayConfirmActivity.this);
        HashMap<String, String> userDetails = session.getUserDetails();

        user_id = userDetails.get(UserSessionManager.USER_ID);

        O_D_ID = getIntent().getExtras().getString("Order_id");


        amount = (TextView) findViewById(R.id.amount);
        pay_ID = (TextView) findViewById(R.id.pay_ID);
        status = (TextView) findViewById(R.id.status);
        Log.d("USERIDD",user_id+"///////"+O_D_ID);

        Intent intent = getIntent();

        try {
            JSONObject jsonDetails = new JSONObject(intent.getStringExtra("PaymentDetails"));
            showDetails(jsonDetails.getJSONObject("response"), intent.getStringExtra("PaymentAmount"));
        } catch (JSONException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }


    private void showDetails(JSONObject jsonDetails, String paymentAmount) throws JSONException {

//        username_txt = (TextView) findViewById(R.id.username_txt);
//        username_txt.setText("Hi," + user_name);
//        payment_id = (TextView) findViewById(R.id.payment_id);
//        status = (TextView) findViewById(R.id.status);
//
//        next_btn = (Button) findViewById(R.id.next_btn);
//        next_btn.setOnClickListener(this);

        paymentId = jsonDetails.getString("id");
        paymentStatus = jsonDetails.getString("state");

        pay_ID.setText(paymentId);
        status.setText("$ " + paymentAmount + " USD" + "\t" + "Payment Completed");

        Log.d("PAYMENTDETAILS", paymentId + "\n" + paymentStatus);
        postFinalPay(paymentId,paymentStatus);

    }

    private void postFinalPay(final String paymentId, final String paymentStatus)
    {
        checkInternet = NetworkChecking.isConnected(PayConfirmActivity.this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ORDER_PAY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //  progressDialog.dismiss();
                            Log.d("ORDERRESP", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String success = jsonObject.getString("success");
                                String message = jsonObject.getString("msg");
                                String response_code = jsonObject.getString("code");
                                if (response_code.equals("10100")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(PayConfirmActivity.this, ""+message, Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(PayConfirmActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                if (response_code.equals("10200")) {
                                    // progressDialog.dismiss();
                                    Toast.makeText(PayConfirmActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                // progressDialog.cancel();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //  progressDialog.cancel();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {



                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                     params.put("user_id", user_id);
                    params.put("order_id", String.valueOf(O_D_ID));
                    params.put("payment_id", paymentId);
                    params.put("status", paymentStatus);


                    Log.d("PARAMORDER:", "PARAMORDER" + params.toString());
                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PayConfirmActivity.this);
            requestQueue.add(stringRequest);
        } else {
            // progressDialog.cancel();
            Toast.makeText(PayConfirmActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


}
