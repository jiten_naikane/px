package com.company.px.activites.egraphics;


import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.company.px.R;

public class CatWebApp extends AppCompatActivity implements View.OnClickListener
{
    View black_c, white_c, red_c, purple_c,pink_c, deepPurple_c, indigo_c, blue_c, cyan_c, lightBlue_c, teal_c, green_c, lightGreen_c, lime_c, yellow_c, amber_c, orange_c, deepOrange_c, brown_c, grey_c, blueGrey_c, c1, c2, c3, c4, c5, c6, c7, c8, c9;
    ImageView back_butt, nxt;
    LinearLayout nextButt, saveButt, ll2;
    EditText whatsapp_E, email_E, describe_E, title_E;
    Button upload_butt;
    TextView select_colors_tv_butt, delv_err, select_style_tv_butt, design_choice_tv, style_name_tv;
    TextInputLayout title_TIL, describe_TIL;
    private Spinner indus_spinner;
    String[] indus_array = { "-Select Industry-","Accounting  |  Finance","Agriculture","Agriculture  |  Pets", "Architectural", "Art  |  Design", "Attorney  |  Law",
    "Automotive", "Bar  |  Nightclub", "Business  |  Consulting", "Childcare", "Cleaning  |  Maintenance", "Communications", "Community  |  Non-Profit", "Computer", "Construction",
    "Cosmetics  |  Beauty", "Dating", "Education", "Entertainment", "Environmental", "Fashion", "Floral", "Food  |  Drink", "Games  |  Recreational", "Home Furnishing", "Industrial",
    "Internet", "Landscaping", "Medical  |  Pharmaceutical", "Photography", "Physical Fitness", "Political", "Real Estate  |  Mortgage", "Religious", "Restaurant", "Retail", "Security",
    "Spa  |  Esthetics", "Sport", "Technology", "Travel  |  Hotel", "Wedding Service"};
    Dialog dialog;
    RecyclerView dialo_colors_recycler;

    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cat_web_app);

        indus_spinner = (Spinner) findViewById(R.id.indus_spinner);
        ArrayAdapter<String> adapter= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item , indus_array);
        indus_spinner.setAdapter(adapter);

        indus_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int sid=indus_spinner.getSelectedItemPosition();
                Toast.makeText(CatWebApp.this, String.valueOf(sid), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        } );


        title_TIL = (TextInputLayout) findViewById(R.id.title_TIL);
        describe_TIL = (TextInputLayout) findViewById(R.id.describe_TIL);
        delv_err = (TextView) findViewById(R.id.delv_err);
        select_style_tv_butt = (TextView) findViewById(R.id.select_style_tv_butt);
        select_style_tv_butt.setOnClickListener(this);
        select_colors_tv_butt = (TextView) findViewById(R.id.select_colors_tv_butt);
        select_colors_tv_butt.setOnClickListener(this);
        design_choice_tv = (TextView) findViewById(R.id.design_choice_tv);
        style_name_tv = (TextView) findViewById(R.id.style_name_tv);

        whatsapp_E = (EditText) findViewById(R.id.whatsapp_E);
        email_E = (EditText) findViewById(R.id.email_E);
        describe_E = (EditText) findViewById(R.id.describe_E);
        title_E = (EditText) findViewById(R.id.title_E);
        nxt = (ImageView)findViewById(R.id.nxt);
        nxt.setColorFilter(Color.WHITE);
        upload_butt = (Button) findViewById(R.id.upload_butt);
        upload_butt.setOnClickListener(this);
        back_butt = (ImageView)findViewById(R.id.back_butt);
        back_butt.setOnClickListener(this);
        nextButt = (LinearLayout) findViewById(R.id.nextButt);
        nextButt.setOnClickListener(this);
        saveButt = (LinearLayout) findViewById(R.id.saveButt);
        saveButt.setOnClickListener(this);






    }






    @Override
    public void onClick(View v)
    {
        if (v==nextButt){

        }
        if (v==back_butt){
            finish();
        }

        if (v==select_style_tv_butt){
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_colors);
            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);




            dialog.show();
        }
        if (v==select_colors_tv_butt){
            dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_colors);
            Window window = dialog.getWindow();
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            dialo_colors_recycler = (RecyclerView) dialog.findViewById(R.id.dialo_colors_recycler);
            ll2 = (LinearLayout) dialog.findViewById(R.id.ll2);
            c1 = (View) dialog.findViewById(R.id.c1);
            c2 = (View) dialog.findViewById(R.id.c2);
            c3 = (View) dialog.findViewById(R.id.c3);
            c4 = (View) dialog.findViewById(R.id.c4);
            c5 = (View) dialog.findViewById(R.id.c5);
            c6 = (View) dialog.findViewById(R.id.c6);
            c7 = (View) dialog.findViewById(R.id.c7);
            c8 = (View) dialog.findViewById(R.id.c8);
            c9 = (View) dialog.findViewById(R.id.c9);
            black_c = (View) dialog.findViewById(R.id.black_c);
            black_c.setOnClickListener(this);
            white_c = (View) dialog.findViewById(R.id.white_c);
            white_c.setOnClickListener(this);
            red_c = (View) dialog.findViewById(R.id.red_c);
            red_c.setOnClickListener(this);
            purple_c = (View) dialog.findViewById(R.id.purple_c);
            purple_c.setOnClickListener(this);
            pink_c = (View) dialog.findViewById(R.id.pink_c);
            pink_c.setOnClickListener(this);
            deepPurple_c = (View) dialog.findViewById(R.id.deepPurple_c);
            deepPurple_c.setOnClickListener(this);
            indigo_c = (View) dialog.findViewById(R.id.indigo_c);
            indigo_c.setOnClickListener(this);
            blue_c = (View) dialog.findViewById(R.id.blue_c);
            blue_c.setOnClickListener(this);
            cyan_c = (View) dialog.findViewById(R.id.cyan_c);
            cyan_c.setOnClickListener(this);
            lightBlue_c = (View) dialog.findViewById(R.id.lightBlue_c);
            lightBlue_c.setOnClickListener(this);
            teal_c = (View) dialog.findViewById(R.id.teal_c);
            teal_c.setOnClickListener(this);
            green_c = (View) dialog.findViewById(R.id.green_c);
            green_c.setOnClickListener(this);
            lightGreen_c = (View) dialog.findViewById(R.id.lightGreen_c);
            lightGreen_c.setOnClickListener(this);
            lime_c = (View) dialog.findViewById(R.id.lime_c);
            lime_c.setOnClickListener(this);
            yellow_c = (View) dialog.findViewById(R.id.yellow_c);
            yellow_c.setOnClickListener(this);
            amber_c = (View) dialog.findViewById(R.id.amber_c);
            amber_c.setOnClickListener(this);
            orange_c = (View) dialog.findViewById(R.id.orange_c);
            orange_c.setOnClickListener(this);
            deepOrange_c = (View) dialog.findViewById(R.id.deepOrange_c);
            deepOrange_c.setOnClickListener(this);
            brown_c = (View) dialog.findViewById(R.id.brown_c);
            brown_c.setOnClickListener(this);
            grey_c = (View) dialog.findViewById(R.id.grey_c);
            grey_c.setOnClickListener(this);
            blueGrey_c = (View) dialog.findViewById(R.id.blueGrey_c);
            blueGrey_c.setOnClickListener(this);


            dialog.show();
        }




        if (v == red_c){
            c1.setBackgroundResource(R.color.red_p1);
            c2.setBackgroundResource(R.color.red_p2);
            c3.setBackgroundResource(R.color.red_p3);
            c4.setBackgroundResource(R.color.red_p4);
            c5.setBackgroundResource(R.color.red_p5);
            c6.setBackgroundResource(R.color.red_p6);
            c7.setBackgroundResource(R.color.red_p7);
            c8.setBackgroundResource(R.color.red_p8);
            c9.setBackgroundResource(R.color.red_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == purple_c){
            c1.setBackgroundResource(R.color.purple_p1);
            c2.setBackgroundResource(R.color.purple_p2);
            c3.setBackgroundResource(R.color.purple_p3);
            c4.setBackgroundResource(R.color.purple_p4);
            c5.setBackgroundResource(R.color.purple_p5);
            c6.setBackgroundResource(R.color.purple_p6);
            c7.setBackgroundResource(R.color.purple_p7);
            c8.setBackgroundResource(R.color.purple_p8);
            c9.setBackgroundResource(R.color.purple_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == pink_c){
            c1.setBackgroundResource(R.color.pink_p1);
            c2.setBackgroundResource(R.color.pink_p2);
            c3.setBackgroundResource(R.color.pink_p3);
            c4.setBackgroundResource(R.color.pink_p4);
            c5.setBackgroundResource(R.color.pink_p5);
            c6.setBackgroundResource(R.color.pink_p6);
            c7.setBackgroundResource(R.color.pink_p7);
            c8.setBackgroundResource(R.color.pink_p8);
            c9.setBackgroundResource(R.color.pink_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == deepPurple_c){
            c1.setBackgroundResource(R.color.deep_purple_p1);
            c2.setBackgroundResource(R.color.deep_purple_p2);
            c3.setBackgroundResource(R.color.deep_purple_p3);
            c4.setBackgroundResource(R.color.deep_purple_p4);
            c5.setBackgroundResource(R.color.deep_purple_p5);
            c6.setBackgroundResource(R.color.deep_purple_p6);
            c7.setBackgroundResource(R.color.deep_purple_p7);
            c8.setBackgroundResource(R.color.deep_purple_p8);
            c9.setBackgroundResource(R.color.deep_purple_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == indigo_c){
            c1.setBackgroundResource(R.color.indigo_p1);
            c2.setBackgroundResource(R.color.indigo_p2);
            c3.setBackgroundResource(R.color.indigo_p3);
            c4.setBackgroundResource(R.color.indigo_p4);
            c5.setBackgroundResource(R.color.indigo_p5);
            c6.setBackgroundResource(R.color.indigo_p6);
            c7.setBackgroundResource(R.color.indigo_p7);
            c8.setBackgroundResource(R.color.indigo_p8);
            c9.setBackgroundResource(R.color.indigo_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == blue_c){
            c1.setBackgroundResource(R.color.blue_p1);
            c2.setBackgroundResource(R.color.blue_p2);
            c3.setBackgroundResource(R.color.blue_p3);
            c4.setBackgroundResource(R.color.blue_p4);
            c5.setBackgroundResource(R.color.blue_p5);
            c6.setBackgroundResource(R.color.blue_p6);
            c7.setBackgroundResource(R.color.blue_p7);
            c8.setBackgroundResource(R.color.blue_p8);
            c9.setBackgroundResource(R.color.blue_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == cyan_c){
            c1.setBackgroundResource(R.color.cyan_p1);
            c2.setBackgroundResource(R.color.cyan_p2);
            c3.setBackgroundResource(R.color.cyan_p3);
            c4.setBackgroundResource(R.color.cyan_p4);
            c5.setBackgroundResource(R.color.cyan_p5);
            c6.setBackgroundResource(R.color.cyan_p6);
            c7.setBackgroundResource(R.color.cyan_p7);
            c8.setBackgroundResource(R.color.cyan_p8);
            c9.setBackgroundResource(R.color.cyan_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == lightBlue_c){
            c1.setBackgroundResource(R.color.light_blue_p1);
            c2.setBackgroundResource(R.color.light_blue_p2);
            c3.setBackgroundResource(R.color.light_blue_p3);
            c4.setBackgroundResource(R.color.light_blue_p4);
            c5.setBackgroundResource(R.color.light_blue_p5);
            c6.setBackgroundResource(R.color.light_blue_p6);
            c7.setBackgroundResource(R.color.light_blue_p7);
            c8.setBackgroundResource(R.color.light_blue_p8);
            c9.setBackgroundResource(R.color.light_blue_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == teal_c){
            c1.setBackgroundResource(R.color.teal_p1);
            c2.setBackgroundResource(R.color.teal_p2);
            c3.setBackgroundResource(R.color.teal_p3);
            c4.setBackgroundResource(R.color.teal_p4);
            c5.setBackgroundResource(R.color.teal_p5);
            c6.setBackgroundResource(R.color.teal_p6);
            c7.setBackgroundResource(R.color.teal_p7);
            c8.setBackgroundResource(R.color.teal_p8);
            c9.setBackgroundResource(R.color.teal_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == green_c){
            c1.setBackgroundResource(R.color.green_p1);
            c2.setBackgroundResource(R.color.green_p2);
            c3.setBackgroundResource(R.color.green_p3);
            c4.setBackgroundResource(R.color.green_p4);
            c5.setBackgroundResource(R.color.green_p5);
            c6.setBackgroundResource(R.color.green_p6);
            c7.setBackgroundResource(R.color.green_p7);
            c8.setBackgroundResource(R.color.green_p8);
            c9.setBackgroundResource(R.color.green_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == lightGreen_c){
            c1.setBackgroundResource(R.color.light_green_p1);
            c2.setBackgroundResource(R.color.light_green_p2);
            c3.setBackgroundResource(R.color.light_green_p3);
            c4.setBackgroundResource(R.color.light_green_p4);
            c5.setBackgroundResource(R.color.light_green_p5);
            c6.setBackgroundResource(R.color.light_green_p6);
            c7.setBackgroundResource(R.color.light_green_p7);
            c8.setBackgroundResource(R.color.light_green_p8);
            c9.setBackgroundResource(R.color.light_green_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == lime_c){
            c1.setBackgroundResource(R.color.lime_p1);
            c2.setBackgroundResource(R.color.lime_p2);
            c3.setBackgroundResource(R.color.lime_p3);
            c4.setBackgroundResource(R.color.lime_p4);
            c5.setBackgroundResource(R.color.lime_p5);
            c6.setBackgroundResource(R.color.lime_p6);
            c7.setBackgroundResource(R.color.lime_p7);
            c8.setBackgroundResource(R.color.lime_p8);
            c9.setBackgroundResource(R.color.lime_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == yellow_c){
            c1.setBackgroundResource(R.color.yellow_p1);
            c2.setBackgroundResource(R.color.yellow_p2);
            c3.setBackgroundResource(R.color.yellow_p3);
            c4.setBackgroundResource(R.color.yellow_p4);
            c5.setBackgroundResource(R.color.yellow_p5);
            c6.setBackgroundResource(R.color.yellow_p6);
            c7.setBackgroundResource(R.color.yellow_p7);
            c8.setBackgroundResource(R.color.yellow_p8);
            c9.setBackgroundResource(R.color.yellow_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == amber_c){
            c1.setBackgroundResource(R.color.amber_p1);
            c2.setBackgroundResource(R.color.amber_p2);
            c3.setBackgroundResource(R.color.amber_p3);
            c4.setBackgroundResource(R.color.amber_p4);
            c5.setBackgroundResource(R.color.amber_p5);
            c6.setBackgroundResource(R.color.amber_p6);
            c7.setBackgroundResource(R.color.amber_p7);
            c8.setBackgroundResource(R.color.amber_p8);
            c9.setBackgroundResource(R.color.amber_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == orange_c){
            c1.setBackgroundResource(R.color.orange_p1);
            c2.setBackgroundResource(R.color.orange_p2);
            c3.setBackgroundResource(R.color.orange_p3);
            c4.setBackgroundResource(R.color.orange_p4);
            c5.setBackgroundResource(R.color.orange_p5);
            c6.setBackgroundResource(R.color.orange_p6);
            c7.setBackgroundResource(R.color.orange_p7);
            c8.setBackgroundResource(R.color.orange_p8);
            c9.setBackgroundResource(R.color.orange_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == deepOrange_c){
            c1.setBackgroundResource(R.color.dep_orange_p1);
            c2.setBackgroundResource(R.color.dep_orange_p2);
            c3.setBackgroundResource(R.color.dep_orange_p3);
            c4.setBackgroundResource(R.color.dep_orange_p4);
            c5.setBackgroundResource(R.color.dep_orange_p5);
            c6.setBackgroundResource(R.color.dep_orange_p6);
            c7.setBackgroundResource(R.color.dep_orange_p7);
            c8.setBackgroundResource(R.color.dep_orange_p8);
            c9.setBackgroundResource(R.color.dep_orange_p9);
            ll2.setVisibility(View.VISIBLE);
        }
        if (v == brown_c){
            c1.setBackgroundResource(R.color.brown_p1);
            c2.setBackgroundResource(R.color.brown_p2);
            c3.setBackgroundResource(R.color.brown_p3);
            c4.setBackgroundResource(R.color.brown_p4);
            c5.setBackgroundResource(R.color.brown_p5);
            ll2.setVisibility(View.GONE);
        }
        if (v == grey_c){
            c1.setBackgroundResource(R.color.grey_p1);
            c2.setBackgroundResource(R.color.grey_p2);
            c3.setBackgroundResource(R.color.grey_p3);
            c4.setBackgroundResource(R.color.grey_p4);
            c5.setBackgroundResource(R.color.grey_p5);
            ll2.setVisibility(View.GONE);
        }
        if (v == blueGrey_c){
            c1.setBackgroundResource(R.color.blue_grey_p1);
            c2.setBackgroundResource(R.color.blue_grey_p2);
            c3.setBackgroundResource(R.color.blue_grey_p3);
            c4.setBackgroundResource(R.color.blue_grey_p4);
            c5.setBackgroundResource(R.color.blue_grey_p5);
            ll2.setVisibility(View.GONE);
        }
        if (v == black_c){
            c1.setBackgroundResource(R.color.black);
            c2.setBackgroundResource(R.color.black);
            c3.setBackgroundResource(R.color.black);
            c4.setBackgroundResource(R.color.black);
            c5.setBackgroundResource(R.color.black);
            ll2.setVisibility(View.GONE);
        }
        if (v == white_c){
            c1.setBackgroundResource(R.color.white);
            c2.setBackgroundResource(R.color.white);
            c3.setBackgroundResource(R.color.white);
            c4.setBackgroundResource(R.color.white);
            c5.setBackgroundResource(R.color.white);
            ll2.setVisibility(View.GONE);
        }


    }



}


