package com.company.px.activites.selfie.all.frameo;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.R;
import com.company.px.activites.selfie.all.stickies.StickerImageView;
import com.company.px.activites.selfie.all.texteo.Texteo;
import com.company.px.adapter.EmojiImagesAdapter_Frameo;
import com.company.px.adapter.EmojiTitleAdapter_Frameo;
import com.company.px.adapter.FrameoFestivalTitleAdapter;
import com.company.px.adapter.FrameoImagesHoriAdapter;
import com.company.px.model.EmojiImagesModel;
import com.company.px.model.EmojiTitlesModel;
import com.company.px.model.FestivalTitlesModel;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class FrameoEdit extends Activity implements View.OnClickListener, View.OnTouchListener {

    StickerImageView emoji_s_1, emoji_s_2, emoji_s_3;
    private Matrix matrix_theme = new Matrix();
    TextView unlockAll_butt, emoji_edt_Butt, outputButt, style1_txt, style2_txt, gallery_Butt, photoEdit_butt, theme_Butt, bg_Butt, bg_multi_colo;
    public ImageView emojiButt, pencil_ic_img, linear_gd_po_L, linear_gd_po_TL, linear_gd_po_T, linear_gd_po_TR, linear_gd_po_R, linear_gd_po_BR, linear_gd_po_B, linear_gd_po_BL,
            img_theme, img_gal_1, img_gal_2, img_gal_3, img_gal_4, img_gal_5, img_gal_6, img_gal_7, img_gal_8, img_gal_9, img_gal_10, img_gal_11, img_gal_12,
            gal_upload_1, gal_upload_2, gal_upload_3, gal_upload_4, gal_upload_5, gal_upload_6, gal_upload_7, gal_upload_8, gal_upload_9, gal_upload_10, gal_upload_11, gal_upload_12,
            gal_x_1, gal_x_2, gal_x_3, gal_x_4, gal_x_5, gal_x_6, gal_x_7, gal_x_8, gal_x_9, gal_x_10, gal_x_11, gal_x_12,
            gal_1_visi_ic, gal_2_visi_ic, gal_3_visi_ic, gal_4_visi_ic, gal_5_visi_ic, gal_6_visi_ic, gal_7_visi_ic, gal_8_visi_ic, gal_9_visi_ic, gal_10_visi_ic, gal_11_visi_ic, gal_12_visi_ic,
            gal_1_Invisi_ic, gal_2_Invisi_ic, gal_3_Invisi_ic, gal_4_Invisi_ic, gal_5_Invisi_ic, gal_6_Invisi_ic, gal_7_Invisi_ic, gal_8_Invisi_ic, gal_9_Invisi_ic, gal_10_Invisi_ic, gal_11_Invisi_ic, gal_12_Invisi_ic,
            gal_1_lockIn_ic, gal_2_lockIn_ic, gal_3_lockIn_ic, gal_4_lockIn_ic, gal_5_lockIn_ic, gal_6_lockIn_ic, gal_7_lockIn_ic, gal_8_lockIn_ic, gal_9_lockIn_ic, gal_10_lockIn_ic, gal_11_lockIn_ic, gal_12_lockIn_ic,
            gal_1_lockOut_ic, gal_2_lockOut_ic, gal_3_lockOut_ic, gal_4_lockOut_ic, gal_5_lockOut_ic, gal_6_lockOut_ic, gal_7_lockOut_ic, gal_8_lockOut_ic, gal_9_lockOut_ic, gal_10_lockOut_ic, gal_11_lockOut_ic, gal_12_lockOut_ic,
            gal_up_1, gal_up_2, gal_up_3, gal_up_4, gal_up_5, gal_up_6, gal_up_7, gal_up_8, gal_up_9, gal_up_10, gal_up_11, gal_up_12, flip_down, flip_right, emoji_lockIn_ic, emoji_lockOut_ic, emoji_visi_ic, emoji_Invisi_ic, bg_visi_ic, bg_Invisi_ic, theme_visi_ic, theme_Invisi_ic, theme_lockIn_ic, theme_lockOut_ic;

    RelativeLayout rL, theme_internet_LL;
    public LinearLayout bg_color_tool, emoji_tool_ll, emojiButt2, gal_1_visi_LL, gal_2_visi_LL, gal_3_visi_LL, gal_4_visi_LL, gal_5_visi_LL, gal_6_visi_LL, gal_7_visi_LL, gal_8_visi_LL, gal_9_visi_LL, gal_10_visi_LL, gal_11_visi_LL, gal_12_visi_LL,
            gal_1_lock_LL, gal_2_lock_LL, gal_3_lock_LL, gal_4_lock_LL, gal_5_lock_LL, gal_6_lock_LL, gal_7_lock_LL, gal_8_lock_LL, gal_9_lock_LL, gal_10_lock_LL, gal_11_lock_LL, gal_12_lock_LL,
            bg_grad_selectors_LL, internet_txt_LL, ll1, ll2, style_1_Butt, style_2_Butt, style1_LL, style2_LL, grad_bg_style1_ll, grad_bg_style2_ll, bgGrad_LL, theme_tab_ll, bg_grad_Tools_ll, ll_angle, ll_St_1,
            font_ll, bottom_sheet, gallery_tab_ll, photoEdit_tab_ll, bg_tab_ll, photoEdit_tool_ll, img_color_adj_LL, colors_ll, gallery_view_ll;
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString, emoji_1_color, emoji_2_color, emoji_3_color;
    Intent CropIntent;
    private static int RESULT_LOAD_IMG3 = 3;
    Uri selectedURI1;
    String selected_cc_ImagePath1 = "", selected_cc_ImagePath2 = "", selected_cc_ImagePath3 = "", selected_cc_ImagePath4 = "", selected_cc_ImagePath5 = "", selected_cc_ImagePath6 = "", selected_cc_ImagePath7 = "", selected_cc_ImagePath8 = "", selected_cc_ImagePath9 = "", selected_cc_ImagePath10 = "", selected_cc_ImagePath11 = "", selected_cc_ImagePath12 = "";
    String EDIT_CAT_ID;
    View style_1_Butt_line, style_2_Butt_line, bg_view, c_bg_v1, c_bg_v2, grad_c1, grad_c2, grad_c3, grad_c4, grad_c5, grad_c6, black_c, white_c, red_c, purple_c, pink_c, deepPurple_c, indigo_c, blue_c, cyan_c, lightBlue_c, teal_c, green_c, lightGreen_c, lime_c, yellow_c, amber_c, orange_c, deepOrange_c, brown_c, grey_c, blueGrey_c, c1, c2, c3, c4, c5, c6, c7, c8, c9;
    ;
    private Boolean emoji_bool=false, emoji_bool_s1 = false, emoji_bool_s2 = false, emoji_bool_s3 = false, emoji_1_bool = false, emoji_2_bool = false, emoji_3_bool = false, emoji_1x_bool = false, emoji_2x_bool = false, emoji_3x_bool = false, theme_bool = false, exit = false, gradV1 = true, gradV2, setBgLL_bool=true, grad_c1_bool, grad_c2_bool, grad_c3_bool, grad_c4_bool, grad_c5_bool, grad_c6_bool, style_2_txt_bool=false, c_bg_gradV1 = true, c_bg_gradV2, c_bg_radio_rad = true, c_bg_radio_Lin = false,
            bw_onstart_bool = false, contrast_onstart_bool = false, opacity_onstart_bool = false, xCut_onstart_bool = false, yCut_onstart_bool = false, dxCut_onstart_bool = false, dyCut_onstart_bool = false, scale_onstart_bool = false, imgBg, cutLayout_1_bool = false, cutLayout_2_bool = false, cutLayout_3_bool = false, cutLayout_4_bool = false, cutLayout_5_bool = false, cutLayout_6_bool = false, cutLayout_7_bool = false, cutLayout_8_bool = false, cutLayout_9_bool = false, cutLayout_10_bool = false, cutLayout_11_bool = false, cutLayout_12_bool = false,
            gal_upload_1_bool = false, gal_upload_2_bool = false, gal_upload_3_bool = false, gal_upload_4_bool = false, gal_upload_5_bool = false, gal_upload_6_bool = false, gal_upload_7_bool = false, gal_upload_8_bool = false, gal_upload_9_bool = false, gal_upload_10_bool = false, gal_upload_11_bool = false, gal_upload_12_bool = false;
    float dX, dY, brightness = 9, gradXF, gradYF;

    int lastAction, con_bw = 1, colorId1,
            img1_contrast_save = 100, img2_contrast_save = 100, img3_contrast_save = 100, img4_contrast_save = 100, img5_contrast_save = 100, img6_contrast_save = 100, img7_contrast_save = 100, img8_contrast_save = 100, img9_contrast_save = 100, img10_contrast_save = 100, img11_contrast_save = 100, img12_contrast_save = 100, theme_contrast_save = 100,
            img1_bw_save = 50, img2_bw_save = 50, img3_bw_save = 50, img4_bw_save = 50, img5_bw_save = 50, img6_bw_save = 50, img7_bw_save = 50, img8_bw_save = 50, img9_bw_save = 50, img10_bw_save = 50, img11_bw_save = 50, img12_bw_save = 50, theme_bw_save = 50,
            img1_opacity_save = 200, img2_opacity_save = 200, img3_opacity_save = 200, img4_opacity_save = 200, img5_opacity_save = 200, img6_opacity_save = 200, img7_opacity_save = 200, img8_opacity_save = 200, img9_opacity_save = 200, img10_opacity_save = 200, img11_opacity_save = 200, img12_opacity_save = 200, theme_opacity_save = 200,
            cutLayout_1_x_axis_save, cutLayout_2_x_axis_save, cutLayout_3_x_axis_save, cutLayout_4_x_axis_save, cutLayout_5_x_axis_save, cutLayout_6_x_axis_save, cutLayout_7_x_axis_save, cutLayout_8_x_axis_save, cutLayout_9_x_axis_save, cutLayout_10_x_axis_save, cutLayout_11_x_axis_save, cutLayout_12_x_axis_save,
            cutLayout_1_y_axis_save, cutLayout_2_y_axis_save, cutLayout_3_y_axis_save, cutLayout_4_y_axis_save, cutLayout_5_y_axis_save, cutLayout_6_y_axis_save, cutLayout_7_y_axis_save, cutLayout_8_y_axis_save, cutLayout_9_y_axis_save, cutLayout_10_y_axis_save, cutLayout_11_y_axis_save, cutLayout_12_y_axis_save,
            cutLayout_1_dx_axis_save, cutLayout_2_dx_axis_save, cutLayout_3_dx_axis_save, cutLayout_4_dx_axis_save, cutLayout_5_dx_axis_save, cutLayout_6_dx_axis_save, cutLayout_7_dx_axis_save, cutLayout_8_dx_axis_save, cutLayout_9_dx_axis_save, cutLayout_10_dx_axis_save, cutLayout_11_dx_axis_save, cutLayout_12_dx_axis_save,
            cutLayout_1_dy_axis_save, cutLayout_2_dy_axis_save, cutLayout_3_dy_axis_save, cutLayout_4_dy_axis_save, cutLayout_5_dy_axis_save, cutLayout_6_dy_axis_save, cutLayout_7_dy_axis_save, cutLayout_8_dy_axis_save, cutLayout_9_dy_axis_save, cutLayout_10_dy_axis_save, cutLayout_11_dy_axis_save, cutLayout_12_dy_axis_save,
            cutLayout_1_scale_save = 340, cutLayout_2_scale_save = 340, cutLayout_3_scale_save= 340, cutLayout_4_scale_save= 340, cutLayout_5_scale_save= 340, cutLayout_6_scale_save= 340, cutLayout_7_scale_save= 340, cutLayout_8_scale_save= 340, cutLayout_9_scale_save= 340, cutLayout_10_scale_save= 340, cutLayout_11_scale_save= 340, cutLayout_12_scale_save= 340;

    SeekBar x_axis_seek_obliq, y_axis_seek_obliq, distance_X_seek_obliq, distance_Y_seek_obliq, scale_seek, bWSeek, contrast, opacity, c_bg_grad_X_axis_seek, c_bg_grad_spacing_seek, c_bg_grad_Y_axis_seek, bg_grad_seek, bg_grad_seek2, bg_corner_seek;
    ColorDrawable g_bg_drw_1, g_bg_drw_2, g_bg_drw_3, g_bg_drw_4, g_bg_drw_5, g_bg_drw_6;
    RadioButton c_bg_linearRadio, c_bg_radialRadio, gradBG_Type_liner, gradBG_Type_sweep, grad_linearRadio, grad_radialRadio, radButt_mx_L_1, radButt_mx_L_2, radButt_mx_L_3, radButt_mx_L_multi, radButt_mx_L_4;
    RadioGroup c_bg_group, gradBG_Type_grp;
    int c_bg_rad_x=20, c_bg_rad_y=20, c_bg_rad_s=100, bg_GRAD_0_1 = 0, lin_grad_bg_x0 = 20, lin_grad_bg_x1 = 20, lin_grad_bg_y1 = 100;
    GradientDrawable gd;
    int screen_height, screen_width, gradType = 0, getViewColo, colorId2, grad_colorId_1, grad_colorId_2, grad_colorId_3, grad_colorId_4, grad_colorId_5, grad_colorId_6;
    RelativeLayout.LayoutParams paramsRL, params_cutLay;
    // these matrices will be used to move and zoom image
    private Matrix matrix = new Matrix();
    private Matrix savedMatrix = new Matrix();
    // we can be in one of these 3 states
    private static final int NONE = 0;
    private static final int DRAG = 1;
    private static final int ZOOM = 2;
    private int mode = NONE;
    // remember some things for zooming
    private PointF start = new PointF();
    private PointF mid = new PointF();
    private float oldDist = 1f;
    private float d = 0f;
    private float newRot = 0f;
    private float[] lastEvent = null;
    ImageView user_img;
    private boolean checkInternet;
    RecyclerView fest_title_recycler, imagesHoriRecycler, emoji_recycler_titles, emoji_recycler_images;
    LinearLayoutManager linearLayoutManager, linearLayoutManager2, linearLayoutManager_ST;
    FrameoFestivalTitleAdapter frameoFestivalTitleAdapter;
    ArrayList<FestivalTitlesModel> festTitleModel = new ArrayList<FestivalTitlesModel>();

    FrameoImagesHoriAdapter frameoImagesHoriAdapter;
    ArrayList<ImagesHoriModel> imagesHoriModel = new ArrayList<ImagesHoriModel>();

    EmojiTitleAdapter_Frameo emojiTitleAdapter;
    ArrayList<EmojiTitlesModel> emojiTitlesModels = new ArrayList<EmojiTitlesModel>();

    GridLayoutManager gridLayoutManager;
    EmojiImagesAdapter_Frameo emojiImagesAdapter;
    ArrayList<EmojiImagesModel> emojiImagesModels = new ArrayList<EmojiImagesModel>();

    CutLayout cutLayout_1, cutLayout_2, cutLayout_3, cutLayout_4, cutLayout_5, cutLayout_6, cutLayout_7, cutLayout_8, cutLayout_9, cutLayout_10, cutLayout_11, cutLayout_12;
    ImageView imgV_1, imgV_2, imgV_3, imgV_4, imgV_5, imgV_6, imgV_7, imgV_8, imgV_9, imgV_10, imgV_11, imgV_12;
    BottomSheetBehavior mBottomSheetBehavior;


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frameo);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Bundle bundle = getIntent().getExtras();
        EDIT_CAT_ID = bundle.getString("EDIT_CAT_ID");

        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        screen_width = displayMetrics.widthPixels;
        screen_height = displayMetrics.heightPixels;

        contrast = (SeekBar) findViewById(R.id.contrast);
        bWSeek = (SeekBar) findViewById(R.id.bWSeek);
        opacity = (SeekBar) findViewById(R.id.opacity);
        c_bg_grad_spacing_seek = (SeekBar) findViewById(R.id.c_bg_grad_spacing_seek);


        bg_view = (View) findViewById(R.id.bg_view);
        bg_view.setOnTouchListener(this);
        c_bg_v1 = (View) findViewById(R.id.c_bg_v1);
        c_bg_v1.setOnTouchListener(this);
        c_bg_v2 = (View) findViewById(R.id.c_bg_v2);
        c_bg_v2.setOnTouchListener(this);

        gradBG_Type_liner = (RadioButton) findViewById(R.id.gradBG_Type_liner);
        gradBG_Type_sweep = (RadioButton) findViewById(R.id.gradBG_Type_sweep);

        c_bg_radialRadio = (RadioButton) findViewById(R.id.c_bg_radialRadio);
        c_bg_linearRadio = (RadioButton) findViewById(R.id.c_bg_linearRadio);

        pencil_ic_img = (ImageView) findViewById(R.id.pencil_ic_img);
        pencil_ic_img.setOnTouchListener(this);
        emojiButt = (ImageView) findViewById(R.id.emojiButt);
        emojiButt.setOnClickListener(this);
        linear_gd_po_L = (ImageView) findViewById(R.id.linear_gd_po_L);
        linear_gd_po_L.setOnClickListener(this);
        linear_gd_po_TL = (ImageView) findViewById(R.id.linear_gd_po_TL);
        linear_gd_po_TL.setOnClickListener(this);
        linear_gd_po_T = (ImageView) findViewById(R.id.linear_gd_po_T);
        linear_gd_po_T.setOnClickListener(this);
        linear_gd_po_TR = (ImageView) findViewById(R.id.linear_gd_po_TR);
        linear_gd_po_TR.setOnClickListener(this);
        linear_gd_po_R = (ImageView) findViewById(R.id.linear_gd_po_R);
        linear_gd_po_R.setOnClickListener(this);
        linear_gd_po_BR = (ImageView) findViewById(R.id.linear_gd_po_BR);
        linear_gd_po_BR.setOnClickListener(this);
        linear_gd_po_B = (ImageView) findViewById(R.id.linear_gd_po_B);
        linear_gd_po_B.setOnClickListener(this);
        linear_gd_po_BL = (ImageView) findViewById(R.id.linear_gd_po_BL);
        linear_gd_po_BL.setOnClickListener(this);

        View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        pencil_ic_img.setRotation(0);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        pencil_ic_img.setRotation(180);
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        style_1_Butt_line = (View) findViewById(R.id.style_1_Butt_line);
        style_2_Butt_line = (View) findViewById(R.id.style_2_Butt_line);
        grad_c1 = (View) findViewById(R.id.grad_c1);
        grad_c1.setOnTouchListener(this);
        grad_c2 = (View) findViewById(R.id.grad_c2);
        grad_c2.setOnTouchListener(this);
        grad_c3 = (View) findViewById(R.id.grad_c3);
        grad_c3.setOnTouchListener(this);
        grad_c4 = (View) findViewById(R.id.grad_c4);
        grad_c4.setOnTouchListener(this);
        grad_c5 = (View) findViewById(R.id.grad_c5);
        grad_c5.setOnTouchListener(this);
        grad_c6 = (View) findViewById(R.id.grad_c6);
        grad_c6.setOnTouchListener(this);

        c1 = (View) findViewById(R.id.c1);
        c1.setOnClickListener(this);
        c2 = (View) findViewById(R.id.c2);
        c2.setOnClickListener(this);
        c3 = (View) findViewById(R.id.c3);
        c3.setOnClickListener(this);
        c4 = (View) findViewById(R.id.c4);
        c4.setOnClickListener(this);
        c5 = (View) findViewById(R.id.c5);
        c5.setOnClickListener(this);
        c6 = (View) findViewById(R.id.c6);
        c6.setOnClickListener(this);
        c7 = (View) findViewById(R.id.c7);
        c7.setOnClickListener(this);
        c8 = (View) findViewById(R.id.c8);
        c8.setOnClickListener(this);
        c9 = (View) findViewById(R.id.c9);
        c9.setOnClickListener(this);
        black_c = (View) findViewById(R.id.black_c);
        black_c.setOnClickListener(this);
        white_c = (View) findViewById(R.id.white_c);
        white_c.setOnClickListener(this);
        red_c = (View) findViewById(R.id.red_c);
        red_c.setOnClickListener(this);
        purple_c = (View) findViewById(R.id.purple_c);
        purple_c.setOnClickListener(this);
        pink_c = (View) findViewById(R.id.pink_c);
        pink_c.setOnClickListener(this);
        deepPurple_c = (View) findViewById(R.id.deepPurple_c);
        deepPurple_c.setOnClickListener(this);
        indigo_c = (View) findViewById(R.id.indigo_c);
        indigo_c.setOnClickListener(this);
        blue_c = (View) findViewById(R.id.blue_c);
        blue_c.setOnClickListener(this);
        cyan_c = (View) findViewById(R.id.cyan_c);
        cyan_c.setOnClickListener(this);
        lightBlue_c = (View) findViewById(R.id.lightBlue_c);
        lightBlue_c.setOnClickListener(this);
        teal_c = (View) findViewById(R.id.teal_c);
        teal_c.setOnClickListener(this);
        green_c = (View) findViewById(R.id.green_c);
        green_c.setOnClickListener(this);
        lightGreen_c = (View) findViewById(R.id.lightGreen_c);
        lightGreen_c.setOnClickListener(this);
        lime_c = (View) findViewById(R.id.lime_c);
        lime_c.setOnClickListener(this);
        yellow_c = (View) findViewById(R.id.yellow_c);
        yellow_c.setOnClickListener(this);
        amber_c = (View) findViewById(R.id.amber_c);
        amber_c.setOnClickListener(this);
        orange_c = (View) findViewById(R.id.orange_c);
        orange_c.setOnClickListener(this);
        deepOrange_c = (View) findViewById(R.id.deepOrange_c);
        deepOrange_c.setOnClickListener(this);
        brown_c = (View) findViewById(R.id.brown_c);
        brown_c.setOnClickListener(this);
        grey_c = (View) findViewById(R.id.grey_c);
        grey_c.setOnClickListener(this);
        blueGrey_c = (View) findViewById(R.id.blueGrey_c);
        blueGrey_c.setOnClickListener(this);
        c1.setBackgroundResource(R.color.dep_orange_p1);
        c2.setBackgroundResource(R.color.dep_orange_p2);
        c3.setBackgroundResource(R.color.dep_orange_p3);
        c4.setBackgroundResource(R.color.dep_orange_p4);
        c5.setBackgroundResource(R.color.dep_orange_p5);
        c6.setBackgroundResource(R.color.dep_orange_p6);
        c7.setBackgroundResource(R.color.dep_orange_p7);
        c8.setBackgroundResource(R.color.dep_orange_p8);
        c9.setBackgroundResource(R.color.dep_orange_p9);

        unlockAll_butt = (TextView) findViewById(R.id.unlockAll_butt);
        unlockAll_butt.setOnClickListener(this);
        emoji_edt_Butt = (TextView) findViewById(R.id.emoji_edt_Butt);
        emoji_edt_Butt.setOnClickListener(this);
        outputButt = (TextView) findViewById(R.id.outputButt);
        outputButt.setOnClickListener(this);
        img_gal_1 = (ImageView) findViewById(R.id.img_gal_1);
        img_gal_1.setOnClickListener(this);
        img_gal_1.setClickable(false);
        img_gal_2 = (ImageView) findViewById(R.id.img_gal_2);
        img_gal_2.setOnClickListener(this);
        img_gal_2.setClickable(false);
        img_gal_3 = (ImageView) findViewById(R.id.img_gal_3);
        img_gal_3.setOnClickListener(this);
        img_gal_3.setClickable(false);
        img_gal_4 = (ImageView) findViewById(R.id.img_gal_4);
        img_gal_4.setOnClickListener(this);
        img_gal_4.setClickable(false);
        img_gal_5 = (ImageView) findViewById(R.id.img_gal_5);
        img_gal_5.setOnClickListener(this);
        img_gal_5.setClickable(false);
        img_gal_6 = (ImageView) findViewById(R.id.img_gal_6);
        img_gal_6.setOnClickListener(this);
        img_gal_6.setClickable(false);
        img_gal_7 = (ImageView) findViewById(R.id.img_gal_7);
        img_gal_7.setOnClickListener(this);
        img_gal_7.setClickable(false);
        img_gal_8 = (ImageView) findViewById(R.id.img_gal_8);
        img_gal_8.setOnClickListener(this);
        img_gal_8.setClickable(false);
        img_gal_9 = (ImageView) findViewById(R.id.img_gal_9);
        img_gal_9.setOnClickListener(this);
        img_gal_9.setClickable(false);
        img_gal_10 = (ImageView) findViewById(R.id.img_gal_10);
        img_gal_10.setOnClickListener(this);
        img_gal_10.setClickable(false);
        img_gal_11 = (ImageView) findViewById(R.id.img_gal_11);
        img_gal_11.setOnClickListener(this);
        img_gal_11.setClickable(false);
        img_gal_12 = (ImageView) findViewById(R.id.img_gal_12);
        img_gal_12.setOnClickListener(this);
        img_gal_12.setClickable(false);

        gal_upload_1 = (ImageView) findViewById(R.id.gal_upload_1);
        gal_upload_1.setOnClickListener(this);
        gal_upload_1.setColorFilter(Color.WHITE);
        gal_upload_2 = (ImageView) findViewById(R.id.gal_upload_2);
        gal_upload_2.setOnClickListener(this);
        gal_upload_2.setColorFilter(Color.WHITE);
        gal_upload_3 = (ImageView) findViewById(R.id.gal_upload_3);
        gal_upload_3.setOnClickListener(this);
        gal_upload_3.setColorFilter(Color.WHITE);
        gal_upload_4 = (ImageView) findViewById(R.id.gal_upload_4);
        gal_upload_4.setOnClickListener(this);
        gal_upload_4.setColorFilter(Color.WHITE);
        gal_upload_5 = (ImageView) findViewById(R.id.gal_upload_5);
        gal_upload_5.setOnClickListener(this);
        gal_upload_5.setColorFilter(Color.WHITE);
        gal_upload_6 = (ImageView) findViewById(R.id.gal_upload_6);
        gal_upload_6.setOnClickListener(this);
        gal_upload_6.setColorFilter(Color.WHITE);
        gal_upload_7 = (ImageView) findViewById(R.id.gal_upload_7);
        gal_upload_7.setOnClickListener(this);
        gal_upload_7.setColorFilter(Color.WHITE);
        gal_upload_8 = (ImageView) findViewById(R.id.gal_upload_8);
        gal_upload_8.setOnClickListener(this);
        gal_upload_8.setColorFilter(Color.WHITE);
        gal_upload_9 = (ImageView) findViewById(R.id.gal_upload_9);
        gal_upload_9.setOnClickListener(this);
        gal_upload_9.setColorFilter(Color.WHITE);
        gal_upload_10 = (ImageView) findViewById(R.id.gal_upload_10);
        gal_upload_10.setOnClickListener(this);
        gal_upload_10.setColorFilter(Color.WHITE);
        gal_upload_11 = (ImageView) findViewById(R.id.gal_upload_11);
        gal_upload_11.setOnClickListener(this);
        gal_upload_11.setColorFilter(Color.WHITE);
        gal_upload_12 = (ImageView) findViewById(R.id.gal_upload_12);
        gal_upload_12.setOnClickListener(this);
        gal_upload_12.setColorFilter(Color.WHITE);

        gal_1_visi_ic = (ImageView) findViewById(R.id.gal_1_visi_ic);
        gal_1_visi_ic.setOnClickListener(this);
        gal_2_visi_ic = (ImageView) findViewById(R.id.gal_2_visi_ic);
        gal_2_visi_ic.setOnClickListener(this);
        gal_3_visi_ic = (ImageView) findViewById(R.id.gal_3_visi_ic);
        gal_3_visi_ic.setOnClickListener(this);
        gal_4_visi_ic = (ImageView) findViewById(R.id.gal_4_visi_ic);
        gal_4_visi_ic.setOnClickListener(this);
        gal_5_visi_ic = (ImageView) findViewById(R.id.gal_5_visi_ic);
        gal_5_visi_ic.setOnClickListener(this);
        gal_6_visi_ic = (ImageView) findViewById(R.id.gal_6_visi_ic);
        gal_6_visi_ic.setOnClickListener(this);
        gal_7_visi_ic = (ImageView) findViewById(R.id.gal_7_visi_ic);
        gal_7_visi_ic.setOnClickListener(this);
        gal_8_visi_ic = (ImageView) findViewById(R.id.gal_8_visi_ic);
        gal_8_visi_ic.setOnClickListener(this);
        gal_9_visi_ic = (ImageView) findViewById(R.id.gal_9_visi_ic);
        gal_9_visi_ic.setOnClickListener(this);
        gal_10_visi_ic = (ImageView) findViewById(R.id.gal_10_visi_ic);
        gal_10_visi_ic.setOnClickListener(this);
        gal_11_visi_ic = (ImageView) findViewById(R.id.gal_11_visi_ic);
        gal_11_visi_ic.setOnClickListener(this);
        gal_12_visi_ic = (ImageView) findViewById(R.id.gal_12_visi_ic);
        gal_12_visi_ic.setOnClickListener(this);

        gal_1_Invisi_ic = (ImageView) findViewById(R.id.gal_1_Invisi_ic);
        gal_1_Invisi_ic.setOnClickListener(this);
        gal_2_Invisi_ic = (ImageView) findViewById(R.id.gal_2_Invisi_ic);
        gal_2_Invisi_ic.setOnClickListener(this);
        gal_3_Invisi_ic = (ImageView) findViewById(R.id.gal_3_Invisi_ic);
        gal_3_Invisi_ic.setOnClickListener(this);
        gal_4_Invisi_ic = (ImageView) findViewById(R.id.gal_4_Invisi_ic);
        gal_4_Invisi_ic.setOnClickListener(this);
        gal_5_Invisi_ic = (ImageView) findViewById(R.id.gal_5_Invisi_ic);
        gal_5_Invisi_ic.setOnClickListener(this);
        gal_6_Invisi_ic = (ImageView) findViewById(R.id.gal_6_Invisi_ic);
        gal_6_Invisi_ic.setOnClickListener(this);
        gal_7_Invisi_ic = (ImageView) findViewById(R.id.gal_7_Invisi_ic);
        gal_7_Invisi_ic.setOnClickListener(this);
        gal_8_Invisi_ic = (ImageView) findViewById(R.id.gal_8_Invisi_ic);
        gal_8_Invisi_ic.setOnClickListener(this);
        gal_9_Invisi_ic = (ImageView) findViewById(R.id.gal_9_Invisi_ic);
        gal_9_Invisi_ic.setOnClickListener(this);
        gal_10_Invisi_ic = (ImageView) findViewById(R.id.gal_10_Invisi_ic);
        gal_10_Invisi_ic.setOnClickListener(this);
        gal_11_Invisi_ic = (ImageView) findViewById(R.id.gal_11_Invisi_ic);
        gal_11_Invisi_ic.setOnClickListener(this);
        gal_12_Invisi_ic = (ImageView) findViewById(R.id.gal_12_Invisi_ic);
        gal_12_Invisi_ic.setOnClickListener(this);


        gal_1_lockIn_ic = (ImageView) findViewById(R.id.gal_1_lockIn_ic);
        gal_1_lockIn_ic.setOnClickListener(this);
        gal_2_lockIn_ic = (ImageView) findViewById(R.id.gal_2_lockIn_ic);
        gal_2_lockIn_ic.setOnClickListener(this);
        gal_3_lockIn_ic = (ImageView) findViewById(R.id.gal_3_lockIn_ic);
        gal_3_lockIn_ic.setOnClickListener(this);
        gal_4_lockIn_ic = (ImageView) findViewById(R.id.gal_4_lockIn_ic);
        gal_4_lockIn_ic.setOnClickListener(this);
        gal_5_lockIn_ic = (ImageView) findViewById(R.id.gal_5_lockIn_ic);
        gal_5_lockIn_ic.setOnClickListener(this);
        gal_6_lockIn_ic = (ImageView) findViewById(R.id.gal_6_lockIn_ic);
        gal_6_lockIn_ic.setOnClickListener(this);
        gal_7_lockIn_ic = (ImageView) findViewById(R.id.gal_7_lockIn_ic);
        gal_7_lockIn_ic.setOnClickListener(this);
        gal_8_lockIn_ic = (ImageView) findViewById(R.id.gal_8_lockIn_ic);
        gal_8_lockIn_ic.setOnClickListener(this);
        gal_9_lockIn_ic = (ImageView) findViewById(R.id.gal_9_lockIn_ic);
        gal_9_lockIn_ic.setOnClickListener(this);
        gal_10_lockIn_ic = (ImageView) findViewById(R.id.gal_10_lockIn_ic);
        gal_10_lockIn_ic.setOnClickListener(this);
        gal_11_lockIn_ic = (ImageView) findViewById(R.id.gal_11_lockIn_ic);
        gal_11_lockIn_ic.setOnClickListener(this);
        gal_12_lockIn_ic = (ImageView) findViewById(R.id.gal_12_lockIn_ic);
        gal_12_lockIn_ic.setOnClickListener(this);


        gal_1_lockOut_ic = (ImageView) findViewById(R.id.gal_1_lockOut_ic);
        gal_1_lockOut_ic.setOnClickListener(this);
        gal_2_lockOut_ic = (ImageView) findViewById(R.id.gal_2_lockOut_ic);
        gal_2_lockOut_ic.setOnClickListener(this);
        gal_3_lockOut_ic = (ImageView) findViewById(R.id.gal_3_lockOut_ic);
        gal_3_lockOut_ic.setOnClickListener(this);
        gal_4_lockOut_ic = (ImageView) findViewById(R.id.gal_4_lockOut_ic);
        gal_4_lockOut_ic.setOnClickListener(this);
        gal_5_lockOut_ic = (ImageView) findViewById(R.id.gal_5_lockOut_ic);
        gal_5_lockOut_ic.setOnClickListener(this);
        gal_6_lockOut_ic = (ImageView) findViewById(R.id.gal_6_lockOut_ic);
        gal_6_lockOut_ic.setOnClickListener(this);
        gal_7_lockOut_ic = (ImageView) findViewById(R.id.gal_7_lockOut_ic);
        gal_7_lockOut_ic.setOnClickListener(this);
        gal_8_lockOut_ic = (ImageView) findViewById(R.id.gal_8_lockOut_ic);
        gal_8_lockOut_ic.setOnClickListener(this);
        gal_9_lockOut_ic = (ImageView) findViewById(R.id.gal_9_lockOut_ic);
        gal_9_lockOut_ic.setOnClickListener(this);
        gal_10_lockOut_ic = (ImageView) findViewById(R.id.gal_10_lockOut_ic);
        gal_10_lockOut_ic.setOnClickListener(this);
        gal_11_lockOut_ic = (ImageView) findViewById(R.id.gal_11_lockOut_ic);
        gal_11_lockOut_ic.setOnClickListener(this);
        gal_12_lockOut_ic = (ImageView) findViewById(R.id.gal_12_lockOut_ic);
        gal_12_lockOut_ic.setOnClickListener(this);


        gal_x_1 = (ImageView) findViewById(R.id.gal_x_1);
        gal_x_1.setOnClickListener(this);
        gal_x_2 = (ImageView) findViewById(R.id.gal_x_2);
        gal_x_2.setOnClickListener(this);
        gal_x_3 = (ImageView) findViewById(R.id.gal_x_3);
        gal_x_3.setOnClickListener(this);
        gal_x_4 = (ImageView) findViewById(R.id.gal_x_4);
        gal_x_4.setOnClickListener(this);
        gal_x_5 = (ImageView) findViewById(R.id.gal_x_5);
        gal_x_5.setOnClickListener(this);
        gal_x_6 = (ImageView) findViewById(R.id.gal_x_6);
        gal_x_6.setOnClickListener(this);
        gal_x_7 = (ImageView) findViewById(R.id.gal_x_7);
        gal_x_7.setOnClickListener(this);
        gal_x_8 = (ImageView) findViewById(R.id.gal_x_8);
        gal_x_8.setOnClickListener(this);
        gal_x_9 = (ImageView) findViewById(R.id.gal_x_9);
        gal_x_9.setOnClickListener(this);
        gal_x_10 = (ImageView) findViewById(R.id.gal_x_10);
        gal_x_10.setOnClickListener(this);
        gal_x_11 = (ImageView) findViewById(R.id.gal_x_11);
        gal_x_11.setOnClickListener(this);
        gal_x_12 = (ImageView) findViewById(R.id.gal_x_12);
        gal_x_12.setOnClickListener(this);

        gal_up_1 = (ImageView) findViewById(R.id.gal_up_1);
        gal_up_1.setOnClickListener(this);
        gal_up_2 = (ImageView) findViewById(R.id.gal_up_2);
        gal_up_2.setOnClickListener(this);
        gal_up_3 = (ImageView) findViewById(R.id.gal_up_3);
        gal_up_3.setOnClickListener(this);
        gal_up_4 = (ImageView) findViewById(R.id.gal_up_4);
        gal_up_4.setOnClickListener(this);
        gal_up_5 = (ImageView) findViewById(R.id.gal_up_5);
        gal_up_5.setOnClickListener(this);
        gal_up_6 = (ImageView) findViewById(R.id.gal_up_6);
        gal_up_6.setOnClickListener(this);
        gal_up_7 = (ImageView) findViewById(R.id.gal_up_7);
        gal_up_7.setOnClickListener(this);
        gal_up_8 = (ImageView) findViewById(R.id.gal_up_8);
        gal_up_8.setOnClickListener(this);
        gal_up_9 = (ImageView) findViewById(R.id.gal_up_9);
        gal_up_9.setOnClickListener(this);
        gal_up_10 = (ImageView) findViewById(R.id.gal_up_10);
        gal_up_10.setOnClickListener(this);
        gal_up_11 = (ImageView) findViewById(R.id.gal_up_11);
        gal_up_11.setOnClickListener(this);
        gal_up_12 = (ImageView) findViewById(R.id.gal_up_12);
        gal_up_12.setOnClickListener(this);
        emoji_visi_ic = (ImageView) findViewById(R.id.emoji_visi_ic);
        emoji_visi_ic.setOnClickListener(this);
        emoji_Invisi_ic = (ImageView) findViewById(R.id.emoji_Invisi_ic);
        emoji_Invisi_ic.setOnClickListener(this);
        emoji_lockIn_ic = (ImageView) findViewById(R.id.emoji_lockIn_ic);
        emoji_lockIn_ic.setOnClickListener(this);
        emoji_lockOut_ic = (ImageView) findViewById(R.id.emoji_lockOut_ic);
        emoji_lockOut_ic.setOnClickListener(this);


        bg_color_tool = (LinearLayout) findViewById(R.id.bg_color_tool);
        emoji_tool_ll = (LinearLayout) findViewById(R.id.emoji_tool_ll);
        emojiButt2 = (LinearLayout) findViewById(R.id.emojiButt2);
        emojiButt2.setOnClickListener(this);
        gal_1_visi_LL = (LinearLayout) findViewById(R.id.gal_1_visi_LL);
        gal_1_visi_LL.setOnClickListener(this);
        gal_2_visi_LL = (LinearLayout) findViewById(R.id.gal_2_visi_LL);
        gal_2_visi_LL.setOnClickListener(this);
        gal_3_visi_LL = (LinearLayout) findViewById(R.id.gal_3_visi_LL);
        gal_3_visi_LL.setOnClickListener(this);
        gal_4_visi_LL = (LinearLayout) findViewById(R.id.gal_4_visi_LL);
        gal_4_visi_LL.setOnClickListener(this);
        gal_5_visi_LL = (LinearLayout) findViewById(R.id.gal_5_visi_LL);
        gal_5_visi_LL.setOnClickListener(this);
        gal_6_visi_LL = (LinearLayout) findViewById(R.id.gal_6_visi_LL);
        gal_6_visi_LL.setOnClickListener(this);
        gal_7_visi_LL = (LinearLayout) findViewById(R.id.gal_7_visi_LL);
        gal_7_visi_LL.setOnClickListener(this);
        gal_8_visi_LL = (LinearLayout) findViewById(R.id.gal_8_visi_LL);
        gal_8_visi_LL.setOnClickListener(this);
        gal_9_visi_LL = (LinearLayout) findViewById(R.id.gal_9_visi_LL);
        gal_9_visi_LL.setOnClickListener(this);
        gal_10_visi_LL = (LinearLayout) findViewById(R.id.gal_10_visi_LL);
        gal_10_visi_LL.setOnClickListener(this);
        gal_11_visi_LL = (LinearLayout) findViewById(R.id.gal_11_visi_LL);
        gal_11_visi_LL.setOnClickListener(this);
        gal_12_visi_LL = (LinearLayout) findViewById(R.id.gal_12_visi_LL);
        gal_12_visi_LL.setOnClickListener(this);

        gal_1_lock_LL = (LinearLayout) findViewById(R.id.gal_1_lock_LL);
        gal_1_lock_LL.setOnClickListener(this);
        gal_2_lock_LL = (LinearLayout) findViewById(R.id.gal_2_lock_LL);
        gal_2_lock_LL.setOnClickListener(this);
        gal_3_lock_LL = (LinearLayout) findViewById(R.id.gal_3_lock_LL);
        gal_3_lock_LL.setOnClickListener(this);
        gal_4_lock_LL = (LinearLayout) findViewById(R.id.gal_4_lock_LL);
        gal_4_lock_LL.setOnClickListener(this);
        gal_5_lock_LL = (LinearLayout) findViewById(R.id.gal_5_lock_LL);
        gal_5_lock_LL.setOnClickListener(this);
        gal_6_lock_LL = (LinearLayout) findViewById(R.id.gal_6_lock_LL);
        gal_6_lock_LL.setOnClickListener(this);
        gal_7_lock_LL = (LinearLayout) findViewById(R.id.gal_7_lock_LL);
        gal_7_lock_LL.setOnClickListener(this);
        gal_8_lock_LL = (LinearLayout) findViewById(R.id.gal_8_lock_LL);
        gal_8_lock_LL.setOnClickListener(this);
        gal_9_lock_LL = (LinearLayout) findViewById(R.id.gal_9_lock_LL);
        gal_9_lock_LL.setOnClickListener(this);
        gal_10_lock_LL = (LinearLayout) findViewById(R.id.gal_10_lock_LL);
        gal_10_lock_LL.setOnClickListener(this);
        gal_11_lock_LL = (LinearLayout) findViewById(R.id.gal_11_lock_LL);
        gal_11_lock_LL.setOnClickListener(this);
        gal_12_lock_LL = (LinearLayout) findViewById(R.id.gal_12_lock_LL);
        gal_12_lock_LL.setOnClickListener(this);

        style2_txt = (TextView) findViewById(R.id.style2_txt);
        style1_txt = (TextView) findViewById(R.id.style1_txt);
        gallery_Butt = (TextView) findViewById(R.id.gallery_Butt);
        gallery_Butt.setOnClickListener(this);
        photoEdit_butt = (TextView) findViewById(R.id.photoEdit_butt);
        photoEdit_butt.setOnClickListener(this);
        bg_Butt = (TextView) findViewById(R.id.bg_Butt);
        bg_Butt.setOnClickListener(this);
        theme_Butt = (TextView) findViewById(R.id.theme_Butt);
        theme_Butt.setOnClickListener(this);
        bg_multi_colo = (TextView) findViewById(R.id.bg_multi_colo);
        bg_multi_colo.setOnClickListener(this);


        bg_visi_ic = (ImageView) findViewById(R.id.bg_visi_ic);
        bg_visi_ic.setOnClickListener(this);
        bg_Invisi_ic = (ImageView) findViewById(R.id.bg_Invisi_ic);
        bg_Invisi_ic.setOnClickListener(this);
        theme_visi_ic = (ImageView) findViewById(R.id.theme_visi_ic);
        theme_visi_ic.setOnClickListener(this);
        theme_Invisi_ic = (ImageView) findViewById(R.id.theme_Invisi_ic);
        theme_Invisi_ic.setOnClickListener(this);
        theme_lockIn_ic = (ImageView) findViewById(R.id.theme_lockIn_ic);
        theme_lockIn_ic.setOnClickListener(this);
        theme_lockOut_ic = (ImageView) findViewById(R.id.theme_lockOut_ic);
        theme_lockOut_ic.setOnClickListener(this);

        flip_right = (ImageView) findViewById(R.id.flip_right);
        flip_right.setOnClickListener(this);
        flip_down = (ImageView) findViewById(R.id.flip_down);
        flip_down.setOnClickListener(this);

        theme_internet_LL = (RelativeLayout) findViewById(R.id.theme_internet_LL);
        internet_txt_LL = (LinearLayout) findViewById(R.id.internet_txt_LL);
        internet_txt_LL.setOnClickListener(this);

        bg_grad_selectors_LL = (LinearLayout) findViewById(R.id.bg_grad_selectors_LL);


        ll1 = (LinearLayout) findViewById(R.id.ll1);
        ll2 = (LinearLayout) findViewById(R.id.ll2);
        ll_angle = (LinearLayout) findViewById(R.id.ll_angle);
        ll_St_1 = (LinearLayout) findViewById(R.id.ll_St_1);
        bgGrad_LL = (LinearLayout) findViewById(R.id.bgGrad_LL);
        theme_tab_ll = (LinearLayout) findViewById(R.id.theme_tab_ll);
        bg_grad_Tools_ll = (LinearLayout) findViewById(R.id.bg_grad_Tools_ll);
        grad_bg_style1_ll = (LinearLayout) findViewById(R.id.grad_bg_style1_ll);
        grad_bg_style2_ll = (LinearLayout) findViewById(R.id.grad_bg_style2_ll);
        style1_LL = (LinearLayout) findViewById(R.id.style1_LL);
        style2_LL = (LinearLayout) findViewById(R.id.style2_LL);
        style_1_Butt = (LinearLayout) findViewById(R.id.style_1_Butt);
        style_1_Butt.setOnClickListener(this);
        style_2_Butt = (LinearLayout) findViewById(R.id.style_2_Butt);
        style_2_Butt.setOnClickListener(this);
        gallery_tab_ll = (LinearLayout) findViewById(R.id.gallery_tab_ll);
        photoEdit_tab_ll = (LinearLayout) findViewById(R.id.photoEdit_tab_ll);
        bg_tab_ll = (LinearLayout) findViewById(R.id.bg_tab_ll);
        photoEdit_tool_ll = (LinearLayout) findViewById(R.id.photoEdit_tool_ll);
        img_color_adj_LL = (LinearLayout) findViewById(R.id.img_color_adj_LL);
        colors_ll = (LinearLayout) findViewById(R.id.colors_ll);
        gallery_view_ll = (LinearLayout) findViewById(R.id.gallery_view_ll);

        bottom_sheet = (LinearLayout) findViewById(R.id.bottom_sheet);
        font_ll = (LinearLayout) findViewById(R.id.font_ll);

        user_img = (ImageView) findViewById(R.id.user_img);

        img_theme = (ImageView) findViewById(R.id.img_theme);
        img_theme.setOnTouchListener(this);
        img_theme.setEnabled(false);

        rL = (RelativeLayout) findViewById(R.id.rL);
        rL.setOnTouchListener(this);

        x_axis_seek_obliq = (SeekBar) findViewById(R.id.x_axis_seek_obliq);
        y_axis_seek_obliq = (SeekBar) findViewById(R.id.y_axis_seek_obliq);
        distance_X_seek_obliq = (SeekBar) findViewById(R.id.distance_X_seek_obliq);
        distance_Y_seek_obliq = (SeekBar) findViewById(R.id.distance_Y_seek_obliq);


        x_axis_seek_obliq.setMax(screen_width);
        x_axis_seek_obliq.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (cutLayout_1_bool) {
                    cutLayout_1_x_axis_save = i;
                    cutLayout_1.invalidate();
                    cutLayout_1.setX_obliq(cutLayout_1_x_axis_save);
                } else if (cutLayout_2_bool) {
                    cutLayout_2_x_axis_save = i;
                    cutLayout_2.invalidate();
                    cutLayout_2.setX_obliq(cutLayout_2_x_axis_save);
                } else if (cutLayout_3_bool) {
                    cutLayout_3_x_axis_save = i;
                    cutLayout_3.invalidate();
                    cutLayout_3.setX_obliq(cutLayout_3_x_axis_save);
                } else if (cutLayout_4_bool) {
                    cutLayout_4_x_axis_save = i;
                    cutLayout_4.invalidate();
                    cutLayout_4.setX_obliq(cutLayout_4_x_axis_save);
                } else if (cutLayout_5_bool) {
                    cutLayout_5_x_axis_save = i;
                    cutLayout_5.invalidate();
                    cutLayout_5.setX_obliq(cutLayout_5_x_axis_save);
                } else if (cutLayout_6_bool) {
                    cutLayout_6_x_axis_save = i;
                    cutLayout_6.invalidate();
                    cutLayout_6.setX_obliq(cutLayout_6_x_axis_save);
                } else if (cutLayout_7_bool) {
                    cutLayout_7_x_axis_save = i;
                    cutLayout_7.invalidate();
                    cutLayout_7.setX_obliq(cutLayout_7_x_axis_save);
                } else if (cutLayout_8_bool) {
                    cutLayout_8_x_axis_save = i;
                    cutLayout_8.invalidate();
                    cutLayout_8.setX_obliq(cutLayout_8_x_axis_save);
                } else if (cutLayout_9_bool) {
                    cutLayout_9_x_axis_save = i;
                    cutLayout_9.invalidate();
                    cutLayout_9.setX_obliq(cutLayout_9_x_axis_save);
                } else if (cutLayout_10_bool) {
                    cutLayout_10_x_axis_save = i;
                    cutLayout_10.invalidate();
                    cutLayout_10.setX_obliq(cutLayout_10_x_axis_save);
                } else if (cutLayout_11_bool) {
                    cutLayout_11_x_axis_save = i;
                    cutLayout_11.invalidate();
                    cutLayout_11.setX_obliq(cutLayout_11_x_axis_save);
                } else if (cutLayout_12_bool) {
                    cutLayout_12_x_axis_save = i;
                    cutLayout_12.invalidate();
                    cutLayout_12.setX_obliq(cutLayout_12_x_axis_save);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        y_axis_seek_obliq.setMax(screen_width);
        y_axis_seek_obliq.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (cutLayout_1_bool) {
                    cutLayout_1_y_axis_save = i;
                    cutLayout_1.invalidate();
                    cutLayout_1.setY_obliq(cutLayout_1_y_axis_save);
                } else if (cutLayout_2_bool) {
                    cutLayout_2_y_axis_save = i;
                    cutLayout_2.invalidate();
                    cutLayout_2.setY_obliq(cutLayout_2_y_axis_save);
                } else if (cutLayout_3_bool) {
                    cutLayout_3_y_axis_save = i;
                    cutLayout_3.invalidate();
                    cutLayout_3.setY_obliq(cutLayout_3_y_axis_save);
                } else if (cutLayout_4_bool) {
                    cutLayout_4_y_axis_save = i;
                    cutLayout_4.invalidate();
                    cutLayout_4.setY_obliq(cutLayout_4_y_axis_save);
                } else if (cutLayout_5_bool) {
                    cutLayout_5_y_axis_save = i;
                    cutLayout_5.invalidate();
                    cutLayout_5.setY_obliq(cutLayout_5_y_axis_save);
                } else if (cutLayout_6_bool) {
                    cutLayout_6_y_axis_save = i;
                    cutLayout_6.invalidate();
                    cutLayout_6.setY_obliq(cutLayout_6_y_axis_save);
                } else if (cutLayout_7_bool) {
                    cutLayout_7_y_axis_save = i;
                    cutLayout_7.invalidate();
                    cutLayout_7.setY_obliq(cutLayout_7_y_axis_save);
                } else if (cutLayout_8_bool) {
                    cutLayout_8_y_axis_save = i;
                    cutLayout_8.invalidate();
                    cutLayout_8.setY_obliq(cutLayout_8_y_axis_save);
                } else if (cutLayout_9_bool) {
                    cutLayout_9_y_axis_save = i;
                    cutLayout_9.invalidate();
                    cutLayout_9.setY_obliq(cutLayout_9_y_axis_save);
                } else if (cutLayout_10_bool) {
                    cutLayout_10_y_axis_save = i;
                    cutLayout_10.invalidate();
                    cutLayout_10.setY_obliq(cutLayout_10_y_axis_save);
                } else if (cutLayout_11_bool) {
                    cutLayout_11_y_axis_save = i;
                    cutLayout_11.invalidate();
                    cutLayout_11.setY_obliq(cutLayout_11_y_axis_save);
                } else if (cutLayout_12_bool) {
                    cutLayout_12_y_axis_save = i;
                    cutLayout_12.invalidate();
                    cutLayout_12.setY_obliq(cutLayout_12_y_axis_save);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        distance_X_seek_obliq.setMax(screen_width);
        distance_X_seek_obliq.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (cutLayout_1_bool) {
                    cutLayout_1_dx_axis_save = i;
                    cutLayout_1.invalidate();
                    cutLayout_1.setDist_x_obliq(cutLayout_1_dx_axis_save);
                } else if (cutLayout_2_bool) {
                    cutLayout_2_dx_axis_save = i;
                    cutLayout_2.invalidate();
                    cutLayout_2.setDist_x_obliq(cutLayout_2_dx_axis_save);
                } else if (cutLayout_3_bool) {
                    cutLayout_3_dx_axis_save = i;
                    cutLayout_3.invalidate();
                    cutLayout_3.setDist_x_obliq(cutLayout_3_dx_axis_save);
                } else if (cutLayout_4_bool) {
                    cutLayout_4_dx_axis_save = i;
                    cutLayout_4.invalidate();
                    cutLayout_4.setDist_x_obliq(cutLayout_4_dx_axis_save);
                } else if (cutLayout_5_bool) {
                    cutLayout_5_dx_axis_save = i;
                    cutLayout_5.invalidate();
                    cutLayout_5.setDist_x_obliq(cutLayout_5_dx_axis_save);
                } else if (cutLayout_6_bool) {
                    cutLayout_6_dx_axis_save = i;
                    cutLayout_6.invalidate();
                    cutLayout_6.setDist_x_obliq(cutLayout_6_dx_axis_save);
                } else if (cutLayout_7_bool) {
                    cutLayout_7_dx_axis_save = i;
                    cutLayout_7.invalidate();
                    cutLayout_7.setDist_x_obliq(cutLayout_7_dx_axis_save);
                } else if (cutLayout_8_bool) {
                    cutLayout_8_dx_axis_save = i;
                    cutLayout_8.invalidate();
                    cutLayout_8.setDist_x_obliq(cutLayout_8_dx_axis_save);
                } else if (cutLayout_9_bool) {
                    cutLayout_9_dx_axis_save = i;
                    cutLayout_9.invalidate();
                    cutLayout_9.setDist_x_obliq(cutLayout_9_dx_axis_save);
                } else if (cutLayout_10_bool) {
                    cutLayout_10_dx_axis_save = i;
                    cutLayout_10.invalidate();
                    cutLayout_10.setDist_x_obliq(cutLayout_10_dx_axis_save);
                } else if (cutLayout_11_bool) {
                    cutLayout_11_dx_axis_save = i;
                    cutLayout_11.invalidate();
                    cutLayout_11.setDist_x_obliq(cutLayout_11_dx_axis_save);
                } else if (cutLayout_12_bool) {
                    cutLayout_12_dx_axis_save = i;
                    cutLayout_12.invalidate();
                    cutLayout_12.setDist_x_obliq(cutLayout_12_dx_axis_save);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        distance_Y_seek_obliq.setMax(screen_width);
        distance_Y_seek_obliq.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (cutLayout_1_bool) {
                    cutLayout_1_dy_axis_save = i;
                    cutLayout_1.invalidate();
                    cutLayout_1.setDist_y_obliq(cutLayout_1_dy_axis_save);
                } else if (cutLayout_2_bool) {
                    cutLayout_2_dy_axis_save = i;
                    cutLayout_2.invalidate();
                    cutLayout_2.setDist_y_obliq(cutLayout_2_dy_axis_save);
                } else if (cutLayout_3_bool) {
                    cutLayout_3_dy_axis_save = i;
                    cutLayout_3.invalidate();
                    cutLayout_3.setDist_y_obliq(cutLayout_3_dy_axis_save);
                } else if (cutLayout_4_bool) {
                    cutLayout_4_dy_axis_save = i;
                    cutLayout_4.invalidate();
                    cutLayout_4.setDist_y_obliq(cutLayout_4_dy_axis_save);
                } else if (cutLayout_5_bool) {
                    cutLayout_5_dy_axis_save = i;
                    cutLayout_5.invalidate();
                    cutLayout_5.setDist_y_obliq(cutLayout_5_dy_axis_save);
                } else if (cutLayout_6_bool) {
                    cutLayout_6_dy_axis_save = i;
                    cutLayout_6.invalidate();
                    cutLayout_6.setDist_y_obliq(cutLayout_6_dy_axis_save);
                } else if (cutLayout_7_bool) {
                    cutLayout_7_dy_axis_save = i;
                    cutLayout_7.invalidate();
                    cutLayout_7.setDist_y_obliq(cutLayout_7_dy_axis_save);
                } else if (cutLayout_8_bool) {
                    cutLayout_8_dy_axis_save = i;
                    cutLayout_8.invalidate();
                    cutLayout_8.setDist_y_obliq(cutLayout_8_dy_axis_save);
                } else if (cutLayout_9_bool) {
                    cutLayout_9_dy_axis_save = i;
                    cutLayout_9.invalidate();
                    cutLayout_9.setDist_y_obliq(cutLayout_9_dy_axis_save);
                } else if (cutLayout_10_bool) {
                    cutLayout_10_dy_axis_save = i;
                    cutLayout_10.invalidate();
                    cutLayout_10.setDist_y_obliq(cutLayout_10_dy_axis_save);
                } else if (cutLayout_11_bool) {
                    cutLayout_11_dy_axis_save = i;
                    cutLayout_11.invalidate();
                    cutLayout_11.setDist_y_obliq(cutLayout_11_dy_axis_save);
                } else if (cutLayout_12_bool) {
                    cutLayout_12_dy_axis_save = i;
                    cutLayout_12.invalidate();
                    cutLayout_12.setDist_y_obliq(cutLayout_12_dy_axis_save);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        scale_seek = (SeekBar) findViewById(R.id.scale_seek);
        scale_seek.setMax(screen_width);
        scale_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("ResourceType")
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(i, i);
                RelativeLayout.LayoutParams lpIMG = new RelativeLayout.LayoutParams(i, i);
                lp.addRule(RelativeLayout.CENTER_IN_PARENT);
                lpIMG.addRule(RelativeLayout.CENTER_IN_PARENT);

                if (cutLayout_1_bool) {
                    cutLayout_1_scale_save = i;
                    cutLayout_1.setLayoutParams(lp);
                    imgV_1.setLayoutParams(lpIMG);
                } else if (cutLayout_2_bool) {
                    cutLayout_2_scale_save = i;
                    cutLayout_2.setLayoutParams(lp);
                    imgV_2.setLayoutParams(lpIMG);
                } else if (cutLayout_3_bool) {
                    cutLayout_3_scale_save = i;
                    cutLayout_3.setLayoutParams(lp);
                    imgV_3.setLayoutParams(lpIMG);
                } else if (cutLayout_4_bool) {
                    cutLayout_4_scale_save = i;
                    cutLayout_4.setLayoutParams(lp);
                    imgV_4.setLayoutParams(lpIMG);
                } else if (cutLayout_5_bool) {
                    cutLayout_5_scale_save = i;
                    cutLayout_5.setLayoutParams(lp);
                    imgV_5.setLayoutParams(lpIMG);
                } else if (cutLayout_6_bool) {
                    cutLayout_6_scale_save = i;
                    cutLayout_6.setLayoutParams(lp);
                    imgV_6.setLayoutParams(lpIMG);
                } else if (cutLayout_7_bool) {
                    cutLayout_7_scale_save = i;
                    cutLayout_7.setLayoutParams(lp);
                    imgV_7.setLayoutParams(lpIMG);
                } else if (cutLayout_8_bool) {
                    cutLayout_8_scale_save = i;
                    cutLayout_8.setLayoutParams(lp);
                    imgV_8.setLayoutParams(lpIMG);
                } else if (cutLayout_9_bool) {
                    cutLayout_9_scale_save = i;
                    cutLayout_9.setLayoutParams(lp);
                    imgV_9.setLayoutParams(lpIMG);
                } else if (cutLayout_10_bool) {
                    cutLayout_10_scale_save = i;
                    cutLayout_10.setLayoutParams(lp);
                    imgV_10.setLayoutParams(lpIMG);
                } else if (cutLayout_11_bool) {
                    cutLayout_11_scale_save = i;
                    cutLayout_11.setLayoutParams(lp);
                    imgV_11.setLayoutParams(lpIMG);
                } else if (cutLayout_12_bool) {
                    cutLayout_12_scale_save = i;
                    cutLayout_12.setLayoutParams(lp);
                    imgV_12.setLayoutParams(lpIMG);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        contrast.setProgress(100);
        contrast.setMax(200);
        contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (cutLayout_1_bool) {
                    img1_contrast_save = progress;
                    imgV_1.setColorFilter(setContrast(img1_contrast_save));
                } else if (cutLayout_2_bool) {
                    img2_contrast_save = progress;
                    imgV_2.setColorFilter(setContrast(img2_contrast_save));
                } else if (cutLayout_3_bool) {
                    img3_contrast_save = progress;
                    imgV_3.setColorFilter(setContrast(img3_contrast_save));

                } else if (cutLayout_4_bool) {
                    img4_contrast_save = progress;
                    imgV_4.setColorFilter(setContrast(img4_contrast_save));

                } else if (cutLayout_5_bool) {
                    img5_contrast_save = progress;
                    imgV_5.setColorFilter(setContrast(img5_contrast_save));

                } else if (cutLayout_6_bool) {
                    img6_contrast_save = progress;
                    imgV_6.setColorFilter(setContrast(img6_contrast_save));

                } else if (cutLayout_7_bool) {
                    img7_contrast_save = progress;
                    imgV_7.setColorFilter(setContrast(img7_contrast_save));

                } else if (cutLayout_8_bool) {
                    img8_contrast_save = progress;
                    imgV_8.setColorFilter(setContrast(img8_contrast_save));

                } else if (cutLayout_9_bool) {
                    img9_contrast_save = progress;
                    imgV_9.setColorFilter(setContrast(img9_contrast_save));

                } else if (cutLayout_10_bool) {
                    img10_contrast_save = progress;
                    imgV_10.setColorFilter(setContrast(img10_contrast_save));

                } else if (cutLayout_11_bool) {
                    img11_contrast_save = progress;
                    imgV_11.setColorFilter(setContrast(img11_contrast_save));

                } else if (cutLayout_12_bool) {
                    img12_contrast_save = progress;
                    imgV_12.setColorFilter(setContrast(img12_contrast_save));

                } else if (theme_bool) {
                    theme_contrast_save = progress;
                    img_theme.setColorFilter(setContrast(img12_contrast_save));

                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (cutLayout_1_bool) {
                    con_bw = 1;
                } else if (cutLayout_2_bool) {
                    con_bw = 1;
                } else if (cutLayout_3_bool) {
                    con_bw = 1;
                } else if (cutLayout_4_bool) {
                    con_bw = 1;
                } else if (cutLayout_5_bool) {
                    con_bw = 1;
                } else if (cutLayout_6_bool) {
                    con_bw = 1;
                } else if (cutLayout_7_bool) {
                    con_bw = 1;
                } else if (cutLayout_8_bool) {
                    con_bw = 1;
                } else if (cutLayout_9_bool) {
                    con_bw = 1;
                } else if (cutLayout_10_bool) {
                    con_bw = 1;
                } else if (cutLayout_11_bool) {
                    con_bw = 1;
                } else if (cutLayout_12_bool) {
                    con_bw = 1;
                } else if (theme_bool) {
                    con_bw = 1;
                }

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        opacity.setMax(255);
        opacity.setProgress(200);
        opacity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (cutLayout_1_bool) {
                    img1_opacity_save = progress;
                    imgV_1.setImageAlpha(img1_opacity_save);

                } else if (cutLayout_2_bool) {
                    img2_opacity_save = progress;
                    imgV_2.setImageAlpha(img2_opacity_save);

                } else if (cutLayout_3_bool) {
                    img3_opacity_save = progress;
                    imgV_3.setImageAlpha(img3_opacity_save);

                } else if (cutLayout_4_bool) {
                    img4_opacity_save = progress;
                    imgV_4.setImageAlpha(img4_opacity_save);

                } else if (cutLayout_5_bool) {
                    img5_opacity_save = progress;
                    imgV_5.setImageAlpha(img5_opacity_save);

                } else if (cutLayout_6_bool) {
                    img6_opacity_save = progress;
                    imgV_6.setImageAlpha(img6_opacity_save);

                } else if (cutLayout_7_bool) {
                    img7_opacity_save = progress;
                    imgV_7.setImageAlpha(img7_opacity_save);

                } else if (cutLayout_8_bool) {
                    img8_opacity_save = progress;
                    imgV_8.setImageAlpha(img8_opacity_save);
                } else if (cutLayout_9_bool) {
                    img9_opacity_save = progress;
                    imgV_9.setImageAlpha(img9_opacity_save);

                } else if (cutLayout_10_bool) {
                    img10_opacity_save = progress;
                    imgV_10.setImageAlpha(img10_opacity_save);

                } else if (cutLayout_11_bool) {
                    img11_opacity_save = progress;
                    imgV_11.setImageAlpha(img11_opacity_save);

                } else if (cutLayout_12_bool) {
                    img12_opacity_save = progress;
                    imgV_12.setImageAlpha(img12_opacity_save);
                } else if (theme_bool) {
                    theme_opacity_save = progress;
                    img_theme.setImageAlpha(img12_opacity_save);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        bWSeek.setMax(1000);
        bWSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                rgbChanger(progress);

                if (cutLayout_1_bool) {
                    img1_bw_save = progress;
                } else if (cutLayout_2_bool) {
                    img2_bw_save = progress;
                } else if (cutLayout_3_bool) {
                    img3_bw_save = progress;
                } else if (cutLayout_4_bool) {
                    img4_bw_save = progress;
                } else if (cutLayout_5_bool) {
                    img5_bw_save = progress;
                } else if (cutLayout_6_bool) {
                    img6_bw_save = progress;
                } else if (cutLayout_7_bool) {
                    img7_bw_save = progress;
                } else if (cutLayout_8_bool) {
                    img8_bw_save = progress;
                } else if (cutLayout_9_bool) {
                    img9_bw_save = progress;
                } else if (cutLayout_10_bool) {
                    img10_bw_save = progress;
                } else if (cutLayout_11_bool) {
                    img11_bw_save = progress;
                } else if (cutLayout_12_bool) {
                    img12_bw_save = progress;
                } else if (theme_bool) {
                    theme_bw_save = progress;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (cutLayout_1_bool) {
                    con_bw = 2;
                } else if (cutLayout_2_bool) {
                    con_bw = 2;
                } else if (cutLayout_3_bool) {
                    con_bw = 2;
                } else if (cutLayout_4_bool) {
                    con_bw = 2;
                } else if (cutLayout_5_bool) {
                    con_bw = 2;
                } else if (cutLayout_6_bool) {
                    con_bw = 2;
                } else if (cutLayout_7_bool) {
                    con_bw = 2;
                } else if (cutLayout_8_bool) {
                    con_bw = 2;
                } else if (cutLayout_9_bool) {
                    con_bw = 2;
                } else if (cutLayout_10_bool) {
                    con_bw = 2;
                } else if (cutLayout_11_bool) {
                    con_bw = 2;
                } else if (cutLayout_12_bool) {
                    con_bw = 2;
                } else if (theme_bool) {
                    con_bw = 2;
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        g_bg_drw_1 = (ColorDrawable) grad_c1.getBackground();
        g_bg_drw_2 = (ColorDrawable) grad_c2.getBackground();
        g_bg_drw_3 = (ColorDrawable) grad_c3.getBackground();
        g_bg_drw_4 = (ColorDrawable) grad_c4.getBackground();
        g_bg_drw_5 = (ColorDrawable) grad_c5.getBackground();
        g_bg_drw_6 = (ColorDrawable) grad_c6.getBackground();
        gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{grad_colorId_1, grad_colorId_2});
        gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
//        bg_view.setBackgroundDrawable(gd);

        gradBG_Type_grp = (RadioGroup) findViewById(R.id.gradBG_Type_grp);
        gradBG_Type_grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("NewApi")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb2 = (RadioButton) group.findViewById(checkedId);

                grad_colorId_1 = g_bg_drw_1.getColor();
                grad_colorId_2 = g_bg_drw_2.getColor();
                grad_colorId_3 = g_bg_drw_3.getColor();
                grad_colorId_4 = g_bg_drw_4.getColor();
                grad_colorId_5 = g_bg_drw_5.getColor();
                grad_colorId_6 = g_bg_drw_6.getColor();
                gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{grad_colorId_1, grad_colorId_2, grad_colorId_3, grad_colorId_4, grad_colorId_5, grad_colorId_6});

                if (rb2 == gradBG_Type_liner) {
                    gradType = 0;
                    gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                    bg_view.setBackgroundDrawable(gd);
                    ll_St_1.setVisibility(View.GONE);
                    ll_angle.setVisibility(View.VISIBLE);
                } else if (rb2 == gradBG_Type_sweep) {
                    gradType = 1;
                    gd.setGradientType(GradientDrawable.SWEEP_GRADIENT);
                    bg_view.setBackgroundDrawable(gd);
                    ll_St_1.setVisibility(View.VISIBLE);
                    ll_angle.setVisibility(View.GONE);
                }

            }
        });


        bg_grad_seek = (SeekBar) findViewById(R.id.bg_grad_seek);
        bg_grad_seek.setMax(10);
        bg_grad_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gradXF = getConvertedValue(progress);
                gd.setGradientCenter(gradXF, gradYF);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bg_grad_seek2 = (SeekBar) findViewById(R.id.bg_grad_seek2);
        bg_grad_seek2.setMax(10);
        bg_grad_seek2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gradYF = getConvertedValue(progress);
                gd.setGradientCenter(gradXF, gradYF);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bg_corner_seek = (SeekBar) findViewById(R.id.bg_corner_seek);
        bg_corner_seek.setMax(360);
        bg_corner_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gd.setCornerRadius(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        c_bg_group = (RadioGroup) findViewById(R.id.c_bg_group);
        c_bg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (rb == c_bg_linearRadio) {
                    c_bg_radio_Lin = true;
                    c_bg_radio_rad = false;
                    c_bg_grad_spacing_seek.setProgress(20);
                    c_bg_grad_X_axis_seek.setProgress(20);
                    c_bg_grad_Y_axis_seek.setProgress(20);

                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                } else if (rb == c_bg_radialRadio) {
                    c_bg_radio_Lin = false;
                    c_bg_radio_rad = true;
                    c_bg_grad_spacing_seek.setProgress(30);
                    c_bg_grad_X_axis_seek.setProgress(1);
                    c_bg_grad_Y_axis_seek.setProgress(1);

                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient1 = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient1);
                    bg_view.setBackground(drawable2);
                }
            }
        });

        c_bg_grad_spacing_seek.setMax(screen_width);
        c_bg_grad_spacing_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (c_bg_radio_rad && progress > 20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    c_bg_rad_s = progress;
                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient1 = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient1);
                    bg_view.setBackground(drawable2);
                } else if (c_bg_radio_Lin && progress > 20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    lin_grad_bg_y1 = progress;
                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        c_bg_grad_X_axis_seek = (SeekBar) findViewById(R.id.c_bg_grad_X_axis_seek);
        c_bg_grad_X_axis_seek.setMax(screen_width);
        c_bg_grad_X_axis_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (c_bg_radio_rad && progress > 20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    c_bg_rad_x = progress;
                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);

                } else if (c_bg_radio_Lin && progress > 20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    lin_grad_bg_x1 = progress;
                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        c_bg_grad_Y_axis_seek = (SeekBar) findViewById(R.id.c_bg_grad_Y_axis_seek);
        c_bg_grad_Y_axis_seek.setMax(screen_width);
        c_bg_grad_Y_axis_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (c_bg_radio_rad && progress > 20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    c_bg_rad_y = progress;
                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                } else if (c_bg_radio_Lin && progress > 20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    lin_grad_bg_x0 = progress;
                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        params_cutLay = new RelativeLayout.LayoutParams(340, 340);
        params_cutLay.addRule(RelativeLayout.CENTER_IN_PARENT);

        paramsRL = new RelativeLayout.LayoutParams(screen_width, screen_width);
        rL.setLayoutParams(paramsRL);
        img_theme.setLayoutParams(paramsRL);
        bg_view.setLayoutParams(paramsRL);

        RectF drawableRect = new RectF(0, 0, screen_width, screen_width);
        RectF viewRect = new RectF(0, 0, screen_width, screen_width);
        matrix_theme.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.START);
        img_theme.setScaleType(ImageView.ScaleType.MATRIX);
        img_theme.setImageMatrix(matrix_theme);


        fest_title_recycler = (RecyclerView) findViewById(R.id.fest_title_recycler);
        frameoFestivalTitleAdapter = new FrameoFestivalTitleAdapter(festTitleModel, FrameoEdit.this, R.layout.row_festival_titles);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        fest_title_recycler.setLayoutManager(linearLayoutManager);

        imagesHoriRecycler = (RecyclerView) findViewById(R.id.imagesHoriRecycler);
        frameoImagesHoriAdapter = new FrameoImagesHoriAdapter(imagesHoriModel, FrameoEdit.this, R.layout.row_images_hori);
        linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        imagesHoriRecycler.setLayoutManager(linearLayoutManager2);

        getFestivalTitles();

    }


    public void rgbChanger(Integer progress) {
        if (cutLayout_1_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };
            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_1.setColorFilter(colorFilter);
        } else if (cutLayout_2_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_2.setColorFilter(colorFilter);
        } else if (cutLayout_3_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_3.setColorFilter(colorFilter);
        } else if (cutLayout_4_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_4.setColorFilter(colorFilter);
        } else if (cutLayout_5_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_5.setColorFilter(colorFilter);
        } else if (cutLayout_6_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_6.setColorFilter(colorFilter);
        } else if (cutLayout_7_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_7.setColorFilter(colorFilter);
        } else if (cutLayout_8_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_8.setColorFilter(colorFilter);
        } else if (cutLayout_9_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_9.setColorFilter(colorFilter);
        } else if (cutLayout_10_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_10.setColorFilter(colorFilter);
        } else if (cutLayout_11_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_11.setColorFilter(colorFilter);
        } else if (cutLayout_12_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, progress
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            imgV_12.setColorFilter(colorFilter);
        }

    }


    public static PorterDuffColorFilter setContrast(int progress) {
        if (progress >= 100) {
            int value = (int) (progress - 100) * 255 / 100;

            return new PorterDuffColorFilter(Color.argb(value, 255, 255, 255), PorterDuff.Mode.OVERLAY);
        } else {
            int value = (int) (100 - progress) * 255 / 100;
            return new PorterDuffColorFilter(Color.argb(value, 0, 0, 0), PorterDuff.Mode.OVERLAY);
        }

    }


    private void getFestivalTitles() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            theme_internet_LL.setVisibility(View.VISIBLE);
            internet_txt_LL.setVisibility(View.GONE);

            Log.d("TFT_URL", AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/" + EDIT_CAT_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/1",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("TFT_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        FestivalTitlesModel fList = new FestivalTitlesModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setCreated_on(jsonObject1.getString("created_on"));
                                        fList.setColor_id(jsonObject1.getString("color_id"));

                                        festTitleModel.add(fList);
                                    }
                                    fest_title_recycler.setAdapter(frameoFestivalTitleAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            internet_txt_LL.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void getImagesHori(String festTitleId) {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            imagesHoriModel.clear();
            Log.d("TIH_URL", AppUrls.BASE_URL + "images/festival/" + festTitleId + "/" + EDIT_CAT_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "images/festival/" + festTitleId + "/" + EDIT_CAT_ID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("TIH_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        ImagesHoriModel ihList = new ImagesHoriModel();
                                        ihList.setFestival_id(jsonObject1.getString("festival_id"));
                                        ihList.setImg1(jsonObject1.getString("img1"));
                                        ihList.setImg2(jsonObject1.getString("img2"));
                                        ihList.setImg3(jsonObject1.getString("img3"));

                                        imagesHoriModel.add(ihList);
                                    }
                                    imagesHoriRecycler.setAdapter(frameoImagesHoriAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
            // Get the Image from data
            selectedURI1 = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            // Get the cursor
            Cursor cursor = getContentResolver().query(selectedURI1, filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgDecodableString = cursor.getString(columnIndex);
            cursor.close();
            // Set the Image in ImageView after decoding the String
//            img_user_1.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

            Log.d("img1_SELEC_URI", selectedURI1.toString());
            Log.d("img1_Decode", imgDecodableString.toString());

            try {
                CropIntent = new Intent("com.android.camera.action.CROP");
                CropIntent.setDataAndType(selectedURI1, "image/*");
                CropIntent.putExtra("crop", "true");
                CropIntent.putExtra("outputX", 300);
                CropIntent.putExtra("outputY", 300);
                CropIntent.putExtra("aspectX", 10);
                CropIntent.putExtra("aspectY", 10);
                CropIntent.putExtra("scaleUpIfNeeded", true);
                CropIntent.putExtra("return-data", true);
                startActivityForResult(CropIntent, RESULT_LOAD_IMG3);
            } catch (ActivityNotFoundException e) {

            }

        }

        /*AFTER CROP*/
        else if (requestCode == RESULT_LOAD_IMG3) {
            if (data != null) {

                Bundle bundle = data.getExtras();
                if (gal_upload_1_bool) {
                    selected_cc_ImagePath1 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath1 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath1);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath1 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath1;

                    img_gal_1.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath1));
                    img_gal_1.setClickable(true);
                } else if (gal_upload_2_bool) {
                    selected_cc_ImagePath2 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath2 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath2);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath2 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath2;

                    img_gal_2.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath2));
                    img_gal_2.setClickable(true);
                } else if (gal_upload_3_bool) {
                    selected_cc_ImagePath3 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath3 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath3);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath3 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath3;

                    img_gal_3.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath3));
                    img_gal_3.setClickable(true);
                } else if (gal_upload_4_bool) {
                    selected_cc_ImagePath4 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath4 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath4);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath4 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath4;

                    img_gal_4.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath4));
                    img_gal_4.setClickable(true);
                } else if (gal_upload_5_bool) {
                    selected_cc_ImagePath5 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath5 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath5);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath5 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath5;

                    img_gal_5.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath5));
                    img_gal_5.setClickable(true);
                } else if (gal_upload_6_bool) {
                    selected_cc_ImagePath6 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath6 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath6);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath6 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath6;

                    img_gal_6.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath6));
                    img_gal_6.setClickable(true);
                } else if (gal_upload_7_bool) {
                    selected_cc_ImagePath7 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath7 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath7);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath7 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath7;

                    img_gal_7.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath7));
                    img_gal_7.setClickable(true);
                } else if (gal_upload_8_bool) {
                    selected_cc_ImagePath8 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath8 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath8);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath8 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath8;

                    img_gal_8.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath8));
                    img_gal_8.setClickable(true);
                } else if (gal_upload_9_bool) {
                    selected_cc_ImagePath9 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath9 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath9);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath9 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath9;

                    img_gal_9.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath9));
                    img_gal_9.setClickable(true);
                } else if (gal_upload_10_bool) {
                    selected_cc_ImagePath10 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath10 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath10);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath10 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath10;

                    img_gal_10.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath10));
                    img_gal_10.setClickable(true);
                } else if (gal_upload_11_bool) {
                    selected_cc_ImagePath11 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath11 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath11);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath11 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath11;

                    img_gal_11.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath11));
                    img_gal_11.setClickable(true);
                } else if (gal_upload_12_bool) {
                    selected_cc_ImagePath12 = selectedURI1.getPath();

                    Bitmap cropbitmap = bundle.getParcelable("data");
                    selected_cc_ImagePath12 = String.valueOf(System.currentTimeMillis()) + ".jpg";
                    File file = new File(Environment.getExternalStorageDirectory(), selected_cc_ImagePath12);

                    try {
                        file.createNewFile();
                        FileOutputStream fos = new FileOutputStream(file);
                        cropbitmap.compress(Bitmap.CompressFormat.PNG, 95, fos);
                    } catch (IOException e) {
                        Toast.makeText(this, "Sorry, Camera Crashed-Please Report as Crash A.", Toast.LENGTH_LONG).show();
                    }
                    selected_cc_ImagePath12 = Environment.getExternalStorageDirectory() + "/" + selected_cc_ImagePath12;

                    img_gal_12.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath12));
                    img_gal_12.setClickable(true);
                }
            }
        }
    }


    public boolean onTouch(View view, MotionEvent event) {

        if (c_bg_v1 == view) {
            c_bg_gradV1 = true;
            c_bg_gradV2 = false;
            gradV2 = false;
            gradV1 = false;
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
        }
        if (c_bg_v2 == view) {
            c_bg_gradV2 = true;
            c_bg_gradV1 = false;
            gradV2 = false;
            gradV1 = false;
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
        }
        if (view == grad_c1) {
            grad_c1_bool = true;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c2) {
            grad_c1_bool = false;
            grad_c2_bool = true;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c3) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = true;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c4) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = true;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c5) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = true;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c6) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = true;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == pencil_ic_img) {
            if (pencil_ic_img.getRotation() == 0) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else if (pencil_ic_img.getRotation() == 180) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }

        if (view == cutLayout_1) {
            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_1.bringToFront();
                    cutLayout_1_bool = true;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_1_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_1_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_1_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_1_dy_axis_save);
                    scale_seek.setProgress(cutLayout_1_scale_save);
                    opacity.setProgress(img1_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img1_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img1_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }


        if (view == cutLayout_2) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_2.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = true;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_2_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_2_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_2_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_2_dy_axis_save);
                    scale_seek.setProgress(cutLayout_2_scale_save);
                    opacity.setProgress(img2_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img2_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img2_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_3) {
            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_3.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = true;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_3_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_3_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_3_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_3_dy_axis_save);
                    scale_seek.setProgress(cutLayout_3_scale_save);
                    opacity.setProgress(img3_opacity_save);

                    if (con_bw == 1) {
                        contrast.setProgress(img3_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img3_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_4) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_4.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = true;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_4_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_4_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_4_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_4_dy_axis_save);
                    scale_seek.setProgress(cutLayout_4_scale_save);
                    opacity.setProgress(img4_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img3_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img3_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_5) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_5.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = true;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_5_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_5_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_5_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_5_dy_axis_save);
                    scale_seek.setProgress(cutLayout_5_scale_save);
                    opacity.setProgress(img5_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img5_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img5_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_6) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_6.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = true;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_6_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_6_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_6_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_6_dy_axis_save);
                    scale_seek.setProgress(cutLayout_6_scale_save);
                    opacity.setProgress(img6_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img6_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img6_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_7) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_7.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = true;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_7_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_7_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_7_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_7_dy_axis_save);
                    scale_seek.setProgress(cutLayout_7_scale_save);
                    opacity.setProgress(img7_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img7_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img7_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_8) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_8.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = true;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_8_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_8_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_8_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_8_dy_axis_save);
                    scale_seek.setProgress(cutLayout_8_scale_save);
                    opacity.setProgress(img8_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img8_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img8_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_9) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_9.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = true;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_9_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_9_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_9_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_9_dy_axis_save);
                    scale_seek.setProgress(cutLayout_9_scale_save);
                    opacity.setProgress(img9_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img9_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img9_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_10) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_10.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = true;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = false;
                    theme_bool = false;

                    x_axis_seek_obliq.setProgress(cutLayout_10_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_10_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_10_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_10_dy_axis_save);
                    scale_seek.setProgress(cutLayout_10_scale_save);
                    opacity.setProgress(img10_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img10_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img10_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_11) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_11.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = true;
                    cutLayout_12_bool = false;
                    theme_bool = false;


                    x_axis_seek_obliq.setProgress(cutLayout_11_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_11_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_11_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_11_dy_axis_save);
                    scale_seek.setProgress(cutLayout_11_scale_save);
                    opacity.setProgress(img11_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img11_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img11_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == cutLayout_12) {

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    cutLayout_12.bringToFront();
                    cutLayout_1_bool = false;
                    cutLayout_2_bool = false;
                    cutLayout_3_bool = false;
                    cutLayout_4_bool = false;
                    cutLayout_5_bool = false;
                    cutLayout_6_bool = false;
                    cutLayout_7_bool = false;
                    cutLayout_8_bool = false;
                    cutLayout_9_bool = false;
                    cutLayout_10_bool = false;
                    cutLayout_11_bool = false;
                    cutLayout_12_bool = true;

                    x_axis_seek_obliq.setProgress(cutLayout_12_x_axis_save);
                    y_axis_seek_obliq.setProgress(cutLayout_12_y_axis_save);
                    distance_X_seek_obliq.setProgress(cutLayout_12_dx_axis_save);
                    distance_Y_seek_obliq.setProgress(cutLayout_12_dy_axis_save);
                    scale_seek.setProgress(cutLayout_12_scale_save);
                    opacity.setProgress(img12_opacity_save);
                    if (con_bw == 1) {
                        contrast.setProgress(img12_contrast_save);
                    } else if (con_bw == 2) {
                        bWSeek.setProgress(img12_bw_save);
                    }

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoEditView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (bg_view == view) {
            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;

                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setBgView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

       /*         case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;*/

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == img_theme) {
            imgBg = true;
            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            img_theme.bringToFront();
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    savedMatrix.set(matrix);
                    start.set(event.getX(), event.getY());
                    mode = DRAG;
                    lastEvent = null;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setThemeView();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    oldDist = spacing(event);
                    if (oldDist > 10f) {
                        savedMatrix.set(matrix);
                        midPoint(mid, event);
                        mode = ZOOM;
                    }
                    lastEvent = new float[4];
                    lastEvent[0] = event.getX(0);
                    lastEvent[1] = event.getX(1);
                    lastEvent[2] = event.getY(0);
                    lastEvent[3] = event.getY(1);
                    d = rotation(event);
            /*        if (emoji_1_bool) {
                        emoji_s_1.setEnabled(false);
                    }
                    if (emoji_2_bool) {
                        emoji_s_2.setEnabled(false);
                    }
                    if (emoji_3_bool) {
                        emoji_s_3.setEnabled(false);
                    }
                    theme_lockIn_ic.setVisibility(View.GONE);
                    theme_lockOut_ic.setVisibility(View.VISIBLE);
                    emoji_lockIn_ic.setVisibility(View.VISIBLE);
                    emoji_lockOut_ic.setVisibility(View.GONE);

                    unlockAll_butt.setVisibility(View.VISIBLE);*/
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    mode = NONE;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        matrix.set(savedMatrix);
                        float dx = event.getX() - start.x;
                        float dy = event.getY() - start.y;
                        matrix.postTranslate(dx, dy);
                    } else if (mode == ZOOM) {
                        float newDist = spacing(event);
                        if (newDist > 10f) {
                            matrix.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix.postScale(scale, scale, mid.x, mid.y);
                        }
                        if (lastEvent != null && event.getPointerCount() == 2) {
                            newRot = rotation(event);
                            float r = newRot - d;
                            float[] values = new float[9];
                            matrix.getValues(values);
                            float tx = values[2];
                            float ty = values[5];
                            float sx = values[0];
                            float xc = (img_theme.getWidth() / 2) * sx;
                            float yc = (img_theme.getHeight() / 2) * sx;
                            matrix.postRotate(r, tx + xc, ty + yc);
                        }
                    }
                    break;
            }

            img_theme.setImageMatrix(matrix);
        }

        if (view == emoji_s_1 || view == emoji_s_2 || view == emoji_s_3) {
            if (view == emoji_s_1)
            {
                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(false);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(true);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(true);
                }

                emoji_bool_s1 = true;
                emoji_bool_s2 = false;
                emoji_bool_s3 = false;

                emoji_s_1.bringToFront();

            }
            else if (view == emoji_s_2)
            {

                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(true);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(false);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(true);
                }

                emoji_bool_s1 = false;
                emoji_bool_s2 = true;
                emoji_bool_s3 = false;

                emoji_s_2.bringToFront();

            }
            else if (view == emoji_s_3)
            {

                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(true);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(true);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(false);
                }

                emoji_bool_s1 = false;
                emoji_bool_s2 = false;
                emoji_bool_s3 = true;

                emoji_s_3.bringToFront();
            }

            emoji_bool = true;

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;

                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setEmoji_LL();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }


        return true;
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }


    @Override
    public void onBackPressed() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Unsaved work, Tap back again to exit", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }

    public float getConvertedValue(int intVal) {
        float floatVal = (float) 0.0;
        floatVal = .1f * intVal;
        return floatVal;
    }


    @SuppressLint("NewApi")
    private void getGradBg_Style_1() {
        g_bg_drw_1 = (ColorDrawable) grad_c1.getBackground();
        g_bg_drw_2 = (ColorDrawable) grad_c2.getBackground();
        g_bg_drw_3 = (ColorDrawable) grad_c3.getBackground();
        g_bg_drw_4 = (ColorDrawable) grad_c4.getBackground();
        g_bg_drw_5 = (ColorDrawable) grad_c5.getBackground();
        g_bg_drw_6 = (ColorDrawable) grad_c6.getBackground();

        if (setBgLL_bool && bg_GRAD_0_1 == 0) {
            bg_view.setBackgroundColor(getViewColo);
        } else if (setBgLL_bool && bg_GRAD_0_1 == 1) {
            if (grad_c1_bool) {
                grad_c1.setBackgroundColor(getViewColo);
            } else if (grad_c2_bool) {
                grad_c2.setBackgroundColor(getViewColo);
            } else if (grad_c3_bool) {
                grad_c3.setBackgroundColor(getViewColo);
            } else if (grad_c4_bool) {
                grad_c4.setBackgroundColor(getViewColo);
            } else if (grad_c5_bool) {
                grad_c5.setBackgroundColor(getViewColo);
            } else if (grad_c6_bool) {
                grad_c6.setBackgroundColor(getViewColo);
            }

            grad_colorId_1 = g_bg_drw_1.getColor();
            grad_colorId_2 = g_bg_drw_2.getColor();
            grad_colorId_3 = g_bg_drw_3.getColor();
            grad_colorId_4 = g_bg_drw_4.getColor();
            grad_colorId_5 = g_bg_drw_5.getColor();
            grad_colorId_6 = g_bg_drw_6.getColor();
            gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{grad_colorId_1, grad_colorId_2, grad_colorId_3, grad_colorId_4, grad_colorId_5, grad_colorId_6});

            if (gradType == 0) {
                gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                bg_view.setBackgroundDrawable(gd);
            } else if (gradType == 1) {
                gd.setGradientType(GradientDrawable.SWEEP_GRADIENT);
                bg_view.setBackgroundDrawable(gd);
            }
        }
    }


    private void getGradBg_Style_2() {
        if (style_2_txt_bool) {
            if (c_bg_gradV1) {
                c_bg_v1.setBackgroundColor(getViewColo);
            } else if (c_bg_gradV2) {
                c_bg_v2.setBackgroundColor(getViewColo);
            }
            ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
            ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
            colorId1 = cd1.getColor();
            int colorId2 = cd2.getColor();

            if (c_bg_radio_Lin) {
                int[] color = {colorId1, colorId2};
                float[] position = {0, 1};
                Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                Shader shader_gradient = lin_grad;

                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                drawable2.getPaint().setShader(shader_gradient);
                bg_view.setBackground(drawable2);
            } else if (c_bg_radio_rad) {
                Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                Shader shader_gradient = rad_grad;

                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                drawable2.getPaint().setShader(shader_gradient);
                bg_view.setBackground(drawable2);
            }
        }
    }


    private void getEmojiTitles() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            emojiTitlesModels.clear();
            Log.d("Emoji_Title_URL", AppUrls.BASE_URL + "stickers/" + "4");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "stickers/" + "4",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Emoji_Title_RES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        EmojiTitlesModel fList = new EmojiTitlesModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setCreated_on(jsonObject1.getString("created_on"));
                                        fList.setColor_id(jsonObject1.getString("color_id"));

                                        emojiTitlesModels.add(fList);
                                    }
                                    emoji_recycler_titles.setAdapter(emojiTitleAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void getEmojiImages(String festTitleId) {
        emojiImagesModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("Emoji_IMG_URL", AppUrls.BASE_URL + "images/stickers/" + festTitleId + "/" + "4");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "images/stickers/" + festTitleId + "/" + "4",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Emoji_IMG_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        EmojiImagesModel ihList = new EmojiImagesModel();
                                        ihList.setImg1(jsonObject1.getString("img1"));
                                        ihList.setImg2(jsonObject1.getString("img2"));
                                        ihList.setImg3(jsonObject1.getString("img3"));
                                        ihList.setColor_filter(jsonObject1.getString("field1"));

                                        emojiImagesModels.add(ihList);
                                    }
                                    emoji_recycler_images.setAdapter(emojiImagesAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void emojis(Bitmap bit, String color_fil) {
        if (!emoji_1_bool || emoji_s_1.getVisibility() == View.GONE) {
            if (!emoji_1_bool) {
                emoji_s_1 = new StickerImageView(this);
                rL.addView(emoji_s_1);
                emoji_s_1.setOnTouchListener(this);
                emoji_1_bool = true;
                emoji_s_1.setImageBitmap(bit);
                emoji_1_color = color_fil;
                emoji_1x_bool = true;
            } else if (emoji_s_1.getVisibility() == View.GONE) {
                emoji_s_1.setVisibility(View.VISIBLE);
                emoji_s_1.setImageBitmap(bit);
                emoji_1_color = color_fil;
                emoji_1x_bool = false;
            }

        } else if (!emoji_2_bool || emoji_s_2.getVisibility() == View.GONE) {
            if (!emoji_2_bool) {
                emoji_s_2 = new StickerImageView(this);
                rL.addView(emoji_s_2);
                emoji_s_2.setOnTouchListener(this);
                emoji_2_bool = true;
                emoji_s_2.setImageBitmap(bit);
                emoji_2_color = color_fil;
                emoji_2x_bool = true;
            } else if (emoji_s_2.getVisibility() == View.GONE) {
                emoji_s_2.setVisibility(View.VISIBLE);
                emoji_s_2.setImageBitmap(bit);
                emoji_2_color = color_fil;
                emoji_2x_bool = false;
            }
        } else if (!emoji_3_bool || emoji_s_3.getVisibility() == View.GONE) {
            if (!emoji_3_bool) {
                emoji_s_3 = new StickerImageView(this);
                rL.addView(emoji_s_3);
                emoji_s_3.setOnTouchListener(this);
                emoji_3_bool = true;
                emoji_s_3.setImageBitmap(bit);
                emoji_3_color = color_fil;
                emoji_3x_bool = true;
            } else if (emoji_s_3.getVisibility() == View.GONE) {
                emoji_s_3.setVisibility(View.VISIBLE);
                emoji_s_3.setImageBitmap(bit);
                emoji_3_color = color_fil;
                emoji_3x_bool = false;
            }

        }

    }


    private void getEmojiDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.emoji_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        emoji_recycler_titles = (RecyclerView) dialog.findViewById(R.id.emoji_recycler_titles);
        emojiTitleAdapter = new EmojiTitleAdapter_Frameo(emojiTitlesModels, FrameoEdit.this, R.layout.row_festival_titles);
        linearLayoutManager_ST = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        emoji_recycler_titles.setLayoutManager(linearLayoutManager_ST);

        emoji_recycler_images = (RecyclerView) dialog.findViewById(R.id.emoji_recycler_images);
        emojiImagesAdapter = new EmojiImagesAdapter_Frameo(emojiImagesModels, FrameoEdit.this, R.layout.row_images_emoji);
        gridLayoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        emoji_recycler_images.setLayoutManager(gridLayoutManager);

        getEmojiTitles();

        dialog.show();
    }


    private void getLogoColor() {
        if (emoji_bool) {
            if (emoji_bool_s1) {
                if (emoji_1_bool && emoji_1_color.equals("1")) {
                    emoji_s_1.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }

            if (emoji_bool_s2) {
                if (emoji_2_bool && emoji_2_color.equals("1")) {
                    emoji_s_2.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }

            if (emoji_bool_s3) {
                if (emoji_3_bool && emoji_3_color.equals("1")) {
                    emoji_s_3.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View view) {
/*
        if (view == unlockAll_butt) {
            if (emoji_1_bool)
            {
                emoji_s_1.setEnabled(true);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setEnabled(true);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setEnabled(true);
            }
            theme_lockIn_ic.setVisibility(View.GONE);
            theme_lockOut_ic.setVisibility(View.VISIBLE);
            emoji_lockIn_ic.setVisibility(View.GONE);
            emoji_lockOut_ic.setVisibility(View.VISIBLE);
            unlockAll_butt.setVisibility(View.GONE);
        }*/


        if (view == emoji_visi_ic) {
            emoji_visi_ic.setVisibility(View.GONE);
            emoji_Invisi_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool)
            {
                emoji_s_1.setVisibility(View.GONE);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setVisibility(View.GONE);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setVisibility(View.GONE);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to Hide", Toast.LENGTH_SHORT).show();

            }

        }
        if (view == emoji_Invisi_ic) {
            emoji_visi_ic.setVisibility(View.VISIBLE);
            emoji_Invisi_ic.setVisibility(View.GONE);
            if (emoji_1_bool)
            {
                emoji_s_1.setVisibility(View.VISIBLE);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setVisibility(View.VISIBLE);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setVisibility(View.VISIBLE);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to Show", Toast.LENGTH_SHORT).show();

            }
        }


        if (view==emoji_lockIn_ic)
        {
            emoji_lockIn_ic.setVisibility(View.GONE);
            emoji_lockOut_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool)
            {
                emoji_s_1.setEnabled(true);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setEnabled(true);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setEnabled(true);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to Lock", Toast.LENGTH_SHORT).show();

            }
        }

        if (view==emoji_lockOut_ic)
        {
            emoji_lockOut_ic.setVisibility(View.GONE);
            emoji_lockIn_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool)
            {
                emoji_s_1.setEnabled(false);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setEnabled(false);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setEnabled(false);
            }
            if (!emoji_1_bool&&!emoji_2_bool&&!emoji_3_bool)
            {
                Toast.makeText(this, "No emoji's to UnLock", Toast.LENGTH_SHORT).show();

            }
        }

        if (view == emojiButt||view==emojiButt2) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                getEmojiDialog();
            } else {
                Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (internet_txt_LL == view) {
            getFestivalTitles();
        }
        if (view == bg_visi_ic) {
            bg_view.setVisibility(View.GONE);
            bg_visi_ic.setVisibility(View.GONE);
            bg_Invisi_ic.setVisibility(View.VISIBLE);
        }

        if (view == bg_Invisi_ic) {
            bg_view.setVisibility(View.VISIBLE);
            bg_visi_ic.setVisibility(View.VISIBLE);
            bg_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == theme_visi_ic) {
            img_theme.setVisibility(View.GONE);
            theme_visi_ic.setVisibility(View.GONE);
            theme_Invisi_ic.setVisibility(View.VISIBLE);
        }

        if (view == theme_Invisi_ic) {
            img_theme.setVisibility(View.VISIBLE);
            theme_visi_ic.setVisibility(View.VISIBLE);
            theme_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == theme_lockIn_ic) {
            img_theme.setEnabled(true);
            theme_lockIn_ic.setVisibility(View.GONE);
            theme_lockOut_ic.setVisibility(View.VISIBLE);
        }

        if (view == theme_lockOut_ic) {
            img_theme.setEnabled(false);
            theme_lockIn_ic.setVisibility(View.VISIBLE);
            theme_lockOut_ic.setVisibility(View.GONE);
        }

        if (view == gal_x_1) {
            cutLayout_1.removeAllViews();
            img_gal_1.setClickable(true);
            gal_1_lock_LL.setVisibility(View.GONE);
            gal_1_visi_LL.setVisibility(View.GONE);
            gal_x_1.setVisibility(View.GONE);
            gal_upload_1.setVisibility(View.VISIBLE);
            gal_up_1.setVisibility(View.GONE);
        }
        if (view == gal_x_2) {
            cutLayout_2.removeAllViews();
            img_gal_2.setClickable(true);
            gal_2_lock_LL.setVisibility(View.GONE);
            gal_2_visi_LL.setVisibility(View.GONE);
            gal_x_2.setVisibility(View.GONE);
            gal_upload_2.setVisibility(View.VISIBLE);
            gal_up_2.setVisibility(View.GONE);
        }
        if (view == gal_x_3) {
            cutLayout_3.removeAllViews();
            img_gal_3.setClickable(true);
            gal_3_lock_LL.setVisibility(View.GONE);
            gal_3_visi_LL.setVisibility(View.GONE);
            gal_x_3.setVisibility(View.GONE);
            gal_upload_3.setVisibility(View.VISIBLE);
            gal_up_3.setVisibility(View.GONE);
        }
        if (view == gal_x_4) {
            cutLayout_4.removeAllViews();
            img_gal_4.setClickable(true);
            gal_4_lock_LL.setVisibility(View.GONE);
            gal_4_visi_LL.setVisibility(View.GONE);
            gal_x_4.setVisibility(View.GONE);
            gal_upload_4.setVisibility(View.VISIBLE);
            gal_up_4.setVisibility(View.GONE);
        }
        if (view == gal_x_5) {
            cutLayout_5.removeAllViews();
            img_gal_5.setClickable(true);
            gal_5_lock_LL.setVisibility(View.GONE);
            gal_5_visi_LL.setVisibility(View.GONE);
            gal_x_5.setVisibility(View.GONE);
            gal_upload_5.setVisibility(View.VISIBLE);
            gal_up_5.setVisibility(View.GONE);
        }
        if (view == gal_x_6) {
            cutLayout_6.removeAllViews();
            img_gal_6.setClickable(true);
            gal_6_lock_LL.setVisibility(View.GONE);
            gal_6_visi_LL.setVisibility(View.GONE);
            gal_x_6.setVisibility(View.GONE);
            gal_upload_6.setVisibility(View.VISIBLE);
            gal_up_6.setVisibility(View.GONE);
        }
        if (view == gal_x_7) {
            cutLayout_7.removeAllViews();
            img_gal_7.setClickable(true);
            gal_7_lock_LL.setVisibility(View.GONE);
            gal_7_visi_LL.setVisibility(View.GONE);
            gal_x_7.setVisibility(View.GONE);
            gal_upload_7.setVisibility(View.VISIBLE);
            gal_up_7.setVisibility(View.GONE);
        }
        if (view == gal_x_8) {
            cutLayout_8.removeAllViews();
            img_gal_8.setClickable(true);
            gal_8_lock_LL.setVisibility(View.GONE);
            gal_8_visi_LL.setVisibility(View.GONE);
            gal_x_8.setVisibility(View.GONE);
            gal_upload_8.setVisibility(View.VISIBLE);
            gal_up_8.setVisibility(View.GONE);
        }
        if (view == gal_x_9) {
            cutLayout_9.removeAllViews();
            img_gal_9.setClickable(true);
            gal_9_lock_LL.setVisibility(View.GONE);
            gal_9_visi_LL.setVisibility(View.GONE);
            gal_x_9.setVisibility(View.GONE);
            gal_upload_9.setVisibility(View.VISIBLE);
            gal_up_9.setVisibility(View.GONE);
        }
        if (view == gal_x_10) {
            cutLayout_10.removeAllViews();
            img_gal_10.setClickable(true);
            gal_10_lock_LL.setVisibility(View.GONE);
            gal_10_visi_LL.setVisibility(View.GONE);
            gal_x_10.setVisibility(View.GONE);
            gal_upload_10.setVisibility(View.VISIBLE);
            gal_up_10.setVisibility(View.GONE);
        }
        if (view == gal_x_11) {
            cutLayout_11.removeAllViews();
            img_gal_11.setClickable(true);
            gal_11_lock_LL.setVisibility(View.GONE);
            gal_11_visi_LL.setVisibility(View.GONE);
            gal_x_11.setVisibility(View.GONE);
            gal_upload_11.setVisibility(View.VISIBLE);
            gal_up_11.setVisibility(View.GONE);
        }
        if (view == gal_x_12) {
            cutLayout_12.removeAllViews();
            img_gal_12.setClickable(true);
            gal_12_lock_LL.setVisibility(View.GONE);
            gal_12_visi_LL.setVisibility(View.GONE);
            gal_x_12.setVisibility(View.GONE);
            gal_upload_12.setVisibility(View.VISIBLE);
            gal_up_12.setVisibility(View.GONE);
        }

        if (view == gal_1_visi_ic) {
            cutLayout_1.setVisibility(View.GONE);
            gal_1_visi_ic.setVisibility(View.GONE);
            gal_1_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_1_Invisi_ic) {
            cutLayout_1.setVisibility(View.VISIBLE);
            gal_1_visi_ic.setVisibility(View.VISIBLE);
            gal_1_Invisi_ic.setVisibility(View.GONE);
        }
        if (view == gal_2_visi_ic) {
            cutLayout_2.setVisibility(View.GONE);
            gal_2_visi_ic.setVisibility(View.GONE);
            gal_2_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_2_Invisi_ic) {
            cutLayout_2.setVisibility(View.VISIBLE);
            gal_2_visi_ic.setVisibility(View.VISIBLE);
            gal_2_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_3_visi_ic) {
            cutLayout_3.setVisibility(View.GONE);
            gal_3_visi_ic.setVisibility(View.GONE);
            gal_3_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_3_Invisi_ic) {
            cutLayout_3.setVisibility(View.VISIBLE);
            gal_3_visi_ic.setVisibility(View.VISIBLE);
            gal_3_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_4_visi_ic) {
            cutLayout_4.setVisibility(View.GONE);
            gal_4_visi_ic.setVisibility(View.GONE);
            gal_4_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_4_Invisi_ic) {
            cutLayout_4.setVisibility(View.VISIBLE);
            gal_4_visi_ic.setVisibility(View.VISIBLE);
            gal_4_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_5_visi_ic) {
            cutLayout_5.setVisibility(View.GONE);
            gal_5_visi_ic.setVisibility(View.GONE);
            gal_5_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_5_Invisi_ic) {
            cutLayout_5.setVisibility(View.VISIBLE);
            gal_5_visi_ic.setVisibility(View.VISIBLE);
            gal_5_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_6_visi_ic) {
            cutLayout_6.setVisibility(View.GONE);
            gal_6_visi_ic.setVisibility(View.GONE);
            gal_6_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_6_Invisi_ic) {
            cutLayout_6.setVisibility(View.VISIBLE);
            gal_6_visi_ic.setVisibility(View.VISIBLE);
            gal_6_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_7_visi_ic) {
            cutLayout_7.setVisibility(View.GONE);
            gal_7_visi_ic.setVisibility(View.GONE);
            gal_7_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_7_Invisi_ic) {
            cutLayout_7.setVisibility(View.VISIBLE);
            gal_7_visi_ic.setVisibility(View.VISIBLE);
            gal_7_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_8_visi_ic) {
            cutLayout_8.setVisibility(View.GONE);
            gal_8_visi_ic.setVisibility(View.GONE);
            gal_8_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_8_Invisi_ic) {
            cutLayout_8.setVisibility(View.VISIBLE);
            gal_8_visi_ic.setVisibility(View.VISIBLE);
            gal_8_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_9_visi_ic) {
            cutLayout_9.setVisibility(View.GONE);
            gal_9_visi_ic.setVisibility(View.GONE);
            gal_9_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_9_Invisi_ic) {
            cutLayout_9.setVisibility(View.VISIBLE);
            gal_9_visi_ic.setVisibility(View.VISIBLE);
            gal_9_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_10_visi_ic) {
            cutLayout_10.setVisibility(View.GONE);
            gal_10_visi_ic.setVisibility(View.GONE);
            gal_10_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_10_Invisi_ic) {
            cutLayout_10.setVisibility(View.VISIBLE);
            gal_10_visi_ic.setVisibility(View.VISIBLE);
            gal_10_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_11_visi_ic) {
            cutLayout_11.setVisibility(View.GONE);
            gal_11_visi_ic.setVisibility(View.GONE);
            gal_11_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_11_Invisi_ic) {
            cutLayout_11.setVisibility(View.VISIBLE);
            gal_11_visi_ic.setVisibility(View.VISIBLE);
            gal_11_Invisi_ic.setVisibility(View.GONE);
        }

        if (view == gal_12_visi_ic) {
            cutLayout_12.setVisibility(View.GONE);
            gal_12_visi_ic.setVisibility(View.GONE);
            gal_12_Invisi_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_12_Invisi_ic) {
            cutLayout_12.setVisibility(View.VISIBLE);
            gal_12_visi_ic.setVisibility(View.VISIBLE);
            gal_12_Invisi_ic.setVisibility(View.GONE);
        }


        if (view == gal_1_lockOut_ic) {
            cutLayout_1.setEnabled(false);
            gal_1_lockOut_ic.setVisibility(View.GONE);
            gal_1_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_1_lockIn_ic) {
            cutLayout_1.setEnabled(true);
            gal_1_lockOut_ic.setVisibility(View.VISIBLE);
            gal_1_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_2_lockOut_ic) {
            cutLayout_2.setEnabled(false);
            gal_2_lockOut_ic.setVisibility(View.GONE);
            gal_2_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_2_lockIn_ic) {
            cutLayout_2.setEnabled(true);
            gal_2_lockOut_ic.setVisibility(View.VISIBLE);
            gal_2_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_3_lockOut_ic) {
            cutLayout_3.setEnabled(false);
            gal_3_lockOut_ic.setVisibility(View.GONE);
            gal_3_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_3_lockIn_ic) {
            cutLayout_3.setEnabled(true);
            gal_3_lockOut_ic.setVisibility(View.VISIBLE);
            gal_3_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_4_lockOut_ic) {
            cutLayout_4.setEnabled(false);
            gal_4_lockOut_ic.setVisibility(View.GONE);
            gal_4_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_4_lockIn_ic) {
            cutLayout_4.setEnabled(true);
            gal_4_lockOut_ic.setVisibility(View.VISIBLE);
            gal_4_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_5_lockOut_ic) {
            cutLayout_5.setEnabled(false);
            gal_5_lockOut_ic.setVisibility(View.GONE);
            gal_5_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_5_lockIn_ic) {
            cutLayout_5.setEnabled(true);
            gal_5_lockOut_ic.setVisibility(View.VISIBLE);
            gal_5_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_6_lockOut_ic) {
            cutLayout_6.setEnabled(false);
            gal_6_lockOut_ic.setVisibility(View.GONE);
            gal_6_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_6_lockIn_ic) {
            cutLayout_6.setEnabled(true);
            gal_6_lockOut_ic.setVisibility(View.VISIBLE);
            gal_6_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_7_lockOut_ic) {
            cutLayout_7.setEnabled(false);
            gal_7_lockOut_ic.setVisibility(View.GONE);
            gal_7_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_7_lockIn_ic) {
            cutLayout_7.setEnabled(true);
            gal_7_lockOut_ic.setVisibility(View.VISIBLE);
            gal_7_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_8_lockOut_ic) {
            cutLayout_8.setEnabled(false);
            gal_8_lockOut_ic.setVisibility(View.GONE);
            gal_8_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_8_lockIn_ic) {
            cutLayout_8.setEnabled(true);
            gal_8_lockOut_ic.setVisibility(View.VISIBLE);
            gal_8_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_9_lockOut_ic) {
            cutLayout_9.setEnabled(false);
            gal_9_lockOut_ic.setVisibility(View.GONE);
            gal_9_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_9_lockIn_ic) {
            cutLayout_9.setEnabled(true);
            gal_9_lockOut_ic.setVisibility(View.VISIBLE);
            gal_9_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_10_lockOut_ic) {
            cutLayout_10.setEnabled(false);
            gal_10_lockOut_ic.setVisibility(View.GONE);
            gal_10_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_10_lockIn_ic) {
            cutLayout_10.setEnabled(true);
            gal_10_lockOut_ic.setVisibility(View.VISIBLE);
            gal_10_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_11_lockOut_ic) {
            cutLayout_11.setEnabled(false);
            gal_11_lockOut_ic.setVisibility(View.GONE);
            gal_11_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_11_lockIn_ic) {
            cutLayout_11.setEnabled(true);
            gal_11_lockOut_ic.setVisibility(View.VISIBLE);
            gal_11_lockIn_ic.setVisibility(View.GONE);
        }
        if (view == gal_12_lockOut_ic) {
            cutLayout_12.setEnabled(false);
            gal_12_lockOut_ic.setVisibility(View.GONE);
            gal_12_lockIn_ic.setVisibility(View.VISIBLE);
        } else if (view == gal_12_lockIn_ic) {
            cutLayout_12.setEnabled(true);
            gal_12_lockOut_ic.setVisibility(View.VISIBLE);
            gal_12_lockIn_ic.setVisibility(View.GONE);
        }


        if (view == gal_upload_1 || view == gal_upload_2 || view == gal_upload_3 || view == gal_upload_4 || view == gal_upload_5 || view == gal_upload_6 || view == gal_upload_7 || view == gal_upload_8 || view == gal_upload_9 || view == gal_upload_10 || view == gal_upload_11 || view == gal_upload_12) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

            if (view == gal_upload_1) {
                gal_upload_1_bool = true;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_2) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = true;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_3) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = true;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_4) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = true;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_5) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = true;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_6) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = true;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_7) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = true;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_8) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = true;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_9) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = true;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_10) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = true;
                gal_upload_11_bool = false;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_11) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = true;
                gal_upload_12_bool = false;
            } else if (view == gal_upload_12) {
                gal_upload_1_bool = false;
                gal_upload_2_bool = false;
                gal_upload_3_bool = false;
                gal_upload_4_bool = false;
                gal_upload_5_bool = false;
                gal_upload_6_bool = false;
                gal_upload_7_bool = false;
                gal_upload_8_bool = false;
                gal_upload_9_bool = false;
                gal_upload_10_bool = false;
                gal_upload_11_bool = false;
                gal_upload_12_bool = true;
            }

        }


        if (view.equals(img_gal_1)) {
            cutLayout_1 = new CutLayout(this);
            cutLayout_1.setOnTouchListener(this);
            imgV_1 = new ImageView(this);
            imgV_1.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath1));
            cutLayout_1.addView(imgV_1);
            cutLayout_1.setLayoutParams(params_cutLay);
//            imgV_1.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_1);

            gal_1_visi_LL.setVisibility(View.VISIBLE);
            gal_1_lock_LL.setVisibility(View.VISIBLE);
            gal_up_1.setVisibility(View.VISIBLE);
            gal_x_1.setVisibility(View.VISIBLE);
            gal_upload_1.setVisibility(View.GONE);
            img_gal_1.setClickable(false);
        }
        if (view.equals(img_gal_2)) {
            cutLayout_2 = new CutLayout(this);
            cutLayout_2.setOnTouchListener(this);
            imgV_2 = new ImageView(this);
            imgV_2.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath2));
            cutLayout_2.addView(imgV_2);
            cutLayout_2.setLayoutParams(params_cutLay);
//            imgV_2.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_2);


            gal_2_visi_LL.setVisibility(View.VISIBLE);
            gal_2_lock_LL.setVisibility(View.VISIBLE);
            gal_up_2.setVisibility(View.VISIBLE);
            gal_x_2.setVisibility(View.VISIBLE);
            gal_upload_2.setVisibility(View.GONE);
            img_gal_2.setClickable(false);
        }
        if (view.equals(img_gal_3)) {
            cutLayout_3 = new CutLayout(this);
            cutLayout_3.setOnTouchListener(this);
            imgV_3 = new ImageView(this);
            imgV_3.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath3));
            cutLayout_3.addView(imgV_3);
            cutLayout_3.setLayoutParams(params_cutLay);
//            imgV_3.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_3);
            gal_3_visi_LL.setVisibility(View.VISIBLE);
            gal_3_lock_LL.setVisibility(View.VISIBLE);
            gal_up_3.setVisibility(View.VISIBLE);
            gal_x_3.setVisibility(View.VISIBLE);
            gal_upload_3.setVisibility(View.GONE);
            img_gal_3.setClickable(false);
        }
        if (view.equals(img_gal_4)) {
            cutLayout_4 = new CutLayout(this);
            cutLayout_4.setOnTouchListener(this);
            imgV_4 = new ImageView(this);
            imgV_4.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath4));
            cutLayout_4.addView(imgV_4);
            cutLayout_4.setLayoutParams(params_cutLay);
//            imgV_4.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_4);
            gal_4_visi_LL.setVisibility(View.VISIBLE);
            gal_4_lock_LL.setVisibility(View.VISIBLE);
            gal_up_4.setVisibility(View.VISIBLE);
            gal_x_4.setVisibility(View.VISIBLE);
            gal_upload_4.setVisibility(View.GONE);
            img_gal_4.setClickable(false);
        }
        if (view.equals(img_gal_5)) {
            cutLayout_5 = new CutLayout(this);
            cutLayout_5.setOnTouchListener(this);
            imgV_5 = new ImageView(this);
            imgV_5.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath5));
            cutLayout_5.addView(imgV_5);
            cutLayout_5.setLayoutParams(params_cutLay);
//            imgV_5.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_5);
            gal_5_visi_LL.setVisibility(View.VISIBLE);
            gal_5_lock_LL.setVisibility(View.VISIBLE);
            gal_up_5.setVisibility(View.VISIBLE);
            gal_x_5.setVisibility(View.VISIBLE);
            gal_upload_5.setVisibility(View.GONE);
            img_gal_5.setClickable(false);
        }
        if (view.equals(img_gal_6)) {
            cutLayout_6 = new CutLayout(this);
            cutLayout_6.setOnTouchListener(this);
            imgV_6 = new ImageView(this);
            imgV_6.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath6));
            cutLayout_6.addView(imgV_6);
            cutLayout_6.setLayoutParams(params_cutLay);
//            imgV_6.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_6);
            gal_6_visi_LL.setVisibility(View.VISIBLE);
            gal_6_lock_LL.setVisibility(View.VISIBLE);
            gal_up_6.setVisibility(View.VISIBLE);
            gal_x_6.setVisibility(View.VISIBLE);
            gal_upload_6.setVisibility(View.GONE);
            img_gal_6.setClickable(false);
        }
        if (view.equals(img_gal_7)) {
            cutLayout_7 = new CutLayout(this);
            cutLayout_7.setOnTouchListener(this);
            imgV_7 = new ImageView(this);
            imgV_7.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath7));
            cutLayout_7.addView(imgV_7);
            cutLayout_7.setLayoutParams(params_cutLay);
//            imgV_7.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_7);
            gal_7_visi_LL.setVisibility(View.VISIBLE);
            gal_7_lock_LL.setVisibility(View.VISIBLE);
            gal_up_7.setVisibility(View.VISIBLE);
            gal_x_7.setVisibility(View.VISIBLE);
            gal_upload_7.setVisibility(View.GONE);
            img_gal_7.setClickable(false);
        }
        if (view.equals(img_gal_8)) {
            cutLayout_8 = new CutLayout(this);
            cutLayout_8.setOnTouchListener(this);
            imgV_8 = new ImageView(this);
            imgV_8.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath8));
            cutLayout_8.addView(imgV_8);
            cutLayout_8.setLayoutParams(params_cutLay);
//            imgV_8.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_8);
            gal_8_visi_LL.setVisibility(View.VISIBLE);
            gal_8_lock_LL.setVisibility(View.VISIBLE);
            gal_up_8.setVisibility(View.VISIBLE);
            gal_x_8.setVisibility(View.VISIBLE);
            gal_upload_8.setVisibility(View.GONE);
            img_gal_8.setClickable(false);
        }
        if (view.equals(img_gal_9)) {
            cutLayout_9 = new CutLayout(this);
            cutLayout_9.setOnTouchListener(this);
            imgV_9 = new ImageView(this);
            imgV_9.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath9));
            cutLayout_9.addView(imgV_9);
            cutLayout_9.setLayoutParams(params_cutLay);
//            imgV_9.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_9);
            gal_9_visi_LL.setVisibility(View.VISIBLE);
            gal_9_lock_LL.setVisibility(View.VISIBLE);
            gal_up_9.setVisibility(View.VISIBLE);
            gal_x_9.setVisibility(View.VISIBLE);
            gal_upload_9.setVisibility(View.GONE);
            img_gal_9.setClickable(false);
        }
        if (view.equals(img_gal_10)) {
            cutLayout_10 = new CutLayout(this);
            cutLayout_10.setOnTouchListener(this);
            imgV_10 = new ImageView(this);
            imgV_10.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath10));
            cutLayout_10.addView(imgV_10);
            cutLayout_10.setLayoutParams(params_cutLay);
//            imgV_10.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_10);
            gal_10_visi_LL.setVisibility(View.VISIBLE);
            gal_10_lock_LL.setVisibility(View.VISIBLE);
            gal_up_10.setVisibility(View.VISIBLE);
            gal_x_10.setVisibility(View.VISIBLE);
            gal_upload_10.setVisibility(View.GONE);
            img_gal_10.setClickable(false);
        }
        if (view.equals(img_gal_11)) {
            cutLayout_11 = new CutLayout(this);
            cutLayout_11.setOnTouchListener(this);
            imgV_11 = new ImageView(this);
            imgV_11.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath11));
            cutLayout_11.addView(imgV_11);
            cutLayout_11.setLayoutParams(params_cutLay);
//            imgV_11.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_11);
            gal_11_visi_LL.setVisibility(View.VISIBLE);
            gal_11_lock_LL.setVisibility(View.VISIBLE);
            gal_up_11.setVisibility(View.VISIBLE);
            gal_x_11.setVisibility(View.VISIBLE);
            gal_upload_11.setVisibility(View.GONE);
            img_gal_11.setClickable(false);
        }
        if (view.equals(img_gal_12)) {
            cutLayout_12 = new CutLayout(this);
            cutLayout_12.setOnTouchListener(this);
            imgV_12 = new ImageView(this);
            imgV_12.setImageBitmap(BitmapFactory.decodeFile(selected_cc_ImagePath12));
            cutLayout_12.addView(imgV_12);
            cutLayout_12.setLayoutParams(params_cutLay);
//            imgV_12.setLayoutParams(params_cutLay);
            rL.addView(cutLayout_12);
            gal_12_visi_LL.setVisibility(View.VISIBLE);
            gal_12_lock_LL.setVisibility(View.VISIBLE);
            gal_up_12.setVisibility(View.VISIBLE);
            gal_x_12.setVisibility(View.VISIBLE);
            gal_upload_12.setVisibility(View.GONE);
            img_gal_12.setClickable(false);
        }

        if (view == gal_up_1) {
            cutLayout_1.bringToFront();
            cutLayout_1_bool = true;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_2) {
            cutLayout_2.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = true;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_3) {
            cutLayout_3.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = true;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_4) {
            cutLayout_4.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = true;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_5) {
            cutLayout_5.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = true;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_6) {
            cutLayout_6.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = true;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_7) {
            cutLayout_7.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = true;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_8) {
            cutLayout_8.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = true;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_9) {
            cutLayout_9.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = true;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_10) {
            cutLayout_10.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = true;
            cutLayout_11_bool = false;
            cutLayout_12_bool = false;
        } else if (view == gal_up_11) {
            cutLayout_11.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = true;
            cutLayout_12_bool = false;
        } else if (view == gal_up_12) {
            cutLayout_12.bringToFront();
            cutLayout_1_bool = false;
            cutLayout_2_bool = false;
            cutLayout_3_bool = false;
            cutLayout_4_bool = false;
            cutLayout_5_bool = false;
            cutLayout_6_bool = false;
            cutLayout_7_bool = false;
            cutLayout_8_bool = false;
            cutLayout_9_bool = false;
            cutLayout_10_bool = false;
            cutLayout_11_bool = false;
            cutLayout_12_bool = true;
        }

        if (emoji_edt_Butt == view) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                setEmoji_LL();
            }
            else {
                Toast.makeText(this, "Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (view == gallery_Butt) {
            gallery_Butt.setTextColor(getColor(R.color.light_blue));
            emoji_edt_Butt.setTextColor(getColor(R.color.white));
            photoEdit_butt.setTextColor(getColor(R.color.white));
            bg_Butt.setTextColor(getColor(R.color.white));
            theme_Butt.setTextColor(getColor(R.color.white));
            gallery_tab_ll.setBackgroundResource(R.color.dark_blue);
            photoEdit_tab_ll.setBackgroundResource(R.color.transparent);
            bg_tab_ll.setBackgroundResource(R.color.transparent);
            theme_tab_ll.setBackgroundResource(R.color.transparent);
            emoji_tool_ll.setBackgroundResource(R.color.transparent);
            emojiButt2.setVisibility(View.GONE);
            gallery_view_ll.setVisibility(View.VISIBLE);
            photoEdit_tool_ll.setVisibility(View.GONE);
            img_color_adj_LL.setVisibility(View.GONE);
            colors_ll.setVisibility(View.GONE);
            bg_grad_selectors_LL.setVisibility(View.GONE);
            bg_color_tool.setVisibility(View.GONE);

            theme_bool = false;
            setBgLL_bool = false;
            emoji_bool = false;
            style_2_txt_bool = false;


        }

        if (view == photoEdit_butt) {
            setPhotoEditView();
        }


        if (view == bg_multi_colo) {
            if (bg_multi_colo.getText().toString().trim().equals("Multi Colors ?")) {
                grad_bg_style1_ll.setVisibility(View.VISIBLE);
                grad_bg_style2_ll.setVisibility(View.GONE);
                bgGrad_LL.setVisibility(View.VISIBLE);
                bg_grad_Tools_ll.setVisibility(View.VISIBLE);
                bg_GRAD_0_1 = 1;
                bg_multi_colo.setText("Single Color ?");
                style_2_txt_bool = false;

            } else if (bg_multi_colo.getText().toString().trim().equals("Single Color ?")) {
                grad_bg_style1_ll.setVisibility(View.GONE);
                grad_bg_style2_ll.setVisibility(View.GONE);
                bgGrad_LL.setVisibility(View.GONE);
                bg_grad_Tools_ll.setVisibility(View.GONE);
                bg_GRAD_0_1 = 0;
                bg_multi_colo.setText("Multi Colors ?");
                style_2_txt_bool = false;
            }

        }

        if (view == bg_Butt) {
            setBgView();
        }

        if (view == theme_Butt) {
            setThemeView();
        }

        if (view == style_1_Butt) {
            style1_LL.setVisibility(View.VISIBLE);
            style2_LL.setVisibility(View.GONE);
            style_1_Butt_line.setBackgroundResource(R.color.white);
            style_2_Butt_line.setBackgroundResource(R.color.blue_grey_p5);
            style_2_txt_bool = false;
            style1_txt.setTextColor(getResources().getColor(R.color.light_blue));
            style2_txt.setTextColor(getResources().getColor(R.color.white));
            style_1_Butt_line.setBackgroundResource(R.color.light_blue);
            style_2_Butt_line.setBackgroundResource(R.color.blue_grey_p5);
            grad_bg_style1_ll.setVisibility(View.VISIBLE);
            grad_bg_style2_ll.setVisibility(View.GONE);
        }
        if (view == style_2_Butt) {
            style2_LL.setVisibility(View.VISIBLE);
            style1_LL.setVisibility(View.GONE);
            style_2_Butt_line.setBackgroundResource(R.color.white);
            style_1_Butt_line.setBackgroundResource(R.color.blue_grey_p5);
            style_2_txt_bool = true;
            style1_txt.setTextColor(getColor(R.color.white));
            style2_txt.setTextColor(getColor(R.color.light_blue));
            style_1_Butt_line.setBackgroundResource(R.color.blue_grey_p5);
            style_2_Butt_line.setBackgroundResource(R.color.light_blue);
            grad_bg_style1_ll.setVisibility(View.GONE);
            grad_bg_style2_ll.setVisibility(View.VISIBLE);
        }


        if (view == outputButt) {
            outputDialog();
        }


        if (view == flip_right) {
            if (cutLayout_1_bool) {
                if (cutLayout_1.getScaleX() == 1) {
                    cutLayout_1.setScaleX(-1);
                } else {
                    cutLayout_1.setScaleX(1);
                }
            } else if (cutLayout_2_bool) {
                if (cutLayout_2.getScaleX() == 1) {
                    cutLayout_2.setScaleX(-1);
                } else {
                    cutLayout_2.setScaleX(1);
                }
            } else if (cutLayout_3_bool) {
                if (cutLayout_3.getScaleX() == 1) {
                    cutLayout_3.setScaleX(-1);
                } else {
                    cutLayout_3.setScaleX(1);
                }
            } else if (cutLayout_4_bool) {
                if (cutLayout_4.getScaleX() == 1) {
                    cutLayout_4.setScaleX(-1);
                } else {
                    cutLayout_4.setScaleX(1);
                }
            } else if (cutLayout_5_bool) {
                if (cutLayout_5.getScaleX() == 1) {
                    cutLayout_5.setScaleX(-1);
                } else {
                    cutLayout_5.setScaleX(1);
                }
            } else if (cutLayout_6_bool) {
                if (cutLayout_6.getScaleX() == 1) {
                    cutLayout_6.setScaleX(-1);
                } else {
                    cutLayout_6.setScaleX(1);
                }
            } else if (cutLayout_7_bool) {
                if (cutLayout_7.getScaleX() == 1) {
                    cutLayout_7.setScaleX(-1);
                } else {
                    cutLayout_7.setScaleX(1);
                }
            } else if (cutLayout_8_bool) {
                if (cutLayout_8.getScaleX() == 1) {
                    cutLayout_8.setScaleX(-1);
                } else {
                    cutLayout_8.setScaleX(1);
                }
            } else if (cutLayout_9_bool) {
                if (cutLayout_9.getScaleX() == 1) {
                    cutLayout_9.setScaleX(-1);
                } else {
                    cutLayout_9.setScaleX(1);
                }
            } else if (cutLayout_10_bool) {
                if (cutLayout_10.getScaleX() == 1) {
                    cutLayout_10.setScaleX(-1);
                } else {
                    cutLayout_10.setScaleX(1);
                }
            } else if (cutLayout_11_bool) {
                if (cutLayout_11.getScaleX() == 1) {
                    cutLayout_11.setScaleX(-1);
                } else {
                    cutLayout_11.setScaleX(1);
                }
            } else if (cutLayout_12_bool) {
                if (cutLayout_12.getScaleX() == 1) {
                    cutLayout_12.setScaleX(-1);
                } else {
                    cutLayout_12.setScaleX(1);
                }
            } else if (theme_bool) {
                if (img_theme.getScaleX() == 1) {
                    img_theme.setScaleX(-1);
                } else {
                    img_theme.setScaleX(1);
                }
            }

        }


        if (view == flip_down) {

            if (theme_bool) {
                if (img_theme.getScaleY() == 1) {

                    img_theme.setScaleY(-1);
                } else {
                    img_theme.setScaleY(1);
                }
            } else if (cutLayout_1_bool) {
                if (cutLayout_1.getScaleY() == 1) {

                    cutLayout_1.setScaleY(-1);
                } else {
                    cutLayout_1.setScaleY(1);
                }
            } else if (cutLayout_2_bool) {
                if (cutLayout_2.getScaleY() == 1) {
                    cutLayout_2.setScaleY(-1);
                } else {
                    cutLayout_2.setScaleY(1);
                }
            } else if (cutLayout_3_bool) {
                if (cutLayout_3.getScaleY() == 1) {
                    cutLayout_3.setScaleY(-1);
                } else {
                    cutLayout_3.setScaleY(1);
                }
            } else if (cutLayout_4_bool) {
                if (cutLayout_4.getScaleY() == 1) {
                    cutLayout_4.setScaleY(-1);
                } else {
                    cutLayout_4.setScaleY(1);
                }
            } else if (cutLayout_5_bool) {
                if (cutLayout_5.getScaleY() == 1) {
                    cutLayout_5.setScaleY(-1);
                } else {
                    cutLayout_5.setScaleY(1);
                }
            } else if (cutLayout_6_bool) {
                if (cutLayout_6.getScaleY() == 1) {
                    cutLayout_6.setScaleY(-1);
                } else {
                    cutLayout_6.setScaleY(1);
                }
            } else if (cutLayout_7_bool) {
                if (cutLayout_7.getScaleY() == 1) {
                    cutLayout_7.setScaleY(-1);
                } else {
                    cutLayout_7.setScaleY(1);
                }
            } else if (cutLayout_8_bool) {
                if (cutLayout_8.getScaleY() == 1) {
                    cutLayout_8.setScaleY(-1);
                } else {
                    cutLayout_8.setScaleY(1);
                }
            } else if (cutLayout_9_bool) {
                if (cutLayout_9.getScaleY() == 1) {
                    cutLayout_9.setScaleY(-1);
                } else {
                    cutLayout_9.setScaleY(1);
                }
            } else if (cutLayout_10_bool) {
                if (cutLayout_10.getScaleY() == 1) {
                    cutLayout_10.setScaleY(-1);
                } else {
                    cutLayout_10.setScaleY(1);
                }
            } else if (cutLayout_11_bool) {
                if (cutLayout_11.getScaleY() == 1) {
                    cutLayout_11.setScaleY(-1);
                } else {
                    cutLayout_11.setScaleY(1);
                }
            } else if (cutLayout_12_bool) {
                if (cutLayout_12.getScaleY() == 1) {
                    cutLayout_12.setScaleY(-1);
                } else {
                    cutLayout_12.setScaleY(1);
                }
            }


        }


        if (view == red_c) {
            c1.setBackgroundResource(R.color.red_p1);
            c2.setBackgroundResource(R.color.red_p2);
            c3.setBackgroundResource(R.color.red_p3);
            c4.setBackgroundResource(R.color.red_p4);
            c5.setBackgroundResource(R.color.red_p5);
            c6.setBackgroundResource(R.color.red_p6);
            c7.setBackgroundResource(R.color.red_p7);
            c8.setBackgroundResource(R.color.red_p8);
            c9.setBackgroundResource(R.color.red_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == purple_c) {
            c1.setBackgroundResource(R.color.purple_p1);
            c2.setBackgroundResource(R.color.purple_p2);
            c3.setBackgroundResource(R.color.purple_p3);
            c4.setBackgroundResource(R.color.purple_p4);
            c5.setBackgroundResource(R.color.purple_p5);
            c6.setBackgroundResource(R.color.purple_p6);
            c7.setBackgroundResource(R.color.purple_p7);
            c8.setBackgroundResource(R.color.purple_p8);
            c9.setBackgroundResource(R.color.purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == pink_c) {
            c1.setBackgroundResource(R.color.pink_p1);
            c2.setBackgroundResource(R.color.pink_p2);
            c3.setBackgroundResource(R.color.pink_p3);
            c4.setBackgroundResource(R.color.pink_p4);
            c5.setBackgroundResource(R.color.pink_p5);
            c6.setBackgroundResource(R.color.pink_p6);
            c7.setBackgroundResource(R.color.pink_p7);
            c8.setBackgroundResource(R.color.pink_p8);
            c9.setBackgroundResource(R.color.pink_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == deepPurple_c) {
            c1.setBackgroundResource(R.color.deep_purple_p1);
            c2.setBackgroundResource(R.color.deep_purple_p2);
            c3.setBackgroundResource(R.color.deep_purple_p3);
            c4.setBackgroundResource(R.color.deep_purple_p4);
            c5.setBackgroundResource(R.color.deep_purple_p5);
            c6.setBackgroundResource(R.color.deep_purple_p6);
            c7.setBackgroundResource(R.color.deep_purple_p7);
            c8.setBackgroundResource(R.color.deep_purple_p8);
            c9.setBackgroundResource(R.color.deep_purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == indigo_c) {
            c1.setBackgroundResource(R.color.indigo_p1);
            c2.setBackgroundResource(R.color.indigo_p2);
            c3.setBackgroundResource(R.color.indigo_p3);
            c4.setBackgroundResource(R.color.indigo_p4);
            c5.setBackgroundResource(R.color.indigo_p5);
            c6.setBackgroundResource(R.color.indigo_p6);
            c7.setBackgroundResource(R.color.indigo_p7);
            c8.setBackgroundResource(R.color.indigo_p8);
            c9.setBackgroundResource(R.color.indigo_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == blue_c) {
            c1.setBackgroundResource(R.color.blue_p1);
            c2.setBackgroundResource(R.color.blue_p2);
            c3.setBackgroundResource(R.color.blue_p3);
            c4.setBackgroundResource(R.color.blue_p4);
            c5.setBackgroundResource(R.color.blue_p5);
            c6.setBackgroundResource(R.color.blue_p6);
            c7.setBackgroundResource(R.color.blue_p7);
            c8.setBackgroundResource(R.color.blue_p8);
            c9.setBackgroundResource(R.color.blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == cyan_c) {
            c1.setBackgroundResource(R.color.cyan_p1);
            c2.setBackgroundResource(R.color.cyan_p2);
            c3.setBackgroundResource(R.color.cyan_p3);
            c4.setBackgroundResource(R.color.cyan_p4);
            c5.setBackgroundResource(R.color.cyan_p5);
            c6.setBackgroundResource(R.color.cyan_p6);
            c7.setBackgroundResource(R.color.cyan_p7);
            c8.setBackgroundResource(R.color.cyan_p8);
            c9.setBackgroundResource(R.color.cyan_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lightBlue_c) {
            c1.setBackgroundResource(R.color.light_blue_p1);
            c2.setBackgroundResource(R.color.light_blue_p2);
            c3.setBackgroundResource(R.color.light_blue_p3);
            c4.setBackgroundResource(R.color.light_blue_p4);
            c5.setBackgroundResource(R.color.light_blue_p5);
            c6.setBackgroundResource(R.color.light_blue_p6);
            c7.setBackgroundResource(R.color.light_blue_p7);
            c8.setBackgroundResource(R.color.light_blue_p8);
            c9.setBackgroundResource(R.color.light_blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == teal_c) {
            c1.setBackgroundResource(R.color.teal_p1);
            c2.setBackgroundResource(R.color.teal_p2);
            c3.setBackgroundResource(R.color.teal_p3);
            c4.setBackgroundResource(R.color.teal_p4);
            c5.setBackgroundResource(R.color.teal_p5);
            c6.setBackgroundResource(R.color.teal_p6);
            c7.setBackgroundResource(R.color.teal_p7);
            c8.setBackgroundResource(R.color.teal_p8);
            c9.setBackgroundResource(R.color.teal_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == green_c) {
            c1.setBackgroundResource(R.color.green_p1);
            c2.setBackgroundResource(R.color.green_p2);
            c3.setBackgroundResource(R.color.green_p3);
            c4.setBackgroundResource(R.color.green_p4);
            c5.setBackgroundResource(R.color.green_p5);
            c6.setBackgroundResource(R.color.green_p6);
            c7.setBackgroundResource(R.color.green_p7);
            c8.setBackgroundResource(R.color.green_p8);
            c9.setBackgroundResource(R.color.green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lightGreen_c) {
            c1.setBackgroundResource(R.color.light_green_p1);
            c2.setBackgroundResource(R.color.light_green_p2);
            c3.setBackgroundResource(R.color.light_green_p3);
            c4.setBackgroundResource(R.color.light_green_p4);
            c5.setBackgroundResource(R.color.light_green_p5);
            c6.setBackgroundResource(R.color.light_green_p6);
            c7.setBackgroundResource(R.color.light_green_p7);
            c8.setBackgroundResource(R.color.light_green_p8);
            c9.setBackgroundResource(R.color.light_green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lime_c) {
            c1.setBackgroundResource(R.color.lime_p1);
            c2.setBackgroundResource(R.color.lime_p2);
            c3.setBackgroundResource(R.color.lime_p3);
            c4.setBackgroundResource(R.color.lime_p4);
            c5.setBackgroundResource(R.color.lime_p5);
            c6.setBackgroundResource(R.color.lime_p6);
            c7.setBackgroundResource(R.color.lime_p7);
            c8.setBackgroundResource(R.color.lime_p8);
            c9.setBackgroundResource(R.color.lime_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == yellow_c) {
            c1.setBackgroundResource(R.color.yellow_p1);
            c2.setBackgroundResource(R.color.yellow_p2);
            c3.setBackgroundResource(R.color.yellow_p3);
            c4.setBackgroundResource(R.color.yellow_p4);
            c5.setBackgroundResource(R.color.yellow_p5);
            c6.setBackgroundResource(R.color.yellow_p6);
            c7.setBackgroundResource(R.color.yellow_p7);
            c8.setBackgroundResource(R.color.yellow_p8);
            c9.setBackgroundResource(R.color.yellow_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == amber_c) {
            c1.setBackgroundResource(R.color.amber_p1);
            c2.setBackgroundResource(R.color.amber_p2);
            c3.setBackgroundResource(R.color.amber_p3);
            c4.setBackgroundResource(R.color.amber_p4);
            c5.setBackgroundResource(R.color.amber_p5);
            c6.setBackgroundResource(R.color.amber_p6);
            c7.setBackgroundResource(R.color.amber_p7);
            c8.setBackgroundResource(R.color.amber_p8);
            c9.setBackgroundResource(R.color.amber_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == orange_c) {
            c1.setBackgroundResource(R.color.orange_p1);
            c2.setBackgroundResource(R.color.orange_p2);
            c3.setBackgroundResource(R.color.orange_p3);
            c4.setBackgroundResource(R.color.orange_p4);
            c5.setBackgroundResource(R.color.orange_p5);
            c6.setBackgroundResource(R.color.orange_p6);
            c7.setBackgroundResource(R.color.orange_p7);
            c8.setBackgroundResource(R.color.orange_p8);
            c9.setBackgroundResource(R.color.orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == deepOrange_c) {
            c1.setBackgroundResource(R.color.dep_orange_p1);
            c2.setBackgroundResource(R.color.dep_orange_p2);
            c3.setBackgroundResource(R.color.dep_orange_p3);
            c4.setBackgroundResource(R.color.dep_orange_p4);
            c5.setBackgroundResource(R.color.dep_orange_p5);
            c6.setBackgroundResource(R.color.dep_orange_p6);
            c7.setBackgroundResource(R.color.dep_orange_p7);
            c8.setBackgroundResource(R.color.dep_orange_p8);
            c9.setBackgroundResource(R.color.dep_orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == brown_c) {
            c1.setBackgroundResource(R.color.brown_p1);
            c2.setBackgroundResource(R.color.brown_p2);
            c3.setBackgroundResource(R.color.brown_p3);
            c4.setBackgroundResource(R.color.brown_p4);
            c5.setBackgroundResource(R.color.brown_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }
        if (view == grey_c) {
            c1.setBackgroundResource(R.color.grey_p1);
            c2.setBackgroundResource(R.color.grey_p2);
            c3.setBackgroundResource(R.color.grey_p3);
            c4.setBackgroundResource(R.color.grey_p4);
            c5.setBackgroundResource(R.color.grey_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }
        if (view == blueGrey_c) {
            c1.setBackgroundResource(R.color.blue_grey_p1);
            c2.setBackgroundResource(R.color.blue_grey_p2);
            c3.setBackgroundResource(R.color.blue_grey_p3);
            c4.setBackgroundResource(R.color.blue_grey_p4);
            c5.setBackgroundResource(R.color.blue_grey_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }

        if (view == black_c) {
            ll1.setVisibility(View.GONE);
            ColorDrawable cd = (ColorDrawable) black_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }
        if (view == white_c) {
            ll1.setVisibility(View.GONE);
            ColorDrawable cd = (ColorDrawable) white_c.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c1) {
            ColorDrawable cd = (ColorDrawable) c1.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c2) {
            ColorDrawable cd = (ColorDrawable) c2.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c3) {
            ColorDrawable cd = (ColorDrawable) c3.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c4) {
            ColorDrawable cd = (ColorDrawable) c4.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c5) {
            ColorDrawable cd = (ColorDrawable) c5.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c6) {
            ColorDrawable cd = (ColorDrawable) c6.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c7) {
            ColorDrawable cd = (ColorDrawable) c7.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c8) {
            ColorDrawable cd = (ColorDrawable) c8.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == c9) {
            ColorDrawable cd = (ColorDrawable) c9.getBackground();
            getViewColo = cd.getColor();

            getGradBg_Style_1();
            getGradBg_Style_2();
            getLogoColor();
        }


        if (view == linear_gd_po_T) {
            gd.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
        } else if (view == linear_gd_po_TR) {
            gd.setOrientation(GradientDrawable.Orientation.TR_BL);
        } else if (view == linear_gd_po_R) {
            gd.setOrientation(GradientDrawable.Orientation.RIGHT_LEFT);
        } else if (view == linear_gd_po_BR) {
            gd.setOrientation(GradientDrawable.Orientation.BR_TL);
        } else if (view == linear_gd_po_B) {
            gd.setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
        } else if (view == linear_gd_po_BL) {
            gd.setOrientation(GradientDrawable.Orientation.BL_TR);
        } else if (view == linear_gd_po_L) {
            gd.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
        } else if (view == linear_gd_po_TL) {
            gd.setOrientation(GradientDrawable.Orientation.TL_BR);
        }


    }

    private void setBgView() {
        gallery_Butt.setTextColor(getColor(R.color.white));
        emoji_edt_Butt.setTextColor(getColor(R.color.white));
        theme_Butt.setTextColor(getColor(R.color.white));
        photoEdit_butt.setTextColor(getColor(R.color.white));
        bg_Butt.setTextColor(getColor(R.color.light_blue));
        gallery_tab_ll.setBackgroundResource(R.color.transparent);
        photoEdit_tab_ll.setBackgroundResource(R.color.transparent);
        bg_tab_ll.setBackgroundResource(R.color.dark_blue);
        theme_tab_ll.setBackgroundResource(R.color.transparent);
        emoji_tool_ll.setBackgroundResource(R.color.transparent);

        emojiButt2.setVisibility(View.GONE);
        gallery_view_ll.setVisibility(View.GONE);
        photoEdit_tool_ll.setVisibility(View.GONE);
        img_color_adj_LL.setVisibility(View.GONE);
        colors_ll.setVisibility(View.VISIBLE);
        bg_grad_selectors_LL.setVisibility(View.VISIBLE);
        bg_color_tool.setVisibility(View.VISIBLE);

        setBgLL_bool = true;
        style_2_txt_bool = false;
        theme_bool = false;
        emoji_bool = false;
    }


    private void setThemeView() {
        gallery_Butt.setTextColor(getResources().getColor(R.color.white));
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        theme_Butt.setTextColor(getResources().getColor(R.color.light_blue));
        photoEdit_butt.setTextColor(getResources().getColor(R.color.white));
        bg_Butt.setTextColor(getResources().getColor(R.color.white));
        gallery_tab_ll.setBackgroundResource(R.color.transparent);
        emoji_tool_ll.setBackgroundResource(R.color.transparent);
        photoEdit_tab_ll.setBackgroundResource(R.color.transparent);
        bg_tab_ll.setBackgroundResource(R.color.transparent);
        theme_tab_ll.setBackgroundResource(R.color.dark_blue);
        emojiButt2.setVisibility(View.GONE);
        gallery_view_ll.setVisibility(View.GONE);
        photoEdit_tool_ll.setVisibility(View.GONE);
        img_color_adj_LL.setVisibility(View.VISIBLE);
        colors_ll.setVisibility(View.GONE);
        bg_grad_selectors_LL.setVisibility(View.GONE);
        bg_color_tool.setVisibility(View.GONE);

        contrast.setProgress(theme_contrast_save);
        opacity.setProgress(theme_opacity_save);
        bWSeek.setProgress(theme_bw_save);

        emoji_bool = false;
        theme_bool = true;
        setBgLL_bool = false;
        style_2_txt_bool = false;

        cutLayout_1_bool = false;
        cutLayout_2_bool = false;
        cutLayout_3_bool = false;
        cutLayout_4_bool = false;
        cutLayout_5_bool = false;
        cutLayout_6_bool = false;
        cutLayout_7_bool = false;
        cutLayout_8_bool = false;
        cutLayout_9_bool = false;
        cutLayout_10_bool = false;
        cutLayout_11_bool = false;
        cutLayout_12_bool = false;

    }

    private void setPhotoEditView() {
        gallery_Butt.setTextColor(getResources().getColor(R.color.white));
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        photoEdit_butt.setTextColor(getResources().getColor(R.color.light_blue));
        bg_Butt.setTextColor(getResources().getColor(R.color.white));
        theme_Butt.setTextColor(getResources().getColor(R.color.white));
        gallery_tab_ll.setBackgroundResource(R.color.transparent);
        photoEdit_tab_ll.setBackgroundResource(R.color.dark_blue);
        theme_tab_ll.setBackgroundResource(R.color.transparent);
        bg_tab_ll.setBackgroundResource(R.color.transparent);
        emoji_tool_ll.setBackgroundResource(R.color.transparent);
        emojiButt2.setVisibility(View.GONE);
        gallery_view_ll.setVisibility(View.GONE);
        bg_color_tool.setVisibility(View.GONE);

        photoEdit_tool_ll.setVisibility(View.VISIBLE);
        img_color_adj_LL.setVisibility(View.VISIBLE);
        colors_ll.setVisibility(View.GONE);
        bg_grad_selectors_LL.setVisibility(View.GONE);
        theme_bool = false;
        emoji_bool = false;
        setBgLL_bool = false;
        style_2_txt_bool = false;

    }

    private void setEmoji_LL() {
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.light_blue));
        gallery_Butt.setTextColor(getResources().getColor(R.color.white));
        photoEdit_butt.setTextColor(getResources().getColor(R.color.white));
        bg_Butt.setTextColor(getResources().getColor(R.color.white));
        theme_Butt.setTextColor(getResources().getColor(R.color.white));
        emoji_tool_ll.setBackgroundResource(R.color.dark_blue);
        gallery_tab_ll.setBackgroundResource(R.color.transparent);
        photoEdit_tab_ll.setBackgroundResource(R.color.transparent);
        bg_tab_ll.setBackgroundResource(R.color.transparent);
        theme_tab_ll.setBackgroundResource(R.color.transparent);
        bg_color_tool.setVisibility(View.GONE);

        emojiButt2.setVisibility(View.VISIBLE);
        gallery_view_ll.setVisibility(View.GONE);
        photoEdit_tool_ll.setVisibility(View.GONE);
        img_color_adj_LL.setVisibility(View.GONE);
        colors_ll.setVisibility(View.VISIBLE);
        bg_grad_selectors_LL.setVisibility(View.GONE);

        theme_bool = false;
        emoji_bool=true ;
        setBgLL_bool = false;
        style_2_txt_bool = false;

    }


    private void outputDialog() {
        final ImageView outPut_img;
        final TextView save_to_gallery_butt, share_to_fb, share_to_messenger, share_to_whatsapp, share_to_Instagram;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_edit_output);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);
        outPut_img = (ImageView) dialog.findViewById(R.id.outPut_img);
        save_to_gallery_butt = (TextView) dialog.findViewById(R.id.save_to_gallery_butt);
        share_to_whatsapp = (TextView) dialog.findViewById(R.id.share_to_whatsapp);

        RelativeLayout.LayoutParams paramsRL = new RelativeLayout.LayoutParams(screen_width, screen_width);
        outPut_img.setLayoutParams(paramsRL);

        final Bitmap bitmapOP = getBitmap(rL);
        outPut_img.setImageBitmap(bitmapOP);

        save_to_gallery_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FrameoEdit.this, "Saved To Gallery", Toast.LENGTH_LONG).show();
                saveChart(bitmapOP, rL.getMeasuredHeight(), rL.getMeasuredWidth());

                dialog.cancel();
            }

        });

        share_to_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Picxture");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "PX Testing..");
//                sharingIntent.putExtra(Intent.EXTRA_STREAM, bitmapOP);
//                sharingIntent.setType("image/jpeg");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                dialog.cancel();
            }

        });

        dialog.show();
    }

    public Bitmap getBitmap(RelativeLayout layout) {
        layout.setDrawingCacheEnabled(true);
        layout.buildDrawingCache();
        Bitmap bmp = Bitmap.createBitmap(layout.getDrawingCache());
        layout.setDrawingCacheEnabled(false);
        return bmp;
    }

    public void saveChart(Bitmap getbitmap, float height, float width) {
        Toast.makeText(this, "SAVE", Toast.LENGTH_LONG).show();

        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString(), "Picxture");
        //   .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"myfolder");
        boolean success = false;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        Log.d("gggggg", folder.getPath());
        File file = new File(folder.getPath() + File.separator + "/" + timeStamp + ".png");
        if (!file.exists()) {
            try {
                success = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileOutputStream ostream = null;
        try {
            ostream = new FileOutputStream(file);
            System.out.println(ostream);
            Bitmap well = getbitmap;
            Bitmap save = Bitmap.createBitmap((int) width, (int) height, Bitmap.Config.ARGB_8888);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            Canvas now = new Canvas(save);
            now.drawRect(new Rect(0, 0, (int) width, (int) height), paint);
            now.drawBitmap(well,
                    new Rect(0, 0, well.getWidth(), well.getHeight()),
                    new Rect(0, 0, (int) width, (int) height), null);
            if (save == null) {
                System.out.println("NULL");
            }
            save.compress(Bitmap.CompressFormat.PNG, 100, ostream);
            ContentValues values = new ContentValues();
            //   values.put(Images.Media.TITLE, this.getString(R.string.picture_title));
            //   values.put(Images.Media.DESCRIPTION, this.getString(R.string.picture_description));
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.ImageColumns.BUCKET_ID, file.toString().toLowerCase(Locale.US).hashCode());
            values.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, file.getName().toLowerCase(Locale.US));
            values.put("_data", file.getAbsolutePath());

            ContentResolver cr = getContentResolver();
            cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        } catch (NullPointerException e) {
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(), &quot;Null error&quot;, Toast.LENGTH_SHORT).show();<br />
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // Toast.makeText(getApplicationContext(), &quot;File error&quot;, Toast.LENGTH_SHORT).show();<br />
        }
//        catch (IOException e){
//            e.printStackTrace();
//            // Toast.makeText(getApplicationContext(), &quot;IO error&quot;, Toast.LENGTH_SHORT).show();<br />
//        }
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}




