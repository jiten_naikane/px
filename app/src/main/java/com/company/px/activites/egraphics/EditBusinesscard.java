package com.company.px.activites.egraphics;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.company.px.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class EditBusinesscard extends AppCompatActivity implements View.OnClickListener {
    ImageView upload_logo, immg1, immg2, img_bg;
    TextView companyName, tagline, name, designation, number, email, offNumber, offEmail;
    LinearLayout bottomSheet, ll1, ll2,linear_gallry;
    Uri img_path;
    private  BottomSheetBehavior mBottomSheetBehavior;
    View black_c, white_c, red_c, purple_c,pink_c, deepPurple_c, indigo_c, blue_c, cyan_c, lightBlue_c, teal_c, green_c, lightGreen_c, lime_c, yellow_c, amber_c, orange_c, deepOrange_c, brown_c, grey_c, blueGrey_c, c1, c2, c3, c4, c5, c6, c7, c8, c9;

    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.businesscard_cl);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String imagePath = getIntent().getStringExtra("upload_logoS");
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);

        linear_gallry=(LinearLayout)findViewById(R.id.linear_gallry);
//save_gallry
        bottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState)
            {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED)
                {
                    mBottomSheetBehavior.setPeekHeight(60);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        upload_logo = (ImageView) findViewById(R.id.upload_logo);
        img_bg = (ImageView) findViewById(R.id.img_bg);
        immg2 = (ImageView) findViewById(R.id.immg2);
        immg2.setOnClickListener(this);
        immg1 = (ImageView) findViewById(R.id.immg1);
        immg1.setOnClickListener(this);

        c1 = (View) findViewById(R.id.c1);
        c2 = (View) findViewById(R.id.c2);
        c3 = (View) findViewById(R.id.c3);
        c4 = (View) findViewById(R.id.c4);
        c5 = (View) findViewById(R.id.c5);
        c6 = (View) findViewById(R.id.c6);
        c7 = (View) findViewById(R.id.c7);
        c8 = (View) findViewById(R.id.c8);
        c9 = (View) findViewById(R.id.c9);
        black_c = (View) findViewById(R.id.black_c);
        black_c.setOnClickListener(this);
        white_c = (View) findViewById(R.id.white_c);
        white_c.setOnClickListener(this);
        red_c = (View) findViewById(R.id.red_c);
        red_c.setOnClickListener(this);
        purple_c = (View) findViewById(R.id.purple_c);
        purple_c.setOnClickListener(this);
        pink_c = (View) findViewById(R.id.pink_c);
        pink_c.setOnClickListener(this);
        deepPurple_c = (View) findViewById(R.id.deepPurple_c);
        deepPurple_c.setOnClickListener(this);
        indigo_c = (View) findViewById(R.id.indigo_c);
        indigo_c.setOnClickListener(this);
        blue_c = (View) findViewById(R.id.blue_c);
        blue_c.setOnClickListener(this);
        cyan_c = (View) findViewById(R.id.cyan_c);
        cyan_c.setOnClickListener(this);
        lightBlue_c = (View) findViewById(R.id.lightBlue_c);
        lightBlue_c.setOnClickListener(this);
        teal_c = (View) findViewById(R.id.teal_c);
        teal_c.setOnClickListener(this);
        green_c = (View) findViewById(R.id.green_c);
        green_c.setOnClickListener(this);
        lightGreen_c = (View) findViewById(R.id.lightGreen_c);
        lightGreen_c.setOnClickListener(this);
        lime_c = (View) findViewById(R.id.lime_c);
        lime_c.setOnClickListener(this);
        yellow_c = (View) findViewById(R.id.yellow_c);
        yellow_c.setOnClickListener(this);
        amber_c = (View) findViewById(R.id.amber_c);
        amber_c.setOnClickListener(this);
        orange_c = (View) findViewById(R.id.orange_c);
        orange_c.setOnClickListener(this);
        deepOrange_c = (View) findViewById(R.id.deepOrange_c);
        deepOrange_c.setOnClickListener(this);
        brown_c = (View) findViewById(R.id.brown_c);
        brown_c.setOnClickListener(this);
        grey_c = (View) findViewById(R.id.grey_c);
        grey_c.setOnClickListener(this);
        blueGrey_c = (View) findViewById(R.id.blueGrey_c);
        blueGrey_c.setOnClickListener(this);

        ll1 = (LinearLayout) findViewById(R.id.ll1);
        ll1.setVisibility(View.GONE);
        ll2 = (LinearLayout) findViewById(R.id.ll2);

        companyName = (TextView) findViewById(R.id.companyName);
        companyName.setText(getIntent().getExtras().getString("companyNameS"));
        tagline = (TextView) findViewById(R.id.tagline);
        tagline.setText(getIntent().getExtras().getString("taglineS"));
        name = (TextView) findViewById(R.id.name);
        name.setText(getIntent().getExtras().getString("nameS"));
        designation = (TextView) findViewById(R.id.designation);
        designation.setText(getIntent().getExtras().getString("designationS"));
        number = (TextView) findViewById(R.id.number);
        number.setText(getIntent().getExtras().getString("numberS"));
        email = (TextView) findViewById(R.id.email);
        email.setText(getIntent().getExtras().getString("emailS"));
        offNumber = (TextView) findViewById(R.id.offNumber);
        offNumber.setText(getIntent().getExtras().getString("offNumberS"));
        offEmail = (TextView) findViewById(R.id.offEmail);
        offEmail.setText(getIntent().getExtras().getString("offEmailS"));


        upload_logo.setImageBitmap(bitmap);




    }



    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View view)
    {
        if (view == immg1){
            img_bg.setImageDrawable(immg1.getDrawable());
        }
        if (view == immg2){
            img_bg.setImageDrawable(immg2.getDrawable());
        }
        if (view == black_c){
            ll1.setVisibility(View.GONE);
        }
        if (view == white_c){
            ll1.setVisibility(View.GONE);
        }
        if (view == red_c){
            c1.setBackgroundResource(R.color.red_p1);
            c2.setBackgroundResource(R.color.red_p2);
            c3.setBackgroundResource(R.color.red_p3);
            c4.setBackgroundResource(R.color.red_p4);
            c5.setBackgroundResource(R.color.red_p5);
            c6.setBackgroundResource(R.color.red_p6);
            c7.setBackgroundResource(R.color.red_p7);
            c8.setBackgroundResource(R.color.red_p8);
            c9.setBackgroundResource(R.color.red_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == purple_c){
            c1.setBackgroundResource(R.color.purple_p1);
            c2.setBackgroundResource(R.color.purple_p2);
            c3.setBackgroundResource(R.color.purple_p3);
            c4.setBackgroundResource(R.color.purple_p4);
            c5.setBackgroundResource(R.color.purple_p5);
            c6.setBackgroundResource(R.color.purple_p6);
            c7.setBackgroundResource(R.color.purple_p7);
            c8.setBackgroundResource(R.color.purple_p8);
            c9.setBackgroundResource(R.color.purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == pink_c){
            c1.setBackgroundResource(R.color.pink_p1);
            c2.setBackgroundResource(R.color.pink_p2);
            c3.setBackgroundResource(R.color.pink_p3);
            c4.setBackgroundResource(R.color.pink_p4);
            c5.setBackgroundResource(R.color.pink_p5);
            c6.setBackgroundResource(R.color.pink_p6);
            c7.setBackgroundResource(R.color.pink_p7);
            c8.setBackgroundResource(R.color.pink_p8);
            c9.setBackgroundResource(R.color.pink_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == deepPurple_c){
            c1.setBackgroundResource(R.color.deep_purple_p1);
            c2.setBackgroundResource(R.color.deep_purple_p2);
            c3.setBackgroundResource(R.color.deep_purple_p3);
            c4.setBackgroundResource(R.color.deep_purple_p4);
            c5.setBackgroundResource(R.color.deep_purple_p5);
            c6.setBackgroundResource(R.color.deep_purple_p6);
            c7.setBackgroundResource(R.color.deep_purple_p7);
            c8.setBackgroundResource(R.color.deep_purple_p8);
            c9.setBackgroundResource(R.color.deep_purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == indigo_c){
            c1.setBackgroundResource(R.color.indigo_p1);
            c2.setBackgroundResource(R.color.indigo_p2);
            c3.setBackgroundResource(R.color.indigo_p3);
            c4.setBackgroundResource(R.color.indigo_p4);
            c5.setBackgroundResource(R.color.indigo_p5);
            c6.setBackgroundResource(R.color.indigo_p6);
            c7.setBackgroundResource(R.color.indigo_p7);
            c8.setBackgroundResource(R.color.indigo_p8);
            c9.setBackgroundResource(R.color.indigo_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == blue_c){
            c1.setBackgroundResource(R.color.blue_p1);
            c2.setBackgroundResource(R.color.blue_p2);
            c3.setBackgroundResource(R.color.blue_p3);
            c4.setBackgroundResource(R.color.blue_p4);
            c5.setBackgroundResource(R.color.blue_p5);
            c6.setBackgroundResource(R.color.blue_p6);
            c7.setBackgroundResource(R.color.blue_p7);
            c8.setBackgroundResource(R.color.blue_p8);
            c9.setBackgroundResource(R.color.blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == cyan_c){
            c1.setBackgroundResource(R.color.cyan_p1);
            c2.setBackgroundResource(R.color.cyan_p2);
            c3.setBackgroundResource(R.color.cyan_p3);
            c4.setBackgroundResource(R.color.cyan_p4);
            c5.setBackgroundResource(R.color.cyan_p5);
            c6.setBackgroundResource(R.color.cyan_p6);
            c7.setBackgroundResource(R.color.cyan_p7);
            c8.setBackgroundResource(R.color.cyan_p8);
            c9.setBackgroundResource(R.color.cyan_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lightBlue_c){
            c1.setBackgroundResource(R.color.light_blue_p1);
            c2.setBackgroundResource(R.color.light_blue_p2);
            c3.setBackgroundResource(R.color.light_blue_p3);
            c4.setBackgroundResource(R.color.light_blue_p4);
            c5.setBackgroundResource(R.color.light_blue_p5);
            c6.setBackgroundResource(R.color.light_blue_p6);
            c7.setBackgroundResource(R.color.light_blue_p7);
            c8.setBackgroundResource(R.color.light_blue_p8);
            c9.setBackgroundResource(R.color.light_blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == teal_c){
            c1.setBackgroundResource(R.color.teal_p1);
            c2.setBackgroundResource(R.color.teal_p2);
            c3.setBackgroundResource(R.color.teal_p3);
            c4.setBackgroundResource(R.color.teal_p4);
            c5.setBackgroundResource(R.color.teal_p5);
            c6.setBackgroundResource(R.color.teal_p6);
            c7.setBackgroundResource(R.color.teal_p7);
            c8.setBackgroundResource(R.color.teal_p8);
            c9.setBackgroundResource(R.color.teal_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == green_c){
            c1.setBackgroundResource(R.color.green_p1);
            c2.setBackgroundResource(R.color.green_p2);
            c3.setBackgroundResource(R.color.green_p3);
            c4.setBackgroundResource(R.color.green_p4);
            c5.setBackgroundResource(R.color.green_p5);
            c6.setBackgroundResource(R.color.green_p6);
            c7.setBackgroundResource(R.color.green_p7);
            c8.setBackgroundResource(R.color.green_p8);
            c9.setBackgroundResource(R.color.green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lightGreen_c){
            c1.setBackgroundResource(R.color.light_green_p1);
            c2.setBackgroundResource(R.color.light_green_p2);
            c3.setBackgroundResource(R.color.light_green_p3);
            c4.setBackgroundResource(R.color.light_green_p4);
            c5.setBackgroundResource(R.color.light_green_p5);
            c6.setBackgroundResource(R.color.light_green_p6);
            c7.setBackgroundResource(R.color.light_green_p7);
            c8.setBackgroundResource(R.color.light_green_p8);
            c9.setBackgroundResource(R.color.light_green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lime_c){
            c1.setBackgroundResource(R.color.lime_p1);
            c2.setBackgroundResource(R.color.lime_p2);
            c3.setBackgroundResource(R.color.lime_p3);
            c4.setBackgroundResource(R.color.lime_p4);
            c5.setBackgroundResource(R.color.lime_p5);
            c6.setBackgroundResource(R.color.lime_p6);
            c7.setBackgroundResource(R.color.lime_p7);
            c8.setBackgroundResource(R.color.lime_p8);
            c9.setBackgroundResource(R.color.lime_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == yellow_c){
            c1.setBackgroundResource(R.color.yellow_p1);
            c2.setBackgroundResource(R.color.yellow_p2);
            c3.setBackgroundResource(R.color.yellow_p3);
            c4.setBackgroundResource(R.color.yellow_p4);
            c5.setBackgroundResource(R.color.yellow_p5);
            c6.setBackgroundResource(R.color.yellow_p6);
            c7.setBackgroundResource(R.color.yellow_p7);
            c8.setBackgroundResource(R.color.yellow_p8);
            c9.setBackgroundResource(R.color.yellow_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == amber_c){
            c1.setBackgroundResource(R.color.amber_p1);
            c2.setBackgroundResource(R.color.amber_p2);
            c3.setBackgroundResource(R.color.amber_p3);
            c4.setBackgroundResource(R.color.amber_p4);
            c5.setBackgroundResource(R.color.amber_p5);
            c6.setBackgroundResource(R.color.amber_p6);
            c7.setBackgroundResource(R.color.amber_p7);
            c8.setBackgroundResource(R.color.amber_p8);
            c9.setBackgroundResource(R.color.amber_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == orange_c){
            c1.setBackgroundResource(R.color.orange_p1);
            c2.setBackgroundResource(R.color.orange_p2);
            c3.setBackgroundResource(R.color.orange_p3);
            c4.setBackgroundResource(R.color.orange_p4);
            c5.setBackgroundResource(R.color.orange_p5);
            c6.setBackgroundResource(R.color.orange_p6);
            c7.setBackgroundResource(R.color.orange_p7);
            c8.setBackgroundResource(R.color.orange_p8);
            c9.setBackgroundResource(R.color.orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == deepOrange_c){
            c1.setBackgroundResource(R.color.dep_orange_p1);
            c2.setBackgroundResource(R.color.dep_orange_p2);
            c3.setBackgroundResource(R.color.dep_orange_p3);
            c4.setBackgroundResource(R.color.dep_orange_p4);
            c5.setBackgroundResource(R.color.dep_orange_p5);
            c6.setBackgroundResource(R.color.dep_orange_p6);
            c7.setBackgroundResource(R.color.dep_orange_p7);
            c8.setBackgroundResource(R.color.dep_orange_p8);
            c9.setBackgroundResource(R.color.dep_orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == brown_c){
            c1.setBackgroundResource(R.color.brown_p1);
            c2.setBackgroundResource(R.color.brown_p2);
            c3.setBackgroundResource(R.color.brown_p3);
            c4.setBackgroundResource(R.color.brown_p4);
            c5.setBackgroundResource(R.color.brown_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }
        if (view == grey_c){
            c1.setBackgroundResource(R.color.grey_p1);
            c2.setBackgroundResource(R.color.grey_p2);
            c3.setBackgroundResource(R.color.grey_p3);
            c4.setBackgroundResource(R.color.grey_p4);
            c5.setBackgroundResource(R.color.grey_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }
        if (view == blueGrey_c){
            c1.setBackgroundResource(R.color.blue_grey_p1);
            c2.setBackgroundResource(R.color.blue_grey_p2);
            c3.setBackgroundResource(R.color.blue_grey_p3);
            c4.setBackgroundResource(R.color.blue_grey_p4);
            c5.setBackgroundResource(R.color.blue_grey_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }



    }
/*
    public Bitmap decodeSampledBitmapFromUri(Uri uri, int reqWidth, int reqHeight)
    {
        Bitmap bm = null;
        try
        {

// First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);
// Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,reqHeight);
// Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            bm = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri), null, options);

        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
        return bm;

    }

    public int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight)
    {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }*/

    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_editbusinesscrd_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.save_gallry:
               Toast.makeText(EditBusinesscard.this,"GALLRAY SAVE",Toast.LENGTH_LONG).show();
                Bitmap bitmap = getBitmap(linear_gallry);
                saveChart(bitmap, linear_gallry.getMeasuredHeight(), linear_gallry.getMeasuredWidth());
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public Bitmap getBitmap(LinearLayout layout)
    {
        layout.setDrawingCacheEnabled(true);
                layout.buildDrawingCache();
                Bitmap bmp = Bitmap.createBitmap(layout.getDrawingCache());
                layout.setDrawingCacheEnabled(false);
        return bmp;
    }

    public void saveChart(Bitmap getbitmap, float height, float width)
    {  Toast.makeText(EditBusinesscard.this,"SAVE",Toast.LENGTH_LONG).show();

            File folder = new File(Environment
            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString());
     //   .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"myfolder");
        boolean success = false;
        if (!folder.exists())
                {
                success = folder.mkdirs();
        }
       String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.getDefault()).format(new Date());
        Log.d("gggggg",folder.getPath());
        File file = new File(folder.getPath() + File.separator + "/"+timeStamp+".png");
      if ( !file.exists() )
                {
        try {
                success = file.createNewFile();
        } catch (IOException e) {
                e.printStackTrace();
        }
        }
        FileOutputStream ostream = null;
        try{
            ostream = new FileOutputStream(file);
            System.out.println(ostream);
            Bitmap well = getbitmap;
                    Bitmap save = Bitmap.createBitmap((int) width, (int) height, Bitmap.Config.ARGB_8888);
                    Paint paint = new Paint();
                    paint.setColor(Color.WHITE);
                    Canvas now = new Canvas(save);
                    now.drawRect(new Rect(0,0,(int) width, (int) height), paint);
                    now.drawBitmap(well,
                    new Rect(0,0,well.getWidth(),well.getHeight()),
                    new Rect(0,0,(int) width, (int) height), null);
           if(save == null) {
               System.out.println("NULL");
            }
                    save.compress(Bitmap.CompressFormat.PNG, 100, ostream);
        }
        catch (NullPointerException e){
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(), &quot;Null error&quot;, Toast.LENGTH_SHORT).show();<br />
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
            // Toast.makeText(getApplicationContext(), &quot;File error&quot;, Toast.LENGTH_SHORT).show();<br />
        }
//        catch (IOException e){
//            e.printStackTrace();
//            // Toast.makeText(getApplicationContext(), &quot;IO error&quot;, Toast.LENGTH_SHORT).show();<br />
//        }
    }




}
