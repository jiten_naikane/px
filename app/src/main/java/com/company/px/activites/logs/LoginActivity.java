package com.company.px.activites.logs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.R;
import com.company.px.activites.egraphics.StepOne_Description;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;
import com.company.px.utility.UserSessionManager;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public  class LoginActivity extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener {

    EditText email_et,password_edt;
    ImageView btn_login;
    String sendemai,sendpass;
    ProgressDialog pprogressDialog;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    LinearLayout btn_googleplus,btn_facebook;
    private GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_SIGN_IN = 420;
    private static final String TAG = LoginActivity.class.getSimpleName();
    String sendFBAccessToken, sendFBUserID, token = "", verified_data, fb_user_id_data, email_data, name_data, gender_data, ref_txn_id;
    String nm = "", lnm = "";
    private Boolean exit = false;
    TextView register_px_txt_butt;

    CallbackManager callbackManager;





    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        pprogressDialog = new ProgressDialog(LoginActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        userSessionManager=new UserSessionManager(LoginActivity.this);

        email_et=(EditText) findViewById(R.id.email_et);
        password_edt=(EditText) findViewById(R.id.password_edt);

        register_px_txt_butt=(TextView) findViewById(R.id.register_px_txt_butt);
        register_px_txt_butt.setOnClickListener(this);

        btn_login=(ImageView)findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);

        btn_googleplus=(LinearLayout)findViewById(R.id.btn_googleplus);
        btn_googleplus.setOnClickListener(this);
        btn_facebook=(LinearLayout)findViewById(R.id.btn_facebook);
        btn_facebook.setOnClickListener(this);

        initializeGPlusSettings();
        initializeFacebookSettings();
        try {
            PackageInfo info = getPackageManager().getPackageInfo("in.innasoft.activitychallenge", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));

            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.FCM_PREFERENCE), Context.MODE_PRIVATE);
        token = sharedPreferences.getString(getString(R.string.FCM_TOKEN), "");
        Log.d("TOKEVELUE", token);
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                JSONObject json = response.getJSONObject();
                                Log.d("LOGFBBB", json.toString());
                                if (json != null) {
                                    try {
                                        sendFBAccessToken = AccessToken.getCurrentAccessToken().getToken();
                                        AccessToken token1 = AccessToken.getCurrentAccessToken();
                                        if (token1 != null) {
                                            Log.d("ACCESSESTOKE", AccessToken.getCurrentAccessToken().getToken());
                                        }

                                        verified_data = json.getString("verified");
                                        fb_user_id_data = json.getString("id");
                                        sendFBUserID = fb_user_id_data;
                                        email_data = json.getString("email");
                                        name_data = json.getString("name");
                                        gender_data = json.getString("gender");
                                        String profile_ptah = "http://graph.facebook.com/" + fb_user_id_data + "/picture?type=large";
                                        Log.d("FACEBOOKPROFILE", verified_data + "," + fb_user_id_data + "," + sendFBUserID + ", " + email_data + ", " + email_data + ", " + gender_data + ",," + profile_ptah);
                                       /* userSessionManager.createUserLoginSession(AccessToken.getCurrentAccessToken().getToken(), fb_user_id_data, name_data, "", email_data, "", "facebook");
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        startActivity(intent);
                                       */

                                        // registerSocialDataFb(fb_user_id_data,name_data, email_data,profile_ptah, "Facebook");
                                        //     String nm="",lnm="";

                                        if (name_data.split("\\w+").length > 1) {

                                            lnm = name_data.substring(name_data.lastIndexOf(" ") + 1);
                                            nm = name_data.substring(0, name_data.lastIndexOf(' '));
                                        } else {
                                            //  firstName = name;
                                        }


                                        Intent i = new Intent(LoginActivity.this, RegisterActivity.class);

                                        i.putExtra("user_data_id", fb_user_id_data);
                                        i.putExtra("name", nm);
                                        i.putExtra("lastname", lnm);
                                        i.putExtra("email", email_data);
                                        i.putExtra("gender", gender_data);
                                        i.putExtra("provider", "Facebook");
                                        i.putExtra("LOGIN", "Facebook");

                                        Log.d("FBBB",  nm + "//" + lnm + "//" + fb_user_id_data + "//" + email_data + "//" + gender_data);
                                        startActivity(i);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,gender,link,email,verified,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d("EXCEPTION", exception.toString());
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void initializeGPlusSettings() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void initializeFacebookSettings() {
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());
    }


    @Override
    public void onClick(View v) {

        if(v==btn_login)
        {
                checkInternet= NetworkChecking.isConnected(LoginActivity.this);

                    if(checkInternet)
                    {

                        sendemai= email_et.getText().toString();
                        sendpass=password_edt.getText().toString();



                        Log.d("LOGINURL", AppUrls.BASE_URL+AppUrls.LOGIN);
                        StringRequest stringRequest=new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN, new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response)
                            {
                                Log.d("LGOINRESP",response);
                                try
                                {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String successResponceCode = jsonObject.getString("code");
                                    if (successResponceCode.equals("10100"))
                                    {
                                        pprogressDialog.dismiss();
                                        JSONObject jobj=jsonObject.getJSONObject("data");
                                        Log.d("JOBJ",jobj.toString());

                                        String token=jobj.getString("token");
                                        String user_id=jobj.getString("user_id");
                                        userSessionManager.createUserLoginSession(token,user_id);
                                        Intent imain = new Intent(LoginActivity.this, StepOne_Description.class);
                                        startActivity(imain);

                                      //  Log.d("JWT",token+"//"+user_id);
/*
                                        String[] parts = token.split("\\.");
                                        byte[] dataDec = Base64.decode(parts[1], Base64.DEFAULT);
                                        String decodedString = "";
                                        try
                                        {
                                            decodedString = new String(dataDec, "UTF-8");
                                            Log.d("TOKENSFSF", decodedString);
                                            JSONObject jsonObject2 = new JSONObject(decodedString);
                                            Log.d("JSONDATAsfa", jsonObject2.toString());


                                           *//* String username =jsonObject2.getString("name");
                                            String email=jsonObject2.getString("email");
                                            String mobile=jsonObject2.getString("mobile");
                                            String status=jsonObject2.getString("status");
                                            String profile_pic=jsonObject2.getString("profile_pic");*//*
                                            userSessionManager.createUserLoginSession(token,user_id);
                                            Intent imain = new Intent(LoginActivity.this, StepOne_Description.class);
                                            startActivity(imain);


                                        }
                                        catch (UnsupportedEncodingException e)
                                        {
                                            e.printStackTrace();
                                        }*/


                                    }
                                    if (successResponceCode.equals("10200")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "User Not Exist..!", Toast.LENGTH_SHORT).show();
                                    }
                                    if (successResponceCode.equals("10300")) {
                                        pprogressDialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Invalid Input..!", Toast.LENGTH_SHORT).show();
                                    }


                                }
                                catch (JSONException e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error)
                            {
                                pprogressDialog.dismiss();

                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                                } else if (error instanceof AuthFailureError) {

                                } else if (error instanceof ServerError) {

                                } else if (error instanceof NetworkError) {

                                } else if (error instanceof ParseError) {

                                }
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<String, String>();

                                params.put("email",sendemai);      // here if we login with mobile no. ,same field and variable use
                                params.put("password",sendpass);

                                Log.d("LOGINPARAM", params.toString());
                                return params;
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                        requestQueue.add(stringRequest);

                    }
                    else
                    {
                        pprogressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "No Internet Connection ..!!", Toast.LENGTH_SHORT).show();

                    }
                }



                if (v==register_px_txt_butt)
                {

                    Intent register_act = new Intent(this, RegisterActivity.class);
                    register_act.putExtra("LOGIN", "Manual");
                    startActivity(register_act);
                }

                if (v==btn_facebook)
                {
                    checkInternet = NetworkChecking.isConnected(this);
                    if (checkInternet) {
                        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
                    } else {
                        Toast.makeText(LoginActivity.this, "No Internet Connetcion...!", Toast.LENGTH_LONG).show();
                    }
                }

                if (v==btn_googleplus)
                {
                    checkInternet = NetworkChecking.isConnected(this);
                    if (checkInternet) {
                        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                        startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
                    } else {
                        Toast.makeText(LoginActivity.this, "No Internet Connetcion...!", Toast.LENGTH_LONG).show();
                    }
                }



        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGPlusSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleGPlusSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result);
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            String personName = acct.getDisplayName();
            //userSessionManager.createUserLoginSession("ASAFAFDFAFAFAF", acct.getId(), acct.getDisplayName(), "", acct.getEmail(), "", "google");
            Log.e(TAG, "DETAIL" + personName + "\n" + acct.getId() + "\n" + acct.getEmail() + "\n" + acct.getPhotoUrl() + "\n" + acct.getFamilyName());
            if (personName.split("\\w+").length > 1) {
                lnm = personName.substring(personName.lastIndexOf(" ") + 1);
                nm = personName.substring(0, personName.lastIndexOf(' '));
            } else {
                //  firstName = name;
            }
            Intent i = new Intent(LoginActivity.this, RegisterActivity.class);

            i.putExtra("name", nm);
            i.putExtra("lastname", lnm);
            i.putExtra("email", acct.getEmail());
            i.putExtra("user_gmail_id", acct.getId());
            i.putExtra("provider", "Google");
            i.putExtra("LOGIN", "Google");
            Log.d("GPLUS",  nm + "//" + lnm + "//" + acct.getEmail() + "//" + acct.getId());
            startActivity(i);
            //  registerGoogleSocialData(acct.getDisplayName(), acct.getId(),acct.getPhotoUrl(), "Google");
        } else {

        }
    }



    private boolean isValidMobile(String send_email)
    {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", send_email)) {
            if(send_email.length() < 10 || send_email.length() > 14) {
                // if(phone.length() != 10) {
                check = false;
                //   txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
