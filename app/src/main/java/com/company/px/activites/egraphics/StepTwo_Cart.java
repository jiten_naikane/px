package com.company.px.activites.egraphics;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.MainActivity;
import com.company.px.R;
import com.company.px.adapter.CartItemsAdapter;
import com.company.px.model.CartItemsModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;
import com.company.px.utility.UserSessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class StepTwo_Cart extends AppCompatActivity implements View.OnClickListener{
    private boolean checkInternet;
    RecyclerView cart_item_recycler;
    LinearLayoutManager linearLayoutManager;
    CartItemsAdapter cartItemsAdapter;
    ArrayList<CartItemsModel> cartItemsModels = new ArrayList<CartItemsModel>();
    String user_ID_S="", O_D_ID;
    UserSessionManager session;
    ImageView nxt;
    LinearLayout nextButt, addMoreButt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_two_cart_items);

        session = new UserSessionManager(this);
        HashMap<String, String> userDetails = session.getUserDetails();
        user_ID_S = userDetails.get(UserSessionManager.USER_ID);

        O_D_ID = getIntent().getExtras().getString("ORDER_ID");

        nextButt = (LinearLayout) findViewById(R.id.nextButt);
        nextButt.setOnClickListener(this);
        addMoreButt = (LinearLayout) findViewById(R.id.addMoreButt);
        addMoreButt.setOnClickListener(this);
        nxt = (ImageView) findViewById(R.id.nxt);
        nxt.setColorFilter(Color.WHITE);

        cart_item_recycler=(RecyclerView)findViewById(R.id.cart_item_recycler) ;
        cartItemsAdapter = new CartItemsAdapter(cartItemsModels, StepTwo_Cart.this, R.layout.row_cart);
        linearLayoutManager =  new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        cart_item_recycler.setLayoutManager(linearLayoutManager);
        cart_item_recycler.setNestedScrollingEnabled(true);

        getCartItems();
    }


    private void getCartItems()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet)
        {
            Log.d("GET_CART_URL", AppUrls.BASE_URL + AppUrls.GET_CART + user_ID_S);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_CART + user_ID_S,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("GET_CART_RESP",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if(responceCode.equals("10100"))
                                {
                                    JSONArray jarray=jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++)
                                    {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        CartItemsModel pList = new CartItemsModel();
                                        pList.setUser_id(jsonObject1.getString("user_id"));
                                        pList.setId(jsonObject1.getString("id"));
                                        pList.setProduct_id(jsonObject1.getString("product_id"));
                                        pList.setProduct_name(jsonObject1.getString("product_name"));
                                        pList.setMrp(jsonObject1.getString("mrp"));
                                        pList.setDesign_brief(jsonObject1.getString("design_brief"));
                                        pList.setDesign_purpose(jsonObject1.getString("design_purpose"));
                                        pList.setHighlighted_text(jsonObject1.getString("highlighted_text"));
                                        pList.setRef_image(jsonObject1.getString("ref_image"));
                                        pList.setRef_file(jsonObject1.getString("ref_file"));
                                        pList.setRef_file2(jsonObject1.getString("ref_file2"));
                                        pList.setRef_web_link(jsonObject1.getString("ref_web_link"));
                                        pList.setRef_about(jsonObject1.getString("ref_about"));
                                        pList.setDesign_appearence_choice(jsonObject1.getString("design_look_choice"));
                                        pList.setDesign_appearence_tags(jsonObject1.getString("design_look_elements"));
                                        pList.setDesign_appearence_about(jsonObject1.getString("design_look_about"));
                                        pList.setColors_tag(jsonObject1.getString("colors"));
                                        pList.setColors_about(jsonObject1.getString("colors_text"));
                                        pList.setDesign_avoided_text(jsonObject1.getString("design_avoided_text"));
                                        pList.setDeliver_whatsapp(jsonObject1.getString("deliver_whatsapp"));
                                        pList.setDeliver_email(jsonObject1.getString("deliver_email"));


                                        cartItemsModels.add(pList);
                                    }
                                    cart_item_recycler.setAdapter(cartItemsAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View view) {
        if (view==nextButt)
        {
            Intent stepThreeIn = new Intent(getApplicationContext(), StepThree_OrderSummary.class);
            stepThreeIn.putExtra("ORDER_ID", O_D_ID);
            startActivity(stepThreeIn);
        }

        if (view==addMoreButt)
        {
            Intent M_A = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(M_A);
        }


    }



}
