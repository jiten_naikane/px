package com.company.px.activites;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.company.px.R;

import static android.widget.Toast.LENGTH_LONG;

public class CustomerSupport extends AppCompatActivity implements View.OnClickListener{

    TextView chat_butt_TV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_support);

        chat_butt_TV = (TextView) findViewById(R.id.chat_butt_TV);
        chat_butt_TV.setOnClickListener(this);


    }


    private void openWhatsApp() {
        String smsNumber = "916309881941";
        try {
            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Hi..");
            sendIntent.putExtra("jid", smsNumber + "@s.whatsapp.net");
            sendIntent.setPackage("com.whatsapp");
            startActivity(sendIntent);
        } catch(Exception e) {
            Toast.makeText(this,"Whatsapp not found\nPlease install Whatsapp to chat with us.", LENGTH_LONG).show();
        }
    }



    @Override
    public void onClick(View view) {
        if (view==chat_butt_TV)
        {
            openWhatsApp();
        }
    }
}
