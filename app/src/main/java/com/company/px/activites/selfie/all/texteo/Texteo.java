package com.company.px.activites.selfie.all.texteo;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.MainActivity;
import com.company.px.R;
import com.company.px.activites.selfie.all.stickies.StickerImageView;
import com.company.px.adapter.EmojiImagesAdapter_Nameo;
import com.company.px.adapter.EmojiImagesAdapter_Texteo;
import com.company.px.adapter.EmojiTitleAdapter_Nameo;
import com.company.px.adapter.EmojiTitleAdapter_Texteo;
import com.company.px.adapter.TexteoFestivalTitleAdapter;
import com.company.px.adapter.TexteoImagesHoriAdapter;
import com.company.px.dbhelper.TexteoDB;
import com.company.px.dbhelper.TexteoImageDB;
import com.company.px.model.EmojiImagesModel;
import com.company.px.model.EmojiTitlesModel;
import com.company.px.model.FestivalTitlesModel;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Texteo extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    StickerImageView emoji_s_1, emoji_s_2, emoji_s_3;
    TextView unlockAll_butt, upload_photo, emoji_edt_Butt, txt_1, txt_2, txt_3, bg_multi_colo, style1_txt, style2_txt, t_txt, font_txt, color_txt, shadow_txt, outputButt, textEditButt, photo_gallery_Butt, theme_edt_Butt, bg_colorsButt, txt_styleButt, double_colo_txt, ss1, ss2, ss3, ss4, ss5, ss6, ss7, ss8, ss9, ss10, se1, se2, se3, se4, se5, se6, se7, se8, se9, se10, sc1, sc2, sc3, sc4, sc5, sc6, sc7, sc8, sc9, sc10, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10;
    public ImageView logo_watermark, txt_lockIn_ic, txt_lockOut_ic, photo_lockIn_ic, photo_lockOut_ic, theme_lockIn_ic, theme_lockOut_ic, emoji_lockIn_ic, emoji_lockOut_ic, txt_align_T, txt_align_B, txt_align_R, txt_align_L, txt_align_C, txt_align_Ch, txt_align_Cv, emoji_visi_ic, emoji_Invisi_ic, up_txt1, up_txt2, up_txt3, editd_txt1_visi_ic, editd_txt1_Invisi_ic, editd_txt2_visi_ic, editd_txt2_Invisi_ic, editd_txt3_visi_ic, editd_txt3_Invisi_ic, flip_down, flip_right, pencil_ic_img, emojiButt, bg_visi_ic, bg_Invisi_ic, theme_visi_ic, theme_Invisi_ic, photo_visi_ic, photo_Invisi_ic, txt_visi_ic, txt_Invisi_ic, linear_gd_po_L, linear_gd_po_TL, linear_gd_po_T, linear_gd_po_TR, linear_gd_po_R, linear_gd_po_BR, linear_gd_po_B, linear_gd_po_BL, img_user, img_theme;
    SeekBar txt_Linespacing_seek, txtBg_opa_seek, txt_corner_seek, txt_L_height_seek, txt_L_width_seek, txt_spacing_seek, txt_rotate_seek, txt_rotateX_seek, txt_rotateY_seek, txt_size_seek_1, txt_size_seek_2, txt_size_seek_3, radius_shadow_seek, dX_shadow_seek, dY_shadow_seek, c_bg_grad_X_axis_seek, c_bg_grad_spacing_seek, c_bg_grad_Y_axis_seek, bg_grad_seek, bg_grad_seek2, bg_corner_seek, grad_position_seek, grad_X_axis_seek, grad_Y_axis_seek, bWSeek, contrast, opacity;
    public LinearLayout bg_grad_selectors_LL, txt_Grad_tool, emojiButt2, internet_txt_LL, img_grid_butt, font_ll, emoji_tool_ll, grad_bg_style2_ll, grad_radd_ll, grad_seek_ll, grad_txt_ll, bg_tool_ll, theme_tool_ll, logo_tool_ll, txt_tool_ll, ll_img_tool, ll_angle, ll_St_1, bg_grad_Tools_ll, t_butt, style1_LL, style2_LL, txt_sub_tabs, txt_fontButt, style_1_Butt, style_2_Butt, txt_colorsButt, txt_shadowButt, bgGrad_LL, grad_bg_style1_ll, ll1, ll2, colors_ll, shadow_LL, text_size_adj_LL, txt_color_tools_LL, bg_color_tool, image_edit_tools;
    View c_bg_v1, c_bg_v2, bg_view, style_1_Butt_line, style_2_Butt_line, t_line, font_line, font_line_v, t_line_v, color_line, color_line_v, shadow_line, shadow_line_v, grad_c1, grad_c2, grad_c3, grad_c4, grad_c5, grad_c6, grad_custom_v1, grad_custom_v2, black_c, white_c, red_c, purple_c, pink_c, deepPurple_c, indigo_c, blue_c, cyan_c, lightBlue_c, teal_c, green_c, lightGreen_c, lime_c, yellow_c, amber_c, orange_c, deepOrange_c, brown_c, grey_c, blueGrey_c, c0,  c1, c2, c3, c4, c5, c6, c7, c8, c9;
    private Boolean checkInternet=false, photo_toolDisplay_bool = false, theme_touch_activateBool=false, txt1_bg_color_check_bool = false, txt2_bg_color_check_bool = false, txt3_bg_color_check_bool = false, emoji_bool=false, emoji_bool_s1 = false, emoji_bool_s2 = false, emoji_bool_s3 = false, exit = false, emoji_1x_bool = false, emoji_2x_bool = false, emoji_3x_bool = false, emoji_1_bool = false, emoji_2_bool = false, emoji_3_bool = false, txt_state_choose = false, tx1_bg_bool = false, tx2_bg_bool = false, c_bg_radio_rad = true, c_bg_radio_Lin=false, c_bg_gradV1 = true, c_bg_gradV2, photo_bool, llBg_bool, txt_shadowButt_bool, txt_colorsButt_bool, setBgLL_bool=false, img_edt_Butt_bool, style_2_txt_bool, theme_edt_Butt_bool, txt_styleButt_bool, grad_c1_bool, grad_c2_bool, grad_c3_bool, grad_c4_bool, grad_c5_bool, grad_c6_bool, s_g_bool=false, txt_1_bool, tx3_bg_bool, txt_2_bool, txt_3_bool, imgBg, gradV1 = true, gradV2, radio_Lin=false, radio_rad = true;
    Typeface tf_blkchcry, tf_burnstown, tf_chlorinr, tf_clementine, tf_deftone, tf_earwig, tf_geo, tf_gotheroin, tf_homoark, tf_musicals, tf_octin, tf_otto, tf_peterbuilt, tf_precious, tf_punk, tf_scriptin, tf_stilltime, tf_zombie;
    String imgDecodableString, EDIT_CAT_ID, emoji_1_color, emoji_2_color, emoji_3_color;
    RadioButton txt_state_txt_rad, txt_state_txt_bg_rad, c_bg_linearRadio, c_bg_radialRadio, gradBG_Type_liner, gradBG_Type_sweep, grad_linearRadio, grad_radialRadio;
    RadioGroup choose_txt_bg_color_Rgroup, c_bg_group, grad_rad_group, gradBG_Type_grp;
    int mode = NONE, screen_width, screen_height, bwValue, txt1_bgOpa=230, txt2_bgOpa=230, txt3_bgOpa=230, txt1_bgCor, txt2_bgCor, txt3_bgCor, txt1_roatae_val, txt2_roatae_val, txt3_roatae_val, txt1_roataeX_val, txt2_roataeX_val, txt3_roataeX_val, txt1_roataeY_val, txt2_roataeY_val, txt3_roataeY_val, c_bg_rad_x, c_bg_rad_y, c_bg_rad_s = 100, rad_x = 0, rad_y = 0, rad_s = 100, bg_GRAD_0_1 = 0, lin_grad_x0 = 0, lin_grad_x1 = 0, lin_grad_y1 = 100, lin_grad_bg_x0 = 0,
            lin_grad_bg_x1 = 0, lin_grad_bg_y1 = 100, txt1_grad_x_val = 0, txt2_grad_x_val = 0, txt3_grad_x_val = 0, txt1_grad_y_val = 0, txt2_grad_y_val = 0, txt3_grad_y_val = 0, txt1_grad_spacing_val = 100, txt2_grad_spacing_val = 100, txt3_grad_spacing_val = 100, lastAction, RESULT_LOAD_IMG = 1, t1_shadow_r, t1_shadow_dx, t1_shadow_dy, t2_shadow_r, t2_shadow_dx, t2_shadow_dy, t3_shadow_r, t3_shadow_dx, t3_shadow_dy, bw_p_photo_save = 50, bw_p_theme_save = 50, contrast_p_photo_save = 100, contrast_p_theme_save = 100, opacity_p_photo_save = 200, opacity_p_theme_save = 200, gradType = 0, txt1_bg_color = Color.GRAY, txt2_bg_color = Color.GRAY, txt3_bg_color = Color.GRAY, getViewColo = Color.BLACK, colorId2, colorId1, grad_colorId_1, grad_colorId_2, grad_colorId_3, grad_colorId_4, grad_colorId_5, grad_colorId_6;
    ColorDrawable g_bg_drw_1, g_bg_drw_2, g_bg_drw_3, g_bg_drw_4, g_bg_drw_5, g_bg_drw_6;
    GradientDrawable gd, gd_txt1_bg, gd_txt2_bg, gd_txt3_bg;
    private Matrix matrix = new Matrix(), matrix_theme = new Matrix(), savedMatrix = new Matrix();
    private static final int NONE = 0, DRAG = 1, ZOOM = 2;
    private PointF start = new PointF(),  mid = new PointF();
    private float newRot = 0f, d = 0f, oldDist = 1f, brightness = 9;
    private float[] lastEvent = null;
    RelativeLayout rL, txt1_rl, txt2_rl, txt3_rl, theme_internet_LL;
    Dialog dialog_imgGRID, output_dialog;
    float dX, dY, gradXF, gradYF, shadow_rad, shadow_dx, shadow_dy, txt1_linSpac, txt2_linSpac, txt3_linSpac, txt1_spacing_val, txt2_spacing_val, txt3_spacing_val;
    Shader shade_grad_title, shade_grad_p1, shade_grad_p2, shade_s_title, shade_s_p1, shade_s_p2;
    BottomSheetBehavior mBottomSheetBehavior;

    RecyclerView fest_title_recycler, grid_titles_recycler, imagesHoriRecycler, grid_img_recycler, emoji_recycler_titles, emoji_recycler_images;
    LinearLayoutManager linearLayoutManager, linearLayoutManager2, linearLayoutManager_ST;
    TexteoFestivalTitleAdapter texteoFestivalTitleAdapter;
    ArrayList<FestivalTitlesModel> festTitleModel = new ArrayList<FestivalTitlesModel>();

    TexteoImagesHoriAdapter texteoImagesHoriAdapter;
    ArrayList<ImagesHoriModel> imagesHoriModel = new ArrayList<ImagesHoriModel>();

    EmojiTitleAdapter_Texteo emojiTitleAdapter;
    ArrayList<EmojiTitlesModel> emojiTitlesModels = new ArrayList<EmojiTitlesModel>();

    GridLayoutManager gridLayoutManager;
    EmojiImagesAdapter_Texteo emojiImagesAdapter;
    ArrayList<EmojiImagesModel> emojiImagesModels = new ArrayList<EmojiImagesModel>();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_texteo_edit);



        /*
         * this checks to see if there are any previous test photo files
         * if there are any photos, they are deleted for the sake of
         * memory
         *
         */

        // if no directory exists, create new directory


        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        screen_width = displayMetrics.widthPixels;
        screen_height = displayMetrics.heightPixels;
        rL = (RelativeLayout) findViewById(R.id.rL);
        img_theme = (ImageView) findViewById(R.id.img_theme);
        img_theme.setOnTouchListener(this);

        RelativeLayout.LayoutParams paramsRL = new RelativeLayout.LayoutParams(screen_width, screen_width + 200);
        rL.setLayoutParams(paramsRL);

        RectF drawableRect = new RectF(0, 0, screen_width, screen_width);
        RectF viewRect = new RectF(0, 0, screen_width, screen_width);
        matrix_theme.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.START);
        img_theme.setScaleType(ImageView.ScaleType.MATRIX);
        img_theme.setImageMatrix(matrix_theme);
        dialog_imgGRID = new Dialog(this);

        pencil_ic_img = (ImageView) findViewById(R.id.pencil_ic_img);
        pencil_ic_img.setOnTouchListener(this);
        pencil_ic_img.setColorFilter(getResources().getColor(R.color.light_blue));

        View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        pencil_ic_img.setRotation(0);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        pencil_ic_img.setRotation(180);
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });


        emojiButt = (ImageView) findViewById(R.id.emojiButt);
        emojiButt.setOnClickListener(this);

        contrast = (SeekBar) findViewById(R.id.contrast);
        bWSeek = (SeekBar) findViewById(R.id.bWSeek);

        Bundle bundle = getIntent().getExtras();
        EDIT_CAT_ID = bundle.getString("EDIT_CAT_ID");

        opacity = (SeekBar) findViewById(R.id.opacity);


        txt_lockIn_ic = (ImageView) findViewById(R.id.txt_lockIn_ic);
        txt_lockIn_ic.setOnClickListener(this);
        txt_lockOut_ic = (ImageView) findViewById(R.id.txt_lockOut_ic);
        txt_lockOut_ic.setOnClickListener(this);
        photo_lockIn_ic = (ImageView) findViewById(R.id.photo_lockIn_ic);
        photo_lockIn_ic.setOnClickListener(this);
        photo_lockOut_ic = (ImageView) findViewById(R.id.photo_lockOut_ic);
        photo_lockOut_ic.setOnClickListener(this);
        theme_lockIn_ic = (ImageView) findViewById(R.id.theme_lockIn_ic);
        theme_lockIn_ic.setOnClickListener(this);
        theme_lockOut_ic = (ImageView) findViewById(R.id.theme_lockOut_ic);
        theme_lockOut_ic.setOnClickListener(this);
        emoji_lockIn_ic = (ImageView) findViewById(R.id.emoji_lockIn_ic);
        emoji_lockIn_ic.setOnClickListener(this);
        emoji_lockOut_ic = (ImageView) findViewById(R.id.emoji_lockOut_ic);
        emoji_lockOut_ic.setOnClickListener(this);

        bg_visi_ic = (ImageView) findViewById(R.id.bg_visi_ic);
        bg_visi_ic.setOnClickListener(this);
        bg_Invisi_ic = (ImageView) findViewById(R.id.bg_Invisi_ic);
        bg_Invisi_ic.setOnClickListener(this);
        theme_visi_ic = (ImageView) findViewById(R.id.theme_visi_ic);
        theme_visi_ic.setOnClickListener(this);
        theme_Invisi_ic = (ImageView) findViewById(R.id.theme_Invisi_ic);
        theme_Invisi_ic.setOnClickListener(this);
        txt_visi_ic = (ImageView) findViewById(R.id.txt_visi_ic);
        txt_visi_ic.setOnClickListener(this);
        txt_Invisi_ic = (ImageView) findViewById(R.id.txt_Invisi_ic);
        txt_Invisi_ic.setOnClickListener(this);
        photo_visi_ic = (ImageView) findViewById(R.id.photo_visi_ic);
        photo_visi_ic.setOnClickListener(this);
        photo_Invisi_ic = (ImageView) findViewById(R.id.photo_Invisi_ic);
        photo_Invisi_ic.setOnClickListener(this);
        linear_gd_po_L = (ImageView) findViewById(R.id.linear_gd_po_L);
        linear_gd_po_L.setOnClickListener(this);
        linear_gd_po_TL = (ImageView) findViewById(R.id.linear_gd_po_TL);
        linear_gd_po_TL.setOnClickListener(this);
        linear_gd_po_T = (ImageView) findViewById(R.id.linear_gd_po_T);
        linear_gd_po_T.setOnClickListener(this);
        linear_gd_po_TR = (ImageView) findViewById(R.id.linear_gd_po_TR);
        linear_gd_po_TR.setOnClickListener(this);
        linear_gd_po_R = (ImageView) findViewById(R.id.linear_gd_po_R);
        linear_gd_po_R.setOnClickListener(this);
        linear_gd_po_BR = (ImageView) findViewById(R.id.linear_gd_po_BR);
        linear_gd_po_BR.setOnClickListener(this);
        linear_gd_po_B = (ImageView) findViewById(R.id.linear_gd_po_B);
        linear_gd_po_B.setOnClickListener(this);
        linear_gd_po_BL = (ImageView) findViewById(R.id.linear_gd_po_BL);
        linear_gd_po_BL.setOnClickListener(this);


        theme_internet_LL = (RelativeLayout) findViewById(R.id.theme_internet_LL);
        txt1_rl = (RelativeLayout) findViewById(R.id.txt1_rl);
        txt1_rl.setOnTouchListener(this);
        txt2_rl = (RelativeLayout) findViewById(R.id.txt2_rl);
        txt2_rl.setOnTouchListener(this);
        txt3_rl = (RelativeLayout) findViewById(R.id.txt3_rl);
        txt3_rl.setOnTouchListener(this);

        internet_txt_LL = (LinearLayout) findViewById(R.id.internet_txt_LL);
        internet_txt_LL.setOnClickListener(this);

        txt_1 = (TextView) findViewById(R.id.txt_1);
        txt_1.setOnTouchListener(this);
        txt_1.setGravity(Gravity.CENTER);
        txt_2 = (TextView) findViewById(R.id.txt_2);
        txt_2.setOnTouchListener(this);
        txt_2.setGravity(Gravity.CENTER);
        txt_3 = (TextView) findViewById(R.id.txt_3);
        txt_3.setOnTouchListener(this);
        txt_3.setGravity(Gravity.CENTER);
        bg_grad_selectors_LL = (LinearLayout) findViewById(R.id.bg_grad_selectors_LL);
        txt_Grad_tool = (LinearLayout) findViewById(R.id.txt_Grad_tool);
        emojiButt2 = (LinearLayout) findViewById(R.id.emojiButt2);
        emojiButt2.setOnClickListener(this);
        txt_align_T = (ImageView) findViewById(R.id.txt_align_T);
        txt_align_T.setOnClickListener(this);
        txt_align_B = (ImageView) findViewById(R.id.txt_align_B);
        txt_align_B.setOnClickListener(this);
        txt_align_R = (ImageView) findViewById(R.id.txt_align_R);
        txt_align_R.setOnClickListener(this);
        txt_align_L = (ImageView) findViewById(R.id.txt_align_L);
        txt_align_L.setOnClickListener(this);
        txt_align_C = (ImageView) findViewById(R.id.txt_align_C);
        txt_align_C.setOnClickListener(this);
        txt_align_Ch = (ImageView) findViewById(R.id.txt_align_Ch);
        txt_align_Ch.setOnClickListener(this);
        txt_align_Cv = (ImageView) findViewById(R.id.txt_align_Cv);
        txt_align_Cv.setOnClickListener(this);
        bg_multi_colo = (TextView) findViewById(R.id.bg_multi_colo);
        bg_multi_colo.setOnClickListener(this);
        style1_txt = (TextView) findViewById(R.id.style1_txt);
        style2_txt = (TextView) findViewById(R.id.style2_txt);
        t_txt = (TextView) findViewById(R.id.t_txt);
        font_txt = (TextView) findViewById(R.id.font_txt);
        color_txt = (TextView) findViewById(R.id.color_txt);
        shadow_txt = (TextView) findViewById(R.id.shadow_txt);
        photo_gallery_Butt = (TextView) findViewById(R.id.photo_gallery_Butt);
        photo_gallery_Butt.setOnClickListener(this);
        theme_edt_Butt = (TextView) findViewById(R.id.theme_edt_Butt);
        theme_edt_Butt.setOnClickListener(this);
        bg_colorsButt = (TextView) findViewById(R.id.bg_colorsButt);
        bg_colorsButt.setOnClickListener(this);
        txt_styleButt = (TextView) findViewById(R.id.txt_styleButt);
        txt_styleButt.setOnClickListener(this);
        outputButt = (TextView) findViewById(R.id.outputButt);
        outputButt.setOnClickListener(this);
        textEditButt = (TextView) findViewById(R.id.textEditButt);
        textEditButt.setOnClickListener(this);
        emoji_edt_Butt = (TextView) findViewById(R.id.emoji_edt_Butt);
        emoji_edt_Butt.setOnClickListener(this);

        up_txt1 = (ImageView) findViewById(R.id.up_txt1);
        up_txt1.setOnClickListener(this);
        up_txt2 = (ImageView) findViewById(R.id.up_txt2);
        up_txt2.setOnClickListener(this);
        up_txt3 = (ImageView) findViewById(R.id.up_txt3);
        up_txt3.setOnClickListener(this);
        editd_txt1_visi_ic = (ImageView) findViewById(R.id.editd_txt1_visi_ic);
        editd_txt1_visi_ic.setOnClickListener(this);
        editd_txt1_Invisi_ic = (ImageView) findViewById(R.id.editd_txt1_Invisi_ic);
        editd_txt1_Invisi_ic.setOnClickListener(this);
        editd_txt2_visi_ic = (ImageView) findViewById(R.id.editd_txt2_visi_ic);
        editd_txt2_visi_ic.setOnClickListener(this);
        editd_txt2_Invisi_ic = (ImageView) findViewById(R.id.editd_txt2_Invisi_ic);
        editd_txt2_Invisi_ic.setOnClickListener(this);
        editd_txt3_visi_ic = (ImageView) findViewById(R.id.editd_txt3_visi_ic);
        editd_txt3_visi_ic.setOnClickListener(this);
        editd_txt3_Invisi_ic = (ImageView) findViewById(R.id.editd_txt3_Invisi_ic);
        editd_txt3_Invisi_ic.setOnClickListener(this);

        emoji_visi_ic = (ImageView) findViewById(R.id.emoji_visi_ic);
        emoji_visi_ic.setOnClickListener(this);
        emoji_Invisi_ic = (ImageView) findViewById(R.id.emoji_Invisi_ic);
        emoji_Invisi_ic.setOnClickListener(this);
        flip_down = (ImageView) findViewById(R.id.flip_down);
        flip_down.setOnClickListener(this);
        flip_right = (ImageView) findViewById(R.id.flip_right);
        flip_right.setOnClickListener(this);

        tf_burnstown = Typeface.createFromAsset(getAssets(), "burnstown.ttf");
        tf_blkchcry = Typeface.createFromAsset(getAssets(), "blkchcry.ttf");
        tf_chlorinr = Typeface.createFromAsset(getAssets(), "chlorinr.ttf");
        tf_clementine = Typeface.createFromAsset(getAssets(), "clementine.ttf");
        tf_deftone = Typeface.createFromAsset(getAssets(), "deftone.ttf");
        tf_earwig = Typeface.createFromAsset(getAssets(), "earwig.ttf");
        tf_geo = Typeface.createFromAsset(getAssets(), "blkchcry.ttf");
        tf_gotheroin = Typeface.createFromAsset(getAssets(), "gotheroin.ttf");
        tf_homoark = Typeface.createFromAsset(getAssets(), "homoark.ttf");
        tf_musicals = Typeface.createFromAsset(getAssets(), "musicals.ttf");
        tf_octin = Typeface.createFromAsset(getAssets(), "octin.ttf");
        tf_otto = Typeface.createFromAsset(getAssets(), "oooto.ttf");
        tf_peterbuilt = Typeface.createFromAsset(getAssets(), "peterbuilt.ttf");
        tf_precious = Typeface.createFromAsset(getAssets(), "precious.ttf");
        tf_punk = Typeface.createFromAsset(getAssets(), "punk.ttf");
        tf_stilltime = Typeface.createFromAsset(getAssets(), "stilltime.ttf");
        tf_zombie = Typeface.createFromAsset(getAssets(), "zombie.ttf");

        gradBG_Type_grp = (RadioGroup) findViewById(R.id.gradBG_Type_grp);
        gradBG_Type_liner = (RadioButton) findViewById(R.id.gradBG_Type_liner);
        gradBG_Type_sweep = (RadioButton) findViewById(R.id.gradBG_Type_sweep);


        c_bg_group = (RadioGroup) findViewById(R.id.c_bg_group);
        c_bg_radialRadio = (RadioButton) findViewById(R.id.c_bg_radialRadio);
        c_bg_linearRadio = (RadioButton) findViewById(R.id.c_bg_linearRadio);
        choose_txt_bg_color_Rgroup = (RadioGroup) findViewById(R.id.choose_txt_bg_color_Rgroup);
        txt_state_txt_rad = (RadioButton) findViewById(R.id.txt_state_txt_rad);
        txt_state_txt_bg_rad = (RadioButton) findViewById(R.id.txt_state_txt_bg_rad);

        double_colo_txt = (TextView) findViewById(R.id.double_colo_txt);
        double_colo_txt.setOnClickListener(this);
        ss1 = (TextView) findViewById(R.id.ss1);
        ss1.setOnClickListener(this);
        ss2 = (TextView) findViewById(R.id.ss2);
        ss2.setOnClickListener(this);
        ss2.setTypeface(tf_geo);
        ss3 = (TextView) findViewById(R.id.ss3);
        ss3.setOnClickListener(this);
        ss4 = (TextView) findViewById(R.id.ss4);
        ss4.setOnClickListener(this);
        ss4.setTypeface(tf_homoark);
        ss5 = (TextView) findViewById(R.id.ss5);
        ss5.setOnClickListener(this);
        ss6 = (TextView) findViewById(R.id.ss6);
        ss6.setOnClickListener(this);
        ss7 = (TextView) findViewById(R.id.ss7);
        ss7.setOnClickListener(this);
        ss8 = (TextView) findViewById(R.id.ss8);
        ss8.setOnClickListener(this);
        ss9 = (TextView) findViewById(R.id.ss9);
        ss9.setOnClickListener(this);
        ss10 = (TextView) findViewById(R.id.ss10);
        ss10.setOnClickListener(this);

        se1 = (TextView) findViewById(R.id.se1);
        se1.setOnClickListener(this);
        se1.setTypeface(tf_blkchcry);
        se2 = (TextView) findViewById(R.id.se2);
        se2.setOnClickListener(this);
        se2.setTypeface(tf_octin);
        se3 = (TextView) findViewById(R.id.se3);
        se3.setOnClickListener(this);
        se4 = (TextView) findViewById(R.id.se4);
        se4.setOnClickListener(this);
        se5 = (TextView) findViewById(R.id.se5);
        se5.setOnClickListener(this);
        se6 = (TextView) findViewById(R.id.se6);
        se6.setOnClickListener(this);
        se7 = (TextView) findViewById(R.id.se7);
        se7.setOnClickListener(this);
        se8 = (TextView) findViewById(R.id.se8);
        se8.setOnClickListener(this);
        se9 = (TextView) findViewById(R.id.se9);
        se9.setOnClickListener(this);
        se10 = (TextView) findViewById(R.id.se10);
        se10.setOnClickListener(this);

        sc1 = (TextView) findViewById(R.id.sc1);
        sc1.setOnClickListener(this);
        sc2 = (TextView) findViewById(R.id.sc2);
        sc2.setOnClickListener(this);
        sc3 = (TextView) findViewById(R.id.sc3);
        sc3.setOnClickListener(this);
        sc3.setTypeface(tf_deftone);
        sc4 = (TextView) findViewById(R.id.sc4);
        sc4.setOnClickListener(this);
        sc4.setTypeface(tf_clementine);
        sc5 = (TextView) findViewById(R.id.sc5);
        sc5.setOnClickListener(this);
        sc5.setTypeface(tf_otto);
        sc6 = (TextView) findViewById(R.id.sc6);
        sc6.setOnClickListener(this);
        sc6.setTypeface(tf_peterbuilt);
        sc7 = (TextView) findViewById(R.id.sc7);
        sc7.setOnClickListener(this);
        sc7.setTypeface(tf_precious);
        sc8 = (TextView) findViewById(R.id.sc8);
        sc8.setOnClickListener(this);
        sc8.setTypeface(tf_scriptin);
        sc9 = (TextView) findViewById(R.id.sc9);
        sc9.setOnClickListener(this);
        sc9.setTypeface(tf_stilltime);
        sc10 = (TextView) findViewById(R.id.sc10);
        sc10.setOnClickListener(this);

        d1 = (TextView) findViewById(R.id.d1);
        d1.setOnClickListener(this);
        d1.setTypeface(tf_burnstown);
        d2 = (TextView) findViewById(R.id.d2);
        d2.setOnClickListener(this);
        d2.setTypeface(tf_chlorinr);
        d3 = (TextView) findViewById(R.id.d3);
        d3.setOnClickListener(this);
        d3.setTypeface(tf_earwig);
        d4 = (TextView) findViewById(R.id.d4);
        d4.setOnClickListener(this);
        d4.setTypeface(tf_gotheroin);
        d5 = (TextView) findViewById(R.id.d5);
        d5.setOnClickListener(this);
        d5.setTypeface(tf_musicals);
        d6 = (TextView) findViewById(R.id.d6);
        d6.setOnClickListener(this);
        d6.setTypeface(tf_punk);
        d7 = (TextView) findViewById(R.id.d7);
        d7.setOnClickListener(this);
        d7.setTypeface(tf_zombie);
        d8 = (TextView) findViewById(R.id.d8);
        d8.setOnClickListener(this);
        d9 = (TextView) findViewById(R.id.d9);
        d9.setOnClickListener(this);
        d10 = (TextView) findViewById(R.id.d10);
        d10.setOnClickListener(this);

        ll_angle = (LinearLayout) findViewById(R.id.ll_angle);
        style1_LL = (LinearLayout) findViewById(R.id.style1_LL);
        style2_LL = (LinearLayout) findViewById(R.id.style2_LL);
        bg_grad_Tools_ll = (LinearLayout) findViewById(R.id.bg_grad_Tools_ll);
        t_butt = (LinearLayout) findViewById(R.id.t_butt);
        t_butt.setOnClickListener(this);
        txt_colorsButt = (LinearLayout) findViewById(R.id.txt_colorsButt);
        txt_colorsButt.setOnClickListener(this);
        txt_shadowButt = (LinearLayout) findViewById(R.id.txt_shadowButt);
        txt_shadowButt.setOnClickListener(this);
        colors_ll = (LinearLayout) findViewById(R.id.colors_ll);
        shadow_LL = (LinearLayout) findViewById(R.id.shadow_LL);
        ll_img_tool = (LinearLayout) findViewById(R.id.ll_img_tool);
        text_size_adj_LL = (LinearLayout) findViewById(R.id.text_size_adj_LL);
        txt_color_tools_LL = (LinearLayout) findViewById(R.id.txt_color_tools_LL);
        bg_color_tool = (LinearLayout) findViewById(R.id.bg_color_tool);
        image_edit_tools = (LinearLayout) findViewById(R.id.image_edit_tools);
        font_ll = (LinearLayout) findViewById(R.id.font_ll);
        img_grid_butt = (LinearLayout) findViewById(R.id.img_grid_butt);
        img_grid_butt.setOnClickListener(this);
        emoji_tool_ll = (LinearLayout) findViewById(R.id.emoji_tool_ll);
        grad_bg_style2_ll = (LinearLayout) findViewById(R.id.grad_bg_style2_ll);
        grad_radd_ll = (LinearLayout) findViewById(R.id.grad_radd_ll);
        unlockAll_butt = (TextView) findViewById(R.id.unlockAll_butt);
        unlockAll_butt.setOnClickListener(this);
        upload_photo = (TextView) findViewById(R.id.upload_photo);
        upload_photo.setOnClickListener(this);
        txt_tool_ll = (LinearLayout) findViewById(R.id.txt_tool_ll);
        logo_tool_ll = (LinearLayout) findViewById(R.id.logo_tool_ll);
        theme_tool_ll = (LinearLayout) findViewById(R.id.theme_tool_ll);
        bg_tool_ll = (LinearLayout) findViewById(R.id.bg_tool_ll);
        grad_txt_ll = (LinearLayout) findViewById(R.id.grad_txt_ll);
        grad_seek_ll = (LinearLayout) findViewById(R.id.grad_seek_ll);


        img_user = (ImageView) findViewById(R.id.img_user);
        img_user.setOnTouchListener(this);


        txt_sub_tabs = (LinearLayout) findViewById(R.id.txt_sub_tabs);
        txt_fontButt = (LinearLayout) findViewById(R.id.txt_fontButt);
        txt_fontButt.setOnClickListener(this);

        ll_St_1 = (LinearLayout) findViewById(R.id.ll_St_1);

        style_1_Butt = (LinearLayout) findViewById(R.id.style_1_Butt);
        style_1_Butt.setOnClickListener(this);
        style_2_Butt = (LinearLayout) findViewById(R.id.style_2_Butt);
        style_2_Butt.setOnClickListener(this);
        bgGrad_LL = (LinearLayout) findViewById(R.id.bgGrad_LL);
        grad_bg_style1_ll = (LinearLayout) findViewById(R.id.grad_bg_style1_ll);
        ll1 = (LinearLayout) findViewById(R.id.ll1);
//        ll1.setVisibility(View.GONE);
        ll2 = (LinearLayout) findViewById(R.id.ll2);

        c_bg_v2 = (View) findViewById(R.id.c_bg_v2);
        c_bg_v2.setOnTouchListener(this);
        c_bg_v1 = (View) findViewById(R.id.c_bg_v1);
        c_bg_v1.setOnTouchListener(this);
        style_1_Butt_line = (View) findViewById(R.id.style_1_Butt_line);
        style_2_Butt_line = (View) findViewById(R.id.style_2_Butt_line);
        bg_view = (View) findViewById(R.id.bg_view);
        bg_view.setOnTouchListener(this);
        font_line = (View) findViewById(R.id.font_line);
        font_line_v = (View) findViewById(R.id.font_line_v);
        t_line = (View) findViewById(R.id.t_line);
        color_line = (View) findViewById(R.id.color_line);
        shadow_line = (View) findViewById(R.id.shadow_line);
        shadow_line_v = (View) findViewById(R.id.shadow_line_v);
        color_line_v = (View) findViewById(R.id.color_line_v);
        t_line_v = (View) findViewById(R.id.t_line_v);
        grad_c1 = (View) findViewById(R.id.grad_c1);
        grad_c1.setOnTouchListener(this);
        grad_c2 = (View) findViewById(R.id.grad_c2);
        grad_c2.setOnTouchListener(this);
        grad_c3 = (View) findViewById(R.id.grad_c3);
        grad_c3.setOnTouchListener(this);
        grad_c4 = (View) findViewById(R.id.grad_c4);
        grad_c4.setOnTouchListener(this);
        grad_c5 = (View) findViewById(R.id.grad_c5);
        grad_c5.setOnTouchListener(this);
        grad_c6 = (View) findViewById(R.id.grad_c6);
        grad_c6.setOnTouchListener(this);

        grad_custom_v1 = (View) findViewById(R.id.grad_custom_v1);
        grad_custom_v1.setOnTouchListener(this);
        grad_custom_v2 = (View) findViewById(R.id.grad_custom_v2);
        grad_custom_v2.setOnTouchListener(this);
        c0 = (View) findViewById(R.id.c0);
        c0.setOnClickListener(this);
        c1 = (View) findViewById(R.id.c1);
        c1.setOnClickListener(this);
        c2 = (View) findViewById(R.id.c2);
        c2.setOnClickListener(this);
        c3 = (View) findViewById(R.id.c3);
        c3.setOnClickListener(this);
        c4 = (View) findViewById(R.id.c4);
        c4.setOnClickListener(this);
        c5 = (View) findViewById(R.id.c5);
        c5.setOnClickListener(this);
        c6 = (View) findViewById(R.id.c6);
        c6.setOnClickListener(this);
        c7 = (View) findViewById(R.id.c7);
        c7.setOnClickListener(this);
        c8 = (View) findViewById(R.id.c8);
        c8.setOnClickListener(this);
        c9 = (View) findViewById(R.id.c9);
        c9.setOnClickListener(this);
        black_c = (View) findViewById(R.id.black_c);
        black_c.setOnClickListener(this);
        white_c = (View) findViewById(R.id.white_c);
        white_c.setOnClickListener(this);
        red_c = (View) findViewById(R.id.red_c);
        red_c.setOnClickListener(this);
        purple_c = (View) findViewById(R.id.purple_c);
        purple_c.setOnClickListener(this);
        pink_c = (View) findViewById(R.id.pink_c);
        pink_c.setOnClickListener(this);
        deepPurple_c = (View) findViewById(R.id.deepPurple_c);
        deepPurple_c.setOnClickListener(this);
        indigo_c = (View) findViewById(R.id.indigo_c);
        indigo_c.setOnClickListener(this);
        blue_c = (View) findViewById(R.id.blue_c);
        blue_c.setOnClickListener(this);
        cyan_c = (View) findViewById(R.id.cyan_c);
        cyan_c.setOnClickListener(this);
        lightBlue_c = (View) findViewById(R.id.lightBlue_c);
        lightBlue_c.setOnClickListener(this);
        teal_c = (View) findViewById(R.id.teal_c);
        teal_c.setOnClickListener(this);
        green_c = (View) findViewById(R.id.green_c);
        green_c.setOnClickListener(this);
        lightGreen_c = (View) findViewById(R.id.lightGreen_c);
        lightGreen_c.setOnClickListener(this);
        lime_c = (View) findViewById(R.id.lime_c);
        lime_c.setOnClickListener(this);
        yellow_c = (View) findViewById(R.id.yellow_c);
        yellow_c.setOnClickListener(this);
        amber_c = (View) findViewById(R.id.amber_c);
        amber_c.setOnClickListener(this);
        orange_c = (View) findViewById(R.id.orange_c);
        orange_c.setOnClickListener(this);
        deepOrange_c = (View) findViewById(R.id.deepOrange_c);
        deepOrange_c.setOnClickListener(this);
        brown_c = (View) findViewById(R.id.brown_c);
        brown_c.setOnClickListener(this);
        grey_c = (View) findViewById(R.id.grey_c);
        grey_c.setOnClickListener(this);
        blueGrey_c = (View) findViewById(R.id.blueGrey_c);
        blueGrey_c.setOnClickListener(this);

        c1.setBackgroundResource(R.color.red_p1);
        c2.setBackgroundResource(R.color.red_p2);
        c3.setBackgroundResource(R.color.red_p3);
        c4.setBackgroundResource(R.color.red_p4);
        c5.setBackgroundResource(R.color.red_p5);
        c6.setBackgroundResource(R.color.red_p6);
        c7.setBackgroundResource(R.color.red_p7);
        c8.setBackgroundResource(R.color.red_p8);
        c9.setBackgroundResource(R.color.red_p9);

        grad_rad_group = (RadioGroup) findViewById(R.id.grad_rad_group);
        grad_linearRadio = (RadioButton) findViewById(R.id.grad_linearRadio);
        grad_radialRadio = (RadioButton) findViewById(R.id.grad_radialRadio);


        choose_txt_bg_color_Rgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (rb == txt_state_txt_rad) {
                    txt_state_choose = false;
                } else if (rb == txt_state_txt_bg_rad) {
                    txt_state_choose = true;
                }
            }
        });


        grad_rad_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (rb == grad_linearRadio) {
                    radio_Lin = true;
                    radio_rad = false;
                    grad_position_seek.setProgress(30);
                    grad_X_axis_seek.setProgress(1);
                    grad_Y_axis_seek.setProgress(1);

                    txt_grad_lin();

                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                    } else {
                        Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_LONG).show();
                    }
                } else if (rb == grad_radialRadio) {
                    radio_Lin = false;
                    radio_rad = true;
                    grad_position_seek.setProgress(30);
                    grad_X_axis_seek.setProgress(1);
                    grad_Y_axis_seek.setProgress(1);

                    txt_grad_rad();

                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                    } else {
                        Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_LONG).show();
                    }
                }

            }

        });


        grad_position_seek = (SeekBar) findViewById(R.id.grad_spacing_seek);
        grad_position_seek.setMax(360);
        grad_position_seek.setProgress(20);
        grad_position_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (radio_rad && progress>20) {
                    rad_s = progress;
                    txt_grad_rad();

                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                        txt1_grad_spacing_val = progress;

                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                        txt2_grad_spacing_val = progress;

                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                        txt3_grad_spacing_val = progress;
                    }

                } else if (radio_Lin && progress>20) {
                    lin_grad_y1 = progress;
                    txt_grad_lin();

                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                        txt1_grad_spacing_val = progress;

                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                        txt2_grad_spacing_val = progress;

                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                        txt3_grad_spacing_val = progress;
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool)) {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool)) {
                    grad_position_seek.setProgress(20);
                }
            }
        });

        grad_X_axis_seek = (SeekBar) findViewById(R.id.grad_X_axis_seek);
        grad_X_axis_seek.setMax(360);
        grad_X_axis_seek.setProgress(20);
        grad_X_axis_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (radio_rad && progress>20) {
                    rad_x = progress;
                    txt_grad_rad();

                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                        txt1_grad_x_val = progress;

                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                        txt2_grad_x_val = progress;

                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                        txt3_grad_x_val = progress;
                    }
                } else if (radio_Lin && progress>20) {
                    lin_grad_x0 = progress;

                    txt_grad_lin();

                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                        txt1_grad_x_val = progress;

                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                        txt1_grad_x_val = progress;

                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                        txt1_grad_x_val = progress;

                    }
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool)) {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool)) {
                    grad_X_axis_seek.setProgress(20);
                }
            }
        });

        grad_Y_axis_seek = (SeekBar) findViewById(R.id.grad_Y_axis_seek);
        grad_Y_axis_seek.setMax(360);
        grad_Y_axis_seek.setProgress(20);
        grad_Y_axis_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (radio_rad && progress>20) {
                    rad_y = progress;
                    txt_grad_rad();

                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                        txt1_grad_y_val = progress;

                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                        txt2_grad_y_val = progress;

                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                        txt3_grad_y_val = progress;

                    }
                } else if (radio_Lin && progress>20) {
                    lin_grad_x1 = progress;
                    txt_grad_lin();

                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                        txt1_grad_y_val = progress;

                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                        txt2_grad_y_val = progress;

                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                        txt3_grad_y_val = progress;

                    }
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool)) {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool)) {
                    grad_Y_axis_seek.setProgress(20);
                }
            }
        });


        contrast.setProgress(100);
        contrast.setMax(200);
        contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (photo_bool) {
                    contrast_p_photo_save = progress;
                    img_user.setColorFilter(setContrast(progress));

                } else if (theme_edt_Butt_bool) {
                    contrast_p_theme_save = progress;
                    img_theme.setColorFilter(setContrast(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        opacity.setMax(255);
        opacity.setProgress(200);
        opacity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (photo_bool) {
                    opacity_p_photo_save = progress;
                    img_user.setImageAlpha(progress);

                } else if (theme_edt_Butt_bool) {
                    opacity_p_theme_save = progress;
                    img_theme.setImageAlpha(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        bWSeek.setMax(500);
        bWSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                bwValue = i;
                if (photo_bool) {
                    bw_p_photo_save = bwValue;
                    rgbChanger();
                }
                if (theme_edt_Butt_bool) {
                    bw_p_theme_save = bwValue;
                    rgbChanger();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        g_bg_drw_1 = (ColorDrawable) grad_c1.getBackground();
        g_bg_drw_2 = (ColorDrawable) grad_c2.getBackground();
        g_bg_drw_3 = (ColorDrawable) grad_c3.getBackground();
        g_bg_drw_4 = (ColorDrawable) grad_c4.getBackground();
        g_bg_drw_5 = (ColorDrawable) grad_c5.getBackground();
        g_bg_drw_6 = (ColorDrawable) grad_c6.getBackground();
        gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{grad_colorId_1, grad_colorId_2});
        gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
//        bg_view.setBackgroundDrawable(gd);


        gradBG_Type_grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @SuppressLint("NewApi")
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb2 = (RadioButton) group.findViewById(checkedId);

                grad_colorId_1 = g_bg_drw_1.getColor();
                grad_colorId_2 = g_bg_drw_2.getColor();
                grad_colorId_3 = g_bg_drw_3.getColor();
                grad_colorId_4 = g_bg_drw_4.getColor();
                grad_colorId_5 = g_bg_drw_5.getColor();
                grad_colorId_6 = g_bg_drw_6.getColor();
                gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{grad_colorId_1, grad_colorId_2, grad_colorId_3, grad_colorId_4, grad_colorId_5, grad_colorId_6});

                if (rb2 == gradBG_Type_liner) {
                    gradType = 0;
                    gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                    bg_view.setBackgroundDrawable(gd);
                    ll_angle.setVisibility(View.VISIBLE);
                    ll_St_1.setVisibility(View.GONE);
                } else if (rb2 == gradBG_Type_sweep) {
                    gradType = 1;
                    gd.setGradientType(GradientDrawable.SWEEP_GRADIENT);
                    bg_view.setBackgroundDrawable(gd);
                    ll_angle.setVisibility(View.GONE);
                    ll_St_1.setVisibility(View.VISIBLE);
                }

            }
        });


        bg_grad_seek = (SeekBar) findViewById(R.id.bg_grad_seek);
        bg_grad_seek.setMax(10);
        bg_grad_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gradXF = getConvertedValue(progress);
                gd.setGradientCenter(gradXF, gradYF);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bg_grad_seek2 = (SeekBar) findViewById(R.id.bg_grad_seek2);
        bg_grad_seek2.setMax(10);
        bg_grad_seek2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gradYF = getConvertedValue(progress);
                gd.setGradientCenter(gradXF, gradYF);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        bg_corner_seek = (SeekBar) findViewById(R.id.bg_corner_seek);
        bg_corner_seek.setMax(360);
        bg_corner_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                gd.setCornerRadius(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        c_bg_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                if (rb == c_bg_linearRadio) {
                    c_bg_radio_Lin = true;
                    c_bg_radio_rad = false;
                    c_bg_grad_spacing_seek.setProgress(20);
                    c_bg_grad_X_axis_seek.setProgress(20);
                    c_bg_grad_Y_axis_seek.setProgress(20);

                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                } else if (rb == c_bg_radialRadio) {
                    c_bg_radio_Lin = false;
                    c_bg_radio_rad = true;
                    c_bg_grad_spacing_seek.setProgress(20);
                    c_bg_grad_X_axis_seek.setProgress(20);
                    c_bg_grad_Y_axis_seek.setProgress(20);

                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient1 = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient1);
                    bg_view.setBackground(drawable2);
                }
            }
        });

        c_bg_grad_spacing_seek = (SeekBar) findViewById(R.id.c_bg_grad_spacing_seek);
        c_bg_grad_spacing_seek.setMax(screen_width);
        c_bg_grad_spacing_seek.setProgress(20);
        c_bg_grad_spacing_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (c_bg_radio_rad && progress>20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    c_bg_rad_s = progress;
                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient1 = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient1);
                    bg_view.setBackground(drawable2);
                } else if (c_bg_radio_Lin && progress>20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();
                    lin_grad_bg_y1 = progress;

                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                }


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        c_bg_grad_X_axis_seek = (SeekBar) findViewById(R.id.c_bg_grad_X_axis_seek);
        c_bg_grad_X_axis_seek.setMax(screen_width);
        c_bg_grad_X_axis_seek.setProgress(20);
        c_bg_grad_X_axis_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (c_bg_radio_rad && progress>20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    c_bg_rad_x = progress;
                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);

                } else if (c_bg_radio_Lin && progress>20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();
                    lin_grad_bg_x1 = progress;

                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        c_bg_grad_Y_axis_seek = (SeekBar) findViewById(R.id.c_bg_grad_Y_axis_seek);
        c_bg_grad_Y_axis_seek.setMax(screen_width);
        c_bg_grad_Y_axis_seek.setProgress(20);
        c_bg_grad_Y_axis_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (c_bg_radio_rad && progress>20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    c_bg_rad_y = progress;
                    Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                    RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                    Shader shader_gradient = rad_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                } else if (c_bg_radio_Lin && progress>20) {
                    ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
                    ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
                    colorId1 = cd1.getColor();
                    colorId2 = cd2.getColor();

                    lin_grad_bg_x0 = progress;
                    int[] color = {colorId1, colorId2};
                    float[] position = {0, 1};
                    Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                    LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                    Shader shader_gradient = lin_grad;

                    Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                    BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                    bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    drawable2.getPaint().setShader(shader_gradient);
                    bg_view.setBackground(drawable2);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });


        radius_shadow_seek = (SeekBar) findViewById(R.id.radius_shadow_seek);
        radius_shadow_seek.setMax(200);
        radius_shadow_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                shadow_rad = getConvertedValue(progress);
                txtShadow();

                if (txt_1_bool) {
                    t1_shadow_r = progress;
                } else if (txt_2_bool) {
                    t2_shadow_r = progress;
                } else if (txt_3_bool) {
                    t3_shadow_r = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    radius_shadow_seek.setProgress(0);
                }
            }
        });
        dX_shadow_seek = (SeekBar) findViewById(R.id.dX_shadow_seek);
        dX_shadow_seek.setMax(200);
        dX_shadow_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                shadow_dx = getConvertedValue(progress);
                txtShadow();

                if (txt_1_bool) {
                    t1_shadow_dx = progress;
                } else if (txt_2_bool) {
                    t2_shadow_dx = progress;
                } else if (txt_3_bool) {
                    t3_shadow_dx = progress;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    dX_shadow_seek.setProgress(0);
                }
            }
        });
        dY_shadow_seek = (SeekBar) findViewById(R.id.dY_shadow_seek);
        dY_shadow_seek.setMax(200);
        dY_shadow_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                shadow_dy = getConvertedValue(progress);
                txtShadow();

                if (txt_1_bool) {
                    t1_shadow_dy = progress;
                } else if (txt_2_bool) {
                    t2_shadow_dy = progress;
                } else if (txt_3_bool) {
                    t3_shadow_dy = progress;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    dY_shadow_seek.setProgress(0);
                }
            }
        });

        txt_Linespacing_seek = (SeekBar) findViewById(R.id.txt_Linespacing_seek);
        txt_spacing_seek = (SeekBar) findViewById(R.id.txt_spacing_seek);
        txt_rotate_seek = (SeekBar) findViewById(R.id.txt_rotate_seek);
        txt_rotateX_seek = (SeekBar) findViewById(R.id.txt_rotateX_seek);
        txt_rotateY_seek = (SeekBar) findViewById(R.id.txt_rotateY_seek);
        txt_L_height_seek = (SeekBar) findViewById(R.id.txt_L_height_seek);
        txt_L_width_seek = (SeekBar) findViewById(R.id.txt_L_width_seek);
        txt_corner_seek = (SeekBar) findViewById(R.id.txt_corner_seek);
        txtBg_opa_seek = (SeekBar) findViewById(R.id.txtBg_opa_seek);


        txt_rotate_seek.setMax(360);
        txt_rotate_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool) {
                    txt1_roatae_val = progress;
                    txt_1.setRotation(txt1_roatae_val);
                } else if (txt_2_bool) {
                    txt2_roatae_val = progress;
                    txt_2.setRotation(txt2_roatae_val);
                } else if (txt_3_bool) {
                    txt3_roatae_val = progress;
                    txt_3.setRotation(txt3_roatae_val);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    txt_rotate_seek.setProgress(0);
                }
            }
        });

        txt_rotateX_seek.setMax(360);
        txt_rotateX_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool) {
                    txt1_roataeX_val = progress;
                    txt_1.setRotationX(txt1_roataeX_val);
                } else if (txt_2_bool) {
                    txt2_roataeX_val = progress;
                    txt_2.setRotationX(txt2_roataeX_val);
                } else if (txt_3_bool) {
                    txt3_roataeX_val = progress;
                    txt_3.setRotationX(txt3_roataeX_val);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    txt_rotateX_seek.setProgress(0);
                }
            }
        });

        txt_rotateY_seek.setMax(360);
        txt_rotateY_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool) {
                    txt1_roataeY_val = progress;
                    txt_1.setRotationY(txt1_roataeY_val);
                } else if (txt_2_bool) {
                    txt2_roataeY_val = progress;
                    txt_2.setRotationY(txt2_roataeY_val);
                } else if (txt_3_bool) {
                    txt3_roataeY_val = progress;
                    txt_3.setRotationY(txt3_roataeY_val);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    txt_rotateY_seek.setProgress(0);
                }
            }
        });


        txt_corner_seek.setMax(360);
        txt_corner_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            Boolean check_edd = false;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool && txt1_rl.getBackground()!=null) {
                    txt1_bgCor = progress;
                    gd_txt1_bg.setCornerRadius(txt1_bgCor);
                    check_edd = true;
                } else if (txt_2_bool && txt2_rl.getBackground()!=null) {
                    txt2_bgCor = progress;
                    gd_txt2_bg.setCornerRadius(txt2_bgCor);
                    check_edd = true;
                } else if (txt_3_bool && txt3_rl.getBackground()!=null) {
                    txt3_bgCor = progress;
                    gd_txt3_bg.setCornerRadius(txt3_bgCor);
                    check_edd = true;
                } else {
                    check_edd = false;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
                else if (!check_edd)
                {
                    Toast.makeText(Texteo.this, "Apply a text background color, to perform this action", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    txt_corner_seek.setProgress(0);
                }
                else if (!check_edd)
                {
                    txt_corner_seek.setProgress(0);
                }
            }
        });


        txtBg_opa_seek.setMax(250);
        txtBg_opa_seek.setProgress(230);
        txtBg_opa_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            Boolean check_edd = false;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool&& txt1_rl.getBackground()!=null) {
                    txt1_bgOpa=progress;
                    gd_txt1_bg.setAlpha(txt1_bgOpa);
                    check_edd = true;
                } else if (txt_2_bool&& txt2_rl.getBackground()!=null) {
                    txt2_bgOpa=progress;
                    gd_txt2_bg.setAlpha(txt2_bgOpa);
                    check_edd = true;
                } else if (txt_3_bool&& txt3_rl.getBackground()!=null) {
                    txt3_bgOpa=progress;
                    gd_txt3_bg.setAlpha(txt3_bgOpa);
                    check_edd = true;
                }  else {
                    check_edd = false;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
                else if (!check_edd)
                {
                    Toast.makeText(Texteo.this, "Apply a text background color, to perform this action", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    txtBg_opa_seek.setProgress(230);
                }
                else if (!check_edd)
                {
                    txtBg_opa_seek.setProgress(230);
                }

            }
        });



        txt_Linespacing_seek.setMax(10);
        txt_Linespacing_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool) {
                    txt1_linSpac = getConvertedValue(progress);
                    txt_1.setLineSpacing(txt1_linSpac, txt1_linSpac);
                } else if (txt_2_bool) {
                    txt2_linSpac = progress;
                    txt_2.setLineSpacing(txt2_linSpac, txt2_linSpac);
                } else if (txt_3_bool) {
                    txt3_linSpac = progress;
                    txt_3.setLineSpacing(txt3_linSpac, txt3_linSpac);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                txt_Linespacing_seek.setProgress(0);
            }
        });

        txt_spacing_seek.setMax(10);
        txt_spacing_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        txt1_spacing_val = getConvertedValue(progress);
                        txt_1.setLetterSpacing(txt1_spacing_val);
                    }
                } else if (txt_2_bool) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        txt2_spacing_val = getConvertedValue(progress);
                        txt_2.setLetterSpacing(txt2_spacing_val);
                    }
                } else if (txt_3_bool) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        txt3_spacing_val = getConvertedValue(progress);
                        txt_3.setLetterSpacing(txt3_spacing_val);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    txt_spacing_seek.setProgress(0);
                }
            }
        });


        txt_L_height_seek.setMax(screen_width+100);
        txt_L_height_seek.setProgress(50);
        txt_L_height_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool) {
                    txt_1.setHeight(progress);
                } else if (txt_2_bool) {
                    txt_2.setHeight(progress);
                } else if (txt_3_bool) {
                    txt_3.setHeight(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (txt_1_bool) {
                    if (txt1_rl.getBackground() == null) {
                        txt1_rl.setBackgroundColor(Color.parseColor("#8CB10000"));
                        tx1_bg_bool = true;
                    }
                } else if (txt_2_bool) {
                    if (txt2_rl.getBackground() == null) {
                        txt2_rl.setBackgroundColor(Color.parseColor("#8CECC500"));
                        tx2_bg_bool = true;
                    }
                } else if (txt_3_bool) {
                    if (txt3_rl.getBackground() == null) {
                        txt3_rl.setBackgroundColor(Color.parseColor("#8C0050A1"));
                        tx3_bg_bool = true;
                    }
                }

                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (tx1_bg_bool) {
                    txt1_rl.setBackground(null);
                    tx1_bg_bool = false;
                } else if (tx2_bg_bool) {
                    txt2_rl.setBackground(null);
                    tx2_bg_bool = false;
                } else if (tx3_bg_bool) {
                    txt3_rl.setBackground(null);
                    tx3_bg_bool = false;
                }
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    txt_L_height_seek.setProgress(0);
                }
            }
        });


        txt_L_width_seek.setMax(screen_width+100);
        txt_L_width_seek.setProgress(300);
        txt_L_width_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool) {
                    txt_1.setWidth(progress);
                } else if (txt_2_bool) {
                    txt_2.setWidth(progress);
                } else if (txt_3_bool) {
                    txt_3.setWidth(progress);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (txt_1_bool) {
                    if (txt1_rl.getBackground() == null) {
                        txt1_rl.setBackgroundColor(Color.parseColor("#8CB10000"));
                        tx1_bg_bool = true;
                    }
                } else if (txt_2_bool) {
                    if (txt2_rl.getBackground() == null) {
                        txt2_rl.setBackgroundColor(Color.parseColor("#8CECC500"));
                        tx2_bg_bool = true;
                    }
                } else if (txt_3_bool) {
                    if (txt3_rl.getBackground() == null) {
                        txt3_rl.setBackgroundColor(Color.parseColor("#8C0050A1"));
                        tx3_bg_bool = true;
                    }
                }

                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (tx1_bg_bool) {
                    txt1_rl.setBackground(null);
                    tx1_bg_bool = false;
                } else if (tx2_bg_bool) {
                    txt2_rl.setBackground(null);
                    tx2_bg_bool = false;
                } else if (tx3_bg_bool) {
                    txt3_rl.setBackground(null);
                    tx3_bg_bool = false;
                }
                if (!(txt_1_bool||txt_2_bool||txt_3_bool))
                {
                    txt_L_width_seek.setProgress(0);
                }
            }
        });


        txt_size_seek_1 = (SeekBar) findViewById(R.id.txt_size_seek_1);
        txt_size_seek_1.setMax(100);
        txt_size_seek_1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_1_bool) {
                    txt_1.setTextSize(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!txt_1_bool) {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }

                if (txt1_rl.getBackground() == null) {
                    txt1_rl.setBackgroundColor(Color.parseColor("#8CB10000"));
                    tx1_bg_bool = true;
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (tx1_bg_bool) {
                    txt1_rl.setBackground(null);
                    tx1_bg_bool = false;
                }
                if (!txt_1_bool) {
                    txt_size_seek_1.setProgress(0);
                }
            }
        });

        txt_size_seek_2 = (SeekBar) findViewById(R.id.txt_size_seek_2);
        txt_size_seek_2.setMax(100);
        txt_size_seek_2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_2_bool) {
                    txt_2.setTextSize(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!txt_2_bool) {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
                if (txt2_rl.getBackground() == null) {
                    txt2_rl.setBackgroundColor(Color.parseColor("#8CECC500"));
                    tx2_bg_bool = true;
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (tx2_bg_bool) {
                    txt2_rl.setBackground(null);
                    tx2_bg_bool = false;
                }
                if (!txt_2_bool) {
                    txt_size_seek_2.setProgress(0);
                }
            }
        });

        txt_size_seek_3 = (SeekBar) findViewById(R.id.txt_size_seek_3);
        txt_size_seek_3.setMax(100);
        txt_size_seek_3.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (txt_3_bool) {
                    txt_3.setTextSize(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (!txt_3_bool) {
                    Toast.makeText(Texteo.this, "Select Text", Toast.LENGTH_SHORT).show();
                }
                if (txt3_rl.getBackground() == null) {
                    txt3_rl.setBackgroundColor(Color.parseColor("#8C0050A1"));
                    tx3_bg_bool = true;
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (tx3_bg_bool) {
                    txt3_rl.setBackground(null);
                    tx3_bg_bool = false;
                }
                if (!txt_3_bool) {
                    txt_size_seek_3.setProgress(0);
                }
            }
        });


        fest_title_recycler = (RecyclerView) findViewById(R.id.fest_title_recycler);
        texteoFestivalTitleAdapter = new TexteoFestivalTitleAdapter(festTitleModel, Texteo.this, R.layout.row_festival_titles);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        fest_title_recycler.setLayoutManager(linearLayoutManager);

        imagesHoriRecycler = (RecyclerView) findViewById(R.id.imagesHoriRecycler);
        texteoImagesHoriAdapter = new TexteoImagesHoriAdapter(imagesHoriModel, Texteo.this, R.layout.row_images_hori);
        linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        imagesHoriRecycler.setLayoutManager(linearLayoutManager2);


        getTextsDialog();
        getFestivalTitles();

    }


    public void rgbChanger() {
        if (photo_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, bwValue
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            img_user.setColorFilter(colorFilter);
        } else if (theme_edt_Butt_bool) {
            float[] colorMatrix = {
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0.33f, 0.33f, 0.33f, 0, brightness,
                    0, 0, 0, 0, bwValue
            };
            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            img_theme.setColorFilter(colorFilter);
        }

    }


    public float getConvertedValue(int intVal) {
        float floatVal = (float) 0.0;
        floatVal = .1f * intVal;
        return floatVal;
    }


    private void getFestivalTitles() {
        festTitleModel.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            theme_internet_LL.setVisibility(View.VISIBLE);
            internet_txt_LL.setVisibility(View.GONE);

            Log.d("TXT_title_URL", AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/" + EDIT_CAT_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_THEME_TITLE + "/" + EDIT_CAT_ID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("TXT_title_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        FestivalTitlesModel fList = new FestivalTitlesModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setCreated_on(jsonObject1.getString("created_on"));
                                        fList.setColor_id(jsonObject1.getString("color_id"));

                                        festTitleModel.add(fList);
                                    }
                                    fest_title_recycler.setAdapter(texteoFestivalTitleAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            internet_txt_LL.setVisibility(View.VISIBLE);
            Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void getImagesHori(String festTitleId) {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            imagesHoriModel.clear();
            Log.d("Txt_IMGS_URL", AppUrls.BASE_URL + "images/festival/" + festTitleId + "/" + EDIT_CAT_ID);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "images/festival/" + festTitleId + "/" + EDIT_CAT_ID,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Txt_IMGS_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        ImagesHoriModel ihList = new ImagesHoriModel();
                                        ihList.setFestival_id(jsonObject1.getString("festival_id"));
                                        ihList.setImg1(jsonObject1.getString("img1"));
                                        ihList.setImg2(jsonObject1.getString("img2"));
                                        ihList.setImg3(jsonObject1.getString("img3"));

                                        imagesHoriModel.add(ihList);
                                    }
                                    imagesHoriRecycler.setAdapter(texteoImagesHoriAdapter);

                                    if (dialog_imgGRID.isShowing()) {
                                        grid_img_recycler.setAdapter(texteoImagesHoriAdapter);
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    private void getEmojiTitles() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            emojiTitlesModels.clear();
            Log.d("Emoji_Title_URL", AppUrls.BASE_URL + "stickers/" + "4");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "stickers/" + "4",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Emoji_Title_RES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        EmojiTitlesModel fList = new EmojiTitlesModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setCreated_on(jsonObject1.getString("created_on"));
                                        fList.setColor_id(jsonObject1.getString("color_id"));

                                        emojiTitlesModels.add(fList);
                                    }
                                    emoji_recycler_titles.setAdapter(emojiTitleAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void getEmojiImages(String festTitleId) {
        emojiImagesModels.clear();
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("Emoji_IMG_URL", AppUrls.BASE_URL + "images/stickers/" + festTitleId + "/" + "4");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "images/stickers/" + festTitleId + "/" + "4",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Emoji_IMG_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        EmojiImagesModel ihList = new EmojiImagesModel();
                                        ihList.setImg1(jsonObject1.getString("img1"));
                                        ihList.setImg2(jsonObject1.getString("img2"));
                                        ihList.setImg3(jsonObject1.getString("img3"));
                                        ihList.setColor_filter(jsonObject1.getString("field1"));

                                        emojiImagesModels.add(ihList);
                                    }
                                    emoji_recycler_images.setAdapter(emojiImagesAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    public void emojis(Bitmap bit, String color_fil) {
        if (!emoji_1_bool || emoji_s_1.getVisibility() == View.GONE) {
            if (!emoji_1_bool) {
                emoji_s_1 = new StickerImageView(this);
                rL.addView(emoji_s_1);
                emoji_s_1.setOnTouchListener(this);
                emoji_1_bool = true;
                emoji_s_1.setImageBitmap(bit);
                emoji_1_color = color_fil;
                emoji_1x_bool = true;

            } else if (emoji_s_1.getVisibility() == View.GONE) {
                emoji_s_1.setVisibility(View.VISIBLE);
                emoji_s_1.setImageBitmap(bit);
                emoji_1_color = color_fil;
                emoji_1x_bool = false;
            }

        } else if (!emoji_2_bool || emoji_s_2.getVisibility() == View.GONE) {
            if (!emoji_2_bool) {
                emoji_s_2 = new StickerImageView(this);
                rL.addView(emoji_s_2);
                emoji_s_2.setOnTouchListener(this);
                emoji_2_bool = true;
                emoji_s_2.setImageBitmap(bit);
                emoji_2_color = color_fil;
                emoji_2x_bool = true;

            } else if (emoji_s_2.getVisibility() == View.GONE) {
                emoji_s_2.setVisibility(View.VISIBLE);
                emoji_s_2.setImageBitmap(bit);
                emoji_2_color = color_fil;
                emoji_2x_bool = false;
            }
        } else if (!emoji_3_bool || emoji_s_3.getVisibility() == View.GONE) {
            if (!emoji_3_bool) {
                emoji_s_3 = new StickerImageView(this);
                rL.addView(emoji_s_3);
                emoji_s_3.setOnTouchListener(this);
                emoji_3_bool = true;
                emoji_s_3.setImageBitmap(bit);
                emoji_3_color = color_fil;
                emoji_3x_bool = true;

            } else if (emoji_s_3.getVisibility() == View.GONE) {
                emoji_s_3.setVisibility(View.VISIBLE);
                emoji_s_3.setImageBitmap(bit);
                emoji_3_color = color_fil;
                emoji_3x_bool = false;
            }
        }
    }

    //UPLOAD IMAGE
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                // Set the Image in ImageView after decoding the String
                img_user.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
//                img_user.setImageAlpha(200);

                photo_visi_ic.setVisibility(View.VISIBLE);
                photo_Invisi_ic.setVisibility(View.GONE);

                RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                img_user.setLayoutParams(params2);
                RectF drawableRect2 = new RectF(0, 0, img_user.getDrawable().getIntrinsicWidth(), img_user.getDrawable().getIntrinsicHeight());
                RectF viewRect2 = new RectF(0, 0, 400, 400);
                matrix.setRectToRect(drawableRect2, viewRect2, Matrix.ScaleToFit.START);
                img_user.setScaleType(ImageView.ScaleType.MATRIX);
                img_user.setImageMatrix(matrix);

                photo_toolDisplay_bool = true;
                img_user.setVisibility(View.VISIBLE);
                ll_img_tool.setVisibility(View.VISIBLE);

            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }


    public boolean onTouch(View view, MotionEvent event)
    {
        if (view == pencil_ic_img) {
            if (pencil_ic_img.getRotation() == 0) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else if (pencil_ic_img.getRotation() == 180) {
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
        if (view == grad_c1) {
            grad_c1_bool = true;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c2) {
            grad_c1_bool = false;
            grad_c2_bool = true;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c3) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = true;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c4) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = true;
            grad_c5_bool = false;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c5) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = true;
            grad_c6_bool = false;
            gradV1 = false;
            gradV2 = false;
        }

        if (view == grad_c6) {
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = true;
            gradV1 = false;
            gradV2 = false;
        }

        if (grad_custom_v1 == view) {
            gradV1 = true;
            gradV2 = false;
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
        }
        if (grad_custom_v2 == view) {
            gradV2 = true;
            gradV1 = false;
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
        }


        if (view == emoji_s_1 || view == emoji_s_2 || view == emoji_s_3) {
            if (view == emoji_s_1) {
                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(false);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(true);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(true);
                }

                emoji_bool_s1 = true;
                emoji_bool_s2 = false;
                emoji_bool_s3 = false;
                emoji_s_1.bringToFront();

            } else if (view == emoji_s_2) {

                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(true);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(false);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(true);
                }

                emoji_bool_s1 = false;
                emoji_bool_s2 = true;
                emoji_bool_s3 = false;
                emoji_s_2.bringToFront();

            } else if (view == emoji_s_3) {

                if (emoji_bool_s1) {
                    emoji_s_1.setControlItemsHidden(true);
                } else if (emoji_bool_s2) {
                    emoji_s_2.setControlItemsHidden(true);
                } else if (emoji_bool_s3) {
                    emoji_s_3.setControlItemsHidden(false);
                }

                emoji_bool_s1 = false;
                emoji_bool_s2 = false;
                emoji_bool_s3 = true;
                emoji_s_3.bringToFront();
            }

            emoji_bool = true;

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:

                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    break;

                case MotionEvent.ACTION_MOVE:

                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;

                    view.setX(newX);
                    view.setY(newY);

                    lastAction = MotionEvent.ACTION_MOVE;

                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (bg_view == view) {
            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setBgLL();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;
//
//                case MotionEvent.ACTION_MOVE:
//                    newX = event.getRawX() + dX;
//                    newY = event.getRawY() + dY;
//                    view.setX(newX);
//                    view.setY(newY);
//                    lastAction = MotionEvent.ACTION_MOVE;
//                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (c_bg_v1 == view) {
            c_bg_gradV1 = true;
            c_bg_gradV2 = false;
            gradV2 = false;
            gradV1 = false;
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
        }
        if (c_bg_v2 == view) {
            c_bg_gradV2 = true;
            c_bg_gradV1 = false;
            gradV2 = false;
            gradV1 = false;
            grad_c1_bool = false;
            grad_c2_bool = false;
            grad_c3_bool = false;
            grad_c4_bool = false;
            grad_c5_bool = false;
            grad_c6_bool = false;
            Toast.makeText(this, "Touched", Toast.LENGTH_SHORT).show();
        }

        if (view == txt_1 || view == txt1_rl) {
            imgBg = false;
            txt_1_bool = true;
            txt_2_bool = false;
            txt_3_bool = false;
            txt1_rl.bringToFront();
            txt_size_seek_1.setVisibility(View.VISIBLE);
            txt_size_seek_2.setVisibility(View.GONE);
            txt_size_seek_3.setVisibility(View.GONE);
            radius_shadow_seek.setProgress(t1_shadow_r);
            dX_shadow_seek.setProgress(t1_shadow_dx);
            dY_shadow_seek.setProgress(t1_shadow_dy);
            grad_X_axis_seek.setProgress(txt1_grad_x_val);
            grad_Y_axis_seek.setProgress(txt1_grad_y_val);
            grad_position_seek.setProgress(txt1_grad_spacing_val);
            txt_L_height_seek.setProgress(txt_1.getHeight());
            txt_L_width_seek.setProgress(txt_1.getWidth());
            txt_rotate_seek.setProgress(txt1_roatae_val);
            txt_rotateX_seek.setProgress(txt1_roataeX_val);
            txt_rotateY_seek.setProgress(txt1_roataeY_val);
            txt_Linespacing_seek.setProgress(Math.round(txt1_linSpac));

            if (txt1_bg_color_check_bool)
            {
                txt_corner_seek.setProgress(txt1_bgCor);
                txtBg_opa_seek.setProgress(txt1_bgOpa);
            }

            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    dX = txt1_rl.getX() - event.getRawX();
                    dY = txt1_rl.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setText_LL();

                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;
                    txt1_rl.setX(newX);
                    txt1_rl.setY(newY);
                    lastAction = MotionEvent.ACTION_MOVE;
                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }


        if (view == txt_2 || view == txt2_rl) {
            imgBg = false;
            txt_1_bool = false;
            txt_2_bool = true;
            txt_3_bool = false;
            txt2_rl.bringToFront();

            txt_size_seek_1.setVisibility(View.GONE);
            txt_size_seek_2.setVisibility(View.VISIBLE);
            txt_size_seek_3.setVisibility(View.GONE);

            grad_X_axis_seek.setProgress(txt2_grad_x_val);
            grad_Y_axis_seek.setProgress(txt2_grad_y_val);
            grad_position_seek.setProgress(txt2_grad_spacing_val);

            radius_shadow_seek.setProgress(t2_shadow_r);
            dX_shadow_seek.setProgress(t2_shadow_dx);
            dY_shadow_seek.setProgress(t2_shadow_dy);
            txt_L_height_seek.setProgress(txt_2.getHeight());
            txt_L_width_seek.setProgress(txt_2.getWidth());
//            txt_spacing_seek.setProgress(Math.round(txt2_spacing_val));
            txt_rotate_seek.setProgress(txt2_roatae_val);
            txt_Linespacing_seek.setProgress(Math.round(txt2_linSpac));

            if (txt2_bg_color_check_bool)
            {
                txt_corner_seek.setProgress(txt2_bgCor);
                txtBg_opa_seek.setProgress(txt2_bgOpa);
            }
            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }
            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    dX = txt2_rl.getX() - event.getRawX();
                    dY = txt2_rl.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setText_LL();

                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;
                    txt2_rl.setX(newX);
                    txt2_rl.setY(newY);
                    lastAction = MotionEvent.ACTION_MOVE;
                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }

        if (view == txt_3 || view == txt3_rl) {
            imgBg = false;
            txt_1_bool = false;
            txt_2_bool = false;
            txt_3_bool = true;
            txt3_rl.bringToFront();
            txt_size_seek_1.setVisibility(View.GONE);
            txt_size_seek_2.setVisibility(View.GONE);
            txt_size_seek_3.setVisibility(View.VISIBLE);

            grad_X_axis_seek.setProgress(txt3_grad_x_val);
            grad_Y_axis_seek.setProgress(txt3_grad_y_val);
            grad_position_seek.setProgress(txt3_grad_spacing_val);

            radius_shadow_seek.setProgress(t3_shadow_r);
            dX_shadow_seek.setProgress(t3_shadow_dx);
            dY_shadow_seek.setProgress(t3_shadow_dy);
            txt_L_height_seek.setProgress(txt_3.getHeight());
            txt_L_width_seek.setProgress(txt_3.getWidth());
//            txt_spacing_seek.setProgress(Math.round(txt3_spacing_val));
            txt_rotate_seek.setProgress(txt3_roatae_val);
            txt_Linespacing_seek.setProgress(Math.round(txt3_linSpac));

            if (txt3_bg_color_check_bool)
            {
                txt_corner_seek.setProgress(txt3_bgCor);
                txtBg_opa_seek.setProgress(txt3_bgOpa);
            }
            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }
            float newX, newY;
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    dX = txt3_rl.getX() - event.getRawX();
                    dY = txt3_rl.getY() - event.getRawY();
                    lastAction = MotionEvent.ACTION_DOWN;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setText_LL();

                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    newX = event.getRawX() + dX;
                    newY = event.getRawY() + dY;
                    txt3_rl.setX(newX);
                    txt3_rl.setY(newY);
                    lastAction = MotionEvent.ACTION_MOVE;
                    break;

                case MotionEvent.ACTION_UP:
                    if (lastAction == MotionEvent.ACTION_DOWN)
                        break;

                default:
                    return false;
            }
            return true;
        }


        if (view == img_theme) {
            txt_1_bool = false;
            txt_2_bool = false;
            txt_3_bool = false;
            imgBg = true;

            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            if (theme_touch_activateBool) {
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        savedMatrix.set(matrix_theme);
                        start.set(event.getX(), event.getY());
                        mode = DRAG;
                        lastEvent = null;
                        if (exit) {
                            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            setThemeLL();
                        } else {
                            if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                                Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                            }
                            exit = true;
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    exit = false;
                                }
                            }, 2 * 1000);
                        }
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        oldDist = spacing(event);
                        if (oldDist > 10f) {
                            savedMatrix.set(matrix_theme);
                            midPoint(mid, event);
                            mode = ZOOM;
                        }
                        lastEvent = new float[4];
                        lastEvent[0] = event.getX(0);
                        lastEvent[1] = event.getX(1);
                        lastEvent[2] = event.getY(0);
                        lastEvent[3] = event.getY(1);
                        d = rotation(event);

                        txt_1.setEnabled(false);
                        txt_2.setEnabled(false);
                        txt_3.setEnabled(false);
                        txt1_rl.setEnabled(false);
                        txt2_rl.setEnabled(false);
                        txt3_rl.setEnabled(false);
                        img_user.setEnabled(false);
                        if (emoji_1_bool) {
                            emoji_s_1.setEnabled(false);
                        }
                        if (emoji_2_bool) {
                            emoji_s_2.setEnabled(false);
                        }
                        if (emoji_3_bool) {
                            emoji_s_3.setEnabled(false);
                        }
                        txt_lockIn_ic.setVisibility(View.VISIBLE);
                        txt_lockOut_ic.setVisibility(View.GONE);
                        photo_lockIn_ic.setVisibility(View.VISIBLE);
                        photo_lockOut_ic.setVisibility(View.GONE);
                        theme_lockIn_ic.setVisibility(View.VISIBLE);
                        theme_lockOut_ic.setVisibility(View.GONE);
                        emoji_lockIn_ic.setVisibility(View.VISIBLE);
                        emoji_lockOut_ic.setVisibility(View.GONE);

                        unlockAll_butt.setVisibility(View.VISIBLE);


                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_POINTER_UP:
                        mode = NONE;
                        lastEvent = null;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (mode == DRAG) {
                            matrix_theme.set(savedMatrix);
                            float dx = event.getX() - start.x;
                            float dy = event.getY() - start.y;
                            matrix_theme.postTranslate(dx, dy);
                        } else if (mode == ZOOM) {
                            float newDist = spacing(event);
                            if (newDist > 10f) {
                                matrix_theme.set(savedMatrix);
                                float scale = (newDist / oldDist);
                                matrix_theme.postScale(scale, scale, mid.x, mid.y);
                            }
                            if (lastEvent != null && event.getPointerCount() == 2) {
                                newRot = rotation(event);
                                float r = newRot - d;
                                float[] values = new float[9];
                                matrix_theme.getValues(values);
                                float tx = values[2];
                                float ty = values[5];
                                float sx = values[0];
                                float xc = (img_theme.getWidth() / 2) * sx;
                                float yc = (img_theme.getHeight() / 2) * sx;
                                matrix_theme.postRotate(r, tx + xc, ty + yc);
                            }
                        }
                        break;
                }
                img_theme.setImageMatrix(matrix_theme);
            }
        }


        if (view == img_user) {
            txt_1_bool = false;
            txt_2_bool = false;
            txt_3_bool = false;
            imgBg = false;

            if (emoji_bool_s1) {
                emoji_s_1.setControlItemsHidden(true);
            } else if (emoji_bool_s2) {
                emoji_s_2.setControlItemsHidden(true);
            } else if (emoji_bool_s3) {
                emoji_s_3.setControlItemsHidden(true);
            }

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    savedMatrix.set(matrix);
                    start.set(event.getX(), event.getY());
                    mode = DRAG;
                    lastEvent = null;
                    if (exit) {
                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        setPhotoLL();
                    } else {
                        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                            Toast.makeText(getApplicationContext(), "Tap Twice for tools", Toast.LENGTH_SHORT).show();
                        }
                        exit = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                exit = false;
                            }
                        }, 2 * 1000);
                    }
                    break;
                case MotionEvent.ACTION_POINTER_DOWN:
                    oldDist = spacing(event);
                    if (oldDist > 10f) {
                        savedMatrix.set(matrix);
                        midPoint(mid, event);
                        mode = ZOOM;
                    }
                    lastEvent = new float[4];
                    lastEvent[0] = event.getX(0);
                    lastEvent[1] = event.getX(1);
                    lastEvent[2] = event.getY(0);
                    lastEvent[3] = event.getY(1);
                    d = rotation(event);

                    txt_1.setEnabled(false);
                    txt_2.setEnabled(false);
                    txt_3.setEnabled(false);
                    txt1_rl.setEnabled(false);
                    txt2_rl.setEnabled(false);
                    txt3_rl.setEnabled(false);
                    if (emoji_1_bool)
                    {
                        emoji_s_1.setEnabled(false);
                    }
                    if (emoji_2_bool)
                    {
                        emoji_s_2.setEnabled(false);
                    }
                    if (emoji_3_bool)
                    {
                        emoji_s_3.setEnabled(false);
                    }
                    txt_lockIn_ic.setVisibility(View.VISIBLE);
                    txt_lockOut_ic.setVisibility(View.GONE);
                    photo_lockIn_ic.setVisibility(View.GONE);
                    photo_lockOut_ic.setVisibility(View.VISIBLE);
                    theme_lockIn_ic.setVisibility(View.VISIBLE);
                    theme_lockOut_ic.setVisibility(View.GONE);
                    emoji_lockIn_ic.setVisibility(View.VISIBLE);
                    emoji_lockOut_ic.setVisibility(View.GONE);

                    unlockAll_butt.setVisibility(View.VISIBLE);

                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_POINTER_UP:
                    mode = NONE;
                    lastEvent = null;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        matrix.set(savedMatrix);
                        float dx = event.getX() - start.x;
                        float dy = event.getY() - start.y;
                        matrix.postTranslate(dx, dy);
                    } else if (mode == ZOOM) {
                        float newDist = spacing(event);
                        if (newDist > 10f) {
                            matrix.set(savedMatrix);
                            float scale = (newDist / oldDist);
                            matrix.postScale(scale, scale, mid.x, mid.y);
                        }
                        if (lastEvent != null && event.getPointerCount() == 2) {
                            newRot = rotation(event);
                            float r = newRot - d;
                            float[] values = new float[9];
                            matrix.getValues(values);
                            float tx = values[2];
                            float ty = values[5];
                            float sx = values[0];
                            float xc = (img_user.getWidth() / 2) * sx;
                            float yc = (img_user.getHeight() / 2) * sx;
                            matrix.postRotate(r, tx + xc, ty + yc);
                        }
                    }
                    break;
            }

            img_user.setImageMatrix(matrix);
        }


        return true;
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);

    }

    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    private float rotation(MotionEvent event) {
        double delta_x = (event.getX(0) - event.getX(1));
        double delta_y = (event.getY(0) - event.getY(1));
        double radians = Math.atan2(delta_y, delta_x);
        return (float) Math.toDegrees(radians);
    }


    private void getTextsDialog() {
        final EditText txt_1_editT, txt_2_editT, txt_3_editT;
        final ImageView updateTextsButt, edit_txt1_clear, edit_txt2_clear, edit_txt3_clear;

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.texteo_text_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        updateTextsButt = (ImageView) dialog.findViewById(R.id.updateTextsButt);
        edit_txt1_clear = (ImageView) dialog.findViewById(R.id.edit_txt1_clear);
        edit_txt2_clear = (ImageView) dialog.findViewById(R.id.edit_txt2_clear);
        edit_txt3_clear = (ImageView) dialog.findViewById(R.id.edit_txt3_clear);
        txt_1_editT = (EditText) dialog.findViewById(R.id.txt_1_editT);
        txt_2_editT = (EditText) dialog.findViewById(R.id.txt_2_editT);
        txt_3_editT = (EditText) dialog.findViewById(R.id.txt_3_editT);


        if (txt_1.getVisibility() == View.VISIBLE) {
            txt_1_editT.setText(txt_1.getText().toString());
        } else if (txt_1.getVisibility() == View.GONE) {
            txt_1_editT.setText("");
        }

        if (txt_2.getVisibility() == View.VISIBLE) {
            txt_2_editT.setText(txt_2.getText().toString());
        } else if (txt_2.getVisibility() == View.GONE) {
            txt_2_editT.setText("");
        }

        if (txt_3.getVisibility() == View.VISIBLE) {
            txt_3_editT.setText(txt_3.getText().toString());
        } else if (txt_3.getVisibility() == View.GONE) {
            txt_3_editT.setText("");
        }

        updateTextsButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((txt_1_editT.length() > 0) || (txt_2_editT.length() > 0) || (txt_3_editT.length() > 0)) {
                    if (txt_1_editT.length() >= 1) {
                        txt_1.invalidate();
                        txt_1.setText(txt_1_editT.getText().toString());
                        editd_txt1_visi_ic.setVisibility(View.GONE);
                        editd_txt1_Invisi_ic.setVisibility(View.VISIBLE);
                        txt_1.setVisibility(View.VISIBLE);
                    } else {

                        txt_1.setVisibility(View.GONE);
                        editd_txt1_visi_ic.setVisibility(View.GONE);
                        editd_txt1_Invisi_ic.setVisibility(View.GONE);
                    }

                    if (txt_2_editT.length() >= 1) {
                        txt_2.invalidate();
                        txt_2.setText(txt_2_editT.getText().toString());
                        editd_txt2_visi_ic.setVisibility(View.GONE);
                        editd_txt2_Invisi_ic.setVisibility(View.VISIBLE);
                        txt_2.setVisibility(View.VISIBLE);
                    } else {
                        txt_2.setVisibility(View.GONE);
                        editd_txt2_visi_ic.setVisibility(View.GONE);
                        editd_txt2_Invisi_ic.setVisibility(View.GONE);
                    }

                    if (txt_3_editT.length() >= 1) {
                        txt_3.invalidate();
                        txt_3.setText(txt_3_editT.getText().toString());
                        editd_txt3_visi_ic.setVisibility(View.GONE);
                        editd_txt3_Invisi_ic.setVisibility(View.VISIBLE);
                        txt_3.setVisibility(View.VISIBLE);
                    } else {
                        txt_3.setVisibility(View.GONE);
                        editd_txt3_visi_ic.setVisibility(View.GONE);
                        editd_txt3_Invisi_ic.setVisibility(View.GONE);
                    }

                    txt_Invisi_ic.setVisibility(View.GONE);
                    txt_visi_ic.setVisibility(View.VISIBLE);

                    dialog.cancel();
                } else {
                    Toast.makeText(Texteo.this, "Please, Enter some Texts to edit", Toast.LENGTH_LONG).show();
                }


                if ((txt_1_editT.length() == 0) || (txt_2_editT.length() == 0) || (txt_3_editT.length() == 0)) {
                    if (txt_1_editT.length() == 0) {
                        txt_1_editT.setText("");
                        txt_1.setText("");
                    }
                    if (txt_2_editT.length() == 0) {
                        txt_2_editT.setText("");
                        txt_2.setText("");
                    }
                    if (txt_3_editT.length() == 0) {
                        txt_3_editT.setText("");
                        txt_3.setText("");
                    }
                }


            }
        });

        edit_txt1_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_1_editT.setText("");
                txt_1.setText("");
            }
        });
        edit_txt2_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_2_editT.setText("");
                txt_2.setText("");
            }
        });
        edit_txt3_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_3_editT.setText("");
                txt_3.setText("");
            }
        });


        dialog.show();
    }


    private void outputDialog() {
        final ImageView outPut_img;
        final RelativeLayout rLL;
        final TextView save_to_gallery_butt, share_to_fb, share_to_messenger, share_to_whatsapp, share_to_Instagram;

        output_dialog = new Dialog(this);
        output_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        output_dialog.setContentView(R.layout.activity_edit_output);
        output_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        output_dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        outPut_img = (ImageView) output_dialog.findViewById(R.id.outPut_img);
        save_to_gallery_butt = (TextView) output_dialog.findViewById(R.id.save_to_gallery_butt);
        share_to_whatsapp = (TextView) output_dialog.findViewById(R.id.share_to_whatsapp);
        logo_watermark = (ImageView) output_dialog.findViewById(R.id.logo_watermark);
        logo_watermark.setColorFilter(Color.WHITE);
        rLL = (RelativeLayout) output_dialog.findViewById(R.id.rLL);

        final Bitmap bitmapOP = getBitmap(rL);
        outPut_img.setImageBitmap(bitmapOP);

        save_to_gallery_butt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Texteo.this, "Saved To Gallery", Toast.LENGTH_LONG).show();
                final Bitmap bitmapOP_final = getBitmap(rLL);
                saveChart(bitmapOP_final, rLL.getMeasuredHeight(), rLL.getMeasuredWidth());
                output_dialog.cancel();
            }

        });

        share_to_whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Picxture");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "PX Testing..");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                output_dialog.cancel();
            }

        });


        output_dialog.show();
    }


    public static PorterDuffColorFilter setContrast(int progress) {
        if (progress >= 100) {
            int value = (int) (progress - 100) * 255 / 100;

            return new PorterDuffColorFilter(Color.argb(value, 255, 255, 255), PorterDuff.Mode.OVERLAY);
        } else {
            int value = (int) (100 - progress) * 255 / 100;
            return new PorterDuffColorFilter(Color.argb(value, 0, 0, 0), PorterDuff.Mode.OVERLAY);
        }

    }


    @SuppressLint("NewApi")
    private void getGradBg_Style_1() {
        g_bg_drw_1 = (ColorDrawable) grad_c1.getBackground();
        g_bg_drw_2 = (ColorDrawable) grad_c2.getBackground();
        g_bg_drw_3 = (ColorDrawable) grad_c3.getBackground();
        g_bg_drw_4 = (ColorDrawable) grad_c4.getBackground();
        g_bg_drw_5 = (ColorDrawable) grad_c5.getBackground();
        g_bg_drw_6 = (ColorDrawable) grad_c6.getBackground();

        if (setBgLL_bool && bg_GRAD_0_1 == 0) {
            bg_view.setBackgroundColor(getViewColo);
        } else if (setBgLL_bool && bg_GRAD_0_1 == 1) {
            if (grad_c1_bool) {
                grad_c1.setBackgroundColor(getViewColo);
            } else if (grad_c2_bool) {
                grad_c2.setBackgroundColor(getViewColo);
            } else if (grad_c3_bool) {
                grad_c3.setBackgroundColor(getViewColo);
            } else if (grad_c4_bool) {
                grad_c4.setBackgroundColor(getViewColo);
            } else if (grad_c5_bool) {
                grad_c5.setBackgroundColor(getViewColo);
            } else if (grad_c6_bool) {
                grad_c6.setBackgroundColor(getViewColo);
            }

            grad_colorId_1 = g_bg_drw_1.getColor();
            grad_colorId_2 = g_bg_drw_2.getColor();
            grad_colorId_3 = g_bg_drw_3.getColor();
            grad_colorId_4 = g_bg_drw_4.getColor();
            grad_colorId_5 = g_bg_drw_5.getColor();
            grad_colorId_6 = g_bg_drw_6.getColor();
            gd = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{grad_colorId_1, grad_colorId_2, grad_colorId_3, grad_colorId_4, grad_colorId_5, grad_colorId_6});

            if (gradType == 0) {
                gd.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                bg_view.setBackgroundDrawable(gd);
            } else if (gradType == 1) {
                gd.setGradientType(GradientDrawable.SWEEP_GRADIENT);
                bg_view.setBackgroundDrawable(gd);
            }
        }
    }


    private void getGradBg_Style_2() {
        if (style_2_txt_bool) {
            if (c_bg_gradV1) {
                c_bg_v1.setBackgroundColor(getViewColo);
            } else if (c_bg_gradV2) {
                c_bg_v2.setBackgroundColor(getViewColo);
            }
            ColorDrawable cd1 = (ColorDrawable) c_bg_v1.getBackground();
            ColorDrawable cd2 = (ColorDrawable) c_bg_v2.getBackground();
            colorId1 = cd1.getColor();
            int colorId2 = cd2.getColor();

            if (c_bg_radio_Lin) {
                int[] color = {colorId1, colorId2};
                float[] position = {0, 1};
                Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
                LinearGradient lin_grad = new LinearGradient(lin_grad_bg_x0, 0, lin_grad_bg_x1, lin_grad_bg_y1, color, position, tile_mode);
                Shader shader_gradient = lin_grad;

                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                drawable2.getPaint().setShader(shader_gradient);
                bg_view.setBackground(drawable2);
            } else if (c_bg_radio_rad) {
                Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;// or TileMode.REPEAT;
                RadialGradient rad_grad = new RadialGradient(c_bg_rad_x, c_bg_rad_y, c_bg_rad_s, colorId1, colorId2, tile_mode1);
                Shader shader_gradient = rad_grad;

                Bitmap bitmap1 = BitmapFactory.decodeResource(getResources(), R.drawable.test);
                BitmapDrawable drawable2 = new BitmapDrawable(getResources(), bitmap1);
                bg_view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                drawable2.getPaint().setShader(shader_gradient);
                bg_view.setBackground(drawable2);
            }
        }
    }


    public void txt_grad_lin() {
        ColorDrawable cd1 = (ColorDrawable) grad_custom_v1.getBackground();
        ColorDrawable cd2 = (ColorDrawable) grad_custom_v2.getBackground();
        colorId1 = cd1.getColor();
        colorId2 = cd2.getColor();

        int[] color = {colorId1, colorId2};
        float[] position = {0, 1};
        Shader.TileMode tile_mode = Shader.TileMode.REPEAT;
        LinearGradient lin_grad = new LinearGradient(lin_grad_x0, 0, lin_grad_x1, lin_grad_y1, color, position, tile_mode);
        if (txt_1_bool) {
            shade_grad_title = lin_grad;
        } else if (txt_2_bool) {
            shade_grad_p1 = lin_grad;
        } else if (txt_3_bool) {
            shade_grad_p2 = lin_grad;
        }

    }

    public void txt_grad_rad() {
        ColorDrawable cd1 = (ColorDrawable) grad_custom_v1.getBackground();
        ColorDrawable cd2 = (ColorDrawable) grad_custom_v2.getBackground();
        colorId1 = cd1.getColor();
        colorId2 = cd2.getColor();

        Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;
        RadialGradient rad_grad = new RadialGradient(rad_x, rad_y, rad_s, colorId1, colorId2, tile_mode1);
        if (txt_1_bool) {
            shade_grad_title = rad_grad;
        } else if (txt_2_bool) {
            shade_grad_p1 = rad_grad;
        } else if (txt_3_bool) {
            shade_grad_p2 = rad_grad;
        }
    }

    public void txt_grad_solid() {
        ColorDrawable cd1 = (ColorDrawable) grad_custom_v1.getBackground();
        colorId1 = cd1.getColor();

        Shader.TileMode tile_mode1 = Shader.TileMode.REPEAT;
        RadialGradient rad_grad_solid = new RadialGradient(rad_x, rad_y, rad_s, colorId1, colorId1, tile_mode1);
        if (txt_1_bool) {
            shade_s_title = rad_grad_solid;
        } else if (txt_2_bool) {
            shade_s_p1 = rad_grad_solid;
        } else if (txt_3_bool) {
            shade_s_p2 = rad_grad_solid;
        }
    }


    private void getGradText() {
        if (txt_colorsButt_bool) {
            if (!txt_state_choose) {
                if (gradV1) {
                    txt1_bg_color = getViewColo;
                    grad_custom_v1.setBackgroundColor(txt1_bg_color);
                } else if (gradV2) {
                    txt2_bg_color = getViewColo;
                    grad_custom_v2.setBackgroundColor(txt2_bg_color);
                } else {
                    txt3_bg_color = getViewColo;
                    grad_custom_v1.setBackgroundColor(txt3_bg_color);
                }

                txt_grad_lin();
                txt_grad_rad();
                txt_grad_solid();

                if (colorId1 != 0 && s_g_bool && radio_rad) {
                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                    } else {
                        Toast.makeText(this, "Select Text", Toast.LENGTH_LONG).show();
                    }
                } else if (colorId1 != 0 && s_g_bool && radio_Lin) {
                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_grad_title);
                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_grad_p1);
                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_grad_p2);
                    } else {
                        Toast.makeText(this, "Select Text", Toast.LENGTH_LONG).show();
                    }
                } else {
                    if (txt_1_bool) {
                        txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_1.getPaint().setShader(shade_s_title);
                    } else if (txt_2_bool) {
                        txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_2.getPaint().setShader(shade_s_p1);
                    } else if (txt_3_bool) {
                        txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                        txt_3.getPaint().setShader(shade_s_p2);
                    } else {
                        Toast.makeText(this, "Select Text", Toast.LENGTH_LONG).show();
                    }
                }
            } else {
                if (getViewColo!=0)
                {
                    if (txt_1_bool) {
                        gd_txt1_bg = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{getViewColo, getViewColo});
                        gd_txt1_bg.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                        txt1_rl.setBackgroundDrawable(gd_txt1_bg);
                        gd_txt1_bg.setAlpha(txt3_bgOpa);
                        txt1_bg_color_check_bool = true;
                    } else if (txt_2_bool) {
                        gd_txt2_bg = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{getViewColo, getViewColo});
                        gd_txt2_bg.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                        txt2_rl.setBackgroundDrawable(gd_txt2_bg);
                        gd_txt2_bg.setAlpha(txt3_bgOpa);
                        txt2_bg_color_check_bool = true;
                    } else if (txt_3_bool) {
                        gd_txt3_bg = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[]{getViewColo, getViewColo});
                        gd_txt3_bg.setGradientType(GradientDrawable.LINEAR_GRADIENT);
                        txt3_rl.setBackgroundDrawable(gd_txt3_bg);
                        gd_txt3_bg.setAlpha(txt3_bgOpa);
                        txt3_bg_color_check_bool = true;
                    }
                }
                else
                {
                    if (txt_1_bool) {
                        txt1_rl.setBackground(null);
                    } else if (txt_2_bool) {
                        txt2_rl.setBackground(null);
                    } else if (txt_3_bool) {
                        txt3_rl.setBackground(null);
                    }
                }


            }
        }

    }


    private void getLogoColor() {
        if (emoji_bool) {
            if (emoji_bool_s1) {
                if (emoji_1_bool && emoji_1_color.equals("1")) {
                    emoji_s_1.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }

            if (emoji_bool_s2) {
                if (emoji_2_bool && emoji_2_color.equals("1")) {
                    emoji_s_2.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }

            if (emoji_bool_s3) {
                if (emoji_3_bool && emoji_3_color.equals("1")) {
                    emoji_s_3.setColorFilter(getViewColo);
                } else {
                    Toast.makeText(this, "This Emoji color can't be edited", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    private void txtShadow() {
        if (txt_1_bool) {
            txt_1.setShadowLayer(
                    shadow_rad, // radius
                    shadow_dx, // dx
                    shadow_dy, // dy
                    getViewColo // shadow color
            );
        } else if (txt_2_bool) {
            txt_2.setShadowLayer(
                    shadow_rad, // radius
                    shadow_dx, // dx
                    shadow_dy, // dy
                    getViewColo // shadow color
            );
        } else if (txt_3_bool) {
            txt_3.setShadowLayer(
                    shadow_rad, // radius
                    shadow_dx, // dx
                    shadow_dy, // dy
                    getViewColo // shadow color
            );
        }

    }


    private void getEmojiDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.emoji_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        emoji_recycler_titles = (RecyclerView) dialog.findViewById(R.id.emoji_recycler_titles);
        emojiTitleAdapter = new EmojiTitleAdapter_Texteo(emojiTitlesModels, Texteo.this, R.layout.row_festival_titles);
        linearLayoutManager_ST = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        emoji_recycler_titles.setLayoutManager(linearLayoutManager_ST);

        emoji_recycler_images = (RecyclerView) dialog.findViewById(R.id.emoji_recycler_images);
        emojiImagesAdapter = new EmojiImagesAdapter_Texteo(emojiImagesModels, Texteo.this, R.layout.row_images_emoji);
        gridLayoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        emoji_recycler_images.setLayoutManager(gridLayoutManager);

        getEmojiTitles();

        dialog.show();
    }

    @SuppressLint({"ResourceAsColor", "NewApi"})
    @Override
    public void onClick(View view)
    {
        if (internet_txt_LL == view) {
            getFestivalTitles();
        }

        if (view == txt_align_T) {
            if (txt_1_bool) {
                txt_1.setGravity(Gravity.TOP);
            } else if (txt_2_bool) {
                txt_2.setGravity(Gravity.TOP);
            } else if (txt_3_bool) {
                txt_3.setGravity(Gravity.TOP);
            }
        }
        if (view == txt_align_B) {
            if (txt_1_bool) {
                txt_1.setGravity(Gravity.BOTTOM);
            } else if (txt_2_bool) {
                txt_2.setGravity(Gravity.BOTTOM);
            } else if (txt_3_bool) {
                txt_3.setGravity(Gravity.BOTTOM);
            }
        }
        if (view == txt_align_R) {

            if (txt_1_bool) {
                txt_1.setGravity(Gravity.RIGHT);
            } else if (txt_2_bool) {
                txt_2.setGravity(Gravity.RIGHT);
            } else if (txt_3_bool) {
                txt_3.setGravity(Gravity.RIGHT);
            }
        }
        if (view == txt_align_L) {
            if (txt_1_bool) {
                txt_1.setGravity(Gravity.LEFT);
            } else if (txt_2_bool) {
                txt_2.setGravity(Gravity.LEFT);
            } else if (txt_3_bool) {
                txt_3.setGravity(Gravity.LEFT);
            }
        }
        if (view == txt_align_C) {
            if (txt_1_bool) {
                txt_1.setGravity(Gravity.CENTER);
            } else if (txt_2_bool) {
                txt_2.setGravity(Gravity.CENTER);
            } else if (txt_3_bool) {
                txt_3.setGravity(Gravity.CENTER);
            }
        }
        if (view == txt_align_Ch) {
            if (txt_1_bool) {
                txt_1.setGravity(Gravity.CENTER_HORIZONTAL);
            } else if (txt_2_bool) {
                txt_2.setGravity(Gravity.CENTER_HORIZONTAL);
            } else if (txt_3_bool) {
                txt_3.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        }
        if (view == txt_align_Cv) {
            if (txt_1_bool) {
                txt_1.setGravity(Gravity.CENTER_VERTICAL);
            } else if (txt_2_bool) {
                txt_2.setGravity(Gravity.CENTER_VERTICAL);
            } else if (txt_3_bool) {
                txt_3.setGravity(Gravity.CENTER_VERTICAL);
            }
        }

        if (view==txt_lockIn_ic)
        {
            txt_lockIn_ic.setVisibility(View.GONE);
            txt_lockOut_ic.setVisibility(View.VISIBLE);
            txt_1.setEnabled(true);
            txt1_rl.setEnabled(true);
            txt_2.setEnabled(true);
            txt2_rl.setEnabled(true);
            txt_3.setEnabled(true);
            txt3_rl.setEnabled(true);
        }
        if (view==txt_lockOut_ic)
        {
            txt_lockOut_ic.setVisibility(View.GONE);
            txt_lockIn_ic.setVisibility(View.VISIBLE);
            txt_1.setEnabled(false);
            txt1_rl.setEnabled(false);
            txt_2.setEnabled(false);
            txt2_rl.setEnabled(false);
            txt_3.setEnabled(false);
            txt3_rl.setEnabled(false);
        }
        if (view==photo_lockIn_ic)
        {
            photo_lockIn_ic.setVisibility(View.GONE);
            photo_lockOut_ic.setVisibility(View.VISIBLE);
            img_user.setEnabled(true);
        }
        if (view==photo_lockOut_ic)
        {
            photo_lockOut_ic.setVisibility(View.GONE);
            photo_lockIn_ic.setVisibility(View.VISIBLE);
            img_user.setEnabled(false);
        }

        if (view==theme_lockIn_ic)
        {
            theme_lockIn_ic.setVisibility(View.GONE);
            theme_lockOut_ic.setVisibility(View.VISIBLE);
            img_theme.setEnabled(true);
            theme_touch_activateBool = true;
        }
        if (view==theme_lockOut_ic)
        {
            theme_lockOut_ic.setVisibility(View.GONE);
            theme_lockIn_ic.setVisibility(View.VISIBLE);
            img_theme.setEnabled(false);
            theme_touch_activateBool = false;
        }

        if (view==emoji_lockIn_ic)
        {
            emoji_lockIn_ic.setVisibility(View.GONE);
            emoji_lockOut_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool)
            {
                emoji_s_1.setEnabled(true);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setEnabled(true);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setEnabled(true);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to Lock", Toast.LENGTH_SHORT).show();

            }


        }
        if (view==emoji_lockOut_ic)
        {
            emoji_lockOut_ic.setVisibility(View.GONE);
            emoji_lockIn_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool)
            {
                emoji_s_1.setEnabled(false);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setEnabled(false);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setEnabled(false);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to UnLock", Toast.LENGTH_SHORT).show();

            }
        }


        if (view == up_txt1) {
            imgBg = false;
            txt_1_bool = true;
            txt_2_bool = false;
            txt_3_bool = false;
            txt1_rl.bringToFront();

            txt_size_seek_1.setVisibility(View.VISIBLE);
            txt_size_seek_2.setVisibility(View.GONE);
            txt_size_seek_3.setVisibility(View.GONE);
        }
        if (view == up_txt2) {
            imgBg = false;
            txt_1_bool = false;
            txt_2_bool = true;
            txt_3_bool = false;
            txt2_rl.bringToFront();

            txt_size_seek_1.setVisibility(View.GONE);
            txt_size_seek_2.setVisibility(View.VISIBLE);
            txt_size_seek_3.setVisibility(View.GONE);
        }
        if (view == up_txt3) {
            imgBg = false;
            txt_1_bool = false;
            txt_2_bool = false;
            txt_3_bool = true;
            txt3_rl.bringToFront();
            txt_size_seek_1.setVisibility(View.GONE);
            txt_size_seek_2.setVisibility(View.GONE);
            txt_size_seek_3.setVisibility(View.VISIBLE);
        }
        if (view == emoji_visi_ic) {
            emoji_visi_ic.setVisibility(View.GONE);
            emoji_Invisi_ic.setVisibility(View.VISIBLE);

            if (emoji_1_bool)
            {
                emoji_s_1.setVisibility(View.GONE);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setVisibility(View.GONE);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setVisibility(View.GONE);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to Hide", Toast.LENGTH_SHORT).show();

            }

        }
        if (view == emoji_Invisi_ic) {
            emoji_visi_ic.setVisibility(View.VISIBLE);
            emoji_Invisi_ic.setVisibility(View.GONE);
            if (emoji_1_bool)
            {
                emoji_s_1.setVisibility(View.VISIBLE);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setVisibility(View.VISIBLE);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setVisibility(View.VISIBLE);
            }
            if (!emoji_1x_bool&&!emoji_2x_bool&&!emoji_3x_bool)
            {
                Toast.makeText(this, "No emoji's to Show", Toast.LENGTH_SHORT).show();

            }
        }
        if (view == editd_txt1_visi_ic) {
            editd_txt1_visi_ic.setVisibility(View.GONE);
            editd_txt1_Invisi_ic.setVisibility(View.VISIBLE);
            txt_1.setVisibility(View.VISIBLE);
        }
        if (view == editd_txt1_Invisi_ic) {
            editd_txt1_visi_ic.setVisibility(View.VISIBLE);
            editd_txt1_Invisi_ic.setVisibility(View.GONE);
            txt_1.setVisibility(View.GONE);
        }

        if (view == editd_txt2_visi_ic) {
            editd_txt2_visi_ic.setVisibility(View.GONE);
            editd_txt2_Invisi_ic.setVisibility(View.VISIBLE);
            txt_2.setVisibility(View.VISIBLE);
        }
        if (view == editd_txt2_Invisi_ic) {
            editd_txt2_visi_ic.setVisibility(View.VISIBLE);
            editd_txt2_Invisi_ic.setVisibility(View.GONE);
            txt_2.setVisibility(View.GONE);
        }

        if (view == editd_txt3_visi_ic) {
            editd_txt3_visi_ic.setVisibility(View.GONE);
            editd_txt3_Invisi_ic.setVisibility(View.VISIBLE);
            txt_3.setVisibility(View.VISIBLE);
        }
        if (view == editd_txt3_Invisi_ic) {
            editd_txt3_visi_ic.setVisibility(View.VISIBLE);
            editd_txt3_Invisi_ic.setVisibility(View.GONE);
            txt_3.setVisibility(View.GONE);
        }

        if (view == bg_multi_colo) {
            if (bg_multi_colo.getText().toString().equals("Multi Colors ?"))
            {
                grad_bg_style1_ll.setVisibility(View.VISIBLE);
                bgGrad_LL.setVisibility(View.VISIBLE);
                bg_grad_Tools_ll.setVisibility(View.VISIBLE);
                bg_GRAD_0_1 = 1;
                style_2_txt_bool = false;
                bg_multi_colo.setText("Single Color ?");
            }
            else if (bg_multi_colo.getText().toString().equals("Single Color ?"))
            {
                grad_bg_style1_ll.setVisibility(View.GONE);
                grad_bg_style2_ll.setVisibility(View.GONE);
                bgGrad_LL.setVisibility(View.GONE);
                bg_grad_Tools_ll.setVisibility(View.GONE);
                bg_GRAD_0_1 = 0;
                style_2_txt_bool = false;
                bg_multi_colo.setText("Multi Colors ?");

            }
        }

        if (view == emojiButt||view==emojiButt2) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                getEmojiDialog();
            } else {
                Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (view == bg_visi_ic) {
            bg_visi_ic.setVisibility(View.GONE);
            bg_Invisi_ic.setVisibility(View.VISIBLE);
            bg_view.setVisibility(View.GONE);
        }
        if (view == bg_Invisi_ic) {
            bg_visi_ic.setVisibility(View.VISIBLE);
            bg_Invisi_ic.setVisibility(View.GONE);
            bg_view.setVisibility(View.VISIBLE);
        }
        if (view == theme_visi_ic) {
            theme_visi_ic.setVisibility(View.GONE);
            theme_Invisi_ic.setVisibility(View.VISIBLE);
            img_theme.setVisibility(View.GONE);
        }
        if (view == theme_Invisi_ic) {
            theme_visi_ic.setVisibility(View.VISIBLE);
            theme_Invisi_ic.setVisibility(View.GONE);
            img_theme.setVisibility(View.VISIBLE);
        }
        if (view == photo_visi_ic) {
            photo_visi_ic.setVisibility(View.GONE);
            photo_Invisi_ic.setVisibility(View.VISIBLE);
            img_user.setVisibility(View.GONE);
        }
        if (view == photo_Invisi_ic) {
            photo_visi_ic.setVisibility(View.VISIBLE);
            photo_Invisi_ic.setVisibility(View.GONE);
            img_user.setVisibility(View.VISIBLE);
        }

        if (view == txt_visi_ic) {
            txt_Invisi_ic.setVisibility(View.VISIBLE);
            txt_visi_ic.setVisibility(View.GONE);
            txt_1.setVisibility(View.GONE);
            txt_2.setVisibility(View.GONE);
            txt_3.setVisibility(View.GONE);
        }
        if (view == txt_Invisi_ic) {
            txt_Invisi_ic.setVisibility(View.GONE);
            txt_visi_ic.setVisibility(View.VISIBLE);
            txt_1.setVisibility(View.VISIBLE);
            txt_2.setVisibility(View.VISIBLE);
            txt_3.setVisibility(View.VISIBLE);
        }


        if (double_colo_txt == view) {
            if (double_colo_txt.getText().toString().trim().equals("Double Colors ?")) {
                txt_grad_rad();

                double_colo_txt.setText("Single Color ?");
                s_g_bool = true;
                grad_radd_ll.setVisibility(View.VISIBLE);
                grad_seek_ll.setVisibility(View.VISIBLE);
                grad_txt_ll.setVisibility(View.VISIBLE);

                if (txt_1_bool) {
                    txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    txt_1.getPaint().setShader(shade_grad_title);
                } else if (txt_2_bool) {
                    txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    txt_2.getPaint().setShader(shade_grad_p1);
                } else if (txt_3_bool) {
                    txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    txt_3.getPaint().setShader(shade_grad_p2);
                } else {
                    Toast.makeText(this, "Select Text", Toast.LENGTH_LONG).show();
                }
            } else if (double_colo_txt.getText().toString().trim().equals("Single Color ?")) {
                txt_grad_solid();

                double_colo_txt.setText("Double Colors ?");
                s_g_bool = false;
                double_colo_txt.setVisibility(View.VISIBLE);
                grad_seek_ll.setVisibility(View.GONE);
                grad_radd_ll.setVisibility(View.GONE);
                grad_txt_ll.setVisibility(View.GONE);
                colors_ll.setVisibility(View.VISIBLE);


                if (txt_1_bool) {
                    txt_1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    txt_1.getPaint().setShader(shade_s_title);
                } else if (txt_2_bool) {
                    txt_2.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    txt_2.getPaint().setShader(shade_s_p1);
                } else if (txt_3_bool) {
                    txt_3.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    txt_3.getPaint().setShader(shade_s_p2);
                } else {
                    Toast.makeText(this, "Select Text", Toast.LENGTH_LONG).show();
                }
            }

        }

        if (img_grid_butt == view) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                getImgGridDialog();
            } else {
                Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (txt_fontButt == view) {
            font_ll.setVisibility(View.VISIBLE);
            text_size_adj_LL.setVisibility(View.GONE);
            txt_color_tools_LL.setVisibility(View.GONE);
            choose_txt_bg_color_Rgroup.setVisibility(View.GONE);
            shadow_LL.setVisibility(View.GONE);
            colors_ll.setVisibility(View.GONE);
            bg_color_tool.setVisibility(View.GONE);
            image_edit_tools.setVisibility(View.GONE);
            bg_grad_selectors_LL.setVisibility(View.GONE);

            txt_styleButt_bool = false;
            txt_colorsButt_bool = false;
            txt_shadowButt_bool = false;
            setBgLL_bool = false;
            img_edt_Butt_bool = false;
            style_2_txt_bool = false;
            emojiButt2.setVisibility(View.GONE);
            txt_Grad_tool.setVisibility(View.GONE);
            color_line.setBackgroundResource(R.color.blue_grey_p4);
            color_line_v.setBackgroundResource(R.color.blue_grey_p4);
            shadow_line.setBackgroundResource(R.color.blue_grey_p4);
            shadow_line_v.setBackgroundResource(R.color.blue_grey_p4);
            font_line.setBackgroundResource(R.color.light_blue);
            font_line_v.setBackgroundResource(R.color.light_blue);
            t_line.setBackgroundResource(R.color.blue_grey_p4);
            t_line_v.setBackgroundResource(R.color.blue_grey_p4);
            t_txt.setTextColor(getColor(R.color.white));
            font_txt.setTextColor(getColor(R.color.light_blue));
            color_txt.setTextColor(getColor(R.color.white));
            shadow_txt.setTextColor(getColor(R.color.white));
        }


        if (txt_colorsButt == view) {
            text_size_adj_LL.setVisibility(View.GONE);
            font_ll.setVisibility(View.GONE);
            shadow_LL.setVisibility(View.GONE);
            choose_txt_bg_color_Rgroup.setVisibility(View.VISIBLE);
            txt_color_tools_LL.setVisibility(View.VISIBLE);
            colors_ll.setVisibility(View.VISIBLE);
            txt_Grad_tool.setVisibility(View.VISIBLE);
            bg_color_tool.setVisibility(View.GONE);
            image_edit_tools.setVisibility(View.GONE);
            emojiButt2.setVisibility(View.GONE);
            bg_grad_selectors_LL.setVisibility(View.GONE);

            txt_styleButt_bool = false;
            txt_shadowButt_bool = false;
            txt_colorsButt_bool = true;
            setBgLL_bool = false;
            img_edt_Butt_bool = false;
            style_2_txt_bool = false;
            color_line.setBackgroundResource(R.color.light_blue);
            color_line_v.setBackgroundResource(R.color.light_blue);
            font_line.setBackgroundResource(R.color.blue_grey_p4);
            font_line_v.setBackgroundResource(R.color.blue_grey_p4);
            t_line.setBackgroundResource(R.color.blue_grey_p4);
            t_line_v.setBackgroundResource(R.color.blue_grey_p4);
            shadow_line.setBackgroundResource(R.color.blue_grey_p4);
            shadow_line_v.setBackgroundResource(R.color.blue_grey_p4);
            t_txt.setTextColor(getColor(R.color.white));
            font_txt.setTextColor(getColor(R.color.white));
            color_txt.setTextColor(getColor(R.color.light_blue));
            shadow_txt.setTextColor(getColor(R.color.white));
        }

        if (txt_shadowButt == view) {
            text_size_adj_LL.setVisibility(View.GONE);
            txt_color_tools_LL.setVisibility(View.GONE);
            font_ll.setVisibility(View.GONE);
            choose_txt_bg_color_Rgroup.setVisibility(View.GONE);
            shadow_LL.setVisibility(View.VISIBLE);
            colors_ll.setVisibility(View.GONE);
            emojiButt2.setVisibility(View.GONE);
            txt_Grad_tool.setVisibility(View.GONE);
            bg_grad_selectors_LL.setVisibility(View.GONE);

            bg_color_tool.setVisibility(View.GONE);
            image_edit_tools.setVisibility(View.GONE);
            txt_styleButt_bool = false;
            txt_colorsButt_bool = false;
            txt_shadowButt_bool = true;
            setBgLL_bool = false;
            img_edt_Butt_bool = false;
            style_2_txt_bool = false;
            color_line.setBackgroundResource(R.color.blue_grey_p4);
            color_line_v.setBackgroundResource(R.color.blue_grey_p4);
            shadow_line.setBackgroundResource(R.color.light_blue);
            shadow_line_v.setBackgroundResource(R.color.light_blue);
            font_line.setBackgroundResource(R.color.blue_grey_p4);
            font_line_v.setBackgroundResource(R.color.blue_grey_p4);
            t_line.setBackgroundResource(R.color.blue_grey_p4);
            t_line_v.setBackgroundResource(R.color.blue_grey_p4);
            t_txt.setTextColor(getColor(R.color.white));
            font_txt.setTextColor(getColor(R.color.white));
            color_txt.setTextColor(getColor(R.color.white));
            shadow_txt.setTextColor(getColor(R.color.light_blue));

        }
        if (t_butt == view) {
            text_size_adj_LL.setVisibility(View.VISIBLE);
            font_ll.setVisibility(View.GONE);
            txt_color_tools_LL.setVisibility(View.GONE);
            shadow_LL.setVisibility(View.GONE);
            colors_ll.setVisibility(View.GONE);
            choose_txt_bg_color_Rgroup.setVisibility(View.GONE);
            bg_color_tool.setVisibility(View.GONE);
            image_edit_tools.setVisibility(View.GONE);
            emojiButt2.setVisibility(View.GONE);
            txt_Grad_tool.setVisibility(View.GONE);
            bg_grad_selectors_LL.setVisibility(View.GONE);

            txt_styleButt_bool = true;
            txt_colorsButt_bool = false;
            setBgLL_bool = false;
            img_edt_Butt_bool = false;
            style_2_txt_bool = false;
            color_line.setBackgroundResource(R.color.blue_grey_p4);
            color_line_v.setBackgroundResource(R.color.blue_grey_p4);
            font_line.setBackgroundResource(R.color.blue_grey_p4);
            font_line_v.setBackgroundResource(R.color.blue_grey_p4);
            t_line.setBackgroundResource(R.color.light_blue);
            t_line_v.setBackgroundResource(R.color.light_blue);
            shadow_line.setBackgroundResource(R.color.blue_grey_p4);
            shadow_line_v.setBackgroundResource(R.color.blue_grey_p4);
            t_txt.setTextColor(getColor(R.color.light_blue));
            font_txt.setTextColor(getColor(R.color.white));
            color_txt.setTextColor(getColor(R.color.white));
            shadow_txt.setTextColor(getColor(R.color.white));
        }


        if (bg_colorsButt == view) {
            setBgLL();
        }
        if (theme_edt_Butt == view) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                setThemeLL();
            }
            else {
                Toast.makeText(this, "Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (photo_gallery_Butt == view) {
            setPhotoLL();
        }

        if (view == textEditButt) {
            getTextsDialog();
        }

        if (txt_styleButt == view) {
            setText_LL();
        }

        if (emoji_edt_Butt == view) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                setEmoji_LL();
            }
            else {
                Toast.makeText(this, "Connect to Internet", Toast.LENGTH_SHORT).show();
            }
        }

        if (view == style_1_Butt) {
            style1_LL.setVisibility(View.VISIBLE);
            style2_LL.setVisibility(View.GONE);
            grad_bg_style2_ll.setVisibility(View.GONE);
            style_1_Butt_line.setBackgroundResource(R.color.light_blue);
            style_2_Butt_line.setBackgroundResource(R.color.blue_grey_p4);
            style_2_txt_bool = false;
            style1_txt.setTextColor(getColor(R.color.light_blue));
            style2_txt.setTextColor(getColor(R.color.white));
            grad_bg_style1_ll.setVisibility(View.VISIBLE);

        }
        if (view == style_2_Butt) {
            grad_bg_style2_ll.setVisibility(View.VISIBLE);
            style2_LL.setVisibility(View.VISIBLE);
            style1_LL.setVisibility(View.GONE);
            style_2_Butt_line.setBackgroundResource(R.color.light_blue);
            style_1_Butt_line.setBackgroundResource(R.color.blue_grey_p4);
            style_2_txt_bool = true;
            style1_txt.setTextColor(getColor(R.color.white));
            style2_txt.setTextColor(getColor(R.color.light_blue));
            grad_bg_style1_ll.setVisibility(View.GONE);
        }


        if (view == upload_photo) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
        }


        if (view == unlockAll_butt) {
            txt_1.setEnabled(true);
            txt_2.setEnabled(true);
            txt_3.setEnabled(true);
            txt1_rl.setEnabled(true);
            txt2_rl.setEnabled(true);
            txt3_rl.setEnabled(true);
            img_user.setEnabled(true);
            if (emoji_1_bool)
            {
                emoji_s_1.setEnabled(true);
            }
            if (emoji_2_bool)
            {
                emoji_s_2.setEnabled(true);
            }
            if (emoji_3_bool)
            {
                emoji_s_3.setEnabled(true);
            }
            txt_lockIn_ic.setVisibility(View.GONE);
            txt_lockOut_ic.setVisibility(View.VISIBLE);
            photo_lockIn_ic.setVisibility(View.GONE);
            photo_lockOut_ic.setVisibility(View.VISIBLE);
            theme_lockIn_ic.setVisibility(View.GONE);
            theme_lockOut_ic.setVisibility(View.VISIBLE);
            emoji_lockIn_ic.setVisibility(View.GONE);
            emoji_lockOut_ic.setVisibility(View.VISIBLE);
            unlockAll_butt.setVisibility(View.GONE);

        }

        if (view == ss1) {
            if (txt_1_bool) {
            } else if (txt_2_bool) {
            } else if (txt_3_bool) {
            }
        }
        if (view == ss2) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_geo);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_geo);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_geo);
            }
        }
        if (view == ss4) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_homoark);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_homoark);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_homoark);
            }
        }

        if (view == se1) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_blkchcry);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_blkchcry);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_blkchcry);
            }
        }
        if (view == se2) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_octin);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_octin);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_octin);
            }
        }

        if (view == sc3) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_deftone);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_deftone);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_deftone);
            }
        }
        if (view == sc4) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_clementine);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_clementine);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_clementine);
            }
        }
        if (view == sc5) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_otto);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_otto);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_otto);
            }
        }
        if (view == sc6) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_peterbuilt);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_peterbuilt);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_peterbuilt);
            }
        }
        if (view == sc7) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_precious);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_precious);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_precious);
            }
        }

        if (view == sc9) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_stilltime);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_stilltime);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_stilltime);
            }
        }


        if (view == d1) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_burnstown);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_burnstown);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_burnstown);
            }
        }
        if (view == d2) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_chlorinr);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_chlorinr);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_chlorinr);
            }
        }
        if (view == d3) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_earwig);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_earwig);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_earwig);
            }
        }
        if (view == d4) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_gotheroin);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_gotheroin);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_gotheroin);
            }
        }
        if (view == d5) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_musicals);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_musicals);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_musicals);
            }
        }
        if (view == d6) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_punk);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_punk);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_punk);
            }
        }
        if (view == d7) {
            if (txt_1_bool) {
                txt_1.setTypeface(tf_zombie);
            } else if (txt_2_bool) {
                txt_2.setTypeface(tf_zombie);
            } else if (txt_3_bool) {
                txt_3.setTypeface(tf_zombie);
            }
        }


        if (view == flip_right) {
            if (theme_edt_Butt_bool) {
                if (img_theme.getScaleX() == 1) {
                    img_theme.setScaleX(-1);
                } else {
                    img_theme.setScaleX(1);
                }
            } else if (photo_bool) {
                if (img_user.getScaleX() == 1) {
                    img_user.setScaleX(-1);
                } else {
                    img_user.setScaleX(1);
                }
            }

        }

        if (view == flip_down) {
            if (theme_edt_Butt_bool) {
                if (img_theme.getScaleY() == 1) {
                    img_theme.setScaleY(-1);
                } else {
                    img_theme.setScaleY(1);
                }
            } else if (photo_bool) {
                if (img_user.getScaleY() == 1) {
                    img_user.setScaleY(-1);
                } else {
                    img_user.setScaleY(1);
                }
            }

        }


        if (view == red_c) {
            c1.setBackgroundResource(R.color.red_p1);
            c2.setBackgroundResource(R.color.red_p2);
            c3.setBackgroundResource(R.color.red_p3);
            c4.setBackgroundResource(R.color.red_p4);
            c5.setBackgroundResource(R.color.red_p5);
            c6.setBackgroundResource(R.color.red_p6);
            c7.setBackgroundResource(R.color.red_p7);
            c8.setBackgroundResource(R.color.red_p8);
            c9.setBackgroundResource(R.color.red_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == purple_c) {
            c1.setBackgroundResource(R.color.purple_p1);
            c2.setBackgroundResource(R.color.purple_p2);
            c3.setBackgroundResource(R.color.purple_p3);
            c4.setBackgroundResource(R.color.purple_p4);
            c5.setBackgroundResource(R.color.purple_p5);
            c6.setBackgroundResource(R.color.purple_p6);
            c7.setBackgroundResource(R.color.purple_p7);
            c8.setBackgroundResource(R.color.purple_p8);
            c9.setBackgroundResource(R.color.purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == pink_c) {
            c1.setBackgroundResource(R.color.pink_p1);
            c2.setBackgroundResource(R.color.pink_p2);
            c3.setBackgroundResource(R.color.pink_p3);
            c4.setBackgroundResource(R.color.pink_p4);
            c5.setBackgroundResource(R.color.pink_p5);
            c6.setBackgroundResource(R.color.pink_p6);
            c7.setBackgroundResource(R.color.pink_p7);
            c8.setBackgroundResource(R.color.pink_p8);
            c9.setBackgroundResource(R.color.pink_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == deepPurple_c) {
            c1.setBackgroundResource(R.color.deep_purple_p1);
            c2.setBackgroundResource(R.color.deep_purple_p2);
            c3.setBackgroundResource(R.color.deep_purple_p3);
            c4.setBackgroundResource(R.color.deep_purple_p4);
            c5.setBackgroundResource(R.color.deep_purple_p5);
            c6.setBackgroundResource(R.color.deep_purple_p6);
            c7.setBackgroundResource(R.color.deep_purple_p7);
            c8.setBackgroundResource(R.color.deep_purple_p8);
            c9.setBackgroundResource(R.color.deep_purple_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == indigo_c) {
            c1.setBackgroundResource(R.color.indigo_p1);
            c2.setBackgroundResource(R.color.indigo_p2);
            c3.setBackgroundResource(R.color.indigo_p3);
            c4.setBackgroundResource(R.color.indigo_p4);
            c5.setBackgroundResource(R.color.indigo_p5);
            c6.setBackgroundResource(R.color.indigo_p6);
            c7.setBackgroundResource(R.color.indigo_p7);
            c8.setBackgroundResource(R.color.indigo_p8);
            c9.setBackgroundResource(R.color.indigo_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == blue_c) {
            c1.setBackgroundResource(R.color.blue_p1);
            c2.setBackgroundResource(R.color.blue_p2);
            c3.setBackgroundResource(R.color.blue_p3);
            c4.setBackgroundResource(R.color.blue_p4);
            c5.setBackgroundResource(R.color.blue_p5);
            c6.setBackgroundResource(R.color.blue_p6);
            c7.setBackgroundResource(R.color.blue_p7);
            c8.setBackgroundResource(R.color.blue_p8);
            c9.setBackgroundResource(R.color.blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == cyan_c) {
            c1.setBackgroundResource(R.color.cyan_p1);
            c2.setBackgroundResource(R.color.cyan_p2);
            c3.setBackgroundResource(R.color.cyan_p3);
            c4.setBackgroundResource(R.color.cyan_p4);
            c5.setBackgroundResource(R.color.cyan_p5);
            c6.setBackgroundResource(R.color.cyan_p6);
            c7.setBackgroundResource(R.color.cyan_p7);
            c8.setBackgroundResource(R.color.cyan_p8);
            c9.setBackgroundResource(R.color.cyan_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lightBlue_c) {
            c1.setBackgroundResource(R.color.light_blue_p1);
            c2.setBackgroundResource(R.color.light_blue_p2);
            c3.setBackgroundResource(R.color.light_blue_p3);
            c4.setBackgroundResource(R.color.light_blue_p4);
            c5.setBackgroundResource(R.color.light_blue_p5);
            c6.setBackgroundResource(R.color.light_blue_p6);
            c7.setBackgroundResource(R.color.light_blue_p7);
            c8.setBackgroundResource(R.color.light_blue_p8);
            c9.setBackgroundResource(R.color.light_blue_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == teal_c) {
            c1.setBackgroundResource(R.color.teal_p1);
            c2.setBackgroundResource(R.color.teal_p2);
            c3.setBackgroundResource(R.color.teal_p3);
            c4.setBackgroundResource(R.color.teal_p4);
            c5.setBackgroundResource(R.color.teal_p5);
            c6.setBackgroundResource(R.color.teal_p6);
            c7.setBackgroundResource(R.color.teal_p7);
            c8.setBackgroundResource(R.color.teal_p8);
            c9.setBackgroundResource(R.color.teal_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == green_c) {
            c1.setBackgroundResource(R.color.green_p1);
            c2.setBackgroundResource(R.color.green_p2);
            c3.setBackgroundResource(R.color.green_p3);
            c4.setBackgroundResource(R.color.green_p4);
            c5.setBackgroundResource(R.color.green_p5);
            c6.setBackgroundResource(R.color.green_p6);
            c7.setBackgroundResource(R.color.green_p7);
            c8.setBackgroundResource(R.color.green_p8);
            c9.setBackgroundResource(R.color.green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lightGreen_c) {
            c1.setBackgroundResource(R.color.light_green_p1);
            c2.setBackgroundResource(R.color.light_green_p2);
            c3.setBackgroundResource(R.color.light_green_p3);
            c4.setBackgroundResource(R.color.light_green_p4);
            c5.setBackgroundResource(R.color.light_green_p5);
            c6.setBackgroundResource(R.color.light_green_p6);
            c7.setBackgroundResource(R.color.light_green_p7);
            c8.setBackgroundResource(R.color.light_green_p8);
            c9.setBackgroundResource(R.color.light_green_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == lime_c) {
            c1.setBackgroundResource(R.color.lime_p1);
            c2.setBackgroundResource(R.color.lime_p2);
            c3.setBackgroundResource(R.color.lime_p3);
            c4.setBackgroundResource(R.color.lime_p4);
            c5.setBackgroundResource(R.color.lime_p5);
            c6.setBackgroundResource(R.color.lime_p6);
            c7.setBackgroundResource(R.color.lime_p7);
            c8.setBackgroundResource(R.color.lime_p8);
            c9.setBackgroundResource(R.color.lime_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == yellow_c) {
            c1.setBackgroundResource(R.color.yellow_p1);
            c2.setBackgroundResource(R.color.yellow_p2);
            c3.setBackgroundResource(R.color.yellow_p3);
            c4.setBackgroundResource(R.color.yellow_p4);
            c5.setBackgroundResource(R.color.yellow_p5);
            c6.setBackgroundResource(R.color.yellow_p6);
            c7.setBackgroundResource(R.color.yellow_p7);
            c8.setBackgroundResource(R.color.yellow_p8);
            c9.setBackgroundResource(R.color.yellow_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == amber_c) {
            c1.setBackgroundResource(R.color.amber_p1);
            c2.setBackgroundResource(R.color.amber_p2);
            c3.setBackgroundResource(R.color.amber_p3);
            c4.setBackgroundResource(R.color.amber_p4);
            c5.setBackgroundResource(R.color.amber_p5);
            c6.setBackgroundResource(R.color.amber_p6);
            c7.setBackgroundResource(R.color.amber_p7);
            c8.setBackgroundResource(R.color.amber_p8);
            c9.setBackgroundResource(R.color.amber_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == orange_c) {
            c1.setBackgroundResource(R.color.orange_p1);
            c2.setBackgroundResource(R.color.orange_p2);
            c3.setBackgroundResource(R.color.orange_p3);
            c4.setBackgroundResource(R.color.orange_p4);
            c5.setBackgroundResource(R.color.orange_p5);
            c6.setBackgroundResource(R.color.orange_p6);
            c7.setBackgroundResource(R.color.orange_p7);
            c8.setBackgroundResource(R.color.orange_p8);
            c9.setBackgroundResource(R.color.orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == deepOrange_c) {
            c1.setBackgroundResource(R.color.dep_orange_p1);
            c2.setBackgroundResource(R.color.dep_orange_p2);
            c3.setBackgroundResource(R.color.dep_orange_p3);
            c4.setBackgroundResource(R.color.dep_orange_p4);
            c5.setBackgroundResource(R.color.dep_orange_p5);
            c6.setBackgroundResource(R.color.dep_orange_p6);
            c7.setBackgroundResource(R.color.dep_orange_p7);
            c8.setBackgroundResource(R.color.dep_orange_p8);
            c9.setBackgroundResource(R.color.dep_orange_p9);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.VISIBLE);
        }
        if (view == brown_c) {
            c1.setBackgroundResource(R.color.brown_p1);
            c2.setBackgroundResource(R.color.brown_p2);
            c3.setBackgroundResource(R.color.brown_p3);
            c4.setBackgroundResource(R.color.brown_p4);
            c5.setBackgroundResource(R.color.brown_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }
        if (view == grey_c) {
            c1.setBackgroundResource(R.color.grey_p1);
            c2.setBackgroundResource(R.color.grey_p2);
            c3.setBackgroundResource(R.color.grey_p3);
            c4.setBackgroundResource(R.color.grey_p4);
            c5.setBackgroundResource(R.color.grey_p5);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }
        if (view == blueGrey_c) {
            c1.setBackgroundResource(R.color.blue_grey_p1);
            c2.setBackgroundResource(R.color.blue_grey_p2);
            c3.setBackgroundResource(R.color.blue_grey_p3);
            c4.setBackgroundResource(R.color.blue_grey_p4);
            c5.setBackgroundResource(R.color.blue_grey_p4);
            ll1.setVisibility(View.VISIBLE);
            ll2.setVisibility(View.GONE);
        }

        if (view == c0) {
            getViewColo = 0;
            getGradText();
            if (txt_1_bool)
            {
                txt1_bg_color_check_bool = false;
            } else if (txt_2_bool)
            {
                txt2_bg_color_check_bool = false;
            } else if (txt_3_bool)
            {
                txt3_bg_color_check_bool = false;
            }
        }
        if (view == black_c) {
            ll1.setVisibility(View.GONE);
            ColorDrawable cd = (ColorDrawable) black_c.getBackground();
            getViewColo = cd.getColor();

            Log.d("XDCVFC", String.valueOf(getViewColo) );

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }
        if (view == white_c) {
            ll1.setVisibility(View.GONE);
            ColorDrawable cd = (ColorDrawable) white_c.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == c1) {
            ColorDrawable cd = (ColorDrawable) c1.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == c2) {
            ColorDrawable cd = (ColorDrawable) c2.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == c3) {
            ColorDrawable cd = (ColorDrawable) c3.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == c4) {
            ColorDrawable cd = (ColorDrawable) c4.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == c5) {
            ColorDrawable cd = (ColorDrawable) c5.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == c6) {
            ColorDrawable cd = (ColorDrawable) c6.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == c7) {
            ColorDrawable cd = (ColorDrawable) c7.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == c8) {
            ColorDrawable cd = (ColorDrawable) c8.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == c9) {
            ColorDrawable cd = (ColorDrawable) c9.getBackground();
            getViewColo = cd.getColor();

            getGradText();
            getGradBg_Style_1();
            getGradBg_Style_2();
            txtShadow();
            getLogoColor();
        }


        if (view == linear_gd_po_T) {
            gd.setOrientation(GradientDrawable.Orientation.TOP_BOTTOM);
        } else if (view == linear_gd_po_TR) {
            gd.setOrientation(GradientDrawable.Orientation.TR_BL);
        } else if (view == linear_gd_po_R) {
            gd.setOrientation(GradientDrawable.Orientation.RIGHT_LEFT);
        } else if (view == linear_gd_po_BR) {
            gd.setOrientation(GradientDrawable.Orientation.BR_TL);
        } else if (view == linear_gd_po_B) {
            gd.setOrientation(GradientDrawable.Orientation.BOTTOM_TOP);
        } else if (view == linear_gd_po_BL) {
            gd.setOrientation(GradientDrawable.Orientation.BL_TR);
        } else if (view == linear_gd_po_L) {
            gd.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
        } else if (view == linear_gd_po_TL) {
            gd.setOrientation(GradientDrawable.Orientation.TL_BR);
        }


        if (view == outputButt) {
   /*         Intent iOutP = new Intent(this, EditOutput.class);
            Bitmap bitmap = Bitmap.createBitmap(rL.getHeight(), rL.getWidth(),
                    Bitmap.Config.ARGB_8888);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream );
            byte[] b = byteArrayOutputStream .toByteArray();
            iOutP.putExtra("OP_BITMAP", b);
            startActivity(iOutP);*/

            outputDialog();
        }
    }

    private void getImgGridDialog() {
        dialog_imgGRID.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_imgGRID.setContentView(R.layout.img_grid_dialog);
        dialog_imgGRID.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog_imgGRID.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

        grid_titles_recycler = (RecyclerView) dialog_imgGRID.findViewById(R.id.grid_titles_recycler);
        texteoFestivalTitleAdapter = new TexteoFestivalTitleAdapter(festTitleModel, Texteo.this, R.layout.row_festival_titles);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        grid_titles_recycler.setLayoutManager(linearLayoutManager);

        grid_img_recycler = (RecyclerView) dialog_imgGRID.findViewById(R.id.grid_img_recycler);
        texteoImagesHoriAdapter = new TexteoImagesHoriAdapter(imagesHoriModel, Texteo.this, R.layout.row_images_hori);
        gridLayoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        grid_img_recycler.setLayoutManager(gridLayoutManager);

        grid_titles_recycler.setAdapter(texteoFestivalTitleAdapter);
        dialog_imgGRID.show();
    }

    private void setEmoji_LL() {
        bg_tool_ll.setBackgroundResource(R.color.transparent);
        theme_tool_ll.setBackgroundResource(R.color.transparent);
        logo_tool_ll.setBackgroundResource(R.color.transparent);
        emoji_tool_ll.setBackgroundResource(R.color.dark_blue);
        txt_tool_ll.setBackgroundResource(R.color.transparent);

        txt_styleButt.setTextColor(getResources().getColor(R.color.white));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.white));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_gallery_Butt.setTextColor(getResources().getColor(R.color.white));
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.light_blue));
        emojiButt2.setVisibility(View.VISIBLE);

        colors_ll.setVisibility(View.INVISIBLE);
        c0.setVisibility(View.GONE);
        bg_grad_selectors_LL.setVisibility(View.GONE);
        choose_txt_bg_color_Rgroup.setVisibility(View.GONE);
        txt_Grad_tool.setVisibility(View.GONE);
        text_size_adj_LL.setVisibility(View.GONE);
        shadow_LL.setVisibility(View.GONE);
        font_ll.setVisibility(View.GONE);
        txt_color_tools_LL.setVisibility(View.GONE);
        bg_color_tool.setVisibility(View.GONE);
        image_edit_tools.setVisibility(View.GONE);
        txt_sub_tabs.setVisibility(View.GONE);
        txt_fontButt.setVisibility(View.GONE);
        txt_styleButt_bool = false;
        txt_colorsButt_bool = false;
        setBgLL_bool = false;
        img_edt_Butt_bool = false;
        style_2_txt_bool = false;
        txt_1_bool = false;
        txt_2_bool = false;
        txt_3_bool = false;
        theme_edt_Butt_bool = false;
        emoji_bool = true;

    }

    private void setText_LL() {
        bg_tool_ll.setBackgroundResource(R.color.transparent);
        theme_tool_ll.setBackgroundResource(R.color.transparent);
        logo_tool_ll.setBackgroundResource(R.color.transparent);
        emoji_tool_ll.setBackgroundResource(R.color.transparent);
        txt_tool_ll.setBackgroundResource(R.color.dark_blue);
        emojiButt2.setVisibility(View.GONE);
        txt_Grad_tool.setVisibility(View.GONE);

        c0.setVisibility(View.VISIBLE);
        text_size_adj_LL.setVisibility(View.VISIBLE);
        bg_grad_selectors_LL.setVisibility(View.GONE);
        shadow_LL.setVisibility(View.GONE);
        font_ll.setVisibility(View.GONE);
        txt_color_tools_LL.setVisibility(View.GONE);
        bg_color_tool.setVisibility(View.GONE);
        image_edit_tools.setVisibility(View.GONE);
        colors_ll.setVisibility(View.GONE);
        txt_sub_tabs.setVisibility(View.VISIBLE);
        txt_fontButt.setVisibility(View.VISIBLE);
        txt_styleButt_bool = true;
        txt_colorsButt_bool = false;
        txt_shadowButt_bool = false;
        setBgLL_bool = false;
        img_edt_Butt_bool = false;
        style_2_txt_bool = false;
        txt_styleButt.setTextColor(getResources().getColor(R.color.light_blue));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.white));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_gallery_Butt.setTextColor(getResources().getColor(R.color.white));
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));


        color_line.setBackgroundResource(R.color.blue_grey_p4);
        color_line_v.setBackgroundResource(R.color.blue_grey_p4);
        shadow_line.setBackgroundResource(R.color.blue_grey_p4);
        shadow_line_v.setBackgroundResource(R.color.blue_grey_p4);
        font_line.setBackgroundResource(R.color.blue_grey_p4);
        font_line_v.setBackgroundResource(R.color.blue_grey_p4);
        t_line.setBackgroundResource(R.color.light_blue);
        t_line_v.setBackgroundResource(R.color.light_blue);
        t_txt.setTextColor(getResources().getColor(R.color.light_blue));
        font_txt.setTextColor(getResources().getColor(R.color.white));
        color_txt.setTextColor(getResources().getColor(R.color.white));
        shadow_txt.setTextColor(getResources().getColor(R.color.white));
        theme_edt_Butt_bool = false;
        llBg_bool = true;
        photo_bool = false;
        emoji_bool = false;

    }

    private void setBgLL() {
        bg_tool_ll.setBackgroundResource(R.color.dark_blue);
        theme_tool_ll.setBackgroundResource(R.color.transparent);
        logo_tool_ll.setBackgroundResource(R.color.transparent);
        txt_tool_ll.setBackgroundResource(R.color.transparent);
        emoji_tool_ll.setBackgroundResource(R.color.transparent);

        txt_styleButt.setTextColor(getResources().getColor(R.color.white));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.light_blue));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_gallery_Butt.setTextColor(getResources().getColor(R.color.white));
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));

        bg_grad_selectors_LL.setVisibility(View.VISIBLE);
        c0.setVisibility(View.GONE);
        txt_Grad_tool.setVisibility(View.GONE);
        emojiButt2.setVisibility(View.GONE);
        choose_txt_bg_color_Rgroup.setVisibility(View.GONE);
        grad_txt_ll.setVisibility(View.GONE);
        text_size_adj_LL.setVisibility(View.GONE);
        txt_color_tools_LL.setVisibility(View.GONE);
        shadow_LL.setVisibility(View.GONE);
        font_ll.setVisibility(View.GONE);
        txt_sub_tabs.setVisibility(View.GONE);
        txt_fontButt.setVisibility(View.GONE);
        bg_color_tool.setVisibility(View.VISIBLE);
        colors_ll.setVisibility(View.VISIBLE);
        image_edit_tools.setVisibility(View.GONE);
        txt_styleButt_bool = false;
        txt_colorsButt_bool = false;
        setBgLL_bool = true;
        img_edt_Butt_bool = false;
        style_2_txt_bool = false;
        txt_1_bool = false;
        txt_2_bool = false;
        txt_3_bool = false;
        theme_edt_Butt_bool = false;
        llBg_bool = true;
        photo_bool = false;
        emoji_bool = false;
    }

    private void setPhotoLL() {
        bg_tool_ll.setBackgroundResource(R.color.transparent);
        theme_tool_ll.setBackgroundResource(R.color.transparent);
        logo_tool_ll.setBackgroundResource(R.color.dark_blue);
        txt_tool_ll.setBackgroundResource(R.color.transparent);
        emoji_tool_ll.setBackgroundResource(R.color.transparent);


        txt_styleButt.setTextColor(getResources().getColor(R.color.white));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.white));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        photo_gallery_Butt.setTextColor(getResources().getColor(R.color.light_blue));
        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        emojiButt2.setVisibility(View.GONE);

        bg_grad_selectors_LL.setVisibility(View.GONE);
        txt_Grad_tool.setVisibility(View.GONE);
        choose_txt_bg_color_Rgroup.setVisibility(View.GONE);
        text_size_adj_LL.setVisibility(View.GONE);
        shadow_LL.setVisibility(View.GONE);
        font_ll.setVisibility(View.GONE);
        txt_color_tools_LL.setVisibility(View.GONE);
        bg_color_tool.setVisibility(View.GONE);
        colors_ll.setVisibility(View.GONE);
        image_edit_tools.setVisibility(View.VISIBLE);
        txt_sub_tabs.setVisibility(View.GONE);
        txt_fontButt.setVisibility(View.GONE);
        txt_styleButt_bool = false;
        txt_colorsButt_bool = false;
        setBgLL_bool = false;
        img_edt_Butt_bool = true;
        style_2_txt_bool = false;
        upload_photo.setVisibility(View.VISIBLE);
        txt_1_bool = false;
        txt_2_bool = false;
        txt_3_bool = false;
        theme_edt_Butt_bool = false;
        llBg_bool = false;
        photo_bool = true;
        emoji_bool = false;

        bWSeek.setProgress(bw_p_photo_save);
        contrast.setProgress(contrast_p_photo_save);
        opacity.setProgress(opacity_p_photo_save);

        if (photo_toolDisplay_bool) {
            ll_img_tool.setVisibility(View.VISIBLE);
        } else {
            ll_img_tool.setVisibility(View.GONE);
        }
    }

    private void setThemeLL() {
        bg_tool_ll.setBackgroundResource(R.color.transparent);
        theme_tool_ll.setBackgroundResource(R.color.dark_blue);
        logo_tool_ll.setBackgroundResource(R.color.transparent);
        txt_tool_ll.setBackgroundResource(R.color.transparent);
        emoji_tool_ll.setBackgroundResource(R.color.transparent);

        emoji_edt_Butt.setTextColor(getResources().getColor(R.color.white));
        txt_styleButt.setTextColor(getResources().getColor(R.color.white));
        bg_colorsButt.setTextColor(getResources().getColor(R.color.white));
        theme_edt_Butt.setTextColor(getResources().getColor(R.color.light_blue));
        photo_gallery_Butt.setTextColor(getResources().getColor(R.color.white));
        choose_txt_bg_color_Rgroup.setVisibility(View.GONE);
        text_size_adj_LL.setVisibility(View.GONE);
        emojiButt2.setVisibility(View.GONE);
        bg_grad_selectors_LL.setVisibility(View.GONE);

        txt_Grad_tool.setVisibility(View.GONE);
        shadow_LL.setVisibility(View.GONE);
        font_ll.setVisibility(View.GONE);
        txt_color_tools_LL.setVisibility(View.GONE);
        bg_color_tool.setVisibility(View.GONE);
        colors_ll.setVisibility(View.GONE);
        image_edit_tools.setVisibility(View.VISIBLE);
        txt_sub_tabs.setVisibility(View.GONE);
        txt_fontButt.setVisibility(View.GONE);
        upload_photo.setVisibility(View.GONE);
        txt_styleButt_bool = false;
        txt_colorsButt_bool = false;
        setBgLL_bool = false;
        img_edt_Butt_bool = true;
        style_2_txt_bool = false;
        txt_1_bool = false;
        txt_2_bool = false;
        txt_3_bool = false;
        theme_edt_Butt_bool = true;
        emoji_bool = false;

        llBg_bool = false;
        photo_bool = false;
        bWSeek.setProgress(bw_p_theme_save);
        contrast.setProgress(contrast_p_theme_save);
        opacity.setProgress(opacity_p_theme_save);

    }


    @Override
    public void onBackPressed()
    {
        Intent i=new Intent (Texteo.this, MainActivity.class);
        startActivity(i);
        /*if (exit) {
            finish();

        } else {
            Toast.makeText(getApplicationContext(), "Unsaved work, Tap back again to exit", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }*/
    }


    public Bitmap getBitmap(RelativeLayout layout) {
        layout.setDrawingCacheEnabled(true);
        layout.buildDrawingCache();
        Bitmap bmp = Bitmap.createBitmap(layout.getDrawingCache());
        layout.setDrawingCacheEnabled(false);
        return bmp;
    }


    public void saveChart(Bitmap getbitmap, float height, float width) {
        Toast.makeText(this, "SAVE", Toast.LENGTH_LONG).show();

        File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString(), "Picxture");
        //   .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"myfolder");
        boolean success = false;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        Log.d("gggggg", folder.getPath());
        File file = new File(folder.getPath() + File.separator + "/" + timeStamp + ".png");
        if (!file.exists()) {
            try {
                success = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileOutputStream ostream = null;
        try {
            ostream = new FileOutputStream(file);
            System.out.println(ostream);
            Bitmap well = getbitmap;
            Bitmap save = Bitmap.createBitmap((int) width, (int) height, Bitmap.Config.ARGB_8888);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            Canvas now = new Canvas(save);
            now.drawRect(new Rect(0, 0, (int) width, (int) height), paint);
            now.drawBitmap(well,
                    new Rect(0, 0, well.getWidth(), well.getHeight()),
                    new Rect(0, 0, (int) width, (int) height), null);
            if (save == null) {
                System.out.println("NULL");
            }
            save.compress(Bitmap.CompressFormat.PNG, 100, ostream);
            ContentValues values = new ContentValues();
            //   values.put(Images.Media.TITLE, this.getString(R.string.picture_title));
            //   values.put(Images.Media.DESCRIPTION, this.getString(R.string.picture_description));
            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.ImageColumns.BUCKET_ID, file.toString().toLowerCase(Locale.US).hashCode());
            values.put(MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME, file.getName().toLowerCase(Locale.US));
            values.put("_data", file.getAbsolutePath());

            ContentResolver cr = getContentResolver();
            cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        } catch (NullPointerException e) {
            e.printStackTrace();
            //Toast.makeText(getApplicationContext(), &quot;Null error&quot;, Toast.LENGTH_SHORT).show();<br />
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // Toast.makeText(getApplicationContext(), &quot;File error&quot;, Toast.LENGTH_SHORT).show();<br />
        }
//        catch (IOException e){
//            e.printStackTrace();
//            // Toast.makeText(getApplicationContext(), &quot;IO error&quot;, Toast.LENGTH_SHORT).show();<br />
//        }
    }


}
