package com.company.px.activites;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.company.px.R;

public class FeedBack_Activity extends AppCompatActivity implements View.OnClickListener {

    ImageView back_butt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);

        back_butt = (ImageView) findViewById(R.id.back_butt);
        back_butt.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if (view==back_butt)
        {
            finish();
        }
    }
}
