package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.px.R;
import com.company.px.itemclicklistener.MenuTitleClickListner;


public class NamBgTitleHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView bg_title;
    public ImageView imgView1, imgView2, imgView3;

    MenuTitleClickListner menuTitleClickListner;

    public NamBgTitleHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        bg_title = (TextView) itemView.findViewById(R.id.festivalTitleTv);
        imgView1 = (ImageView) itemView.findViewById(R.id.imgView1);
        imgView2 = (ImageView) itemView.findViewById(R.id.imgView2);
        imgView3 = (ImageView) itemView.findViewById(R.id.imgView3);

    }

    @Override
    public void onClick(View view)
    {
        this.menuTitleClickListner.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MenuTitleClickListner ic)
    {
        this.menuTitleClickListner=ic;
    }
}
