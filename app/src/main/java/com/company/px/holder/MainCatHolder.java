package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.px.R;
import com.company.px.itemclicklistener.CartItemsClickListner;

/**
 * Created by anu on 1/20/2018.
 */

public class MainCatHolder extends RecyclerView.ViewHolder {

    public TextView main_cat_Tv;
    public ImageView main_cat_ic;
    public  RecyclerView sub_cat_Rv;

    public MainCatHolder(View itemView) {
        super(itemView);

        main_cat_Tv = (TextView) itemView.findViewById(R.id.main_cat_Tv);
        main_cat_ic = (ImageView) itemView.findViewById(R.id.main_cat_ic);
        sub_cat_Rv = (RecyclerView) itemView.findViewById(R.id.sub_cat_Rv);
    }



}
