package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.px.R;
import com.company.px.itemclicklistener.CategoryClickListner;

/**
 * Created by anu on 1/20/2018.
 */

public class PublicProductsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView prod_thumb_img;
    public TextView prod_name, prod_aft_price, prod_bef_price;
    CategoryClickListner categoryClickListner;

    public PublicProductsHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        prod_thumb_img = (ImageView) itemView.findViewById(R.id.prod_thumb_img);
        prod_name = (TextView) itemView.findViewById(R.id.prod_name);
        prod_aft_price = (TextView) itemView.findViewById(R.id.prod_aft_price);
        prod_bef_price = (TextView) itemView.findViewById(R.id.prod_bef_price);

    }

    @Override
    public void onClick(View v) {
        this.categoryClickListner.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(CategoryClickListner ic)
    {
        this.categoryClickListner =ic;
    }

}
