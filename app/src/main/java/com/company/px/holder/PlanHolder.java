package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.px.R;
import com.company.px.itemclicklistener.CategoryClickListner;
import com.company.px.itemclicklistener.PlanItemClickListener;

/**
 * Created by anu on 1/20/2018.
 */

public class PlanHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView title_T, choose_T;
    PlanItemClickListener planItemClickListener;

    public PlanHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        title_T = (TextView) itemView.findViewById(R.id.title_T);
        choose_T = (TextView) itemView.findViewById(R.id.choose_T);

    }

    @Override
    public void onClick(View v) {
        this.planItemClickListener.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(PlanItemClickListener ic)
    {
        this.planItemClickListener =ic;
    }

}
