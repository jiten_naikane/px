package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.company.px.R;
import com.company.px.itemclicklistener.EmojiTitleClickListner;
import com.company.px.itemclicklistener.FestivalTitleClickListner;


public class EmojiTitleHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


    public TextView festivalTitleTv;


    EmojiTitleClickListner emojiTitleClickListner;

    public EmojiTitleHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        festivalTitleTv = (TextView) itemView.findViewById(R.id.festivalTitleTv);

    }

    @Override
    public void onClick(View view)
    {
        this.emojiTitleClickListner.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(EmojiTitleClickListner ic)
    {
        this.emojiTitleClickListner=ic;
    }
}
