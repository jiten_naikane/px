package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.company.px.R;
import com.company.px.itemclicklistener.ImageHoriClickListner;


public class ImagesHoriHolder extends RecyclerView.ViewHolder {
    public ImageView imgView1, imgView2, imgView3;
    ImageHoriClickListner imageHoriClickListner;

    public ImagesHoriHolder(View itemView) {
        super(itemView);
        imgView1 = (ImageView) itemView.findViewById(R.id.imgView1);
        imgView2 = (ImageView) itemView.findViewById(R.id.imgView2);
        imgView3 = (ImageView) itemView.findViewById(R.id.imgView3);

    }

}
