package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.company.px.R;
import com.company.px.itemclicklistener.SelectedImageItemClickListener2;


public class SelectedImageHolder2 extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView delete;
    public ImageView user_img;
    public RelativeLayout rL;
    SelectedImageItemClickListener2 selectedImageItemClickListener2;

    public SelectedImageHolder2(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);


        delete = (TextView) itemView.findViewById(R.id.selected_image_delete_tv);
        user_img = (ImageView) itemView.findViewById(R.id.selected_image_iv);
        rL = (RelativeLayout) itemView.findViewById(R.id.rL);
    }

    @Override
    public void onClick(View view) {
        this.selectedImageItemClickListener2.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(SelectedImageItemClickListener2 ic) {
        this.selectedImageItemClickListener2 = ic;
    }
}
