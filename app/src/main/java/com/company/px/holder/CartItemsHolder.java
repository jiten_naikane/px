package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.px.R;
import com.company.px.itemclicklistener.CartItemsClickListner;

/**
 * Created by anu on 1/20/2018.
 */

public class CartItemsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView product_T, cost_T;
    CartItemsClickListner cartItemsClickListner;

    public CartItemsHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        product_T = (TextView) itemView.findViewById(R.id.product_T);
        cost_T = (TextView) itemView.findViewById(R.id.cost_T);

    }

    @Override
    public void onClick(View v) {
        this.cartItemsClickListner.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(CartItemsClickListner ic)
    {
        this.cartItemsClickListner =ic;
    }

}
