package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.px.R;

/**
 * Created by anu on 1/20/2018.
 */

public class SubCatHolder extends RecyclerView.ViewHolder {

    public TextView sub_cat_Tv;
    public ImageView sub_cat_ic;

    public SubCatHolder(View itemView) {
        super(itemView);

        sub_cat_Tv = (TextView) itemView.findViewById(R.id.sub_cat_Tv);
        sub_cat_ic = (ImageView) itemView.findViewById(R.id.sub_cat_ic);
    }



}
