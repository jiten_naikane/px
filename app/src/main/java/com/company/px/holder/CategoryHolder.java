package com.company.px.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.px.R;
import com.company.px.itemclicklistener.CategoryClickListner;

/**
 * Created by anu on 1/20/2018.
 */

public class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView cat_img;
    public TextView cat_name;
    CategoryClickListner categoryClickListner;

    public CategoryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        cat_img = (ImageView) itemView.findViewById(R.id.cat_img);
        cat_name = (TextView) itemView.findViewById(R.id.cat_name);

    }

    @Override
    public void onClick(View v) {
        this.categoryClickListner.onItemClick(v, getLayoutPosition());
    }

    public void setItemClickListener(CategoryClickListner ic)
    {
        this.categoryClickListner =ic;
    }

}
