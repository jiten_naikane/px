package com.company.px.model;

/**
 * Created by anu on 12/27/2017.
 */

public class EmojiImagesModel {

   public String img1;
   public String img2;
   public String img3;
   public String color_filter;


    public String getColor_filter() {
        return color_filter;
    }

    public void setColor_filter(String color_filter) {
        this.color_filter = color_filter;
    }


    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }





}
