package com.company.px.model;

/**
 * Created by anu on 1/20/2018.
 */


public class PrivateProductsModel {

    public String id;
    public String p_category_id;
    public String p_category_id_sub;
    public String p_category_id_child;
    public String name;
    public String description;
    public String description2;
    public String img1;
    public String img2;
    public String img3;
    public String img4;
    public String img5;
    public String img6;
    public String img7;
    public String img8;
    public String img9;
    public String img10;
    public String styles_tags;
    public String normal_delivery_info;
    public String quick_delivery_info;
    public String about_free_edits;
    public String about_output_files;
    public String price;
    public String price_before_discount;
    public String discount;
    public String product_mode;
    public String is_active;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getP_category_id() {
        return p_category_id;
    }

    public void setP_category_id(String p_category_id) {
        this.p_category_id = p_category_id;
    }

    public String getP_category_id_sub() {
        return p_category_id_sub;
    }

    public void setP_category_id_sub(String p_category_id_sub) {
        this.p_category_id_sub = p_category_id_sub;
    }

    public String getP_category_id_child() {
        return p_category_id_child;
    }

    public void setP_category_id_child(String p_category_id_child) {
        this.p_category_id_child = p_category_id_child;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription2() {
        return description2;
    }

    public void setDescription2(String description2) {
        this.description2 = description2;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }

    public String getImg4() {
        return img4;
    }

    public void setImg4(String img4) {
        this.img4 = img4;
    }

    public String getImg5() {
        return img5;
    }

    public void setImg5(String img5) {
        this.img5 = img5;
    }

    public String getImg6() {
        return img6;
    }

    public void setImg6(String img6) {
        this.img6 = img6;
    }

    public String getImg7() {
        return img7;
    }

    public void setImg7(String img7) {
        this.img7 = img7;
    }

    public String getImg8() {
        return img8;
    }

    public void setImg8(String img8) {
        this.img8 = img8;
    }

    public String getImg9() {
        return img9;
    }

    public void setImg9(String img9) {
        this.img9 = img9;
    }

    public String getImg10() {
        return img10;
    }

    public void setImg10(String img10) {
        this.img10 = img10;
    }

    public String getStyles_tags() {
        return styles_tags;
    }

    public void setStyles_tags(String styles_tags) {
        this.styles_tags = styles_tags;
    }

    public String getNormal_delivery_info() {
        return normal_delivery_info;
    }

    public void setNormal_delivery_info(String normal_delivery_info) {
        this.normal_delivery_info = normal_delivery_info;
    }

    public String getQuick_delivery_info() {
        return quick_delivery_info;
    }

    public void setQuick_delivery_info(String quick_delivery_info) {
        this.quick_delivery_info = quick_delivery_info;
    }

    public String getAbout_free_edits() {
        return about_free_edits;
    }

    public void setAbout_free_edits(String about_free_edits) {
        this.about_free_edits = about_free_edits;
    }

    public String getAbout_output_files() {
        return about_output_files;
    }

    public void setAbout_output_files(String about_output_files) {
        this.about_output_files = about_output_files;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_before_discount() {
        return price_before_discount;
    }

    public void setPrice_before_discount(String price_before_discount) {
        this.price_before_discount = price_before_discount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getProduct_mode() {
        return product_mode;
    }

    public void setProduct_mode(String product_mode) {
        this.product_mode = product_mode;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }






}
