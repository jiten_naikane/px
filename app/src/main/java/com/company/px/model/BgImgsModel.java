package com.company.px.model;

/**
 * Created by anu on 12/27/2017.
 */

public class BgImgsModel {

   public String bgtitle_id;
   public String img1;
   public String img2;
   public String img3;

    public String getBgtitle_id() {
        return bgtitle_id;
    }

    public void setBgtitle_id(String bgtitle_id) {
        this.bgtitle_id = bgtitle_id;
    }

    public String getImg1() {
        return img1;
    }

    public void setImg1(String img1) {
        this.img1 = img1;
    }

    public String getImg2() {
        return img2;
    }

    public void setImg2(String img2) {
        this.img2 = img2;
    }

    public String getImg3() {
        return img3;
    }

    public void setImg3(String img3) {
        this.img3 = img3;
    }





}
