package com.company.px.model;

/**
 * Created by anu on 1/20/2018.
 */

public class SubCatModel {
    public String id;
    public String name;
    public String image;
    public String is_active;
    public String has_childcat;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getHas_childcat() {
        return has_childcat;
    }

    public void setHas_childcat(String has_subCat) {
        this.has_childcat = has_subCat;
    }


}
