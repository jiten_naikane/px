package com.company.px.model;

/**
 * Created by anu on 1/20/2018.
 */

public class CategoriesModel {

        String id;
        String name;
        String royalty;
        String free_edit;
        String image;
        String is_active;

    public String getRoyalty() {
        return royalty;
    }

    public void setRoyalty(String royalty) {
        this.royalty = royalty;
    }

    public String getFree_edit() {
        return free_edit;
    }

    public void setFree_edit(String free_edit) {
        this.free_edit = free_edit;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }
}
