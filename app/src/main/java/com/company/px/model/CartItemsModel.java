package com.company.px.model;

/**
 * Created by anu on 1/20/2018.
 */

public class CartItemsModel {

    String id;
    String product_id;
    String user_id;
    String quantity;
    String mrp;
    String discount;
    String total_price;
    String design_brief;
    String design_purpose;
    String highlighted_text;
    String ref_image;
    String ref_file;
    String ref_file2;
    String ref_web_link;
    String ref_about;
    String design_appearence_choice;
    String design_appearence_tags;
    String design_appearence_about;
    String colors_tag;
    String colors_about;
    String design_avoided_text;
    String deliver_email;
    String deliver_whatsapp;
    String product_name;

    public String getRef_file() {
        return ref_file;
    }

    public void setRef_file(String ref_file) {
        this.ref_file = ref_file;
    }

    public String getRef_file2() {
        return ref_file2;
    }

    public void setRef_file2(String ref_file2) {
        this.ref_file2 = ref_file2;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getDesign_brief() {
        return design_brief;
    }

    public void setDesign_brief(String design_brief) {
        this.design_brief = design_brief;
    }

    public String getDesign_purpose() {
        return design_purpose;
    }

    public void setDesign_purpose(String design_purpose) {
        this.design_purpose = design_purpose;
    }

    public String getHighlighted_text() {
        return highlighted_text;
    }

    public void setHighlighted_text(String highlighted_text) {
        this.highlighted_text = highlighted_text;
    }

    public String getRef_image() {
        return ref_image;
    }

    public void setRef_image(String ref_image) {
        this.ref_image = ref_image;
    }

    public String getRef_web_link() {
        return ref_web_link;
    }

    public void setRef_web_link(String ref_web_link) {
        this.ref_web_link = ref_web_link;
    }

    public String getRef_about() {
        return ref_about;
    }

    public void setRef_about(String ref_about) {
        this.ref_about = ref_about;
    }

    public String getDesign_appearence_choice() {
        return design_appearence_choice;
    }

    public void setDesign_appearence_choice(String design_appearence_choice) {
        this.design_appearence_choice = design_appearence_choice;
    }

    public String getDesign_appearence_tags() {
        return design_appearence_tags;
    }

    public void setDesign_appearence_tags(String design_appearence_tags) {
        this.design_appearence_tags = design_appearence_tags;
    }

    public String getDesign_appearence_about() {
        return design_appearence_about;
    }

    public void setDesign_appearence_about(String design_appearence_about) {
        this.design_appearence_about = design_appearence_about;
    }

    public String getColors_tag() {
        return colors_tag;
    }

    public void setColors_tag(String colors_tag) {
        this.colors_tag = colors_tag;
    }

    public String getColors_about() {
        return colors_about;
    }

    public void setColors_about(String colors_about) {
        this.colors_about = colors_about;
    }

    public String getDesign_avoided_text() {
        return design_avoided_text;
    }

    public void setDesign_avoided_text(String design_avoided_text) {
        this.design_avoided_text = design_avoided_text;
    }

    public String getDeliver_email() {
        return deliver_email;
    }

    public void setDeliver_email(String deliver_email) {
        this.deliver_email = deliver_email;
    }

    public String getDeliver_whatsapp() {
        return deliver_whatsapp;
    }

    public void setDeliver_whatsapp(String deliver_whatsapp) {
        this.deliver_whatsapp = deliver_whatsapp;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }




}
