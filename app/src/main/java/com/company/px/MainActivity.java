package com.company.px;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.activites.egraphics.StepTwo_Cart;
import com.company.px.activites.CustomerSupport;
import com.company.px.activites.FeedBack_Activity;
import com.company.px.activites.MyOrderActivity;
import com.company.px.activites.MyProfile;
import com.company.px.activites.T_C;
import com.company.px.adapter.MainCatAdapter;
import com.company.px.adapter.SubCatAdapter;
import com.company.px.model.MainCatModel;
import com.company.px.model.SubCatModel;
import com.company.px.utility.AppLocationService;
import com.company.px.utility.AppUrls;
import com.company.px.utility.GPSTracker;
import com.company.px.utility.NetworkChecking;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    AppLocationService appLocationService;
    private Boolean exit = false;
    private DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar;
    ImageView heart_iic, options_img_butt;
    RecyclerView main_cat_RV, sub_cat_RV;
    LinearLayoutManager linearLayoutManager_main_cat, linearLayoutManager_sub_cat;
    MainCatAdapter mainCatAdapter;
    SubCatAdapter subCatAdapter;
    ArrayList<MainCatModel> mainCatModels = new ArrayList<MainCatModel>();
    ArrayList<SubCatModel> subCatModels = new ArrayList<SubCatModel>();
    private boolean checkInternet;
    LinearLayout profile_butt, orderHistory_butt, saved_ord_butt, cust_service_butt, feedback_butt, tc_butt;

    GPSTracker gps;
    Geocoder geocoder;
    List<Address> addresses = null;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gps = new GPSTracker(MainActivity.this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        options_img_butt = (ImageView) findViewById(R.id.options_img_butt);
        options_img_butt.setOnClickListener(this);
        heart_iic = (ImageView) findViewById(R.id.heart_iic);
        heart_iic.setColorFilter(getColor(R.color.red_p7));
        heart_iic.setAlpha(90);
        appLocationService = new AppLocationService(MainActivity.this);
        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                }
            }
        }


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


                return false;
            }
        });

        View navigationheadderView = navigationView.getHeaderView(0);


        main_cat_RV = (RecyclerView) findViewById(R.id.main_cat_RV);
        mainCatAdapter = new MainCatAdapter(mainCatModels, MainActivity.this, R.layout.row_main_cat);
        linearLayoutManager_main_cat = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        main_cat_RV.setLayoutManager(linearLayoutManager_main_cat);

        sub_cat_RV = (RecyclerView) findViewById(R.id.sub_cat_RV);
        subCatAdapter = new SubCatAdapter(subCatModels, this, R.layout.row_sub_cat);
        linearLayoutManager_sub_cat = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        sub_cat_RV.setLayoutManager(linearLayoutManager_sub_cat);







        getCat();
    }


    private void getCat() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            Log.d("Cat_URL", AppUrls.BASE_URL + "product_categories");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "product_categories",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("Cat_RESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if (responceCode.equals("10100")) {
                                    JSONArray jarray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++) {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        MainCatModel fList = new MainCatModel();
                                        fList.setId(jsonObject1.getString("id"));
                                        fList.setName(jsonObject1.getString("name"));
                                        fList.setIs_active(jsonObject1.getString("is_active"));
                                        fList.setHas_subCat(jsonObject1.getString("has_subcategories"));
                                        fList.setImage(jsonObject1.getString("image"));
                                        mainCatModels.add(fList);

                                         /*   JSONArray subCatArr = jsonObject1.getJSONArray("subcategories");
                                            Log.d("SUB_CATS", subCatArr.toString());
                                            for (int ii = 0; ii < subCatArr.length(); ii++)
                                            {
                                                JSONObject jsonObject2 = subCatArr.getJSONObject(ii);

                                                SubCatModel SList = new SubCatModel();
                                                SList.setId(jsonObject2.getString("id"));
                                                SList.setName(jsonObject2.getString("name"));
                                                SList.setHas_childcat(jsonObject2.getString("has_childcategories"));
                                                SList.setImage(jsonObject2.getString("image"));

                                                subCatModels.add(SList);
                                            }
                                            sub_cat_RV.setAdapter(subCatAdapter);*/

                                    }
                                    main_cat_RV.setAdapter(mainCatAdapter);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    } else if (error instanceof AuthFailureError) {
                    } else if (error instanceof ServerError) {
                    } else if (error instanceof NetworkError) {
                    } else if (error instanceof ParseError) {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }


    private void getOptionsDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.options_menu_home);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);


        profile_butt = (LinearLayout) dialog.findViewById(R.id.profile_butt);
        profile_butt.setOnClickListener(this);
        orderHistory_butt = (LinearLayout) dialog.findViewById(R.id.orderHistory_butt);
        orderHistory_butt.setOnClickListener(this);
        saved_ord_butt = (LinearLayout) dialog.findViewById(R.id.saved_ord_butt);
        saved_ord_butt.setOnClickListener(this);
        cust_service_butt = (LinearLayout) dialog.findViewById(R.id.cust_service_butt);
        cust_service_butt.setOnClickListener(this);
        feedback_butt = (LinearLayout) dialog.findViewById(R.id.feedback_butt);
        feedback_butt.setOnClickListener(this);
        tc_butt = (LinearLayout) dialog.findViewById(R.id.tc_butt);
        tc_butt.setOnClickListener(this);
        LinearLayout dialog_rL = (LinearLayout) dialog.findViewById(R.id.dialog_rL);

        dialog_rL.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                dialog.dismiss();
                return false;
            }
        });

        dialog.show();
    }


    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (exit) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                moveTaskToBack(true);
            } else {
                Toast.makeText(getApplicationContext(), "Tap Back Button again to exit", Toast.LENGTH_SHORT).show();
                exit = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        exit = false;
                    }
                }, 2 * 1000);
            }
        }
    }


    @Override
    public void onClick(View view) {
        if (view == options_img_butt) {
            getOptionsDialog();
        }
        if (view == profile_butt) {
            Intent profile = new Intent(getApplicationContext(), MyProfile.class);
            startActivity(profile);
        }
        if (view == orderHistory_butt) {
            Intent order = new Intent(getApplicationContext(), MyOrderActivity.class);
            startActivity(order);
        }
        if (view == saved_ord_butt) {
            Intent cart_item = new Intent(getApplicationContext(), StepTwo_Cart.class);
            startActivity(cart_item);
        }
        if (view == cust_service_butt) {
            Intent customer_service = new Intent(getApplicationContext(), CustomerSupport.class);
            startActivity(customer_service);
        }
        if (view == feedback_butt) {
            Intent feedback = new Intent(getApplicationContext(), FeedBack_Activity.class);
            startActivity(feedback);
        }
        if (view == tc_butt) {
            Intent feedback = new Intent(getApplicationContext(), T_C.class);
            startActivity(feedback);
        }
    }

    public static String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            } else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        } catch (Exception e) {
        }
        return null;
    }




}
