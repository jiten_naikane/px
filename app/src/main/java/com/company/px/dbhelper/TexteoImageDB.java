package com.company.px.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anu on 3/25/2018.
 */

public class TexteoImageDB extends SQLiteOpenHelper
{

    private static final int DATABASE_VERSION =1;
    private static final String DATABASE_NAME ="texteoimage.db";






    private static final String TABLE_TEXTEOIMAGE ="texteoimages";

    public static final String FESTIVAL_ID = "festival_id";
    public static final String IMAGE1 = "img1";
    public static final String IMAGE2 = "img2";
    public static final String IMAGE3 = "img3";




    public TexteoImageDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public TexteoImageDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {



        String CREATE_IMAGE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TEXTEOIMAGE + "("
                + FESTIVAL_ID + " TEXT ,"
                + IMAGE1 + " TEXT ,"
                + IMAGE2 + " TEXT ,"
                + IMAGE3 + " TEXT " +
                ")";



        db.execSQL(CREATE_IMAGE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEXTEOIMAGE);
        onCreate(db);
    }



    public void addImages(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_TEXTEOIMAGE, null, contentValues);
        db.close();
    }






    public List<String> getImageId() {
        List<String> imgid = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TEXTEOIMAGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                imgid.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return imgid;
    }

    public List<String> getImageOne() {
        List<String> img_one = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TEXTEOIMAGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                img_one.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return img_one;
    }

    public List<String> getImageTwo() {
        List<String> img_two = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TEXTEOIMAGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                img_two.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return img_two;
    }

    public List<String> getImageThree() {
        List<String> img_one = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TEXTEOIMAGE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                img_one.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return img_one;
    }


















    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("DELETE FROM "+TABLE_TEXTEOIMAGE); //delete all rows in a table
        db.close();
    }




    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }


}
