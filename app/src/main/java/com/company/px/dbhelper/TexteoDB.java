package com.company.px.dbhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anu on 3/25/2018.
 */

public class TexteoDB extends SQLiteOpenHelper
{

    private static final int DATABASE_VERSION =2;
    private static final String DATABASE_NAME ="texteo.db";



    private static final String TABLE_TEXTEOTITLE ="texteo_title";

    public static final String ID = "id";
    public static final String TITLE = "name";
    public static final String IS_ACTIVE = "is_active";
    public static final String CREATED_ON = "created_on";
    public static final String COLOR_ID = "color_id";





    public TexteoDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public TexteoDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {

        String CREATE_TITLE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_TEXTEOTITLE + "("
                + ID + " TEXT ,"
                + TITLE + " TEXT ,"
                + IS_ACTIVE + " TEXT ,"
                + CREATED_ON + " TEXT ,"
                + COLOR_ID + " TEXT " +
                ")";




        db.execSQL(CREATE_TITLE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEXTEOTITLE);

        onCreate(db);
    }

    public void addTitle(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_TEXTEOTITLE, null, contentValues);
        db.close();
    }

    public List<String> getId() {
        List<String> id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TEXTEOTITLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return id;
    }

    public List<String> getTitleName() {
        List<String> activity_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TEXTEOTITLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_name.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return activity_name;
    }

    public List<String> getIsActive() {
        List<String> activity_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TEXTEOTITLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_name.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return activity_name;
    }

    public List<String> getCreated() {
        List<String> activity_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TEXTEOTITLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_name.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return activity_name;
    }

    public List<String> getColorId() {
        List<String> activity_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_TEXTEOTITLE;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                activity_name.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return activity_name;
    }





    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_TEXTEOTITLE); //delete all rows in a table

        db.close();
    }




    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }


}
