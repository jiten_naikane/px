package com.company.px.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.MainActivity;
import com.company.px.R;
import com.company.px.holder.MainCatHolder;
import com.company.px.model.MainCatModel;
import com.company.px.model.SubCatModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainCatAdapter extends RecyclerView.Adapter<MainCatHolder> {
    public ArrayList<MainCatModel> mainCatModels;
    public MainActivity context;
    LayoutInflater li;
    int resource;
    private boolean checkInternet;

    LinearLayoutManager linearLayoutManager_sub_cat;
    SubCatAdapter subCatAdapter;
    ArrayList<SubCatModel> subCatModels = new ArrayList<SubCatModel>();


    public MainCatAdapter(ArrayList<MainCatModel> mainCatModels, MainActivity context, int resource) {
        this.mainCatModels = mainCatModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MainCatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        MainCatHolder slh = new MainCatHolder(layout);


        subCatAdapter = new SubCatAdapter(subCatModels, context, R.layout.row_sub_cat);
        linearLayoutManager_sub_cat =  new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        slh.sub_cat_Rv.setLayoutManager(linearLayoutManager_sub_cat);
        slh.sub_cat_Rv.setAdapter(subCatAdapter);


        return slh;
    }




    @Override
    public void onBindViewHolder(final MainCatHolder holder, final int position)
    {
        holder.main_cat_Tv.setText(mainCatModels.get(position).getName());
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + mainCatModels.get(position).getImage())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.main_cat_ic);



            checkInternet = NetworkChecking.isConnected(context);
            if (checkInternet)
            {
                Log.d("Cat_AD_URL", AppUrls.BASE_URL + "product_categories");
                StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + "product_categories",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("Cat_AD_RESP",response);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String responceCode = jsonObject.getString("code");
                                    if(responceCode.equals("10100"))
                                    {
                                        JSONArray jarray=jsonObject.getJSONArray("data");
                                        for (int i = 0; i < jarray.length(); i++)
                                        {
                                            JSONObject jsonObject1 = jarray.getJSONObject(i);
                                            JSONArray subCatArr = jsonObject1.getJSONArray("subcategories");
                                            Log.d("AAAAAA", subCatArr.toString());
                                            Log.d("DSCDS", String.valueOf(subCatArr.length()));


                                            for (int ii = 0; ii < subCatArr.length(); ii++)
                                            {
                                                JSONObject jsonObject2 = subCatArr.getJSONObject(ii);

                                                SubCatModel SList = new SubCatModel();
                                                SList.setId(jsonObject2.getString("id"));
                                                SList.setName(jsonObject2.getString("name"));
                                                SList.setHas_childcat(jsonObject2.getString("has_childcategories"));
                                                SList.setImage(jsonObject2.getString("image"));

                                                subCatModels.add(SList);
                                            }
                                        }
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError)
                        {
                        } else if (error instanceof AuthFailureError)
                        {
                        } else if (error instanceof ServerError)
                        {
                        } else if (error instanceof NetworkError)
                        {
                        } else if (error instanceof ParseError)
                        {
                        }
                    }
                });
                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.add(stringRequest);
            }else {
                Toast.makeText(context, "Please Connect to Internet", Toast.LENGTH_SHORT).show();
            }


    }

    @Override
    public int getItemCount() {
        return this.mainCatModels.size();
    }










}
