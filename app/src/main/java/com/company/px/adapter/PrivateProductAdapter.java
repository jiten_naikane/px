package com.company.px.adapter;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.company.px.R;
import com.company.px.activites.egraphics.StepOne_Description;
import com.company.px.activites.egraphics.Products;
import com.company.px.activites.logs.LoginActivity;
import com.company.px.holder.PrivateProductsHolder;
import com.company.px.itemclicklistener.CategoryClickListner;
import com.company.px.model.PrivateProductsModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.UserSessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class PrivateProductAdapter extends RecyclerView.Adapter<PrivateProductsHolder> {
    public ArrayList<PrivateProductsModel> pvtProModels;
    public Products context;
    LayoutInflater li;
    int resource;
    Dialog dialog;
    String main_categ;
    ImageView prod_img_a;
    UserSessionManager session;


    public PrivateProductAdapter(ArrayList<PrivateProductsModel> pvtProModels, Products context, int resource, String cat_ID) {
        this.pvtProModels = pvtProModels;
        this.context = context;
        this.resource = resource;
        this.main_categ = cat_ID;
        session = new UserSessionManager(context);

        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PrivateProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        PrivateProductsHolder slh = new PrivateProductsHolder(layout);

        return slh;
    }

    @Override
    public void onBindViewHolder(PrivateProductsHolder holder, final int position) {
        holder.prod_name.setText(pvtProModels.get(position).getName());
        holder.prod_aft_price.setText(pvtProModels.get(position).getPrice());
        holder.prod_bef_price.setText(pvtProModels.get(position).getPrice_before_discount());
        holder.prod_bef_price.setPaintFlags(holder.prod_bef_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg1())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.prod_thumb_img);


        holder.setItemClickListener(new CategoryClickListner() {
            @Override
            public void onItemClick(View view, int layoutPosition) {
                dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.product_details_alert);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.getWindow().setLayout(ActionBar.LayoutParams.FILL_PARENT, ActionBar.LayoutParams.FILL_PARENT);

                LinearLayout closeButt_a = (LinearLayout) dialog.findViewById(R.id.closeButt_a);
                prod_img_a = (ImageView) dialog.findViewById(R.id.prod_img_a);


                ImageView nxt = (ImageView) dialog.findViewById(R.id.nxt);
                nxt.setColorFilter(Color.WHITE);
                ImageView prod_img_1 = (ImageView) dialog.findViewById(R.id.prod_img_1);
                ImageView prod_img_2 = (ImageView) dialog.findViewById(R.id.prod_img_2);
                ImageView prod_img_3 = (ImageView) dialog.findViewById(R.id.prod_img_3);
                ImageView prod_img_4 = (ImageView) dialog.findViewById(R.id.prod_img_4);
                ImageView prod_img_5 = (ImageView) dialog.findViewById(R.id.prod_img_5);
                ImageView prod_img_6 = (ImageView) dialog.findViewById(R.id.prod_img_6);
                ImageView prod_img_7 = (ImageView) dialog.findViewById(R.id.prod_img_7);
                ImageView prod_img_8 = (ImageView) dialog.findViewById(R.id.prod_img_8);
                ImageView prod_img_9 = (ImageView) dialog.findViewById(R.id.prod_img_9);
                ImageView prod_img_10 = (ImageView) dialog.findViewById(R.id.prod_img_10);
                TextView prod_name_a = (TextView) dialog.findViewById(R.id.prod_name_a);
                TextView prod_before_price_a = (TextView) dialog.findViewById(R.id.prod_before_price_a);
                prod_before_price_a.setPaintFlags(prod_before_price_a.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                TextView prod_price_a = (TextView) dialog.findViewById(R.id.prod_price_a);
                TextView prod_tags_a = (TextView) dialog.findViewById(R.id.prod_tags_a);
                TextView prod_desc1_a = (TextView) dialog.findViewById(R.id.prod_desc1_a);
                TextView prod_desc2_a = (TextView) dialog.findViewById(R.id.prod_desc2_a);
                TextView prod_abt_edt_a = (TextView) dialog.findViewById(R.id.prod_abt_edt_a);
                TextView prod_abt_output_a = (TextView) dialog.findViewById(R.id.prod_abt_output_a);
                TextView prod_norm_deliv_a = (TextView) dialog.findViewById(R.id.prod_norm_deliv_a);
                TextView prod_quik_deliv_a = (TextView) dialog.findViewById(R.id.prod_quik_deliv_a);
                LinearLayout nextButt = (LinearLayout) dialog.findViewById(R.id.nextButt);


                prod_name_a.setText(pvtProModels.get(position).getName());
                prod_before_price_a.setText(pvtProModels.get(position).getPrice_before_discount());
                prod_price_a.setText(pvtProModels.get(position).getPrice());
                prod_tags_a.setText(pvtProModels.get(position).getStyles_tags());
                prod_desc1_a.setText(pvtProModels.get(position).getDescription());
                prod_desc2_a.setText(pvtProModels.get(position).getDescription2());
                prod_abt_edt_a.setText(pvtProModels.get(position).getAbout_free_edits());
                prod_abt_output_a.setText(pvtProModels.get(position).getAbout_output_files());
                prod_norm_deliv_a.setText(pvtProModels.get(position).getNormal_delivery_info());
                prod_quik_deliv_a.setText(pvtProModels.get(position).getQuick_delivery_info());


                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg1())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_a);

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg1())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_1);
                if (pvtProModels.get(position).getImg1().length() == 0) {
                    prod_img_1.setVisibility(View.GONE);
                } else {
                    prod_img_1.setVisibility(View.VISIBLE);
                }

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg2())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_2);
                if (pvtProModels.get(position).getImg2().length() == 0) {
                    prod_img_2.setVisibility(View.GONE);
                } else {
                    prod_img_2.setVisibility(View.VISIBLE);
                }

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg3())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_3);
                if (pvtProModels.get(position).getImg3().length() == 0) {
                    prod_img_3.setVisibility(View.GONE);
                } else {
                    prod_img_3.setVisibility(View.VISIBLE);
                }

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg4())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_4);
                if (pvtProModels.get(position).getImg4().length() == 0) {
                    prod_img_4.setVisibility(View.GONE);
                } else {
                    prod_img_4.setVisibility(View.VISIBLE);
                }

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg5())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_5);
                if (pvtProModels.get(position).getImg5().length() == 0) {
                    prod_img_5.setVisibility(View.GONE);
                } else {
                    prod_img_5.setVisibility(View.VISIBLE);
                }

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg6())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_6);
                if (pvtProModels.get(position).getImg6().length() == 0) {
                    prod_img_6.setVisibility(View.GONE);
                } else {
                    prod_img_6.setVisibility(View.VISIBLE);
                }

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg7())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_7);
                if (pvtProModels.get(position).getImg7().length() == 0) {
                    prod_img_7.setVisibility(View.GONE);
                } else {
                    prod_img_7.setVisibility(View.VISIBLE);
                }

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg8())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_8);
                if (pvtProModels.get(position).getImg8().length() == 0) {
                    prod_img_8.setVisibility(View.GONE);
                } else {
                    prod_img_8.setVisibility(View.VISIBLE);
                }

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg9())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_9);
                if (pvtProModels.get(position).getImg9().length() == 0) {
                    prod_img_9.setVisibility(View.GONE);
                } else {
                    prod_img_9.setVisibility(View.VISIBLE);
                }

                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg10())
                        .placeholder(R.drawable.thumb_placeholder)
                        .into(prod_img_10);
                if (pvtProModels.get(position).getImg10().length() == 0) {
                    prod_img_10.setVisibility(View.GONE);
                } else {
                    prod_img_10.setVisibility(View.VISIBLE);
                }

                prod_img_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg1())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });

                prod_img_2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg2())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });

                prod_img_3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg3())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });

                prod_img_4.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg4())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });

                prod_img_5.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg5())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });

                prod_img_6.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg6())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });

                prod_img_7.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg7())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });

                prod_img_8.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg8())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });

                prod_img_9.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg9())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });

                prod_img_10.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(context)
                                .load(AppUrls.BASE_IMAGE_URL + pvtProModels.get(position).getImg10())
                                .placeholder(R.drawable.thumb_placeholder)
                                .into(prod_img_a);
                    }
                });
                nextButt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (session.checkLogin()) {
                            Intent login = new Intent(context, LoginActivity.class);
                            context.startActivity(login);
                        } else {
                            Intent nxt_ii = new Intent(context, StepOne_Description.class);
                            nxt_ii.putExtra("MAINCATEGID", main_categ);
                            nxt_ii.putExtra("PRODUCT_ID", pvtProModels.get(position).getId());
                            context.startActivity(nxt_ii);
                        }
                    }
                });


                closeButt_a.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                dialog.show();
                //showProductDetailsAlert();
            }
        });


    }

    @Override
    public int getItemCount() {
        return this.pvtProModels.size();
    }



}
