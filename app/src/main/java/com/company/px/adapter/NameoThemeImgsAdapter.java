package com.company.px.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.company.px.R;
import com.company.px.activites.selfie.all.mixme.MIxme;
import com.company.px.activites.selfie.all.nameo.NameoEdit;
import com.company.px.holder.NamBgTitleHolder;
import com.company.px.holder.NamBgTitleHolder;
import com.company.px.model.BgImgsModel;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class NameoThemeImgsAdapter extends RecyclerView.Adapter<NamBgTitleHolder>
{
    public ArrayList<BgImgsModel> bgImgsModel;
    public NameoEdit context;
    LayoutInflater li;
    private int resource, screen_width;
    private boolean first_run=false;

    public NameoThemeImgsAdapter(ArrayList<BgImgsModel> bgImgsModel, NameoEdit context, int resource) {
        this.context = context;
        this.bgImgsModel = bgImgsModel;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public NamBgTitleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        NamBgTitleHolder fth = new NamBgTitleHolder(layout);
        context.theme_Invisi_ic = (ImageView) context.findViewById(R.id.theme_Invisi_ic);
        context.theme_visi_ic = (ImageView) context.findViewById(R.id.theme_visi_ic);
        context.img_theme = (ImageView) context.findViewById(R.id.img_theme);

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        screen_width = displayMetrics.widthPixels;
        return fth;
    }

    @Override
    public void onBindViewHolder(final NamBgTitleHolder holder, final int position) {
            if (!first_run) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + bgImgsModel.get(position).getImg1())
                        .resize(screen_width, screen_width)
                        .into(context.img_theme);
                first_run = true;
            }

//        theme_visi_ic.setVisibility(View.VISIBLE);
//        theme_Invisi_ic.setVisibility(View.GONE);

        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + bgImgsModel.get(position).getImg1())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView1);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + bgImgsModel.get(position).getImg2())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView2);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + bgImgsModel.get(position).getImg3())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView3);

        if (bgImgsModel.get(position).getImg3().length()==0)
        {
            holder.imgView3.setVisibility(View.GONE);
        } else
        {
            holder.imgView3.setVisibility(View.VISIBLE);
        }
        if (bgImgsModel.get(position).getImg2().length()==0)
        {
            holder.imgView2.setVisibility(View.GONE);
        } else
        {
            holder.imgView2.setVisibility(View.VISIBLE);
        }


        holder.imgView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + bgImgsModel.get(position).getImg1())
                        .resize(screen_width, screen_width)
                        .into(context.img_theme);
                context.theme_visi_ic.setVisibility(View.VISIBLE);
            }
        });

        holder.imgView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + bgImgsModel.get(position).getImg2())
                        .resize(screen_width, screen_width)
                        .into(context.img_theme);
                context.theme_visi_ic.setVisibility(View.VISIBLE);
            }
        });

        holder.imgView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + bgImgsModel.get(position).getImg3())
                        .resize(screen_width, screen_width)
                        .into(context.img_theme);
                context.theme_visi_ic.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public int getItemCount() {
        return bgImgsModel.size();
    }

}