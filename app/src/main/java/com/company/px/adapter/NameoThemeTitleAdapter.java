package com.company.px.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.px.activites.selfie.all.nameo.NameoEdit;
import com.company.px.holder.NamBgTitleHolder;
import com.company.px.itemclicklistener.MenuTitleClickListner;
import com.company.px.model.NamBgTitlesModel;

import java.util.ArrayList;


public class NameoThemeTitleAdapter extends RecyclerView.Adapter<NamBgTitleHolder> {
    public ArrayList<NamBgTitlesModel> namBgTitlesModel;
    public NameoEdit context;
    LayoutInflater li;
    int resource;

    public NameoThemeTitleAdapter(ArrayList<NamBgTitlesModel> namBgTitlesModel, NameoEdit context, int resource)
    {
        this.context = context;
        this.namBgTitlesModel = namBgTitlesModel;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public NamBgTitleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        NamBgTitleHolder fth = new NamBgTitleHolder(layout);

        return fth;
    }
    
    @Override
    public void onBindViewHolder(NamBgTitleHolder holder, final int position) {
        holder.bg_title.setText(namBgTitlesModel.get(position).getName());

        context.getImagesBg(String.valueOf(namBgTitlesModel.get(position).getId()));

        holder.setItemClickListener(new MenuTitleClickListner() {
            @Override
            public void onItemClick(View view, int layoutPosition) {
                context.getImagesBg(String.valueOf(namBgTitlesModel.get(position).getId()));
            }
        });


    }

    @Override
    public int getItemCount() {
        return namBgTitlesModel.size();
    }







}
