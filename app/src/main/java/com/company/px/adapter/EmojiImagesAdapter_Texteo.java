package com.company.px.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.company.px.R;
import com.company.px.activites.selfie.all.nameo.NameoEdit;
import com.company.px.activites.selfie.all.stickies.StickerImageView;
import com.company.px.activites.selfie.all.texteo.Texteo;
import com.company.px.holder.EmojiImagesHolder;
import com.company.px.model.EmojiImagesModel;
import com.company.px.utility.AppUrls;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class EmojiImagesAdapter_Texteo extends RecyclerView.Adapter<EmojiImagesHolder> {
    public ArrayList<EmojiImagesModel> emojiImagesModels;
    public Texteo context;
    LayoutInflater li;
    int resource;

    public EmojiImagesAdapter_Texteo(ArrayList<EmojiImagesModel> emojiImagesModels, Texteo context, int resource)
    {
        this.context = context;
        this.emojiImagesModels = emojiImagesModels;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public EmojiImagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        EmojiImagesHolder fth = new EmojiImagesHolder(layout);

        return fth;
    }

    @Override
    public void onBindViewHolder(EmojiImagesHolder holder, final int position) {

        final String color_filt = emojiImagesModels.get(position).getColor_filter();

        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + emojiImagesModels.get(position).getImg1())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView1);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + emojiImagesModels.get(position).getImg2())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView2);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + emojiImagesModels.get(position).getImg3())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView3);

        if (emojiImagesModels.get(position).getImg3().length()==0)
        {
            holder.imgView3.setVisibility(View.GONE);
        } else
        {
            holder.imgView3.setVisibility(View.VISIBLE);
        }
        if (emojiImagesModels.get(position).getImg2().length()==0)
        {
            holder.imgView2.setVisibility(View.GONE);
        } else
        {
            holder.imgView2.setVisibility(View.VISIBLE);
        }

        holder.imgView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = getBitmapFromURL(AppUrls.BASE_IMAGE_URL + emojiImagesModels.get(position).getImg1());
                context.emojis(bitmap, color_filt);
                context.emoji_visi_ic.setVisibility(View.VISIBLE);
                context.colors_ll.setVisibility(View.VISIBLE);

            }
        });

        holder.imgView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = getBitmapFromURL(AppUrls.BASE_IMAGE_URL + emojiImagesModels.get(position).getImg2());
                context.emojis(bitmap, color_filt);
                context.emoji_visi_ic.setVisibility(View.VISIBLE);
                context.colors_ll.setVisibility(View.VISIBLE);


            }
        });

        holder.imgView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = getBitmapFromURL(AppUrls.BASE_IMAGE_URL + emojiImagesModels.get(position).getImg3());
                context.emojis(bitmap, color_filt);
                context.emoji_visi_ic.setVisibility(View.VISIBLE);
                context.colors_ll.setVisibility(View.VISIBLE);


            }
        });


    }

    @Override
    public int getItemCount() {
        return emojiImagesModels.size();
    }


    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
