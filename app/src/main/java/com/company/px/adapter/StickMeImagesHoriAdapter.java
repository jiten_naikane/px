package com.company.px.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.company.px.R;
import com.company.px.activites.selfie.all.stickies.StickiesEdit;
import com.company.px.activites.selfie.all.stickies.StickerImageView;
import com.company.px.holder.ImagesHoriHolder;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class StickMeImagesHoriAdapter extends RecyclerView.Adapter<ImagesHoriHolder> {
    public ArrayList<ImagesHoriModel> imagesHoriModel;
    public StickiesEdit context;
    LayoutInflater li;
    int resource;

    public StickMeImagesHoriAdapter(ArrayList<ImagesHoriModel> imagesHoriModel, StickiesEdit context, int resource)
    {
        this.context = context;
        this.imagesHoriModel = imagesHoriModel;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ImagesHoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        ImagesHoriHolder fth = new ImagesHoriHolder(layout);

        return fth;
    }

    @Override
    public void onBindViewHolder(ImagesHoriHolder holder, final int position) {

        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL+imagesHoriModel.get(position).getImg1())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView1);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL+imagesHoriModel.get(position).getImg2())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView2);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL+imagesHoriModel.get(position).getImg3())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView3);

        if (imagesHoriModel.get(position).getImg2().length()==0)
        {
            holder.imgView2.setVisibility(View.GONE);
        } else
        {
            holder.imgView2.setVisibility(View.VISIBLE);
        }

        if (imagesHoriModel.get(position).getImg3().length()==0)
        {
            holder.imgView3.setVisibility(View.GONE);
        } else
        {
            holder.imgView3.setVisibility(View.VISIBLE);
        }


        holder.imgView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = getBitmapFromURL(AppUrls.BASE_IMAGE_URL+imagesHoriModel.get(position).getImg1());
                context.getSticky(bitmap);

            }
        });

        holder.imgView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = getBitmapFromURL(AppUrls.BASE_IMAGE_URL+imagesHoriModel.get(position).getImg2());
                context.getSticky(bitmap);
            }
        });

        holder.imgView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = getBitmapFromURL(AppUrls.BASE_IMAGE_URL+imagesHoriModel.get(position).getImg3());
                context.getSticky(bitmap);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesHoriModel.size();
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
