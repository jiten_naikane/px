package com.company.px.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.company.px.R;
import com.company.px.activites.selfie.all.dubf.DubFaceEdit;
import com.company.px.holder.ImagesHoriHolder;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class DubfaceImagesHoriAdapter extends RecyclerView.Adapter<ImagesHoriHolder> {
    public ArrayList<ImagesHoriModel> imagesHoriModel;
    public DubFaceEdit context;
    LayoutInflater li;
    int resource;
    ImageView img_theme;

    public DubfaceImagesHoriAdapter(ArrayList<ImagesHoriModel> imagesHoriModel, DubFaceEdit context, int resource)
    {
        this.context = context;
        this.imagesHoriModel = imagesHoriModel;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ImagesHoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        ImagesHoriHolder fth = new ImagesHoriHolder(layout);
        img_theme  = (ImageView) context.findViewById(R.id.img_theme);


        return fth;
    }

    @Override
    public void onBindViewHolder(ImagesHoriHolder holder, final int position) {
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView1);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg2())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView2);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg3())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView3);


        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        final int screen_width = displayMetrics.widthPixels;
        int screen_height = displayMetrics.heightPixels;

        holder.imgView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                        .resize(screen_width, screen_width)
                        .into(img_theme);

            }
        });

        holder.imgView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg2())
                        .resize(screen_width, screen_width)
                        .into(img_theme);

            }
        });

        holder.imgView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg3())
                        .resize(screen_width, screen_width)
                        .into(img_theme);

            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesHoriModel.size();
    }


}
