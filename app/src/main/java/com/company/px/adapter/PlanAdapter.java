package com.company.px.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.px.R;
import com.company.px.activites.egraphics.StepOne_Description;
import com.company.px.holder.PlanHolder;
import com.company.px.model.PlanModel;

import java.util.ArrayList;


public class PlanAdapter extends RecyclerView.Adapter<PlanHolder> {
    public ArrayList<PlanModel> planModels;
    public StepOne_Description context;
    LayoutInflater li;
    int resource;

    public PlanAdapter(ArrayList<PlanModel> planModels, StepOne_Description context, int resource) {
        this.planModels = planModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PlanHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        PlanHolder slh = new PlanHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final PlanHolder holder, final int position)
    {
        holder.title_T.setText(planModels.get(position).getName());
        holder.choose_T.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.choose_T.setBackgroundResource(R.drawable.butt_one);
                holder.choose_T.setText("Selected");
            }
        });


    }

    @Override
    public int getItemCount() {
        return this.planModels.size();
    }

}
