package com.company.px.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.company.px.activites.logs.CountryListActivity;
import com.company.px.filters.CustomFilterForCountryList;
import com.company.px.holder.CountryHolder;
import com.company.px.itemclicklistener.CountryItemClickListener;
import com.company.px.model.CountriesModel;
import com.squareup.picasso.Picasso;

import com.company.px.R;

import java.util.ArrayList;


public class CountryAdapter extends RecyclerView.Adapter<CountryHolder>implements Filterable {
    public ArrayList<CountriesModel> countryModels,filterList;
    public CountryListActivity context;
    CustomFilterForCountryList filter;
    LayoutInflater li;
    int resource;
    Typeface typeface;

    public CountryAdapter(ArrayList<CountriesModel> countryModels, CountryListActivity context, int resource) {
        this.countryModels = countryModels;
        this.context = context;
        this.resource = resource;
        this.filterList = countryModels;
       // typeface = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.fonttype_one));
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        String className = this.getClass().getCanonicalName();
        Log.d("CURRENTCLASSNAME" , className);
    }

    @Override
    public Filter getFilter() {
        if(filter==null)
        {
            filter=new CustomFilterForCountryList(filterList,this);
        }

        return filter;
    }

    @Override
    public CountryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        CountryHolder slh = new CountryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(CountryHolder holder, final int position)
    {

          final String country_code=countryModels.get(position).getCode();
        holder.country_name.setText(countryModels.get(position).getName()+"  ( "+country_code+" )");



        Picasso.with(context)
                .load(countryModels.get(position).getImage_path())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.flag_image);


        holder.setItemClickListener(new CountryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",country_code);
                returnIntent.putExtra("flag",countryModels.get(position).getImage_path());
                context.setResult(Activity.RESULT_OK,returnIntent);
                context.finish();
              //  context.setCountryName(countryModels.get(pos).getName(),countryModels.get(pos).getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.countryModels.size();
    }
}
