package com.company.px.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.company.px.activites.egraphics.StepTwo_Cart;
import com.company.px.holder.CartItemsHolder;
import com.company.px.itemclicklistener.CartItemsClickListner;
import com.company.px.model.CartItemsModel;

import java.util.ArrayList;


public class CartItemsAdapter extends RecyclerView.Adapter<CartItemsHolder> {
    public ArrayList<CartItemsModel> cartItemsModels;
    public StepTwo_Cart context;
    LayoutInflater li;
    int resource;

    public CartItemsAdapter(ArrayList<CartItemsModel> cartItemsModels, StepTwo_Cart context, int resource) {
        this.cartItemsModels = cartItemsModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public CartItemsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        CartItemsHolder slh = new CartItemsHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(CartItemsHolder holder, final int position)
    {
        holder.product_T.setText(cartItemsModels.get(position).getProduct_name());
        holder.cost_T.setText(cartItemsModels.get(position).getMrp());

        holder.setItemClickListener(new CartItemsClickListner() {
            @Override
            public void onItemClick(View view, int layoutPosition) {
                Toast.makeText(context, "TESTING", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.cartItemsModels.size();
    }

}
