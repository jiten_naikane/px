package com.company.px.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.px.MainActivity;
import com.company.px.R;
import com.company.px.holder.SubCatHolder;
import com.company.px.model.SubCatModel;
import com.company.px.utility.AppUrls;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class SubCatAdapter extends RecyclerView.Adapter<SubCatHolder> {
    public ArrayList<SubCatModel> subCatModels;
    public MainActivity context;
    LayoutInflater li;
    int resource;

    public SubCatAdapter(ArrayList<SubCatModel> subCatModels, MainActivity context, int resource) {
        this.subCatModels = subCatModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SubCatHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        SubCatHolder slh = new SubCatHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(SubCatHolder holder, final int position)
    {
        holder.sub_cat_Tv.setText(subCatModels.get(position).getName());

        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + subCatModels.get(position).getImage())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.sub_cat_ic);
    }

    @Override
    public int getItemCount() {
        return this.subCatModels.size();
    }

}
