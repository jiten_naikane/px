package com.company.px.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.company.px.R;
import com.company.px.activites.selfie.all.mixme.MIxme;
import com.company.px.activites.selfie.all.texteo.Texteo;
import com.company.px.holder.ImagesHoriHolder;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MixmeImagesHoriAdapter extends RecyclerView.Adapter<ImagesHoriHolder> {
    public ArrayList<ImagesHoriModel> imagesHoriModel;
    public MIxme context;
    LayoutInflater li;
    int resource;

    public MixmeImagesHoriAdapter(ArrayList<ImagesHoriModel> imagesHoriModel, MIxme context, int resource)
    {
        this.context = context;
        this.imagesHoriModel = imagesHoriModel;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ImagesHoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        ImagesHoriHolder fth = new ImagesHoriHolder(layout);
        return fth;
    }

    @Override
    public void onBindViewHolder(ImagesHoriHolder holder, final int position) {
        final ImageView img_theme  = (ImageView) context.findViewById(R.id.img_theme);

        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView1);
        holder.imgView1.setBackgroundResource(R.color.gray);

        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg2())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView2);
        holder.imgView2.setBackgroundResource(R.color.gray);


        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg3())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView3);
        holder.imgView3.setBackgroundResource(R.color.gray);


        if (imagesHoriModel.get(position).getImg3().length()==0)
        {
            holder.imgView3.setVisibility(View.GONE);
        } else
        {
            holder.imgView3.setVisibility(View.VISIBLE);
        }
        if (imagesHoriModel.get(position).getImg2().length()==0)
        {
            holder.imgView2.setVisibility(View.GONE);
        } else
        {
            holder.imgView2.setVisibility(View.VISIBLE);
        }


        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        final int screen_width = displayMetrics.widthPixels;
        int screen_height = displayMetrics.heightPixels;

        holder.imgView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                        .resize(screen_width, screen_width)
                        .into(img_theme);
                img_theme.bringToFront();
                context.theme_lockIn_ic.setVisibility(View.VISIBLE);
                context.theme_lockOut_ic.setVisibility(View.GONE);
            }
        });

        holder.imgView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg2())
                        .resize(screen_width, screen_width)
                        .into(img_theme);
                img_theme.bringToFront();
                context.theme_lockIn_ic.setVisibility(View.VISIBLE);
                context.theme_lockOut_ic.setVisibility(View.GONE);
            }
        });

        holder.imgView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg3())
                        .resize(screen_width, screen_width)
                        .into(img_theme);
                img_theme.bringToFront();
                context.theme_lockIn_ic.setVisibility(View.VISIBLE);
                context.theme_lockOut_ic.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesHoriModel.size();
    }


}
