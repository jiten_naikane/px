package com.company.px.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.company.px.R;
import com.company.px.activites.selfie.all.frameo.FrameoEdit;
import com.company.px.holder.ImagesHoriHolder;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class FrameoImagesHoriAdapter extends RecyclerView.Adapter<ImagesHoriHolder> {
    public ArrayList<ImagesHoriModel> imagesHoriModel;
    public FrameoEdit context;
    LayoutInflater li;
    int resource, screen_width, screen_height;
    private Boolean first_run = false;

    public FrameoImagesHoriAdapter(ArrayList<ImagesHoriModel> imagesHoriModel, FrameoEdit context, int resource)
    {
        this.context = context;
        this.imagesHoriModel = imagesHoriModel;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ImagesHoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        ImagesHoriHolder fth = new ImagesHoriHolder(layout);

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        screen_width = displayMetrics.widthPixels;
        screen_height = displayMetrics.heightPixels;

        return fth;
    }

    @Override
    public void onBindViewHolder(ImagesHoriHolder holder, final int position) {
        if (!first_run) {
            Picasso.with(context)
                    .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                    .resize(screen_width, screen_width)
                    .into(context.img_theme);
            first_run = true;
        }
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView1);
        holder.imgView1.setScaleType(ImageView.ScaleType.FIT_XY);

        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg2())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView2);
        holder.imgView2.setScaleType(ImageView.ScaleType.FIT_XY);

        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg3())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.imgView3);
        holder.imgView3.setScaleType(ImageView.ScaleType.FIT_XY);

        if (imagesHoriModel.get(position).getImg3().length()==0)
        {
            holder.imgView3.setVisibility(View.GONE);
        } else
        {
            holder.imgView3.setVisibility(View.VISIBLE);
        }
        if (imagesHoriModel.get(position).getImg2().length()==0)
        {
            holder.imgView2.setVisibility(View.GONE);
        } else
        {
            holder.imgView2.setVisibility(View.VISIBLE);
        }

        holder.imgView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                        .resize(screen_width, screen_width)
                        .into(context.img_theme);
//                ll_img_tool.setVisibility(View.VISIBLE);
                context.img_theme.setVisibility(View.VISIBLE);
//                theme_visi_ic.setVisibility(View.VISIBLE);
            }
        });

        holder.imgView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg2())
                        .resize(screen_width, screen_width)
                        .into(context.img_theme);
//                ll_img_tool.setVisibility(View.VISIBLE);
                context.img_theme.setVisibility(View.VISIBLE);
//                theme_visi_ic.setVisibility(View.VISIBLE);
            }
        });

        holder.imgView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg3())
                        .resize(screen_width, screen_width)
                        .into(context.img_theme);
//                ll_img_tool.setVisibility(View.VISIBLE);
                context.img_theme.setVisibility(View.VISIBLE);
//                theme_visi_ic.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return imagesHoriModel.size();
    }


}
