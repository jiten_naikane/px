package com.company.px.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.company.px.R;
import com.company.px.activites.selfie.all.mixme.MIxme;
import com.company.px.activites.selfie.all.texteo.Texteo;
import com.company.px.holder.FestivalTitleHolder;
import com.company.px.itemclicklistener.FestivalTitleClickListner;
import com.company.px.model.FestivalTitlesModel;

import java.util.ArrayList;


public class MixmeFestivalTitleAdapter extends RecyclerView.Adapter<FestivalTitleHolder> {
    public ArrayList<FestivalTitlesModel> festivalTitlesModel;
    public MIxme context;
    LayoutInflater li;
    int resource;

    public MixmeFestivalTitleAdapter(ArrayList<FestivalTitlesModel> festivalTitlesModel, MIxme context, int resource)
    {
        this.context = context;
        this.festivalTitlesModel = festivalTitlesModel;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public FestivalTitleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        FestivalTitleHolder fth = new FestivalTitleHolder(layout);

        return fth;
    }

    @Override
    public void onBindViewHolder(FestivalTitleHolder holder, final int position) {
        holder.festivalTitleTv.setText(festivalTitlesModel.get(position).getName());
        context.getImagesHori(festivalTitlesModel.get(position).getId());
        context.theme_visi_ic.setVisibility(View.VISIBLE);
        context.theme_Invisi_ic.setVisibility(View.GONE);

        holder.setItemClickListener(new FestivalTitleClickListner() {
            @Override
            public void onItemClick(View view, int layoutPosition) {
                context.getImagesHori(festivalTitlesModel.get(position).getId());
            }
        });

        if (festivalTitlesModel.get(position).getColor_id().equals("1"))
        {
            holder.festivalTitleTv.setTextColor(Color.RED);
            holder.festivalTitleTv.setBackgroundResource(R.drawable.title_stroke);
        }

    }

    @Override
    public int getItemCount() {
        return festivalTitlesModel.size();
    }







}
