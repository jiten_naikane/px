package com.company.px.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.px.R;
import com.company.px.activites.egraphics.Products;
import com.company.px.fragment.HomeFragment;
import com.company.px.holder.CategoryHolder;
import com.company.px.itemclicklistener.CategoryClickListner;
import com.company.px.model.PublicProductsModel;
import com.company.px.model.CategoriesModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {
    public ArrayList<CategoriesModel> catModels;
    public HomeFragment context;
    LayoutInflater li;
    int resource;

    public CategoryAdapter(ArrayList<CategoriesModel> catModels, HomeFragment context, int resource) {
        this.catModels = catModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CategoryAdapter(ArrayList<PublicProductsModel> catProModel, Products products, int row_cat_products) {
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        CategoryHolder slh = new CategoryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, final int position)
    {
        holder.cat_name.setText(catModels.get(position).getName());

        holder.setItemClickListener(new CategoryClickListner() {
            @Override
            public void onItemClick(View view, int layoutPosition) {
                Intent ii = new Intent(context.getActivity(), Products.class);
                ii.putExtra("catID", catModels.get(position).getId());
                ii.putExtra("royalID", catModels.get(position).getRoyalty());
                ii.putExtra("editID", catModels.get(position).getFree_edit());
                ii.putExtra("catName", catModels.get(position).getName());

                Log.d("ccdcdcd" , catModels.get(position).getRoyalty());
                context.startActivity(ii);


            }
        });

        Picasso.with(context.getContext())
                .load(catModels.get(position).getImage())
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.cat_img);



        Log.d("TESTIMG",catModels.get(position).getImage().toString());
    }

    @Override
    public int getItemCount() {
        return this.catModels.size();
    }
}
