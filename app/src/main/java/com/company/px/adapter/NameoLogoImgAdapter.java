package com.company.px.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.company.px.R;
import com.company.px.activites.selfie.all.nameo.NameoEdit;
import com.company.px.holder.ImagesHoriHolder;
import com.company.px.model.ImagesHoriModel;
import com.company.px.utility.AppUrls;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class NameoLogoImgAdapter extends RecyclerView.Adapter<ImagesHoriHolder> {
    public ArrayList<ImagesHoriModel> imagesHoriModel;
    public NameoEdit context;
    private LayoutInflater li;
    private int resource;
    private boolean first_run = false;

    public NameoLogoImgAdapter(ArrayList<ImagesHoriModel> imagesHoriModel, NameoEdit context, int resource) {
        this.context = context;
        this.imagesHoriModel = imagesHoriModel;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ImagesHoriHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        ImagesHoriHolder fth = new ImagesHoriHolder(layout);

        return fth;
    }

    @Override
    public void onBindViewHolder(final ImagesHoriHolder holder, final int position) {
        if (!first_run) {
            Picasso.with(context)
                    .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                    .into(context.img_logo);
            first_run = true;
        }

//        context.ll_img_tool.setVisibility(View.VISIBLE);
        context.img_logo.setVisibility(View.VISIBLE);
        context.logo_visi_ic.setVisibility(View.VISIBLE);

        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                .into(holder.imgView1);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg2())
                .into(holder.imgView2);
        Picasso.with(context)
                .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg3())
                .into(holder.imgView3);


        if (imagesHoriModel.get(position).getImg3().length() == 0) {
            holder.imgView3.setVisibility(View.GONE);
        } else {
            holder.imgView3.setVisibility(View.VISIBLE);
        }
        if (imagesHoriModel.get(position).getImg2().length() == 0) {
            holder.imgView2.setVisibility(View.GONE);
        } else {
            holder.imgView2.setVisibility(View.VISIBLE);
        }

        holder.imgView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg1())
                        .into(context.img_logo);
            }
        });

        holder.imgView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg2())
                        .into(context.img_logo);
            }
        });

        holder.imgView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Picasso.with(context)
                        .load(AppUrls.BASE_IMAGE_URL + imagesHoriModel.get(position).getImg3())
                        .into(context.img_logo);
            }
        });
    }


    @Override
    public int getItemCount() {
        return imagesHoriModel.size();
    }


    public Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}

