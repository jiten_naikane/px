package com.company.px.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.px.R;
import com.company.px.activites.selfie.all.nameo.NameoEdit;
import com.company.px.activites.selfie.all.texteo.Texteo;
import com.company.px.holder.EmojiTitleHolder;
import com.company.px.itemclicklistener.EmojiTitleClickListner;
import com.company.px.model.EmojiTitlesModel;

import java.util.ArrayList;


public class EmojiTitleAdapter_Texteo extends RecyclerView.Adapter<EmojiTitleHolder> {
    public ArrayList<EmojiTitlesModel> emojiTitlesModels;
    public Texteo context;
    LayoutInflater li;
    int resource;
    String emojiTitleId;


    public EmojiTitleAdapter_Texteo(ArrayList<EmojiTitlesModel> emojiTitlesModels, Texteo context, int resource)
    {
        this.context = context;
        this.emojiTitlesModels = emojiTitlesModels;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public EmojiTitleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        EmojiTitleHolder fth = new EmojiTitleHolder(layout);
        return fth;
    }

    @Override
    public void onBindViewHolder(EmojiTitleHolder holder, final int position) {
        holder.festivalTitleTv.setText(emojiTitlesModels.get(position).getName());

        emojiTitleId = emojiTitlesModels.get(position).getId();
        context.getEmojiImages(emojiTitleId);

        holder.setItemClickListener(new EmojiTitleClickListner() {
            @Override
            public void onItemClick(View view, int layoutPosition) {
                emojiTitleId = emojiTitlesModels.get(position).getId();
                context.getEmojiImages(emojiTitleId);
            }
        });

        if (emojiTitlesModels.get(position).getColor_id().equals("1"))
        {
            holder.festivalTitleTv.setTextColor(Color.RED);
            holder.festivalTitleTv.setBackgroundResource(R.drawable.title_stroke);
        }
    }

    @Override
    public int getItemCount() {
        return emojiTitlesModels.size();
    }



}
