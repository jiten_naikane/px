package com.company.px.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.company.px.R;
import com.company.px.activites.selfie.all.frameo.FrameoEdit;
import com.company.px.holder.FestivalTitleHolder;
import com.company.px.itemclicklistener.FestivalTitleClickListner;
import com.company.px.model.FestivalTitlesModel;

import java.util.ArrayList;


public class FrameoFestivalTitleAdapter extends RecyclerView.Adapter<FestivalTitleHolder> {
    public ArrayList<FestivalTitlesModel> festivalTitlesModel;
    public FrameoEdit context;
    LayoutInflater li;
    int resource;
    String festTitleId;


    public FrameoFestivalTitleAdapter(ArrayList<FestivalTitlesModel> festivalTitlesModel, FrameoEdit context, int resource)
    {
        this.context = context;
        this.festivalTitlesModel = festivalTitlesModel;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public FestivalTitleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource,parent,false);
        FestivalTitleHolder fth = new FestivalTitleHolder(layout);
        return fth;
    }

    @Override
    public void onBindViewHolder(FestivalTitleHolder holder, final int position) {
        holder.festivalTitleTv.setText(festivalTitlesModel.get(position).getName());
        festTitleId = festivalTitlesModel.get(position).getId();
        context.getImagesHori(festTitleId);

        holder.setItemClickListener(new FestivalTitleClickListner() {
            @Override
            public void onItemClick(View view, int layoutPosition) {
                festTitleId = festivalTitlesModel.get(position).getId();
                context.getImagesHori(festTitleId);
            }
        });

        if (festivalTitlesModel.get(position).getColor_id().equals("1"))
        {
            holder.festivalTitleTv.setTextColor(Color.RED);
            holder.festivalTitleTv.setBackgroundResource(R.drawable.title_stroke);
        }


    }

    @Override
    public int getItemCount() {
        return festivalTitlesModel.size();
    }







}
