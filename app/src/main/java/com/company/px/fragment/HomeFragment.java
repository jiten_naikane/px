package com.company.px.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.px.activites.egraphics.StepOne_Description;
import com.company.px.R;
import com.company.px.activites.selfie.all.dubf.DubFaceEdit;
import com.company.px.activites.selfie.all.frameo.FrameoEdit;
import com.company.px.activites.selfie.all.mixme.MIxme;
import com.company.px.activites.selfie.all.nameo.NameoEdit;
import com.company.px.activites.selfie.all.texteo.Texteo;
import com.company.px.activites.selfie.all.stickies.StickiesEdit;
import com.company.px.adapter.CategoryAdapter;
import com.company.px.model.CategoriesModel;
import com.company.px.model.SliderModel;
import com.company.px.utility.AppUrls;
import com.company.px.utility.NetworkChecking;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeFragment extends Fragment implements  View.OnClickListener {
    private boolean checkInternet;

    SliderLayout viewPager;
    TextView referalCode, frameo_butt,stickme_butt,dubface_butt, design_create_butt, nameo_butt, texteo_butt, mixme_butt;
    View view;
    ArrayList<String> imgArray = new ArrayList<String>();
    ArrayList<SliderModel> sliderModel = new ArrayList<SliderModel>();
    String banner_mage, refCod_S;

    RecyclerView catRecycler;
    ArrayList<CategoriesModel> catMod = new ArrayList<CategoriesModel>();
    CategoryAdapter categoryAdap;
    LinearLayoutManager llM;
    GridLayoutManager gridLayoutManager;
    int selectedColor = Color.BLACK;
    int unselectedColor = Color.RED;
    Animation hyperspaceJumpAnimation;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        view =inflater.inflate(R.layout.fragment_home, container, false);
        hyperspaceJumpAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.anim_vv);

        viewPager = (SliderLayout)view.findViewById(R.id.slider_image);
        viewPager.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
        viewPager.getPagerIndicator().setDefaultIndicatorColor(selectedColor, unselectedColor);

        referalCode=(TextView)view.findViewById(R.id.referalCode);
        referalCode.setOnClickListener(this);
        frameo_butt=(TextView)view.findViewById(R.id.frameo_butt);
        stickme_butt=(TextView)view.findViewById(R.id.stickme_butt);
        dubface_butt=(TextView)view.findViewById(R.id.dubface_butt);
        design_create_butt = (TextView) view.findViewById(R.id.design_create_butt);
        mixme_butt = (TextView) view.findViewById(R.id.mixme_butt);

        nameo_butt=(TextView) view.findViewById(R.id.nameo_butt);
        texteo_butt=(TextView) view.findViewById(R.id.texteo_butt);

        design_create_butt.setOnClickListener(this);
        mixme_butt.setOnClickListener(this);
        frameo_butt.setOnClickListener(this);
        stickme_butt.setOnClickListener(this);
        dubface_butt.setOnClickListener(this);
        nameo_butt.setOnClickListener(this);
        texteo_butt.setOnClickListener(this);

        catRecycler = (RecyclerView) view.findViewById(R.id.catRecycler);
        catRecycler.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.HORIZONTAL, false);
        catRecycler.setLayoutManager(gridLayoutManager);
        categoryAdap = new CategoryAdapter(catMod,HomeFragment.this, R.layout.row_categories);
        catRecycler.setAdapter(categoryAdap);





        getSliders();
        getCategory();

        return view;
    }


    private void getSliders()
    {
        checkInternet = NetworkChecking.isConnected(getActivity());
        if (checkInternet) {
            Log.d("SLIDER_URL", AppUrls.BASE_URL + AppUrls.GET_SLIDER_IMAGES);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_SLIDER_IMAGES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("SLIDER_RESPONSE", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");

                                if(responceCode.equals("10100")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        SliderModel sMod = new SliderModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        String id = jsonObject1.getString("id");
                                        String image = jsonObject1.getString("image");

                                        imgArray.add(AppUrls.BASE_IMAGE_URL + image);
                                        banner_mage = AppUrls.BASE_IMAGE_URL + image;

                                        sMod.setImage(image);
                                        sliderModel.add(sMod);

                                        HashMap<String,String> file_maps = new HashMap<String, String>();

                                        file_maps.put("Hannibal",banner_mage);
                                        for(String name : file_maps.keySet())
                                        {
                                            TextSliderView textSliderView = new TextSliderView(getContext());
                                            textSliderView
                                                    .description("")
                                                    .image(file_maps.get(name))
                                                    .setScaleType(BaseSliderView.ScaleType.CenterInside);
                                            textSliderView.bundle(new Bundle());
                                            viewPager.addSlider(textSliderView);
                                        }
//                                        viewPager.setPresetTransformer(SliderLayout.Transformer.Default);
//                                        viewPager.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//                                        viewPager.setDuration(3000);
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
            requestQueue.add(stringRequest);

        }else {
            Toast.makeText(getActivity(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
            /*Snackbar snackbar = Snackbar.make(view, "No Internet Connection...!", Snackbar.LENGTH_LONG);
            snackbar.show();*/
        }

    }


    private void getCategory()
    {
        checkInternet = NetworkChecking.isConnected(getContext());
        if (checkInternet)
        {
            Log.d("CAT_URL", AppUrls.BASE_URL + AppUrls.GET_CATEGORY);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GET_CATEGORY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("CAT_RESP",response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String responceCode = jsonObject.getString("code");
                                if(responceCode.equals("10100"))
                                {
                                    JSONArray jarray=jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jarray.length(); i++)
                                    {
                                        JSONObject jsonObject1 = jarray.getJSONObject(i);

                                        CategoriesModel cList = new CategoriesModel();
                                        cList.setId(jsonObject1.getString("id"));
                                        cList.setName(jsonObject1.getString("name"));
                                        cList.setFree_edit(jsonObject1.getString("free_edit"));
                                        cList.setRoyalty(jsonObject1.getString("royalty"));
                                        cList.setImage(AppUrls.BASE_IMAGE_URL + jsonObject1.getString("image"));
                                        cList.setIs_active(jsonObject1.getString("is_active"));
                                        Log.d("DERF",jsonObject1.getString("image"));

                                        catMod.add(cList);
                                    }
                                    catRecycler.setAdapter(categoryAdap);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    {
                    } else if (error instanceof AuthFailureError)
                    {
                    } else if (error instanceof ServerError)
                    {
                    } else if (error instanceof NetworkError)
                    {
                    } else if (error instanceof ParseError)
                    {
                    }
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);
        }else {
            Toast.makeText(getContext(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
        }
    }





    @Override
    public void onClick(View v)
    {

        if(v==referalCode)
        {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Picxture");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Referral Code : " + refCod_S + "\n" + "picxture.com");
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
        if(v==design_create_butt)
        {
            Intent design_create_butt = new Intent(getActivity(), StepOne_Description.class);
            startActivity(design_create_butt);


        }
         if(v==frameo_butt)
         {
             Intent frameo_butt = new Intent(getActivity(), FrameoEdit.class);
             frameo_butt.putExtra("EDIT_CAT_ID","1");
             startActivity(frameo_butt);

         }
        if(v==stickme_butt)
        {
            Intent stickme_butt=new Intent(getActivity(), StickiesEdit.class);
            stickme_butt.putExtra("EDIT_CAT_ID","4");
            startActivity(stickme_butt);
        }

        if(v==dubface_butt)
        {
            Intent dubface_butt=new Intent(getActivity(), DubFaceEdit.class);
            dubface_butt.putExtra("EDIT_CAT_ID","2");
            startActivity(dubface_butt);
        }
        if(v==texteo_butt)
        {
            Intent texteo_butt=new Intent(getActivity(), Texteo.class);
            texteo_butt.putExtra("EDIT_CAT_ID","5");
            startActivity(texteo_butt);
        }
        if(v==nameo_butt)
        {
            Intent texteo_butt=new Intent(getActivity(), NameoEdit.class);
            texteo_butt.putExtra("EDIT_CAT_ID","6");
            texteo_butt.putExtra("EDIT_CAT_ID_2","7");
            startActivity(texteo_butt);
        }
        if(v==mixme_butt)
        {
            Intent mixme_butt=new Intent(getActivity(), MIxme.class);
            mixme_butt.putExtra("EDIT_CAT_ID","3");
            startActivity(mixme_butt);
        }
    }


}
