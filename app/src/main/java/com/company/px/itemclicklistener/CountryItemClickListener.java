package com.company.px.itemclicklistener;

import android.view.View;

public interface CountryItemClickListener
{
    void onItemClick(View v, int pos);
}
