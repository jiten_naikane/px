package com.company.px.itemclicklistener;

import android.view.View;

/**
 * Created by anu on 3/5/2018.
 */

public interface PlanItemClickListener {
    void onItemClick(View view, int layoutPosition);
}
