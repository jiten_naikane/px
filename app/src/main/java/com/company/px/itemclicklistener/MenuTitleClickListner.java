package com.company.px.itemclicklistener;

import android.view.View;

/**
 * Created by anu on 12/27/2017.
 */

public interface MenuTitleClickListner {
    void onItemClick(View view, int layoutPosition);
}
